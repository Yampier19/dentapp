<tr>
    <td class="body" width="100%" cellpadding="0" cellspacing="0">
    <table class="inner-body" align="center" cellpadding="0" cellspacing="0" role="presentation" style="min-width: 320px; max-width: 630px; width: 100%">
    <!-- Body content -->
    <tr>
        <td class="content-cell">
            <div style="background-color:transparent">
                <div class="m_-9148611935437022283block-grid" style="min-width:320px;max-width:630px;word-wrap:break-word;word-break:break-word;Margin:0 auto;background-color:#ffffff">
                    <div style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff">
                        <div class="m_-9148611935437022283col" style="min-width:320px;max-width:630px;display:table-cell;vertical-align:top;width:630px">
                            <div class="m_-9148611935437022283col_cont" style="width:100%!important">
                                <div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px">
                                    
                                    @yield('body')

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    </table>
    </td>
</tr>