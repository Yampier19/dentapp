<div style="background-color:transparent">
  <div class="m_-9148611935437022283block-grid" style="min-width:320px;max-width:630px;word-wrap:break-word;word-break:break-word;Margin:0 auto;background-color:#8752bf">
    <div style="border-collapse:collapse;display:table;width:100%;background-color:#e7344c">
      <div class="m_-9148611935437022283col" style="min-width:320px;max-width:630px;display:table-cell;vertical-align:top;width:630px">
        <div class="m_-9148611935437022283col_cont" style="width:100%!important">
          <div style="border-top:0px solid #e5eaf3;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px">
            
            <!-- <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;vertical-align:top;border-spacing:0;border-collapse:collapse;min-width:100%" role="presentation" valign="top">
              <tbody>
                <tr style="vertical-align:top" valign="top">
                  <td style="word-break:break-word;vertical-align:top;min-width:100%;padding-top:10px;padding-right:35px;padding-bottom:0px;padding-left:35px" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;vertical-align:top;border-spacing:0;border-collapse:collapse;border-top:1px solid #f1f4f8;width:100%" align="center" role="presentation" valign="top">
                      <tbody>
                        <tr style="vertical-align:top" valign="top">
                          <td style="word-break:break-word;vertical-align:top" valign="top"><span></span></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table> -->
        
          <div style="color:#ffffff;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.5;padding-top:15px;padding-right:35px;padding-bottom:15px;padding-left:35px">
            <div style="line-height:1.5;font-size:12px;text-align:left;color:#ffffff;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">© {{ date('Y') }} DentApp. Todos los derechos reservados.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>