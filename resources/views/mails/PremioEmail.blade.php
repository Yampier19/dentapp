@extends('mails.layout')

@section('body')

<!-- MENSAJE SUPERIOR -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" valign="top" style="table-layout:fixed;vertical-align:top;border-spacing:0px;border-collapse:collapse;min-width:100%">
    <tbody>
        <tr valign="top" style="vertical-align:top">
            <td valign="top" style="text-align:center;word-break:break-word;vertical-align:top;min-width:100%;padding:20px">
                <font size="6">{{ $information->asunto }}</font>
            </td>
        </tr>
    </tbody>
</table>

<!-- IMAGEN + DESCRIPCIÓN CON FECHA -->
<div style="border:0px solid transparent;padding:0px"><br>   
    <div style="text-align:center">
        <img src="https://i.ibb.co/ftL46Yz/premio.jpg" alt="icon.png" width="277" height="274" style="margin-right:0px" data-image-whitelisted="" class="CToWUd"><br>
    </div>

    <!-- DATOS DEL PREMIO -->
    <!-- <div style="line-height:1.5;padding:10px 40px; margin-bottom: 40px">
        <div style="line-height:1.5;font-size:12px">
            <p style="font-size:15px;line-height:1.5;text-align:center;word-break:break-word;font-family:inherit;margin:0px">
                Permita al menos 5 minutos para que los fondos estén disponibles en su cuenta.
            </p>
        </div>
    </div> -->
</div>

<div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px">            
    <div style="color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.2;padding-top:20px;padding-right:40px;padding-bottom:10px;padding-left:40px">
        <div style="line-height:1.2;font-size:12px;color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
            <p style="font-size:46px;line-height:1.2;text-align:center;word-break:break-word;margin:0">
                <span style="font-size:46px;color:#003188"><strong>Hola {{ $information->nombre }}</strong></span>
            </p>
        </div>
    </div>
</div>

<div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px">            
    <div style="color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.5;padding-top:10px;padding-right:40px;padding-bottom:10px;padding-left:40px">
        <div style="line-height:1.5;font-size:12px;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;color:#555555">
            <p style="text-align:center;line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:20px;color:#003188">{{ $information->descripcion }}</span>
            </p>
        </div>
    </div>
</div>

@if($information->entrega == "Online")
    <!-- @include('mails.button') -->
@endif

<!-- INFORMACION DEL REPORTE DE DEPOSITO O TRANSFERENCIA -->
<div style="border:0px solid transparent;padding:0px"> 
    <div style="color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.2;padding-top:20px;padding-right:40px;padding-bottom:20px;padding-left:40px">
        <div style="line-height:1.2;font-size:12px;color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
            <p style="font-size:14px;line-height:1.2;text-align:center;word-break:break-word;margin:0">
                <span style="font-size:14px"><strong style="color:#003188">Información del Premio Canjeado <br>{{ date('Y-m-d') }}</strong></span>
            </p>
        </div>
    </div>
</div>

<div style="border:0px solid transparent;padding:0px">
    <div style="color:#8752bf;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.5;padding-top:0px;padding-right:40px;padding-bottom:20px;padding-left:40px">
        <div style="line-height:1.5;font-size:12px;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;color:#8752bf">
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>Premio: {{ $information->premio }}</strong></strong></span></span>
            </p>
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>Cantidad: {{ $information->cantidad }}</strong></span></span>
            </p>
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>Puntos: {{ $information->total_estrellas }}</strong></span></span>
            </p>
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>Fecha de Redención: {{ $information->fecha_redencion }}</strong></span></span>
            </p>
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>Link del premio: {{ $information->referencia }}</strong></span></span>
            </p>
        </div>
    </div>
</div> 

@stop