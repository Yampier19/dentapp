@extends('mails.layout')

@section('body')

<!-- IMAGEN -->
<div style="border:0px solid transparent;padding:0px">  
    <div style="text-align:center">
        <img src="https://i.ibb.co/T1QVSyb/deposit-Notification.png" alt="icon.png" width="100%" height="100%" style="margin-right:0px" data-image-whitelisted="" class="CToWUd"><br>
    </div>
</div>

<!-- SALUDO + CONFIRMACION -->
<div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px">            
    <div style="color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.2;padding-top:20px;padding-right:40px;padding-bottom:10px;padding-left:40px">
        <div style="line-height:1.2;font-size:12px;color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
            <p style="font-size:46px;line-height:1.2;text-align:center;word-break:break-word;margin:0">
                <span style="font-size:46px;color:#003188"><strong>Hola {{ $information->name }}</strong></span>
            </p>
        </div>
    </div>
</div>

<div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px">            
    <div style="color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.5;padding-top:10px;padding-right:40px;padding-bottom:10px;padding-left:40px">
        <div style="line-height:1.5;font-size:12px;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;color:#555555">
            <p style="text-align:center;line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:16px;color:#6d89bc">{{ $information->message }}</span>
            </p>
        </div>
    </div>
</div>

<!-- INFORMACION DEL REPORTE DE DEPOSITO O TRANSFERENCIA -->
<div style="border:0px solid transparent;padding:0px"> 
    <div style="color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.2;padding-top:20px;padding-right:40px;padding-bottom:20px;padding-left:40px">
        <div style="line-height:1.2;font-size:12px;color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif">
            <p style="font-size:14px;line-height:1.2;text-align:center;word-break:break-word;margin:0">
                <span style="font-size:14px"><strong style="color:#003188">Información de la declaración de movimiento:</strong></span>
            </p>
        </div>
    </div>
</div>

<div style="border:0px solid transparent;padding:0px">
    <div style="color:#8752bf;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.5;padding-top:0px;padding-right:40px;padding-bottom:20px;padding-left:40px">
        <div style="line-height:1.5;font-size:12px;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;color:#8752bf">
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>{{ $information->movement }}</strong></strong></span></span>
            </p>
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>{{ $information->accounts }}</strong></span></span>
            </p>
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>{{ $information->reference }}</strong></span></span>
            </p>
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>{{ $information->fee }}</strong></span></span>
            </p>
            <p style="line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:12px;color:#000080"><span><strong>{{ $information->state }}</strong></span></span>
            </p>
        </div>
    </div>
</div>

<!-- CANALES DE SOPORTE POR SI NO HIZO LA VAINA -->
<div style="border:0px solid transparent;padding:0px">
    <div style="color:#8752bf;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;line-height:1.5;padding-top:20px;padding-right:40px;padding-bottom:20px;padding-left:40px">
        <div style="line-height:1.5;font-size:12px;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;color:#8752bf">
            <p style="text-align:center;line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:16px;color:#000080"><span><strong>Canales de Soporte</strong></span></span>
            </p>
            <p style="text-align:center;line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:16px"><span style="color:#000080"><strong>1 –</strong></span> <span style="color:#808080"><a href="mailto:support@ibufalo.com" target="_blank">support@ibufalo.com</a></span></span>
            </p>
            <p style="text-align:center;line-height:1.5;word-break:break-word;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;margin:0">
                <span style="font-size:16px"><span style="color:#000080"><strong>2 –</strong></span> <span style="color:#808080"><a style="color:#808080" href="https://ibufalo.com/" rel="noopener" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://ibufalo.com/&amp;source=gmail&amp;ust=1606241076829000&amp;usg=AOvVaw35z82T4ipxUKChd0_JmsuV">https://ibufalo.com</a></span></span>
            </p>
        </div>
    </div>
</div>

@stop