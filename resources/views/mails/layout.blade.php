<div class="">
  <div class="aHl"></div>
  <div id=":1a2" tabindex="-1"></div>
  <div id=":19r" class="ii gt">
    <div id=":19q" class="a3s aiL msg-9148611935437022283"><u></u>
      <div style="margin:0;padding:0;background-color:#f1f4f8">
        <table style="table-layout:fixed;vertical-align:top;min-width:320px;border-spacing:0;border-collapse:collapse;background-color:#f1f4f8;width:100%" cellpadding="0" cellspacing="0" role="presentation" width="100%" bgcolor="#f1f4f8" valign="top">
          <tbody>
            <tr style="vertical-align:top" valign="top">
              <td style="word-break:break-word;vertical-align:top" valign="top">

                @include('mails.header') 

                @include('mails.content')
                                    
                @include('mails.footer') 

                <div style="background-color:transparent">
                    <div class="m_-9148611935437022283block-grid" style="min-width:320px;max-width:630px;word-wrap:break-word;word-break:break-word;Margin:0 auto;background-color:transparent">
                    <div style="border-collapse:collapse;display:table;width:100%;background-color:transparent">
                        <div class="m_-9148611935437022283col" style="min-width:320px;max-width:630px;display:table-cell;vertical-align:top;width:630px">
                        <div class="m_-9148611935437022283col_cont" style="width:100%!important">
                            <div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px">
                            <div class="m_-9148611935437022283mobile_hide">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;vertical-align:top;border-spacing:0;border-collapse:collapse;min-width:100%" role="presentation" valign="top">
                                <tbody>
                                    <tr style="vertical-align:top" valign="top">
                                    <td style="word-break:break-word;vertical-align:top;min-width:100%;padding-top:30px;padding-right:10px;padding-bottom:0px;padding-left:10px" valign="top">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;vertical-align:top;border-spacing:0;border-collapse:collapse;border-top:0px solid #bbbbbb;width:100%" align="center" role="presentation" valign="top">
                                        <tbody>
                                            <tr style="vertical-align:top" valign="top">
                                            <td style="word-break:break-word;vertical-align:top" valign="top"><span></span></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                    </td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
        
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="yj6qo"></div>
      <div class="adL"></div>
    </div>
  </div>
  <div id=":1a6" class="ii gt" style="display:none">';
    <div id=":1a7" class="a3s aiL undefined"></div>
  </div>
  <div class="hi"></div>
</div>