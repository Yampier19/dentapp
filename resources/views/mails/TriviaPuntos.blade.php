<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0;">
  <meta name="format-detection" content="telephone=no" />
  <style>
    body {
      margin: 0;
      padding: 0;
      min-width: 100%;
      width: 100% !important;
      height: 100% !important;
    }

    body,
    table,
    td,
    div,
    p,
    a {
      -webkit-font-smoothing: antialiased;
      text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
      line-height: 100%;
      text-decoration-line: none !important;
    }

    table,
    td {
      border-collapse: collapse !important;
      border-spacing: 0;
    }

    img {
      border: 0;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }
    @media all and (min-width: 560px) {
      .container {
        border-radius: 8px;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        -khtml-border-radius: 8px;
      }
    }
    a,
    a:hover {
      color: #127DB3;
    }

    .footer a,
    .footer a:hover {
      color: #999999;
    }
  </style>
  <title>DentApp Mail</title>
</head>

<body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" marginwidth="0" marginheight="0" width="100%" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
	background-color: #F0F0F0;
	color: #000000;" bgcolor="#F0F0F0" text="#000000">

  <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;" class="background">
    <tr>
      <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;" bgcolor="#F0F0F0">
        <table border="0" cellpadding="0" cellspacing="0" align="center" width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;" class="wrapper">
          <tr>
            <td align="center" valign="top" style="background: #e72f4b; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 10px;
			padding-bottom: 10px;">
              <img border="0" vspace="0" hspace="0" src="https://i.ibb.co/8bFhsCF/DentApp.png" width="35%" height="35%" alt="Logo" title="Logo" style="
				color: #000000;
				font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" />
            </td>
          </tr>
        </table>

        <table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#FFFFFF" width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;" class="container">

          <tr>
          <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
			" class="hero"><img border="0" vspace="0" hspace="0" src="https://i.ibb.co/99cbLwd/2-KV-GANAR-PUNTOS-TRIVIA.jpg" alt="Please enable images to view this content" title="Hero Image" width="560" style="
			width: 100%;
			max-width: 560px;
			color: #000000; font-size: 13px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" /></td>
          
          </tr>
          <tr>
            <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 35px; font-weight: 300; line-height: 150%;
			padding-top: 20px;
			color: #e72f4b; font-weight: bold;
			font-family: sans-serif;" class="subheader">
              ¡FELICITACIONES!
            </td>
          </tr>

          <tr>
          <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 23px; font-weight: bold; line-height: 130%;
			padding-top: 5px;
			color: #000000;
			font-family: sans-serif;" class="header">
              {{ $information->nombre }}
            </td>
          </tr>

          <tr>
            <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 400; line-height: 160%;
			padding-top: 25px; 
			color: #000000;
			font-family: sans-serif;" class="paragraph">
              Ganaste <b style="color: red">{{ $information->puntos }} puntos</b> por responder correctamenete las <br> 
            <b>trivias de tu capacitación.</b> Ahora tienes <b style="color: red">{{ $information->total }} puntos</b><br>
              en tu cuenta. úsalos para canjear los mejores premios. 
            </td>
          </tr>

         
         
          <tr>
            <td align="center" valign="top" style="background: #f7f7f7; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 400; line-height: 160%;
			padding-top: 20px;
			padding-bottom: 25px;
			color: #000000;
      font-style: italic;
			font-family: sans-serif;" class="paragraph">
              <b style="color: #e72f4b">¡</b>En <b style="color: #e72f4b">DentApp</b> amamos verte sonreir <br>
              y valoramos cada esfuerzo<b style="color: #e72f4b">!</b>
            </td>
          </tr>
        </table>

        <table border="0" cellpadding="0" cellspacing="0" align="center" width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;" class="wrapper">
          <tr>
            <td align="center" valign="top" style="background: #f7f7f7; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 10px; padding-bottom: 10px" class="social-icons">
              <table width="256" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; border-spacing: 0; padding: 0;">
                <tr>
                  <!-- ICON 3 -->
                  <td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 95px; border-collapse: collapse; border-spacing: 0;"><a target="_blank" href="https://api.whatsapp.com/send?phone=+573125614712&text=Buen día, necesito ayuda con DentApp, mi nombre es ..." style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
					color: #000000;" alt="G" title="WhatsApp" width="44" height="44" src="https://i.ibb.co/kGXmLdP/whatsapp.png"></a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>


        <table border="0" cellpadding="0" cellspacing="0" align="center" width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 560px;" class="wrapper">
          <tr>
            <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 87.5%;
			padding-bottom: 10px;">
              <img border="0" vspace="0" hspace="0" src="https://i.ibb.co/SRQq07d/footer.jpg" width="100%" height="35%" alt="Logo" title="Logo" style="
				color: #000000;
				font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" />
            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

</body>

</html>