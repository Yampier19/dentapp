@extends('layouts.admin.app')
@push('scripts')
    <script src="{{ asset('js/noticia/noticia.js') }}" defer></script>
@endpush
<script src="{{ asset('js/noticia/confirmDelete.js') }}"></script>
@section('content')
    <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todas las noticias</label>
    <div class="row">
        <div class="col d-flex justify-content-end">
            <button id="open-modal-new" class="btn btn-round" style="background-color: #51A2A7">
                <i class="material-icons">add</i>CREAR NOTICIA
            </button>
            <a style="background-color:#51A2A7" class="btn btn-round" href="{{ url('informativo/inicio') }}"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
        </div>
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped " cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff">
                        <tr>
                            <th class="font-weight-bold">Título</th>
                            <th class="font-weight-bold">Tipo de Noticia</th>
                            <th class="font-weight-bold">Fecha de inicio</th>
                            <th class="font-weight-bold">Fecha de finalización</th>
                            <th class="font-weight-bold">Información</th>
                            <th class="font-weight-bold">Estado</th>
                            <th class="font-weight-bold">Documento</th>
                            <th class="font-weight-bold">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered" style="background-color: #fff; color: #000">
                        @foreach ($noticias as $noticia)
                            <tr>
                                <td>{{ $noticia->nombre }}</td>
                                <td>{{ $noticia->tipo }}</td>
                                <td>{{ date_format(date_create($noticia->fecha_inicio), 'F j, Y') }}</td>
                                <td>{{ $noticia->fecha_fin ? date_format(date_create($noticia->fecha_fin), 'F j, Y') : 'Sin Vencimiento' }}</td>
                                <td>{{ $noticia->informacion }}</td>
                                <td class="text-center">
                                    <span class='badge font-weight-bold {{ $noticia->estado ? 'text-white bg-success' : 'text-white bg-danger' }}'>
                                        {{ $noticia->estado ? 'Activo' : 'Inactivo' }}
                                    </span>
                                </td>
                                <td class="text-center">
                                    @if ($noticia->documento)
                                        <a href="{{ route('noticias.documento', ['filename' => basename($noticia->documento)]) }}"
                                            target="_blank">
                                            @switch(pathinfo($noticia->documento)['extension'])
                                                @case('jpg')
                                                @case('jpeg')

                                                @case('png')
                                                    <span class="material-icons text-dark" style="font-size: 2em">image</span>
                                                @break

                                                @case('pdf')
                                                    <span class="material-icons text-dark" style="font-size: 2em">picture_as_pdf</span>
                                                @break

                                                @case('mp4')
                                                @case('wm')

                                                @case('wmv')
                                                    <span class="material-icons text-dark" style="font-size: 2em">play_arrow</span>
                                                @break
                                            @endswitch
                                        </a>
                                    @else()
                                        <span>No posee...</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a class="btn text-white btn-sm rounded" href="{{ route('noticias.edit', $noticia->id_noticia) }}" style="background-color: #51A2A7;">
                                        <span class="iconify" data-icon="fa6-solid:trash-can"></span>
                                    </a>
                                    {{-- <a class="btn text-white btn-sm bg-danger" href="{{route('noticias.confirm',$noticia->id_noticia)}}" style="color: #da4f4f;"><span class="material-icons">delete</span></a> --}}
                                    {{-- <button data-id="{{ $noticia->id_noticia }}" type="button" class="btn btn-danger btn-sm delete-new"><span class="iconify" data-width="15" data-icon="bi:trash-fill"></span></button> --}}
                                    <form action="{{route('noticias.destroy',$noticia->id_noticia)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('¿Desea eliminar esta noticia?')" type="submit" class="btn btn-danger btn-sm rounded">
                                            <span class="iconify" data-icon="fa6-solid:trash-can"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                        Creacion de noticias</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form onsubmit="return checkSubmit();" action="{{ route('noticias.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col">
                                <input required name="nombre" id="nombre" type="text" placeholder="Nombre"
                                    class="form-control @error('nombre') is-invalid @enderror">
                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col-12 mb-2">
                                <label for="">Tipo de Noticia:</label>
                                <input required list="nottipo" name="tipo" id="tipo" value="{{ old('tipo') }}"
                                    class="form-control @error('tipo') is-invalid @enderror" />
                                <datalist id="nottipo">
                                    @foreach ($tipos as $tipo)
                                        <option value="{{ $tipo->nombre }}">
                                    @endforeach
                                </datalist>
                                @error('tipo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <textarea required name="informacion" id="informacion" type="text" placeholder="Informacion"
                                    class="form-control @error('informacion') is-invalid @enderror">{{ old('informacion') }}</textarea>
                                @error('informacion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label for="name_user">Fecha de Inicio:</label>
                                <input min="{{ now()->format('Y-m-d') }}" required name="fecha_inicio" id="fecha_inicio" type="date"
                                    value="{{ old('fecha_inicio', date('Y-m-d')) }}"
                                    class="form-control @error('fecha_inicio') is-invalid @enderror">
                                @error('fecha_inicio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <div class="row mx-auto">
                                    <label for="name_user mx-auto">Fecha de Vencimiento:</label>
                                    <div class="form-check ml-auto">
                                        <label class="form-check-label ">
                                            ¿Noticia sin vencimiento?
                                            <input class="form-check-input" type="checkbox" value="1"
                                                id="withoutDate" name="withoutDate">
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <input min="{{ now()->addday(1)->format('Y-m-d') }}" name="fecha_fin" id="fecha_fin" type="date"
                                    value="{{ old('fecha_fin') }}"
                                    class="form-control @error('fecha_fin') is-invalid @enderror">
                                @error('fecha_fin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <script>
                                    var determine = document.getElementById("withoutDate");
                                    var disablefecha_fin = function() {
                                        if (determine.checked) {
                                            document.getElementById("fecha_fin").disabled = true;
                                            document.getElementById("fecha_fin").value = null;
                                        } else {
                                            document.getElementById("fecha_fin").disabled = false;
                                        }
                                    }
                                    determine.onclick = disablefecha_fin;
                                    disablefecha_fin();
                                </script>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="">Subir documento de noticia:</label>
                                <input type="file" name="documento" id="documento" accept=".jpg,.jpeg,.png,.pdf"
                                    class="form-control @error('documento') is-invalid @enderror" id="">
                                @error('documento')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label for="">Subir Imagen preview de noticia:</label>
                                <input type="file" name="imagen" id="imagen" accept=".jpg,.jpeg,.png"
                                    class="form-control @error('imagen') is-invalid @enderror" id="">
                                @error('imagen')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="">Estado:</label><br>
                                <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="estado" id="estado" checked
                                            id="inlineRadio1" value="1"> Noticia Activada
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="estado"
                                            id="estado" value="0"> Noticia Desactivada
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                </div>

                <div class="modal-footer">
                    <input class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;"
                        type="submit" value="CREAR NOTICIA">
                    {{-- <button id="agregarnoticia" type="button" style="background-color: #51A2A7; border-radius: 40px;" class="btn mx-auto col-md-6">Crear Noticia</button> --}}
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
