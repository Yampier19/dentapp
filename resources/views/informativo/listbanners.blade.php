@extends('layouts.admin.app')
<script src="{{ asset('js/banner/confirmDelete.js') }}"></script>
@section('content')
    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #F28D8D">Todos los banners</label>

    <div class="row">
        <div class="col d-flex justify-content-end">
            <a style="background-color:#51A2A7" class="btn " href="{{ url('informativo/inicio') }}"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
        </div>
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped " cellspacing="0" width="100%">
                    <thead style="background-color:#51A2A7; color: #fff">
                        <tr>
                            <th>Distribuidor</th>
                            <th>Imagen</th>
                            <th>Video</th>
                            <th>Fecha de publicación</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered" style="background-color: #fff; color: #000">
                        @foreach ($banners as $banner)
                            <tr>
                                <td class="align-middle distribuidor{{ $banner->id_banner }}"
                                    id="{{ $banner->distribuidor }}">
                                    {{ $banner->distribuidor == 'All' ? 'General' : $banner->distribuidor }}
                                </td>
                                <td class="align-middle">{{ basename($banner->imagen) }}</td>
                                <td class="align-middle text-center">
                                    @if ($banner->archivo)
                                        <a href="{{ route('banner.archive', ['filename' => basename($banner->archivo)]) }}"
                                            target="_blank" class="btn btn-sm btn-danger mx-auto">
                                            <span class="material-icons">play_arrow</span>
                                        </a>
                                    @else()
                                        <span>No posee video</span>
                                    @endif
                                </td>
                                <td class="align-middle">{{ date_format(date_create($banner->created_at), 'F j, Y') }}</td>
                                <td class="">
                                    <a style="background-color: #51A2A7" class="btn btn-sm rounded text-white" id="{{ $banner->id_banner }}"
                                        data-target="#exampleModal" data-toggle="modal" class="beditar">
                                        <span class="iconify" data-icon="akar-icons:edit"></span>
                                    </a>
                                    <form class="d-inline-block" action="{{ route('banner.destroy', $banner->id_banner) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="return confirm('¿Desea eliminar este banner?')" class="btn btn-danger btn-sm rounded">
                                            <span class="iconify" data-icon="fa6-solid:trash-can"></span>
                                        </button>
                                    </form>
                                    {{-- <button type="button" data-id="{{ $banner->id_banner }}" class="btn btn-danger btn-sm delete-banner"><span class="iconify" data-icon="bi:trash-fill"></span></button> --}}
                                </td>
                                </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

    <!-- MODAL DE EDITAR BANNER -->
    <div class="modal fade bd-example-modal-lg2" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                        Editar Banner Código (<span id="bannercod">0</span>)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="d-flex justify-content-md-center">
                        @if (isset($banner->imagen))
                            <img style="width:200px;max-height:200px;border-radius:100%"
                                src="{{ route('banner.preview', ['filename' => basename($banner->imagen)]) }}">
                        @endif
                    </div>
                    <form id="bupdate" action="{{ route('banner.update') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col">
                                <label for="name_user">Distribuidor:</label>
                                <select name="distribuidor" id="selectDist" class="form-control @error('distribuidor') is-invalid @enderror">
                                    <option value="All">General para todos los Usuarios</option>
                                    <option value="Palermo">Palermo</option>
                                    <option value="Dentales Antioquia SAS">Dentales Antioquia SAS</option>
                                    <option value="Casa Dental Gabriel Velasquez">Casa Dental Gabriel Velasquez</option>
                                    <option value="Aldental SA">Aldental SA</option>
                                    <option value="Dentales Padilla LTDA">Dentales Padilla LTDA</option>
                                    <option value="Orbidental SAS">Orbidental SAS</option>
                                    <option value="Luis Fernando Posada Henao">Luis Fernando Posada Henao</option>
                                    <option value="Dental 83 SAS">Dental 83 SAS</option>
                                    <option value="Jose Antonio Eraso Rivas">Jose Antonio Eraso Rivas</option>
                                    <option value="Dental Nader SAS">Dental Nader SAS</option>
                                    <option value="Casa Odontologica Importadora">Casa Odontologica Importadora</option>
                                    <option value="Dentales Market SAS">Dentales Market SAS</option>
                                    <option value="Adental SAS">Adental SAS</option>
                                </select>
                                @error('distribuidor')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row mt-4">
                            <div class="col">
                                <label for="">Cambiar Imagen de Banner:</label>
                                <input type="file" name="imagen" accept=".jpg,.jpeg,.png"
                                    class="form-control  @error('imagen') is-invalid @enderror">
                                @error('imagen')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label for="">Cambiar Video de Banner:</label>
                                <input type="file" name="archivo" accept=".mp4"
                                    class="form-control  @error('archivo') is-invalid @enderror">
                                @error('archivo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <input id="idbanner" type="hidden" class="form-control" name="id_banner" value="">

                        <div class="row col-md-12 mx-auto d-block mt-3">
                            <div class="progress mb-3">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                                    aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%" role="progressbar">
                                </div>
                            </div>
                            <div id="uploadStatus" style="display:none"></div>
                        </div>
                </div>

                <div class="modal-footer">
                    <input class="btn mx-auto col-md-6" style="background-color: #F28D8D; border-radius: 40px;"
                        type="submit" value="ACTUALIZAR BANNER">
                </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="redirect" value="{{ Request::url() }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        //Seteamos la edición de la respuesta
        $(".beditar").click(function() {
            var id = $(this).attr("id");
            var distribuidor = $('.distribuidor' + id).attr('id');

            //Seteo el modal de editar con el input val (dierespuesta con el id) y el resto de inputs
            $("#selectDist").val(distribuidor);
            $("#bannercod").text(id);
            $("#idbanner").val(id);
        });

        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('#exampleModal').modal('show');
        }

        $("#bupdate").on('submit', function(e) {
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $(this).attr('action');
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                type: type,
                url: url,
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".progress-bar").width('0%');
                    jQuery('#uploadStatus').hide();
                    jQuery('#uploadStatus').html('');
                },
                error: function() {
                    $('#uploadStatus').html('<span class="invalid-feedback d-block" role="alert"><strong>No se pudo editar de forma exitosa la capacitación, error de conexión</strong></span>');
                },
                success: function(resp) {
                    console.log(resp);
                    jQuery('#uploadStatus').show();
                    jQuery.each(resp.errors, function(key, value) {
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + value + '</strong></span></p>');
                    });

                    if (resp.success) {
                        $('#uploadStatus').append('<p><span style="color:#28A74B;" class="invalid-feedback d-block" role="alert"><strong>' + resp.success + '</strong></span></p>');
                        alert(resp.success);
                        location.href = $('#redirect').val();
                    }

                    if (resp.fallo)
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + resp.fallo + '</strong></span></p>');
                },
            });
        });
    });
</script>
