@extends('layouts.admin.app')

@section('content')
    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    @if ($errors->any())
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            Error en los datos enviados al generar el contenido informativo, por favor abre el modal y corrige los datos.
        </div>
    @endif

    <div class="row" style="margin-top: 5%">

        <!-------------------------------- CARDs NOTICIAS --------------------------------->
        <div class="col-lg-6 col-md-6">
            <div class="row mx-auto">
                <label for="" class="lead" style="color: #51A2A7; font-weight: bold">Noticias</label>
                <a href="{{ route('informativo.listnoticias') }}" class="ml-auto text-secondary">Ver todos</a>
            </div>

            @forelse ($noticias as $noticia)
                <div class="card">
                    <div class="card-header mb-0">
                        <div class="row mx-auto">
                            <h4 class="card-title" style="color: #51A2A7; font-weight: bold">{{ $noticia->nombre }}</h4>
                        </div>
                        <p class="category mb-0" style="color: #51A2A7;">
                            {{ date_format(date_create($noticia->fecha_inicio), 'F j, Y') }} -
                            {{ $noticia->fecha_fin ? date_format(date_create($noticia->fecha_fin), 'F j, Y') : 'Sin Vencimiento' }}
                        </p>
                    </div>
                    <div class="card-body">
                        <p>Estado: (<span class='{{ $noticia->estado ? 'text-success' : 'text-danger' }}'>
                                {{ $noticia->estado ? 'Activo' : 'Inactivo' }}
                            </span>)
                        </p>
                    </div>
                </div>
            @empty
                <div class="card">
                    <div class="card-header">
                        <div class="row mx-auto">
                            <h4 class="card-title" style="color: #51A2A7; font-weight: bold">No hay Noticias Registradas
                            </h4>
                        </div>
                    </div>
                </div>
            @endforelse

            <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal"
                data-target=".bd-example-modal-lg">
                <i class="material-icons">add</i>CREAR NOTICIA
            </button>

            <!-- MODAL DE CREAR NOTICIA -->
            <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                                Creacion de noticias</h5>
                            <button id="crearnoticia" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form onsubmit="return checkSubmit();" id="form" action="{{ route('noticias.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="col">
                                        <input required name="nombre" id="" type="text" placeholder="Nombre" value="{{ old('nombre') }}"
                                            class="form-control @error('nombre') is-invalid @enderror">
                                        @error('nombre')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col-12 mb-2">
                                        <label for="">Tipo de Noticia:</label>
                                        <input required list="nottipo" name="tipo" value="{{ old('tipo') }}"
                                            class="form-control @error('tipo') is-invalid @enderror" />
                                        <datalist id="nottipo">
                                            @foreach ($tipos as $tipo)
                                                <option value="{{ $tipo->nombre }}">
                                            @endforeach
                                        </datalist>
                                        @error('tipo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <textarea required name="informacion" id="" type="text" placeholder="Informacion"
                                            class="form-control @error('informacion') is-invalid @enderror">{{ old('informacion') }}</textarea>
                                        @error('informacion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <label for="name_user">Fecha de Inicio:</label>
                                        <input required max="{{ now()->format('Y-m-d') }}" min="{{ now()->format('Y-m-d') }}" name="fecha_inicio" id="" type="date" value="{{ old('fecha_inicio', date('Y-m-d')) }}"
                                            class="form-control @error('fecha_inicio') is-invalid @enderror">
                                        @error('fecha_inicio')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <div class="row mx-auto">
                                            <label for="name_user mx-auto">Fecha de Vencimiento:</label>
                                            <div class="form-check ml-auto">
                                                <label class="form-check-label ">
                                                    ¿Noticia sin vencimiento?
                                                    <input class="form-check-input" type="checkbox" value="1"
                                                        id="checkboxDetermine" name="withoutDate">
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <input min="{{ now()->addDay(1)->format('Y-m-d') }}" name="fecha_fin" id="checkboxConditioned" type="date" value="{{ old('fecha_fin') }}"
                                            class="form-control @error('fecha_fin') is-invalid @enderror">
                                        @error('fecha_fin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <script>
                                            var determine = document.getElementById("checkboxDetermine");
                                            var disableCheckboxConditioned = function() {
                                                if (determine.checked) {
                                                    document.getElementById("checkboxConditioned").disabled = true;
                                                    document.getElementById("checkboxConditioned").value = null;
                                                } else {
                                                    document.getElementById("checkboxConditioned").disabled = false;
                                                }
                                            }
                                            determine.onclick = disableCheckboxConditioned;
                                            disableCheckboxConditioned();
                                        </script>
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col">
                                        <label for="">Subir documento de noticia:</label>
                                        <input type="file" name="documento" accept=".jpg,.jpeg,.png,.pdf"
                                            class="form-control @error('documento') is-invalid @enderror" id="">
                                        @error('documento')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <label for="">Subir Imagen preview de noticia:</label>
                                        <input type="file" name="imagen" accept=".jpg,.jpeg,.png"
                                            class="form-control @error('imagen') is-invalid @enderror" id="">
                                        @error('imagen')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col">
                                        <label for="">Estado:</label><br>
                                        <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="estado" checked
                                                    id="inlineRadio1" value="1"> Noticia Activada
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check form-check-radio form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="estado" id="inlineRadio1"
                                                    value="0"> Noticia Desactivada
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <div class="modal-footer">
                            <button id="button" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;"
                                type="submit">CREAR NOTICIA</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-------------------------------- CARDs BANNERS --------------------------------->
        <div class="col-lg-6 col-md-6">
            <div class="row mx-auto">
                <label class="lead" for="" style="color: #F28D8D; font-weight: bold">Banners</label>
                <a href="{{ route('informativo.listbanners') }}" class="ml-auto text-secondary
                            ">Ver todos</a>
            </div>

            @forelse ($banners as $banner)
                <div class="card">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{ route('banner.preview', ['filename' => basename($banner->imagen)]) }}"
                                    class="w-100" alt="">
                            </div>
                            <div class="col-md-6 my-auto">
                                <p style="color: #F28D8D; font-weight: bold" class="mb-0">Fecha de publicación:</p>
                                <p class="font-weight-bold mb-0">
                                    Distribuidor: {{ $banner->distribuidor == 'All' ? 'General' : $banner->distribuidor }}
                                </p>
                                <p>{{ date_format(date_create($banner->created_at), 'F j, Y') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="card">
                    <div class="card-header">
                        <div class="row mx-auto">
                            <h4 class="card-title" style="color: #F28D8D; font-weight: bold">No hay Banners Registrados</h4>
                        </div>
                    </div>
                </div>
            @endforelse

            <button class="btn btn-round" style="background-color: #F28D8D" data-toggle="modal"
                data-target=".bd-example-modal-lg2">
                <i class="material-icons">add</i>AGREGAR BANNER
            </button>

            <!-- MODAL DE AGREGAR BANNER -->
            <div class="modal fade bd-example-modal-lg2" id="exampleModal" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                                Creacion de banner</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form onsubmit="return checkSubmit();" id="bstore" action="{{ route('banner.store') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-row">
                                    <div class="col">
                                        <label for="">Seleccione Distribuidor o Banner General:</label>
                                        <select name="distribuidor" class="form-control @error('distribuidor') is-invalid @enderror">
                                            <option value="All" selected>General para todos los usuarios</option>
                                            <option value="Palermo" {{ old('distribuidor') == 'Palermo' ? 'selected' : '' }}>Palermo</option>
                                            <option value="Dentales Antioquia SAS" {{ old('distribuidor') == 'Dentales Antioquia SAS' ? 'selected' : '' }}>Dentales Antioquia SAS</option>
                                            <option value="Casa Dental Gabriel Velasquez" {{ old('distribuidor') == 'Casa Dental Gabriel Velasquez' ? 'selected' : '' }}>Casa Dental Gabriel Velasquez</option>
                                            <option value="Aldental SA" {{ old('distribuidor') == 'Aldental SA' ? 'selected' : '' }}>Aldental SA</option>
                                            <option value="Dentales Padilla LTDA" {{ old('distribuidor') == 'Dentales Padilla LTDA' ? 'selected' : '' }}>Dentales Padilla LTDA</option>
                                            <option value="Orbidental SAS" {{ old('distribuidor') == 'Orbidental SAS' ? 'selected' : '' }}>Orbidental SAS</option>
                                            <option value="Luis Fernando Posada Henao" {{ old('distribuidor') == 'Luis Fernando Posada Henao' ? 'selected' : '' }}>Luis Fernando Posada Henao</option>
                                            <option value="Dental 83 SAS" {{ old('distribuidor') == 'Dental 83 SAS' ? 'selected' : '' }}>Dental 83 SAS</option>
                                            <option value="Jose Antonio Eraso Rivas" {{ old('distribuidor') == 'Jose Antonio Eraso Rivas' ? 'selected' : '' }}>Jose Antonio Eraso Rivas</option>
                                            <option value="Dental Nader SAS" {{ old('distribuidor') == 'Dental Nader SAS' ? 'selected' : '' }}>Dental Nader SAS</option>
                                            <option value="Casa Odontologica Importadora" {{ old('distribuidor') == 'Casa Odontologica Importadora' ? 'selected' : '' }}>Casa Odontologica Importadora</option>
                                            <option value="Dentales Market SAS" {{ old('distribuidor') == 'Dentales Market SAS' ? 'selected' : '' }}>Dentales Market SAS</option>
                                            <option value="Adental SAS" {{ old('distribuidor') == 'Adental SAS' ? 'selected' : '' }}>Adental SAS</option>
                                        </select>
                                        @error('distribuidor')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row mt-3">
                                    <div class="col">
                                        <label for="">Imagen de Banner:</label>
                                        <input type="file" name="imagen" accept=".jpg,.jpeg,.png"
                                            class="form-control  @error('imagen') is-invalid @enderror">
                                        @error('imagen')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <label for="">Archivo de video del Banner (Opcional):</label>
                                        <input type="file" name="archivo" accept=".mp4"
                                            class="form-control  @error('archivo') is-invalid @enderror">
                                        @error('archivo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="progress my-5">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%" role="progressbar">
                                    </div>
                                </div>
                                <div id="uploadStatus" style="display:none"></div>
                        </div>

                        <div class="modal-footer">
                            <input class="btn mx-auto col-md-6" style="background-color: #F28D8D; border-radius: 40px;"
                                type="submit" value="CREAR BANNER">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
    <input type="hidden" id="redirect" value="{{ route('informativo.index') }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }

        $("#bstore").on('submit', function(e) {
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $(this).attr('action');
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                type: type,
                url: url,
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".progress-bar").width('0%');
                    jQuery('#uploadStatus').hide();
                    jQuery('#uploadStatus').html('');
                },
                error: function() {
                    $('#uploadStatus').html('<span class="invalid-feedback d-block" role="alert"><strong>No se pudo cargar de forma exitosa el banner, error de conexión</strong></span>');
                },
                success: function(resp) {
                    console.log(resp);
                    jQuery('#uploadStatus').show();
                    jQuery.each(resp.errors, function(key, value) {
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + value + '</strong></span></p>');
                    });

                    if (resp.success) {
                        $('#uploadStatus').append('<p><span style="color:#28A74B;" class="invalid-feedback d-block" role="alert"><strong>' + resp.success + '</strong></span></p>');
                        alert(resp.success);
                        location.href = $('#redirect').val();
                    }

                    if (resp.fallo)
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + resp.fallo + '</strong></span></p>');
                },
            });
        });
    });
</script>
