@extends('layouts.admin.app')

@section('content')


@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif

<div class="row" style="margin-top: 3%">

    <!-------------------------------- CARDs PREMIOS / NIVELES --------------------------------->
    <div class="col-lg-6 col-md-6">
        <div class="row mx-auto">
            <label class="lead" for="" style="color: #51A2A7; font-weight: bold">Control de premios</label>
            <a href="{{route('navidad.list_premios')}}" class="ml-auto text-secondary">Ver todos</a>
        </div>

        @forelse($premios as $premio)
            <!-- DIV DEL PREMIO -->
                <div class="card my-3">
                    <div class="row">
                        <div class="col-md-4 p-3">
                            <div style="width: 100%; height: 120px" class="text-center">
                                @if( $premio->distribuidor == 'N/A' )
                                    <img src="{{ route('premio.icon', ['filename' => basename($premio->premio->foto)]) }}" width="100%" height="100%" alt="">
                                @else
                                    <!-- <img src="{{ $premio->foto }}" width="100%" height="100%" alt=""> -->
                                    <img src="{{ route('premio.icon', ['filename' => basename($premio->premio->foto)]) }}" width="100%" height="100%" alt="">
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 p-3">
                            <p class="mb-0" style="color: #51A2A7; font-weight: bold">{{ $premio->premio->nombre }}</p>
                            <p class="mb-0">Marca - <span class="font-weight-bold">{{ $premio->premio->marca }}</span></p>
                            <p class="mb-0">Distribuidor - <span class="font-weight-bold">{{ $premio->premio->distribuidor }}</span></p>
                            <p class="mt-2">
                                {{ $premio->premio->descripcion }}
                            </p>
                        </div>
                    </div>
                </div>
            <!-- FIN DIV DEL PREMIO -->
        @empty
            <!-- NO HAY PREMIOS -->
                <div class="card my-3">
                    <div class="row">
                        <div class="col-10 m-auto p-3">
                            <h4 style="color: #51A2A7; font-weight: bold">
                                No hay premios cargados en el evento, presiona ver todos y añadelos...
                            </h4>
                        </div>
                    </div>
                </div>
            <!-- FIN NO HAY PREMIOS -->
        @endforelse
     
    </div>

    <!-------------------------------- CARDs CANJEADOS --------------------------------->
    <div class="col-lg-6 col-md-6">
        <div class="row mx-auto">
            <label for="" class="lead" style="color: #F28D8D; font-weight: bold">Premios canjeados</label>
            <a href="{{route('navidad.list_canjeados')}}" class="ml-auto text-secondary">Ver todos</a>
        </div>
        
        @forelse($canjeados as $canjeado)
            <!-- DIV DE CANJEADO -->
                <div class="card my-3">
                    <div class="card-header">
                        <div class="row mx-auto">
                            <h4 class="card-title" style="color: #F28D8D; font-weight: bold">{{ $canjeado->nombre }}</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="mb-0">Cliente: {{ $canjeado->navidad ? $canjeado->navidad->nombre." ".$canjeado->navidad->apellido : "Usuario" }}</p>
                        <p class="mb-0">Fecha de rendicion: {{ date_format(date_create($canjeado->created_at), "F j, Y") }}</p>
                        <p class="font-weight-bold">
                            {{ $canjeado->completado ? "Ya fue redimido" : "Aún por redimir" }}
                        </p>
                    </div>
                </div>
            <!-- FIN DIV DE CANJEADO -->
        @empty
            <!-- DIV NO HAY CANJEADOS -->
                <div class="card my-3">
                    <div class="row">
                        <div class="col-10 m-auto p-3">
                            <h4 style="color: #F28D8D; font-weight: bold">
                                No hay premios canjeados en el evento todavia...
                            </h4>
                        </div>
                    </div>
                </div>
            <!-- FIN DIV NO HAY CANJEADOS -->
        @endforelse

    </div>

</div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<style>
    .file-input>[type='file'] {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 10;
        cursor: pointer;
    }

    .file-input>.button {
        display: inline-block;
        cursor: pointer;
        background: #eee;
        padding: 4px 12px;
        border-radius: 2px;
        margin-right: 8px;
    }

    .file-input:hover>.button {
        background: dodgerblue;
        color: white;
    }

    .file-input>.label {
        color: #333;
        white-space: nowrap;
        opacity: .3;
    }

    .file-input.-chosen>.label {
        opacity: 1;
    }
</style>
@endsection
