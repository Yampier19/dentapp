@extends('layouts.admin.app')

@section('content')

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('contacts'))
        @if(!Session::get('contacts')->isEmpty())
            <div style="padding: 10px; color: #ffffff; margin-bottom: 1%;">
                <label for="">Contactos que no pudieron ser agregados a la campaña en Mail Chimp</label>
                <ul class="text-dark">
                    @foreach(Session::get('contacts') as $contacto)
                        <li>{{ $contacto }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endif

    <div class="row my-5">
        <div class="col d-flex justify-content-center align-items-center">
            <img src="{{ url('Img/navidad.png') }}" alt="" width="50%">
        </div>
    </div>

    <div class="card-group">

        <!-- CARD #1 -->
        <div class="card" data-animation="true" style="cursor: pointer">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <a class="d-block blur-shadow-image" href="{{route('navidad.notificacion')}}">
                <img src="https://fondosmil.com/fondo/2024.jpg" alt="img-blur-shadow" class="img-fluid shadow border-radius-lg">
                </a>
                <div class="colored-shadow" style="background-image: url(&quot;https://demos.creative-tim.com/test/material-dashboard-pro/assets/img/products/product-1-min.jpg&quot;);"></div>
            </div>
            <div class="card-body text-center">
                <h5 class="font-weight-normal mt-3">
                <a href="javascript:;" class="h3">Enviar Notificación</a>
                </h5>
                <p class="mb-0">
                    Aqui podras enviar los push para los correos y para la app movil
                </p>
            </div>

            <hr class="dark horizontal my-0">

            <div class="card-footer d-flex">
                <i class="fas fa-bells"></i>
                <p class="text-sm my-auto">DentApp Navidad</p>
            </div>
        </div>


         <!-- CARD #2 -->
         <div class="card" data-animation="true" style="cursor: pointer">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <a class="d-block blur-shadow-image" href="{{route('navidad.subscribe')}}">
                <img src="https://fondosmil.com/fondo/780.jpg" alt="img-blur-shadow" class="img-fluid shadow border-radius-lg">
                </a>
                <div class="colored-shadow" style="background-image: url(&quot;https://demos.creative-tim.com/test/material-dashboard-pro/assets/img/products/product-1-min.jpg&quot;);"></div>
            </div>
            <div class="card-body text-center">
                <h5 class="font-weight-normal mt-3">
                <a href="javascript:;" class="h3">Agregar contactos</a>
                </h5>
                <p class="mb-0">
                    Esta seccion es para agregar los contactos de MailtChip a la campaña
                </p>
            </div>

            <hr class="dark horizontal my-0">

            <div class="card-footer d-flex">
                <i class="fas fa-bells"></i>
                <p class="text-sm my-auto">DentApp Navidad</p>
            </div>
        </div>


         <!-- CARD #3 -->
         <div class="card" data-animation="true">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <a class="d-block blur-shadow-image" href="{{route('navidad.list_users')}}">
                <img src="https://st1.uvnimg.com/25/c7/e245c90c43e8a6b477fa31d6762f/d3ceaec2d34c4043a6186ff5ee84a61a" alt="img-blur-shadow" class="img-fluid shadow border-radius-lg">
                </a>
                <div class="colored-shadow" style="background-image: url(&quot;https://demos.creative-tim.com/test/material-dashboard-pro/assets/img/products/product-1-min.jpg&quot;);"></div>
            </div>
            <div class="card-body text-center">
                <h5 class="font-weight-normal mt-3">
                <a href="javascript:;" href="{{route('navidad.list_users')}}" class="h3">Usuarios Participantes</a>
                </h5>
                <p class="mb-0">
                    ¿Quieres mirar los doctores que han participado en la campaña? <br />
                    Da Click en esta seccion para ir a un listado detallado
                </p>
            </div>

            <hr class="dark horizontal my-0">

            <div class="card-footer d-flex">
                <i class="fas fa-bells"></i>
                <p class="text-sm my-auto">DentApp Navidad</p>
            </div>
        </div>


    </div>
   
@endsection
