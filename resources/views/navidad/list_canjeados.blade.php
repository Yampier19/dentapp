@extends('layouts.admin.app')

@section('content')

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

@if (Session::has('error'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('error') }}
    </div>
@endif

<div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
    <div class="col">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #E7344C">
            Todos los premios canjeados Navidad
            @if($usuario)
                <br><span class="text-primary">{{ $usuario->nombre." ".$usuario->apellido }}</span>
            @endif
        </label>
    </div>
    <div class="col" style="display: flex;justify-content: flex-end !important">
        @if($usuario)
            <a href="{{ route('navidad.list_canjeados') }}">
                <button class="btn btn-round mr-2" style="background-color: #E7344C">
                    <i class="material-icons mr-2">chrome_reader_mode</i>VER TODOS LOS CANJEADOS
                </button>
            </a>
        @endif

        <div class="row">
            <div class="col"> <a href="{{ route('navidad.list') }}">
                <button class="btn btn-round" style="background-color: #E7344C">
                    <i class="material-icons mr-2">reply</i>IR Al PANEL NAVIDAD
                </button>
            </a></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive shadow p-3 mb-5 rounded">
            <table id="example" class="table table-striped" cellspacing="0" width="100%">
                <thead >
                    <tr>
                        <th>Avatar</th>
                        <th>Usuario</th>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Premio</th>
                        <th>Marca</th>
                        <th>Descripción</th>
                        <th>Referencia</th>
                        <th>Completado</th>
                        <th>Cambiar</th>
                    </tr>
                </thead>
                <tbody class="table-light" style="background-color: #fff; color: #000">
                    @foreach($canjeados as $canjeado)
                        <tr>
                            <td>
                                <img style="width:100px;max-height:100px;"
                                    src="{{ route('cliente.icon', ['filename' => basename($canjeado->navidad->cliente ? $canjeado->navidad->cliente->foto : null)]) }}">
                            </td>
                            <td>{{  $canjeado->navidad ? $canjeado->navidad->nombre." ".$canjeado->navidad->apellido : "Usuario" }}</td>
                            <td>{{ date_format(date_create($canjeado->created_at), 'F j, Y') }}</td>
                            <td>{{ $canjeado->tipo }}</td>
                            <td>{{ $canjeado->nombre }}</td>
                            <td>{{ $canjeado->marca }}</td>
                            <td>{{ $canjeado->descripcion }}</td>
                            <td>
                                @if($canjeado->referencia != "No canjeado" && $canjeado->tipo == "bono")
                                    <a href="https://api.activarpromo.com/productos/viewpdf/{{ $canjeado->referencia }}" 
                                        style="cursor: pointer; text-decoration: underline" target="_blank">
                                        {{ $canjeado->referencia }}
                                    </a>
                                @else
                                    {{ $canjeado->referencia }}
                                @endif 
                            </td>
                            <td>{{ $canjeado->completado ? "Canjeado" : "Falta redimir" }}</td>
                            <td>
                                @if(!$canjeado->completado && $canjeado->tipo != "Cena")
                                    <a id="{{ $canjeado->id_navidad_canjeado }}" data-toggle="modal" data-target="#modalEdit" class="feditar">
                                        <i class="far fa-edit"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>

<!-- MODAL DE EDITAR CANJEADO -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modificar Premio a uno Distinto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('navidad.canjeado.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="col-12">
                        <label class="d-block" for="referencia">Selecciona el nuevo premio al cual quieras cambiar el canjeado:</label>
                        <select name="id_premio" id="premioSelected" class="form-control custom-select custom-select-sm @error('id_premio') is-invalid @enderror">
                            <option value="" selected disabled>Selecciona un Premio...</option>
                            @foreach($premios as $premio)
                                <option value="{{ $premio->premio->id_premio }}">{{ $premio->premio->nombre }}</option>
                            @endforeach
                        </select>
                        @error('id_premio')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <input id="idCanjeado" type="hidden" class="form-control" name="id_canjeado" value="">

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Cambiar Premio Ganado</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR CANJEADO -->

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {

        $(".feditar").click(function() {
            var id = $(this).attr("id");
            $("#idCanjeado").val(id);
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('#modalEdit').modal('show');
        }
    });
</script>