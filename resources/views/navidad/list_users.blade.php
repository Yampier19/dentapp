@extends('layouts.admin.app')

@section('content')

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    <div class="row my-5">
        <div class="col d-flex justify-content-center align-items-center">
            <img src="{{ url('Img/navidad.png') }}" alt="" width="50%">
        </div>
    </div>

    <div class="row">
        <div class="col d-flex justify-content-center">
            <label for="" class="lead"
            style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #E7344C">USUARIOS PARTICIPANTES</label>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead >
                        <tr>
                            <th>Avatar</th>
                            <th>Nombre Cliente</th>
                            <th>Correo</th>
                            <th>Tickets Disponibles</th>
                            <th>Giros realizados</th>
                            <th>Premios ganados</th>
                            <th>Premios sin Redimir</th>
                            <th>Fecha participante</th>
                            <th>Canjeados</th>
                            <th>Facturas</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach($participantes as $participante)
                            <tr>
                                <td>
                                    <img style="width:100px;max-height:100px;"
                                    src="{{ route('cliente.icon', ['filename' => basename($participante->cliente ? $participante->cliente->foto : null)]) }}">
                                </td>
                                <td>{{ $participante->nombre." ".$participante->apellido }}</td>
                                <td>{{ $participante->correo }}</td>
                                <td>{{ $participante->tickets + $participante->tickets_especial }}</td>
                                <td>{{ $participante->tickets_usados }}</td>
                                <td>{{ $participante->canjeados ? $participante->canjeados->count() : "0" }}</td>
                                <td>{{ $participante->canjeados ? $participante->canjeados->where('completado',0)->count() : "0" }}</td>
                                <td>{{ $participante->fecha_participante }}</td>
                                <td>
                                    <a href="{{ route('navidad.list_canjeados', $participante->FK_id_cliente) }}" style="color: #00a7d0; cursor: pointer"> 
                                        <i class="fas fa-award"></i> Ver
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('navidad.facturas', $participante->FK_id_cliente) }}" style="color: #00a7d0; cursor: pointer">
                                        <i class="fas fa-file-invoice"></i> Ver
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>


    

@endsection



