@extends('layouts.admin.app')

@section('content')

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

@if (Session::has('error'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('error') }}
    </div>
@endif

<div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
    <div class="col">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #E7344C">
            Todos las facturas Navidad
            @if($usuario)
                <br><span class="text-primary">{{ $usuario->nombre." ".$usuario->apellido }}</span>
            @endif
        </label>
    </div>
    <div class="col" style="display: flex;justify-content: flex-end !important">
        @if($usuario)
            <a href="{{ route('navidad.facturas') }}">
                <button class="btn btn-round mr-2" style="background-color: #E7344C">
                    <i class="material-icons mr-2">chrome_reader_mode</i>VER TODAS LAS FACTURAS
                </button>
            </a>
        @endif

        <a href="{{ route('navidad.list') }}">
            <button class="btn btn-round" style="background-color: #E7344C">
                <i class="material-icons mr-2">reply</i>IR Al PANEL NAVIDAD
            </button>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive shadow p-3 mb-5 rounded">
            <table id="example" class="table table-striped" cellspacing="0" width="100%">
                <thead >
                    <tr>
                        <th>Avatar</th>
                        <th>Nombres</th>
                        <th>Correo</th>
                        <th>Tipo</th>
                        <th>Fecha Factura</th>
                        <th>Facturado</th>
                        <th>Campaña</th>
                        <th>Estado</th>
                        <!-- <th>Tickets Ganados</th> -->
                        <th>Productos</th>
                    </tr>
                </thead>
                <tbody class="table-light" style="background-color: #fff; color: #000">
                    @foreach($facturas as $factura)
                        <tr>
                            <td>
                                <img style="width:100px;max-height:100px;"
                                src="{{ route('cliente.icon', ['filename' => basename($factura->campaña->cliente ? $factura->campaña->cliente->foto : null)]) }}">
                            </td>
                            <td>{{ $factura->campaña->nombre." ".$factura->campaña->apellido }}</td>
                            <td>{{ $factura->campaña->correo }}</td>
                            <td>{{ $factura->tipo == "normal" ? "Factura Campaña" : "Registro por Campaña" }}</td>
                            <td>{{ $factura->created_at }}</td>
                            <td>${{ number_format($factura->factura->monto_total, 2, ',', '.') }}</td>
                            <td>Navidad</td>
                            <td>{{ $factura->factura->estado }}</td>
                            <!-- <td>
                                <?php 
                                    /* $tickets = 0;
                                    if($factura->factura->monto_total >= 400000 && $factura->factura->estado == "aceptado"){
                                        $tickets++;
                                        $ganar = array("RESINA FILTEK Z350XT", "RESINA FILTEK ONE", "RESINA FILTEK UNIVERSAL", "RELYX U200", "RELYX ULTIMATE");
                                        
                                        foreach ($factura->productos as $key => $producto) {
                                            if(in_array($producto->producto, $ganar)){
                                                $tickets++;
                                                break;
                                            }
                                        }
                                    } */
                                ?>
                                {{-- $tickets --}}
                            </td> -->
                            <td>
                                <!-- AQUI QUE REDIRIJA A CLIENTE.DETAILS->$id  -->
                                <a id="{{ $factura->id_navidad_factura }}" class="openModal" style="color: #E7344C; cursor: pointer"> 
                                    <i class="far fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>

<!-- MODAL CON EL DETALLADO DE PRODUCTOS DE LA FACTURA -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel" style="color: #51A2A7">
                    PRODUCTOS DE LA FACTURA
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="div_productos">
                <div class="row-reverse p-0 m-0 mb-3">
                    <div class="col" style="background: #51A2A7">
                        <label for="" class="font-weight-bold text-white lead p-2">Listado:</label>
                    </div>
                    <div class="col m-0 p-0">
                        <ul style="width: 100% !important" class="p-0 m-0">
                            <li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">Nombre de la Encuesta</li>
                            <li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">Numero de preguntas contestadas</li>
                            <li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">Numero de preguntas omitidas</li>
                            <li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">Puntos obtenidos</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="urlModal" value="{{ route('navidad.productos') }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        //ABRIR MODAL CON LA DATA RECIBIDA POR AJAX
        $('.openModal').click(function(){
            var id = $(this).attr('id');
            var url = $('#urlModal').val();
            
            $.ajax({
                url: url+'/'+id,
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content")
                },
                type: 'POST',
                success: function(response){
                    if(response.productos){
                        $("#div_productos").empty();

                        html = '';
                        for (let producto of response.productos) {
                            html += '<li style="border-style: solid; border-color: #e0e0e0; list-style:none; padding: 5px">'+producto["producto"]+'</li>';
                        }
                        html =  '<div class="row-reverse p-0 m-0 mb-3" >'+
                                    '<div class="col" style="background: #51A2A7">'+
                                        '<label for="" class="font-weight-bold text-white lead p-2">Listado:</label>'+
                                    '</div>'+
                                    '<div class="col m-0 p-0">'+
                                        '<ul style="width: 100% !important" class="p-0 m-0">'+
                                            html+
                                        '</ul>'+
                                    '</div>'+
                                '</div>';
                        $("#div_productos").append(html);
                            
                        $('#exampleModal').modal('show')
                    }
                    else{
                        Swal.fire(
                            response.message,
                            'Presiona el boton para cerrar el modal',
                            'warning'
                        )
                    }
                }
            });   
        }); 
    });
</script>