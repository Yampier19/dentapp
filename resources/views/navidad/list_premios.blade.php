@extends('layouts.admin.app')

@section('content')

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

@if (Session::has('error'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('error') }}
    </div>
@endif

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

<div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
    <div class="col">
        <label for="" class="lead"
            style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #E7344C">Todos los premios
            Navidad</label>
    </div>
    <div class="col" style="display: flex;justify-content: flex-end !important">
        <div class="row">
            <div class="col">
                <button class="btn btn-round" style="background-color: #E7344C"  data-toggle="modal" data-target=".bd-example-modal-lg2">
                    <i class="material-icons mr-2">add_circle</i>ADJUNTAR PREMIO
                </button>
            </div>
            <div class="col"> <a href="{{ route('navidad.list') }}">
                <button class="btn btn-round" style="background-color: #E7344C">
                    <i class="material-icons mr-2">reply</i>IR Al PANEL NAVIDAD
                </button>
            </a></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive shadow p-3 mb-5 rounded">
            <table id="example" class="table table-striped" cellspacing="0" width="100%">
                <thead >
                    <tr>
                        <th>Imagen</th>
                        <th>Tipo</th>
                        <th>Nombre del premio</th>
                        <th>Marca</th>
                        <th>Distribuidor</th>
                        <th>Descripción</th>
                        <th>Entrega</th>
                        <th>Tag</th>
                        <th>Stock</th>
                        <th>Ganado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="table-light" style="background-color: #fff; color: #000">
                    <!-- PREMIO DE CENA NAVIDEÑA -->
                    <tr>
                        <td>
                            <img style="width:100px;max-height:100px; border-radius: 60px"
                            src="{{ url('Img/navidad.png') }}">
                        </td>
                        <td class="font-weight-bold">Cena Navideña</td>
                        <td>Cena Navideña</td>
                        <td>Ninguna</td>
                        <td>Campaña Navidad</td>
                        <td>Premio redimible para unicamente dos usuarios</td>
                        <td>Domicilio</td>
                        <td>Cena</td>
                        <td>{{ (2 - $canjeado->whereNull('FK_id_premio')->count()) }}</td>
                        <td>{{ $canjeado->whereNull('FK_id_premio')->count() }}</td>
                        <td>
                            No editable
                        </td>
                    </tr>
                    @foreach($premios as $premio)
                        <tr>
                            <td>
                                <img style="width:100px;max-height:100px; border-radius: 60px"
                                src="{{ route('premio.icon', ['filename' => basename($premio->premio->foto)]) }}">
                            </td>
                            <td class="font-weight-bold">{{ $premio->tipo }}</td>
                            <td>{{ $premio->premio->nombre }}</td>
                            <td>{{ $premio->premio->marca }}</td>
                            <td>{{ $premio->premio->distribuidor }}</td>
                            <td>{{ $premio->premio->descripcion }}</td>
                            <td>{{ $premio->premio->entrega }}</td>
                            <td>{{ $premio->premio->tag }}</td>
                            <td>{{ $premio->premio->stock }}</td>
                            <td>{{ $canjeado->where('FK_id_premio', $premio->FK_id_premio)->count() }}</td>
                            <td>
                                <a id="{{ route('navidad.premio.delete', ['id' => $premio->id_navidad_premios]) }}" class="deletePremio">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- MODAL DE AGREGAR PREMIO -->
<div class="modal fade bd-example-modal-lg2" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #E7344C; font-weight: bold">
                    Seleccionar premios a ganar en el evento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('navidad.premio.store') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-row mb-2 text-center">
                        <div class="col">
                            <label for="">Tipo de Premio:</label>
                            <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipo" id="inlineTipo"
                                        value="bono"> Bono
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-radio form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="tipo" id="inlineTipo"
                                        value="producto"> Producto
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <!-- SELECT DE PRODUCTOS BONO -->
                    <div class="form-row mb-5 d-none" id="bonoPremio">
                        <div class="col-12">
                            <div class="row-reverse">
                                <div class="col m-0 p-0 ">
                                    <label for="">Selecciona un premio:</label>
                                </div>
                                <div class="col m-0 p-0">
                                    <select name="id_premio" id="bonoSelected" class="col-6 form-control custom-select custom-select-sm @error('id_premio') is-invalid @enderror">
                                        <option value="" selected disabled>Selecciona un Premio...</option>
                                        @foreach($bonos as $bono)
                                            <option value="{{ $bono->id_premio }}">{{ $bono->nombre }}</option>
                                        @endforeach
                                    </select>
                                    @error('id_premio')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIN SELECT -->

                    <!-- SELECT DE PRODUCTOS PRODUCTO -->
                    <div class="form-row mb-5 d-none" id="productoPremio">
                        <div class="col-12">
                            <div class="row-reverse">
                                <div class="col m-0 p-0 ">
                                    <label for="">Selecciona un premio:</label>
                                </div>
                                <div class="col m-0 p-0">
                                    <select name="id_premio" id="productoSelected" class="col-6 form-control custom-select custom-select-sm @error('id_premio') is-invalid @enderror">
                                        <option value="" selected disabled>Selecciona un Premio...</option>
                                        @foreach($productos as $producto)
                                            <option value="{{ $producto->id_premio }}">{{ $producto->nombre }}</option>
                                        @endforeach
                                    </select>
                                    @error('id_premio')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIN SELECT -->

                    <div class="form-row">
                        <div class="col">
                            <input id="nombrePremio" type="text" placeholder="Nombre" disabled style="background: #eeeeee; border-radius: 10px" class="form-control px-3 premioInput">
                        </div>
                        <div class="col">
                            <input id="marcaPremio" type="text" placeholder="Marca" disabled style="background: #eeeeee; border-radius: 10px" class="form-control px-3 premioInput">
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Categoría del Premio:</label>
                            <input id="tagPremio" disabled style="background: #eeeeee; border-radius: 10px" class="form-control px-3 premioInput" />
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <textarea id="descripcionPremio" type="text" placeholder="Descripción" disabled style="background: #eeeeee; border-radius: 10px" class="form-control px-3 premioInput"></textarea>
                        </div>
                    </div>
                    <div class="form-row ">
                        <div class="col">
                            <input id="stockPremio" type="text" placeholder="Stock" disabled style="background: #eeeeee; border-radius: 10px" class="form-control px-3 premioInput">
                        </div>
                        <div class="col">
                            <input id="entregaPremio" type="text" placeholder="Entrega" disabled style="background: #eeeeee; border-radius: 10px" class="form-control px-3 premioInput">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6"  style="background-color: #E7344C; border-radius: 40px;">ENLAZAR PREMIO</button>
            </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" id="urlModal" value="{{ route('navidad.detail.premio') }}">
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        //ABRIR MODAL CON LA DATA RECIBIDA POR AJAX
        $('#bonoSelected').change(function(){
            var id = $(this).val();
            var url = $('#urlModal').val();

            $.ajax({
                url: url+'/'+id,
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content")
                },
                type: 'POST',
                success: function(response){
                    if(response.premio){
                        //seteamos los valores de todo
                        $('#nombrePremio').val(response.premio["nombre"]);
                        $('#marcaPremio').val(response.premio["marca"]);
                        $('#tagPremio').val(response.premio["tag"]);
                        $('#descripcionPremio').val(response.premio["descripcion"]);
                        $('#stockPremio').val(response.premio["stock"]);
                        $('#entregaPremio').val(response.premio["entrega"]);
                    }
                    else{
                        $('.premioInput').val(""),

                        Swal.fire(
                            response.message,
                            'Presiona el boton para cerrar el modal',
                            'warning'
                        )
                    }
                }
            });
        });

        $('#productoSelected').change(function(){
            var id = $(this).val();
            var url = $('#urlModal').val();

            $.ajax({
                url: url+'/'+id,
                data: {
                    "_token": $("meta[name='csrf-token']").attr("content")
                },
                type: 'POST',
                success: function(response){
                    if(response.premio){
                        //seteamos los valores de todo
                        $('#nombrePremio').val(response.premio["nombre"]);
                        $('#marcaPremio').val(response.premio["marca"]);
                        $('#tagPremio').val(response.premio["tag"]);
                        $('#descripcionPremio').val(response.premio["descripcion"]);
                        $('#stockPremio').val(response.premio["stock"]);
                        $('#entregaPremio').val(response.premio["entrega"]);
                    }
                    else{
                        $('.premioInput').val(""),

                        Swal.fire(
                            response.message,
                            'Presiona el boton para cerrar el modal',
                            'warning'
                        )
                    }
                }
            });
        });

        //PREMIO A CARGAR
        $(':radio[name="tipo"]').change(function(){
            let value = $(this).val();

            $('#bonoPremio').addClass('d-none');
            $('#bonoSelected').removeAttr('required');

            $('#productoPremio').addClass('d-none');
            $('#productoSelected').removeAttr('required');

            if(value == "bono"){
                $('#bonoPremio').removeClass('d-none');
                $('#bonoSelected').attr('required', true);
            }
            else{
                $('#productoPremio').removeClass('d-none');
                $('#productoSelected').attr('required', true);
            }
        });

        //DELETE DE PREMIO
        $('.deletePremio').click(function(){
            var url = $(this).attr('id');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Estas Segur@ de eliminar el premio para la campaña?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, por favor!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    swalWithBootstrapButtons.fire(
                        'Perfecto!',
                        'El premio sera removido de la campaña.',
                        'success'
                    );
                    location.href = url;
                }
                else if (result.dismiss === Swal.DismissReason.cancel) {
                    swalWithBootstrapButtons.fire(
                        'Cancelado!',
                        'Tu cambio de premio no sera procesado.',
                        'error'
                    )
                }
            })
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('#exampleModal').modal('show');
        }
    });
</script>
