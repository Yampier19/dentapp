@extends('layouts.admin.app')

@section('content')
    <div class="col-md-12 mx-auto">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item active" onclick="f1()">
                        <a id="p-ganados" class="nav-link font-weight-bold bg-dark text-white" style="cursor: pointer; color: #454040">Puntos Ganados</a>
                    </li>
                    <li class="nav-item" onclick="f2()">
                        <a id="f-cargadas" class="nav-link font-weight-bold" style="cursor: pointer; color: #454040">Facturas
                            Cargadas</a>
                    </li>
                    <li class="nav-item" onclick="f3()">
                        <a id="p-canjeados" class="nav-link font-weight-bold" style="cursor: pointer; color: #454040">Premios
                            Canjeados</a>
                    </li>
                    <li class="nav-item" onclick="f4()">
                        <a id="h-acciones" class="nav-link font-weight-bold" style="cursor: pointer; color: #454040">Historial de
                            Acciones</a>
                    </li>
                    <li class="nav-item" onclick="f5()">
                        <a id="t-contestadas" class="nav-link font-weight-bold" style="cursor: pointer; color: #454040">Trivias
                            Contestadas</a>
                    </li>
                </ul>
            </div>
        </nav>
        <a href="{{ route('cliente.index') }}" class="btn btn-round float-right mt-4" style="background-color: #51A2A7">
            <span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span><b>Volver</b>
        </a>
        <!-- LISTA DE PUNTOS -->
        <div id="puntos">
            <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">
                Historial de Puntos Ganados: {{ $cliente->nombre . ' ' . $cliente->apellido }}
            </label>
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="puntos table table-striped " cellspacing="0" width="100%">
                            <thead style="background-color: #51A2A7; color: #fff">
                                <tr>
                                    <th>Cliente</th>
                                    <th>Descripcion</th>
                                    <th>Cantidad</th>
                                    <th>Fecha de Ganado</th>
                                </tr>
                            </thead>
                            <tbody class="table-bordered text-dark" style="background-color: #fff; color: #000">
                                @foreach ($cliente->estrellas as $puntos)
                                    <tr>
                                        <td class="align-middle">{{ $cliente->nombre . ' ' . $cliente->apellido }}</td>
                                        <td class="align-middle">{{ $puntos->descripcion }}</td>
                                        <td class="align-middle">
                                            <i class="fas fa-star text-warning"></i>
                                            {{ number_format($puntos->cantidad, 0, ',', '.') }}
                                        </td>
                                        <td class="align-middle">
                                            {{ date_format(date_create($puntos->fecha_registro), 'F j, Y') }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN LISTA DE PUNTOS -->


        <!-- LISTA DE FACTURAS -->
        <div id="facturas" style="display: none">

            <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">
                Historial de Facturas: {{ $cliente->nombre . ' ' . $cliente->apellido }}
            </label>

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive shadow p-3 mb-5 rounded">
                        <table class="facturas table table-striped" cellspacing="0" width="100%">
                            <thead style="background-color: #51A2A7; color: #fff">
                                <tr>
                                    <th>Referencia</th>
                                    <th>Distribuidor</th>
                                    <th>Telefóno</th>
                                    <th>Puntos</th>
                                    <th>Valor</th>
                                    <th>Fecha</th>
                                    <th>Foto</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody class="table-bordered text-dark">
                                @foreach ($cliente->facturas as $factura)
                                    <tr>
                                        <td class="align-middle">{{ $factura->referencia }}</td>
                                        <td class="align-middle">{{ $factura->distribuidor }}</td>
                                        <td class="align-middle">{{ $factura->telefono }}</td>
                                        <td class="align-middle">
                                            <i class="fas fa-star text-warning"></i>
                                            {{ number_format($factura->puntos, 0, ',', '.') }}
                                        </td>
                                        <td class="align-middle" id="monto{{ $factura->id_factura }}" name="{{ $factura->monto_total }}">
                                            ${{ number_format($factura->monto_total, 3, ',', '.') }}
                                        </td>
                                        <td class="align-middle">
                                            {{ date_format(date_create($factura->fecha_registro), 'F j, Y') }}
                                        </td>
                                        <td class="align-middle text-center">
                                            @if ($factura->foto)
                                                <a id="{{ route('factura.archive', ['filename' => basename($factura->foto)]) }}" class="btn btn-sm btn-danger mx-auto text-white w_factura">
                                                    @switch(pathinfo($factura->foto)['extension'])
                                                        @case('jpg')
                                                        @case('jpeg')

                                                        @case('png')
                                                            <i class="fas fa-image"></i>
                                                        @break

                                                        @case('pdf')
                                                            <i class="fa fa-file-pdf-o"></i>
                                                        @break
                                                    @endswitch
                                                </a>
                                            @else
                                                <span>No posee...</span>
                                            @endif
                                        </td>
                                        <td class="align-middle">{{ $factura->estado }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- LISTA DE PREMIOS -->
        <div id="premios" style="display: none">
            <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">
                Historial de Premios Canjeados: {{ $cliente->nombre . ' ' . $cliente->apellido }}
            </label>

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="example" class="premios table table-striped " cellspacing="0" width="100%">
                            <thead style="background-color: #51A2A7; color: #fff">
                                <tr>
                                    <th>Premio</th>
                                    <th>Cantidad</th>
                                    <th>Estrellas</th>
                                    <th>Fecha Redención</th>
                                    <th>Referencia</th>
                                    <th>Estado</th>
                                    <th>Datos de Entrega</th>
                                </tr>
                            </thead>
                            <tbody class="table-bordered text-dark">
                                @foreach ($cliente->canjeados as $premio)
                                    <tr>
                                        <td class="align-middle">{{ $premio->nombre }}</td>
                                        <td class="align-middle">{{ $premio->cantidad }}</td>
                                        <td class="align-middle">
                                            <i class="fas fa-star text-warning"></i>
                                            {{ number_format($premio->total_estrellas, 0, ',', '.') }}
                                        </td>
                                        <td class="align-middle">
                                            {{ date_format(date_create($premio->fecha_redencion), 'F j, Y') }}</td>
                                        <td class="align-middle" id="ref{{ $premio->id_premio_canjeado }}">
                                            {{ $premio->referencia }}</td>
                                        <td class="align-middle" id="est{{ $premio->id_premio_canjeado }}">
                                            {{ $premio->estado }}</td>
                                        <td class="align-middle">
                                            <a id="{{ route('canjeado.datos', $premio->id_premio_canjeado) }}"
                                                class="btn btn-sm btn-danger mx-auto w_entrega text-white">
                                                Ver Datos <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN LISTA DE PREMIOS -->

        <!-- LISTA DE HISTORIAL -->
        <div id="cambios" style="display: none">
            <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">
                Historial de Cambios: {{ $cliente->nombre . ' ' . $cliente->apellido }}
            </label>

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="example" class="cambios table table-striped " cellspacing="0" width="100%">
                            <thead style="background-color: #51A2A7; color: #fff">
                                <tr>
                                    <th>Usuario</th>
                                    <th>Acción generada</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody class="table-bordered text-dark">
                                @foreach ($cliente->acciones as $historial)
                                    <tr>
                                        <td class="align-middle">{{ $cliente->nombre . ' ' . $cliente->apellido }}
                                        </td>
                                        <td class="align-middle">{{ $historial->accion }}</td>
                                        <td class="align-middle">
                                            {{ date_format(date_create($historial->created_at->modify('-5 hours')), 'F j, Y H:i:s') }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN LISTA DE HISTORIAL -->

        <!-- LISTA DE TRIVIAS -->
        <div id="trivias" style="display: none">
            <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">
                Historial de Trivias / Encuestas: {{ $cliente->nombre . ' ' . $cliente->apellido }}
            </label>

            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="example" class="trivias table table-striped " cellspacing="0" width="100%">
                            <thead style="background-color: #51A2A7; color: #fff">
                                <tr>
                                    <th>Nombre</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Puntos Ganados</th>
                                    <th>Respuestas</th>
                                </tr>
                            </thead>
                            <tbody class="table-bordered text-dark">
                                @foreach ($cliente->trivias as $trivial)
                                    @if ($trivial->descripcion == 'skip')
                                    @break
                                @endif
                                <tr class="table-light" data-toggle="collapse" data-target=".row{{ $trivial->id_trivia_cliente }}">
                                    <td class="align-middle">{{ $trivial->nombre }}</td>
                                    <td class="align-middle">{{ $trivial->trivia ? $trivial->trivia->tipo : 'Trivia' }}</td>
                                    <td class="align-middle">{{ $trivial->descripcion }}</td>
                                    <td class="align-middle">
                                        <i class="fas fa-star text-warning"></i>
                                        {{ number_format($trivial->numero_estrellas, 0, ',', '.') }}
                                    </td>
                                    <td class="align-middle accordion-row">
                                        <a class="btn btn-sm btn-danger mx-auto text-white">
                                            Ver Respuestas <i class="fas fa-clipboard"></i>
                                        </a>
                                    </td>
                                </tr>
                                <!-- RESPUESTAS + PREGUNTAS -->
                                @forelse($trivial->respuestas as $respuesta)
                                    <tr class="accordion">
                                        <td colspan="2" class="align-middle hiddenRow">
                                            <div class="collapse row{{ $trivial->id_trivia_cliente }}">
                                                Pregunta: {{ $respuesta->pregunta }}
                                            </div>
                                        </td>
                                        <td colspan="2" class="align-middle hiddenRow">
                                            <div class="collapse row{{ $trivial->id_trivia_cliente }}">
                                                Respuesta: {{ $respuesta->informacion }}
                                            </div>
                                        </td>
                                        <td class="align-middle hiddenRow">
                                            <div class="collapse row{{ $trivial->id_trivia_cliente }}">
                                                Fue: {{ $respuesta->correcta ? 'Correcta' : 'Incorrecta' }}
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="accordion">
                                        <td colspan="5" class="align-middle hiddenRow">
                                            <div class="collapse row{{ $trivial->id_trivia_cliente }}">
                                                No posee respuestas
                                            </div>
                                        </td>
                                    </tr>
                                @endforelse
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN LISTA DE TRIVIAS -->

</div>




<script>
    $(document).ready(function() {
        $('.puntos').DataTable({
            dom: 'Bfrtip',
            "bSort": false,
            pageLength: 5,
            buttons: [{
                    extend: 'excelHtml5',
                    text: '<i class="fas fa-file-csv" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a Excel',
                    className: 'boton'
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fas fa-file-pdf" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a PDF',
                    className: "boton"
                },
            ],

            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
        $('.facturas').DataTable({
            dom: 'Bfrtip',
            "bSort": false,
            pageLength: 20,
            buttons: [{
                    extend: 'excelHtml5',
                    text: '<i class="fas fa-file-csv" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a Excel',
                    className: 'boton'
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fas fa-file-pdf" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a PDF',
                    className: "boton"
                },
            ],

            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
        $('.premios').DataTable({
            dom: 'Bfrtip',
            "bSort": false,
            pageLength: 5,
            buttons: [{
                    extend: 'excelHtml5',
                    text: '<i class="fas fa-file-csv" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a Excel',
                    className: 'boton'
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fas fa-file-pdf" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a PDF',
                    className: "boton"
                },
            ],

            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
        $('.cambios').DataTable({
            dom: 'Bfrtip',
            "bSort": false,
            pageLength: 5,
            buttons: [{
                    extend: 'excelHtml5',
                    text: '<i class="fas fa-file-csv" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a Excel',
                    className: 'boton'
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fas fa-file-pdf" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a PDF',
                    className: "boton"
                },
            ],

            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
        $('.trivias').DataTable({
            dom: 'Bfrtip',
            "bSort": false,
            pageLength: 5,
            buttons: [{
                    extend: 'excelHtml5',
                    text: '<i class="fas fa-file-csv" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a Excel',
                    className: 'boton'
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fas fa-file-pdf" style="font-size: 25px; color: #51A2A7"></i> ',
                    titleAttr: 'Exportar a PDF',
                    className: "boton"
                },
            ],

            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
    });
</script>
<style>
    .boton {
        background-color: Transparent !important;
        background-repeat: no-repeat !important;
        background: Transparent no-repeat !important;
        border: none !important;
        cursor: pointer !important;
        overflow: hidden !important;
        outline: none !important;
        padding: 0 !important;

    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
        background: #51A2A7 !important;
        color: white !important;
        /*change the hover text color*/
        border-radius: 20px;
        border-color: #51A2A7 !important;
    }

    table>tbody>tr>th {
        color: #51A2A7;
    }

    table>tbody>tr {
        background: #FFFFFF;
        box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.05);
        border-radius: 4px;
    }

    table>thead>tr>th {
        color: #51A2A7;
        font-weight: bold !important;
        text-align: center;
    }

    table>thead>tr {
        background: #FFFFFF;
        box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.05);
        border-radius: 4px;
    }


    table>tbody>tr>td>a>i {
        color: #51A2A7;
        font-size: 18px !important;
    }

    table>tbody>tr>td {
        text-align: center;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current {
        background: #51A2A7;
        color: white !important;
        border-radius: 20px;
        pointer-events: none !important;

    }
</style>

<script>
    function f1() {
        $('#puntos').css({
            'display': 'block',
        });
        $('#facturas').css({
            'display': 'none',
        });
        $('#premios').css({
            'display': 'none',
        });
        $('#cambios').css({
            'display': 'none',
        });
        $('#trivias').css({
            'display': 'none',
        });
    };

    $(document).ready(function() {
        $('#p-ganados').click(function() {
            $('#p-ganados').addClass('bg-dark text-white rounded');
            $('#f-cargadas').removeClass('bg-dark text-white rounded');
            $('#p-canjeados').removeClass('bg-dark text-white rounded');
            $('#h-acciones').removeClass('bg-dark text-white rounded');
            $('#t-contestadas').removeClass('bg-dark text-white rounded');
        })

        $('#f-cargadas').click(function() {
            $('#f-cargadas').addClass('bg-dark text-white rounded');
            $('#p-ganados').removeClass('bg-dark text-white rounded');
            $('#p-canjeados').removeClass('bg-dark text-white rounded');
            $('#h-acciones').removeClass('bg-dark text-white rounded');
            $('#t-contestadas').removeClass('bg-dark text-white rounded');
        })

        $('#p-canjeados').click(function() {
            $('#p-canjeados').addClass('bg-dark text-white rounded');
            $('#p-ganados').removeClass('bg-dark text-white rounded');
            $('#f-cargadas').removeClass('bg-dark text-white rounded');
            $('#h-acciones').removeClass('bg-dark text-white rounded');
            $('#t-contestadas').removeClass('bg-dark text-white rounded');
        })

        $('#h-acciones').click(function() {
            $('#h-acciones').addClass('bg-dark text-white rounded');
            $('#p-ganados').removeClass('bg-dark text-white rounded');
            $('#f-cargadas').removeClass('bg-dark text-white rounded');
            $('#p-canjeados').removeClass('bg-dark text-white rounded');
            $('#t-contestadas').removeClass('bg-dark text-white rounded');
        })

        $('#t-contestadas').click(function() {
            $('#t-contestadas').addClass('bg-dark text-white rounded');
            $('#p-ganados').removeClass('bg-dark text-white rounded');
            $('#f-cargadas').removeClass('bg-dark text-white rounded');
            $('#p-canjeados').removeClass('bg-dark text-white rounded');
            $('#h-acciones').removeClass('bg-dark text-white rounded');
        })
    })

    function f2() {
        $('#puntos').css({
            'display': 'none',
        });
        $('#facturas').css({
            'display': 'block',
        });
        $('#premios').css({
            'display': 'none',
        });
        $('#cambios').css({
            'display': 'none',
        });
        $('#trivias').css({
            'display': 'none',
        });
    };

    function f3() {
        $('#puntos').css({
            'display': 'none',
        });
        $('#facturas').css({
            'display': 'none',
        });
        $('#premios').css({
            'display': 'block',
        });
        $('#cambios').css({
            'display': 'none',
        });
        $('#trivias').css({
            'display': 'none',
        });
    };

    function f4() {
        $('#puntos').css({
            'display': 'none',
        });
        $('#facturas').css({
            'display': 'none',
        });
        $('#premios').css({
            'display': 'none',
        });
        $('#cambios').css({
            'display': 'block',
        });
        $('#trivias').css({
            'display': 'none',
        });
    };

    function f5() {
        $('#puntos').css({
            'display': 'none',
        });
        $('#facturas').css({
            'display': 'none',
        });
        $('#premios').css({
            'display': 'none',
        });
        $('#cambios').css({
            'display': 'none',
        });
        $('#trivias').css({
            'display': 'block',
        });
    };
</script>



<style>
    .accordion-row {
        cursor: pointer;
    }

    .hiddenRow {
        padding: 0 !important;
    }


</style>


<script>
    $(document).ready(function() {
        md.initDashboardPageCharts();

        $(".w_factura").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
        });

        $(".w_entrega").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=600,height=500");
        });


    });
</script>
@endsection
