@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<!-- ESTILOS -->
<style>
    .popover {
        background: #39A5DB;
    }

    .bg_rose{
        background-color: #EDB2AB !important;
        color: white;
    }

    .noborder{
        border: 0;
        outline: none !important;
        background-color: transparent !important;
    }
</style>

<!-- PASO SECCIÓN 2 -->
<div id="container_principal">
    <div class="row">
        <div class="col-md-6 ml-auto">
            <div class="row">
                <hr style="position:absolute; border:2px dotted #999999; width:70%; left: 5%; top: 25%" />
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Agregar<br>usuarios</label>
                    </div>
                </div>
                <div class="col row-reverse ">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <label for="" class="font-weight-bold text-white" style="font-size: 20px">2</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Revision<br>pendiente</label>
                    </div>
                </div>
                <div class="col row-reverse">
                    <div class="bg-white d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px">
                        <label for="" class="font-weight-bold " style="font-size: 20px">3</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Creación y<br>estado</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5">
        @if ($errors->any())
            <div class="alert alert-danger d-block w-100">
                <label class="text-white">Porfavor Corrige los siguientes elementos, toma en cuenta que su indice (dato.indice)
                    representa la fila de la tabla</label>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <ul class="list-unstyled">
            <li>Usted ha exportado ({{ count($import) }}) usuarios: </li>
            <li>
                <ul>
                    <li>(<span id="goodRows">0</span>) tienen todos los campos necesarios para su creación</li>
                    <li class="text-danger font-weight-bold">Se han encontrado (<span id="errorCol">0</span>) errores, por favor arreglelos o eliminelos</li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col table-responsive">
            <table class="w-100" id="example">
                <thead style="background: #FFFFFF; box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.05);">
                    <tr style="color: #F28D8D;">
                        <th scope="col" style="font-weight: bold;">Fila</th>
                        <th scope="col" style="font-weight: bold;">Nombre</th>
                        <th scope="col" style="font-weight: bold;">Apellido</th>
                        <th scope="col" style="font-weight: bold;">Segmento</th>
                        <th scope="col" style="font-weight: bold;">Teléfono</th>
                        <th scope="col" style="font-weight: bold;">Correo electrónico</th>
                        <th scope="col" style="font-weight: bold;">Especialidad</th>
                        <th scope="col" style="font-weight: bold;">Opciones</th>
                    </tr>
                </thead>
                <tbody>

                    <?php $contador = 0; $gRow = 0; $eCol = 0;  ?>
                    @foreach ($import as $usuario)
                        <?php 
                            $contador++;
                            $e_global = false;
                            //TRUE ES CUANDO HAY ERROR

                            //ERRORES DE NOMBRE
                            if(isset($usuario["nombre"])) 
                                $e_nombre = false;
                            else{ 
                                $e_nombre = true; $eCol++;
                            }

                            //ERRORES DE APELLIDO
                            if(isset($usuario["apellido"]))
                                $e_apellido = false;
                            else{ 
                                $e_apellido = true;  $eCol++;
                            }

                            //ERRORES DE ESPECIALIDAD
                            if(isset($usuario["especialidad"]))
                                $e_especialidad = false;
                            else{ 
                                $e_especialidad = true; $eCol++; 
                            }

                            //ERRORES DE SEGMENTO
                            if(isset($usuario["segmento"])){
                                //if( strtolower($usuario["segmento"]) != "invest" && strtolower($usuario["segmento"]) != "focus" && strtolower($usuario["segmento"]) != "otro") {
                                if ( !in_array(strtolower($usuario["segmento"]), array('invest','focus','otro'), true ) ) {
                                    $e_segmento = true; $eCol++; 
                                }else
                                    $e_segmento = false;
                            }
                            else $e_segmento = true;

                            //ERRORES DE TELEFONO
                            if(isset($usuario["telefono"])){
                                if(!is_numeric($usuario["telefono"])){
                                    $e_telefono = true; $eCol++; 
                                }else
                                    $e_telefono = false;
                            }
                            else $e_telefono = true;

                            //ERRORES DE correo
                            if(isset($usuario["correo"])){
                                if(!filter_var($usuario["correo"], FILTER_VALIDATE_EMAIL)){
                                    if (!preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $usuario["correo"])){
                                        $e_correo = true; $eCol++; 
                                    }else
                                        $e_correo = false;
                                }else
                                    $e_correo = false;
                            }
                            else $e_correo = true;

                            //CHEQUEO SI HAY ERROR GLOBAL ENTONCES EN ALGÚN PUNTO
                            if($e_correo || $e_nombre || $e_apellido || $e_segmento || $e_especialidad || $e_telefono)
                                $e_global = true;
                            else{
                                $gRow++;
                            }
                        ?>
                        <tr id="row-{{ $contador }}" class="{{ $e_global ? 'bg_rose' : '' }} table-bordered">
                            <td class="text-center">
                                {{ $contador - 1 }}
                            </td>
                            <td id="nombre-{{ $contador }}" class="{{ $e_nombre ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["nombre"]) ? $usuario["nombre"] : "" }}
                            </td>
                            <td id="apellido-{{ $contador }}" class="{{ $e_apellido ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["apellido"]) ? $usuario["apellido"] : "" }}
                            </td>
                            <td id="segmento-{{ $contador }}" class="{{ $e_segmento ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["segmento"]) ? $usuario["segmento"] : "" }}
                            </td>
                            <td id="telefono-{{ $contador }}" class="{{ $e_telefono ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["telefono"]) ? $usuario["telefono"] : "" }}
                            </td>
                            <td id="correo-{{ $contador }}" class="{{ $e_correo ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["correo"]) ? $usuario["correo"] : "" }}
                            </td>
                            <td id="especialidad-{{ $contador }}" class="{{ $e_especialidad ? 'text-danger font-weight-bold' : '' }}">
                                {{ isset($usuario["especialidad"]) ? $usuario["especialidad"] : "" }}
                            </td>
                            <td class="text-center">
                                @if($e_global)
                                    <a id="eIcon-{{ $contador }}">
                                        <i class="fas fa-exclamation-triangle text-danger"></i>
                                    </a>
                                @endif
                                <a id="{{ $contador }}" class="editRow" data-toggle="modal" data-target=".bd-example-modal-lg-1">
                                    <i class="fas fa-edit mx-3" style="color: #F28D8D"></i>
                                </a>
                                <a id="{{ $contador }}" class="deleteRow">
                                    <i class="fas fa-trash text-secondary"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- FIN SECCIÓN 2 -->

<!-- MODAL DE EDITAR USUARIOS -->
<div class="modal fade bd-example-modal-lg-1" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Editar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="formEdit">
                    <div class="form-row">
                        <div class="col">
                            <label for="">Nombre:</label>
                            <input id="editName" type="text" class="form-control" placeholder="Nombre*" required>
                        </div>
                        <div class="col">
                            <label for="">Apellido:</label>
                            <input id="editApellido" type="text" class="form-control" placeholder="Apellido*" required>
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Segmento:</label>
                            <select id="editSegmento" class="form-control" required>
                                <option value="" selected disabled>Selecciona un Segmento</option>
                                <option value="Invest">Invest</option>
                                <option value="Focus">Focus</option>
                                <option value="OTRO">Otro</option>
                            </select>
                        </div>
                        <div class="col">
                            <label for="">Teléfono:</label>
                            <input id="editTelefono" type="number" min="0" class="form-control  mt-1" placeholder="Telefono*" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <label for="">Correo Electrónico:</label>
                            <input id="editCorreo" type="email" class="form-control" placeholder="Correo electronico*" required>
                        </div>
                        <div class="col">
                            <label for="">Especialidad:</label>
                            <input id="editEspecialidad" type="text" class="form-control" required>
                        </div>
                    </div>
                    <input type="hidden" id="rowID">
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">EDITAR USUARIO</button>
            </div>
            </form>

        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR -->

<!-- PASO SECCIÓN 3 -->
<div id="container_principal_2" style="display: none;">
    <div class="row">
        <div class="col-md-6 ml-auto">
            <div class="row">
                <hr style="position:absolute; border:2px dotted #999999; width:70%; left: 5%; top: 25%" />
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Agregar<br>usuarios</label>
                    </div>
                </div>
                <div class="col row-reverse ">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <i class="fas fa-check text-white"></i>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Revision<br>pendiente</label>
                    </div>
                </div>
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <label for="" class="font-weight-bold text-white" style="font-size: 20px">3</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Creación y<br>estado</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col">
            <table class="table">
                <thead style="background: #FFFFFF; box-shadow: -3px 3px 6px rgba(0, 0, 0, 0.05);">
                    <tr style="color: #F28D8D;">
                        <th scope="col" style="font-weight: bold;">Nombre</th>
                        <th scope="col" style="font-weight: bold;">Apellido</th>
                        <th scope="col" style="font-weight: bold;">Segmento</th>
                        <th scope="col" style="font-weight: bold;">Telefono</th>
                        <th scope="col" style="font-weight: bold;">Correo electronico</th>
                        <th scope="col" style="font-weight: bold;">Especialidad</th>
                    </tr>
                </thead>
                <tbody>
                    <form action="{{ route('cliente.importstore') }}" method="post" id="formInputs">
                        @csrf
                        <?php $contador = 0; ?>
                        @foreach ($import as $usuario)
                            <?php $contador++; ?>
                            <tr id="rowI-{{ $contador }}" class="table-bordered">
                                <td id="nombreI-{{ $contador }}">
                                    <input name="nombre[]" type="text" class="noborder" readonly value='{{ isset($usuario["nombre"]) ? $usuario["nombre"] : "" }}'>
                                </td>
                                <td id="apellidoI-{{ $contador }}">
                                    <input name="apellido[]" type="text" class="noborder" readonly value='{{ isset($usuario["apellido"]) ? $usuario["apellido"] : "" }}'>
                                </td>
                                <td id="segmentoI-{{ $contador }}">
                                    <input name="segmento[]" type="text" class="noborder" readonly value='{{ isset($usuario["segmento"]) ? $usuario["segmento"] : "" }}'>
                                </td>
                                <td id="telefonoI-{{ $contador }}">
                                    <input name="telefono[]" type="text" class="noborder" readonly value='{{ isset($usuario["telefono"]) ? $usuario["telefono"] : "" }}'>
                                </td>
                                <td id="correoI-{{ $contador }}">
                                    <input name="correo[]" type="text" class="noborder" readonly value='{{ isset($usuario["correo"]) ? $usuario["correo"] : "" }}'>
                                </td>
                                <td id="especialidadI-{{ $contador }}">
                                    <input name="especialidad[]" type="text" class="noborder" readonly value='{{ isset($usuario["especialidad"]) ? $usuario["especialidad"] : "" }}'>
                                </td>
                            </tr>
                        @endforeach
                    </form>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- FIN PASO DE SECCIÓN 3 -->

<div class="row mt-5">
    <div class="col-md-12">
        <hr style="height:2px;border-width:0;color:gray;background-color:#F28D8D">
    </div>
</div>

<!-- BOTONES PARA CAMBIOS DE SECCIÓN + ENVIO -->
<div class="row ">
    <div class="col d-flex justify-content-end">
        <button {{ $eCol > 0 ? "disabled" : "" }} class="btn btn-round" id="confirmar" onclick="container_principal();" style="background-color: #51A2A7">
            CONFIRMAR
        </button>
        <a href="javascript:;" onclick="sweetcheck()" id="aceptar" class="btn btn-round" style="background-color: #51A2A7; display: none">
            ACEPTAR Y PUBLICAR
        </a>
    </div>
</div>
<!-- FIN BOTONES -->

<input type="hidden" id="numRows" value="{{ $gRow }}">
<input type="hidden" id="numCols" value="{{ $eCol }}">

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<!-- SCRIPT DE CARGA INICIAL DEL DOM -->
<script>
    $(document).ready(function() {
        //SETTEO LOS VALORES DE BIEN Y ERRORES
        var rows = $('#numRows').val(); $('#goodRows').text(rows);
        var cols = $('#numCols').val(); $('#errorCol').text(cols);
        
        //SECCIÓN DE EDITAR FILAS
            //abrir modal
            $('.editRow').click(function(){
                var id = $(this).attr('id');
                
                //Recogo los Valores de la Tabla
                var nombre = $('#nombre-'+id).text().trim();
                var apellido = $('#apellido-'+id).text().trim();
                var segmento = $('#segmento-'+id).text().trim();
                var telefono = parseInt($('#telefono-'+id).text().trim());
                var correo = $('#correo-'+id).text().trim();
                var especialidad = $('#especialidad-'+id).text().trim();

                //Setteo los valores en el modal
                $('#rowID').val(id);
                $('#editName').val(nombre);
                $('#editApellido').val(apellido);
                $('#editSegmento').val(segmento);
                $('#editTelefono').val(telefono);
                $('#editCorreo').val(correo);
                $('#editEspecialidad').val(especialidad);
            });

            //Submit de Form
            $( "#formEdit" ).on('submit', function(e){
                e.preventDefault();

                //Cogemos los datos
                var id = $('#rowID').val();
                var nombre = $('#editName').val();
                var apellido = $('#editApellido').val();
                var segmento = $('#editSegmento').val();
                var telefono = $('#editTelefono').val();
                var correo = $('#editCorreo').val();
                var especialidad = $('#editEspecialidad').val();

                //Corregimos los datos el td y el tr
                $('#nombre-'+id).text(nombre); $('#nombreI-'+id+' input').val(nombre);
                $('#apellido-'+id).text(apellido); $('#apellidoI-'+id+' input').val(apellido);
                $('#segmento-'+id).text(segmento); $('#segmentoI-'+id+' input').val(segmento);
                $('#telefono-'+id).text(telefono); $('#telefonoI-'+id+' input').val(telefono);
                $('#correo-'+id).text(correo); $('#correoI-'+id+' input').val(correo);
                $('#especialidad-'+id).text(especialidad); $('#especialidadI-'+id+' input').val(especialidad);

                $('#row-'+id).removeClass('bg_rose');
                $('#eIcon-'+id).remove();
                //$('#row-'+id+' td').removeClass('text-danger font-weight-bold');
                var error = 0;
                $('#row-'+id+' td').each(function(indice,elemento) {
                    $(elemento).hasClass('text-danger') ? error++ : "";
                    $(elemento).removeClass('text-danger font-weight-bold');
                });
                //Cerramos el Modal
                $('#editModal').modal('hide');

                //Arreglamos el número de errores por resolver
                rows++; cols -= error;
                $('#goodRows').text(rows<0 ? 0 : rows);
                $('#errorCol').text(cols<0 ? 0 : cols);

                if(cols < 1){
                    $('#confirmar').removeAttr('disabled');
                }
            });

        //SECCIÓN DE ELIMINAR FILA
            //Elimino fila con el click de la papelera
            $('.deleteRow').click(function(){
                var id = $(this).attr('id');

                Swal.fire({
                    icon: 'warning',
                    title: '¿Esta seguro de borrar este usuario?',
                    //type: 'warning',
                    confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                        var error = 0;
                        $('#row-'+id+' td').each(function(indice,elemento) {
                            $(elemento).hasClass('text-danger') ? error++ : "";
                            $(elemento).removeClass('text-danger font-weight-bold');
                        });

                        Swal.fire({
                            title: 'Usuario a Importar Eliminado!',
                            icon: 'success',
                        });

                        cols -= error;
                        $('#errorCol').text(cols<0 ? 0 : cols);

                        if(cols < 1){
                            $('#confirmar').removeAttr('disabled');
                        }

                        $('#row-'+id).remove();
                        $('#rowI-'+id).remove();
                    }
                });
            });
    });
</script>

<!-- FUNCION UE PASA DE ESTADO 2 A 3 -->
<script>
    function container_principal() {
        $("#container_principal").css({
            'display': 'none'
        })
        $("#container_principal_2").css({
            'display': 'block'
        })
        $("#confirmar").css({
            'display': 'none'
        })
        $("#aceptar").css({
            'display': 'block'
        })
    }
</script>

<!-- ALERTAS SWEET DE ELIMINAR Y DE ACEPTAR CARGAR TODO -->
<script>
    function sweetcheck() {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Los usuarios se estan subiendo',
            showConfirmButton: false,
        });
        $('#formInputs').submit();
    }
</script>
@endsection