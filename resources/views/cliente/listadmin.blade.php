@extends('layouts.admin.app')
@push('scripts')
    <script src="{{ asset('js/admin/admin.js') }}" defer></script>
@endpush
@section('content')
    <link rel="stylesheet" href="{{ asset('css/permisoDeModulos.css') }}">
    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Usuario Registrado Exitosamente',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    @if (Session::has('permission'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'No tienes los permisos suficientes',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif

    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
            <label for="" class="lead"
                style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #F28D8D">Todos los
                <strong>Administradores</strong></label>
        </div>
        <div class="col" style="display: flex;justify-content: flex-end !important">
            <button {{-- data-toggle="modal" data-target=".bd-example-modal-lg" --}} id="crear_usuario" class="btn btn-round"
                style="background-color: #F28D8D">
                <i class="material-icons">add</i><strong>AGREGAR ADMINISTRADOR</strong>
            </button>
            <a class="btn font-weight-bold  btn-round" href="{{ url('notificaciones/all') }}"><span class="iconify mr-2"
                    data-icon="akar-icons:arrow-back-thick"></span><strong>Volver</strong></a>
        </div>
    </div>

    <div class="row">
        <div class="col-11 m-auto card p-5" style="border-radius: 30px">
            <div class="row p-5">
                <div class="col-sm-12">
                    <h2 class="text-center font-weight-bold"> Administradores</h2>
                    <div class="d-flex justify-content-center">
                        <span style="font-size:5em" class="iconify" data-icon="carbon:user-multiple"></span>
                    </div>
                </div>
            </div>
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped table-hover text-center table-sm mt-4 align-items-center mb-0">
                    <thead>
                        <tr>
                            <!-- <th>Avatar</th> -->
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Usuario</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Estado</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Rol</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Opciones</th>
                        </tr>
                    </thead>
                    <tbody id="">
                        @foreach ($admin as $a)
                            @if ($a->id_useradmin != auth()->user()->id_useradmin)
                                <tr>
                                    <td>
                                        {{ $a->nombre }} {{ $a->apellido }} <br>
                                        {{ $a->email }}
                                    </td>
                                    <td>
                                        @if ($a->estado == 'activo')
                                        <span class="badge badge-success">
                                            {{ $a->estado }}
                                        </span>
                                        @else
                                        <span class="badge badge-danger">
                                            {{ $a->estado }}
                                        </span>
                                        @endif
                                    </td>
                                    <td>{{ $a->roles->rol }}</td>
                                    @if ($a->FK_id_loginrol != 1)
                                        <td class="text-center">
                                            <a class="btn text-white btn-sm d-inline-block"
                                                href="{{ route('admin.edit', $a->id_useradmin) }}"
                                                style="color: #F28D8D;background-color:#64C2C8"><span class="iconify" data-width="15" data-icon="akar-icons:edit"></span></a>
                                                <form class="d-inline-block" action="{{ route('admin.destroy',$a->id_useradmin) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" onclick="return confirm('¿Desea eliminar este usuario?')" title="No se puede eliminar"
                                                    class="btn btn-danger text-white btn-sm" style="color: #F28D8D;"><span class="iconify" data-icon="fa6-solid:trash-can"></span></button>
                                                </form>
                                        </td>
                                    @else
                                        <td class="text-center">
                                            <button title="No se puede editar" disabled
                                                class="btn text-white btn-sm d-inline-block"><span class="iconify" data-width="15" data-icon="ci:close-small"></span></button>
                                                <button title="No se puede eliminar" disabled
                                                class="btn text-white btn-sm d-inline-block" style="color: #F28D8D;"><span class="iconify" data-icon="fa6-solid:trash-can"></span></button>
                                            </td>
                                    @endif
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- MODAL ADMIN-->
    <div class="modal fade bd-example-modal-lg" id="crearusuariomodal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                        <strong id="title"></strong>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="was-validated" autocomplete="off">
                    @csrf
                    <div class="modal-body pl-5 pb-5 pr-5">
                        <p class="text-danger font-weight-bold mt-2 mb-5">{{ $errors->first('permisos') }}</p>
                        <div class="form-row mt-2">
                            <div class="col">
                                <label for="label-form"><strong>Nombre</strong></label>
                                <input required name="nombre" id="nombre" type="text" placeholder="Digite el Nombre"
                                    class="form-control validate @error('nombre') is-invalid @enderror">
                                <span class="text-danger" id="invalid-nombre" for=""></span>
                            </div>
                            <div class="col">
                                <label for="label-form"><strong>Apellido</strong></label>
                                <input required name="apellido" id="apellido" type="text"
                                    placeholder="Digite el Apellido" value="{{ old('apellido') }}"
                                    class="form-control validate @error('apellido') is-invalid @enderror">
                                <span class="text-danger" id="invalid-apellido" for=""></span>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="label-form"><strong>Correo</strong></label>
                                <input required name="email" id="email" type="text"
                                    placeholder="Digite el Correo electrónico"
                                    class="form-control validate @error('email') is-invalid @enderror"
                                    value="{{ old('email') }}">
                                <span class="text-danger" id="invalid-email" for=""></span>
                            </div>
                            <div class="col password">
                                <label for="label-form"><strong>Contraseña</strong></label>
                                <input autocomplete="off" name="password" id="password" type="password"
                                    placeholder="Contraseña (Opcional)"
                                    class="form-control validate @error('password') is-invalid @enderror"
                                    value="{{ old('password') }}">
                                <span class="text-danger" id="invalid-password" for=""></span>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="label-form"><strong>Estado</strong></label>
                                <select required name="estado" id="estado"
                                    class="form-control validate @error('estado') is-invalid @enderror">
                                    <option value="" selected disabled>Seleccione su estado</option>
                                    <option value="activo" {{ old('estado') == 'activo' ? 'selected' : '' }}>Activo
                                    </option>
                                    <option value="inactivo" {{ old('estado') == 'inactivo' ? 'selected' : '' }}>Inactivo
                                    </option>
                                </select>
                                <span class="text-danger" id="invalid-estado" for=""></span>
                            </div>
                            <div class="col">
                                <label for="label-form"><strong>Rol</strong></label>
                                <select required name="FK_id_loginrol" id="user_rol"
                                    class="form-control validate @error('FK_id_loginrol') is-invalid @enderror">
                                    <option value="" selected disabled>Seleccione una opción...</option>
                                    @foreach ($rol as $i => $a)
                                        <option value="{{ $a->id_loginrol }}">{{ $a->rol }}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger" id="invalid-rol" for=""></span>
                            </div>
                        </div>
                        <div id="listado" class="row col-md-12 my-3">
                            <div class="col-md-6 d-none" id="base_permisos">
                                <label for="name_user"><strong>Permisos de Base de Datos:</strong></label><br><br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_store" name="p_store">
                                    <div></div>
                                </div>
                                <label for="p_store" class="d-inline-block mb-2">Creación de datos</label>
                                <br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_update"
                                        name="p_update">
                                    <div></div>
                                </div>
                                <label for="p_update" class="d-inline-block mb-2">Actualización de datos</label>
                                <br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_destroy"
                                        name="p_destroy">
                                    <div></div>
                                </div>
                                <label for="p_destroy" class="d-inline-block mb-2">Eliminación de datos</label>
                            </div>

                            <div class="col-md-6 d-none" id="modulos_permisos">
                                <label for="name_user"><strong>Permisos de Módulos:</strong></label><br><br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_productos"
                                        name="p_productos">
                                    <div></div>
                                </div>
                                <label for="p_productos" class="d-inline-block mb-2">Productos</label>
                                <br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_premios"
                                        name="p_premios">
                                    <div></div>
                                </div>
                                <label for="p_premios" class="d-inline-block mb-2">Premios</label>
                                <br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_facturas"
                                        name="p_facturas">
                                    <div></div>
                                </div>
                                <label for="p_facturas" class="d-inline-block mb-2">Facturas</label>
                                <br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_noticias"
                                        name="p_noticias">
                                    <div></div>
                                </div>
                                <label for="p_noticias" class="d-inline-block mb-2">Noticias y Banners</label>
                                <br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_capacitaciones"
                                        name="p_capacitaciones">
                                    <div></div>
                                </div>
                                <label for="p_capacitaciones" class="d-inline-block mb-2">Capacitaciones</label>
                                <br>
                                <div class="switch d-inline-block">
                                    <input class="" type="checkbox" value="1" id="p_trivia"
                                        name="p_trivia">
                                    <div></div>
                                </div>
                                <label for="p_trivia" class="d-inline-block mb-2">Trivias y Encuestas</label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id" value="{{ $a->id_useradmin }}">
                    <div class="modal-footer">
                        <button class="btn mx-auto col-md-6"
                            style="background-color: #F28D8D; border-radius: 40px;" id="addNewDataForm">Agregar</button>
                        <button class="btn mx-auto col-md-6"
                            style="background-color: #F28D8D; border-radius: 40px;" id="editDataForm">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

    @if ($errors->any())
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: '¡Completa todos los campos!',
            })
        </script>
    @endif
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $("#user_rol").change(function() {
            var selectValue = $(this).val();
            //1 Super Admin
            //2 Call Center
            //3 Básico

            //base_permisos
            //modulos_permisos

            if (selectValue == 1) {
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").addClass("d-none");
            } else if (selectValue == 2) {
                $("#base_permisos").removeClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            } else {
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
        });

        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('.bd-example-modal-lg').modal('show');
        }

        $('#crear_usuario').click(function() {
            $("#password").val('');
            $("#email").val('');
        })
    });
</script>

<script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#tbody tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>

<template id="administradores">
    <tr>
        <td>
            <div class="d-flex px-2 py-1">
                <div class="d-flex flex-column justify-content-center">
                    <h6 class="mb-0 text-xs"></h6>
                    <p class="text-xs text-secondary mb-0"></p>
                </div>
            </div>
        </td>
        <td class="align-middle text-center text-sm">
            <span class="badge badge-sm badge-success"></span>
        </td>
        <td class="align-middle text-center text-sm">
            <span class="text-secondary text-xs font-weight-normal"></span>
        </td>
        <td class="align-middle">
            <button data-id="" class="btn editRow btn-info rounded"><span class="iconify" data-icon="akar-icons:edit"></span></button>
            <button data-id="" class="btn deleteRow btn-danger rounded"><span class="iconify" data-icon="akar-icons:trash-can"></span></button>
        </td>
    </tr>
</template>
