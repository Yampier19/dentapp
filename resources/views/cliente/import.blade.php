@extends('layouts.admin.app')

@section('content')

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row" id="pasos">
        <div class="col-md-6 ml-auto">
            <div class="row">
                <hr style="position:absolute; border:2px dotted #999999; width:70%; left: 5%; top: 25%" />
                <div class="col row-reverse">
                    <div class="d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; background-color: #51A2A7">
                        <label for="" class="font-weight-bold text-white" style="font-size: 20px">1</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px; color: #51A2A7">Agregar<br>usuarios</label>
                    </div>
                </div>
                <div class="col row-reverse ">
                    <div class="bg-white d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px; ">
                        <label for="" class="font-weight-bold" style="font-size: 20px">2</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Revision<br>pendiente</label>
                    </div>
                </div>
                <div class="col row-reverse">
                    <div class="bg-white d-flex justify-content-center align-items-center" style="width: 60px; height: 60px; border-radius: 60px">
                        <label for="" class="font-weight-bold " style="font-size: 20px">3</label>
                    </div>
                    <div>
                        <label for="" class="text-center font-weight-bold" style="font-size: 14px;">Creación y<br>estado</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="container">
            <button style="display:none"  class="btn btn-round text-white colr" onclick="Agregar();" id="Agregar" style="background-color:#51A2A7; color: black">
                <i class="material-icons">add</i>Agregar Usuarios Manual
            </button>
            <button style="background-color: #F28D8D" class="btn btn-round" id="importar" style="background-color: white; color: black;" onclick="Importar()">
                <i  class="fas fa-file-csv mr-2"></i><span  class="iconify mr-2" data-width="10" data-icon="ri:file-excel-2-fill"></span> Importar usuarios
            </button>
            <a class="btn font-weight-bold  btn-round" href="{{ url('cliente/inicio') }}"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- CONTENEDOR PARA AGREGAR USUARIOS -->

    <div class="container card p-4 mx-auto my-5 col-md-12" style="border-radius: 30px" id="contenedor">
        <div class="row mx-auto mb-3 col-10 mt-5">
            <label for="" class="font-weight-bold lead" style="color: #64C1C6">
                Datos Usuario
            </label>
        </div>
        <div class="container p-2">
            <div class="row">
                <div class="col-md-10 m-auto">

                    <!-- FORMULARIO BASE INCIAL -->
                    <form action="{{ route('cliente.importform') }}" id="forminicial" method="post">
                        @csrf
                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="nombre[]" id="" class="form-control" placeholder="Nombre*" required>
                            </div>
                            <div class="col">
                                <input type="text" name="apellido[]" id="" class="form-control" placeholder="Apellido*" required>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <select name="segmento[]" id="" class="form-control" required>
                                    <option value="" selected disabled>Segmento</option>
                                    <option value="Focus">Focus</option>
                                    <option value="Invest">Invest</option>
                                </select>
                            </div>
                            <div class="col">
                                <input type="number" name="telefono[]" class="form-control mt-1" id="" placeholder="Telefono*" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input type="email" name="correo[]" class="form-control mt-1" placeholder="Correo electronico*" id="" required>
                            </div>
                            <div class="col">
                                <select name="especialidad[]" id="" class="form-control" required>
                                    <option value="" selected disabled>Especialidad</option>
                                    <option value="Especialista - Odontología">Especialista - Odontología</option>
                                    <option value="Especialista - Endodoncia">Especialista - Endodoncia</option>
                                    <option value="Especialista - Maxilofacial">Especialista - Maxilofacial</option>
                                    <option value="Especialista - Ortodoncia">Especialista - Ortodoncia</option>
                                    <option value="Especialista - Rehabilitación Oral">Especialista - Rehabilitación Oral</option>
                                    <option value="Especialista - Operatoria Dental">Especialista - Operatoria Dental</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" id="campos" style="display: none">

                            </div>
                        </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-10 m-auto ">
                    <a href="javascript:;" onclick="AgregarCampos()" class="text-danger font-weight-bolt"><i class="fa fa-plus mr-2"></i><b>AGREGAR
                            OTRO</b></a>
                </div>
            </div>
            <div class="modal-footer mt-5">
                <button type="submit" class="btn btn-round" onclick="Agregar();" style="background-color: #51A2A7">
                    <i class="material-icons">add</i>CREAR USUARIOS
                </button>
            </div>
            </form>
        </div>
    </div>

    <!-- CONTENEDOR IMPORTAR USUARIOS -->

    <div id="cont2" style="display: none">
        <div style="border-radius: 30px" class="card p-5 container mx-auto col-md-12">
            <div class="row d-flex justify-content-center" id="contenedor_b_p">
                <div>
                    <div class="pie" style="display: none">
                        <div class="pie-holder">
                            <svg viewBox="-8 -5 115 110">
                                <path d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#C1C1C1" stroke-width="5" fill-opacity="0" />
                                <path id="progress_animate1" d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94" stroke="#F28D8D" stroke-width="5" fill-opacity="0" style="stroke-dasharray: 0, 295.351; stroke-dashoffset: 0;" />
                            </svg>

                            <span><strong id="score-1">0</strong>%</span>

                        </div>
                    </div>

                    <form id="formImport" action="{{ route('cliente.importcsv') }}" method="post" enctype="multipart/form-data">
                        <div class="d-flex justify-content-center mb-3">
                            <span style="color: #51A2A7" class="iconify" data-width="100" data-icon="ri:file-excel-2-fill"></span>
                        </div>
                        @csrf
                        <span class="btn btn-round btn-file" style="background: #F28D8D;" id="botoncargue">
                            <span class="fileinput-new">SELECCIONE EL ARCHIVO</span>
                            <input type="file" name="file" accept=".xls,.csv,.xlsx" id="progress" required />
                        </span>
                        @error('file')
                            <span class="invalid-feedback text-center" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </form>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <label for="" id="text3" style="display: none;">Su archivo se esta importando, espere unos minutos</label>
            </div>
            <div class="row d-flex justify-content-center mt-3">
                <label for="" id="text1">La primera línea del archivo xlsx o csv se tomará como cabecera</label>
            </div>
            <div class="row d-flex justify-content-center">
                <label for="" id="text2">El documento debe ser .xlsx o cvs</label>
            </div>
            <div class="row d-flex justify-content-center">
                <label for="" id="text4">Descarga la <a href="{{ url('csv/3.xlsx') }}" download="plantilla_dentapp.xlsx" download>Plantilla de ejemplo aqui</a></label>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

    <script>
        function Importar() {
            $("#importar").css({
                'background-color': '#F28D8D',
                'color': 'white'

            })
            $("#Agregar").css({
                'background-color': 'white',
                'color': 'black'
            })
            $("#contenedor").css({
                'display': 'none'
            })
        }

        var nextinput = 0;

        function AgregarCampos() {
            nextinput++;
            campo = '<div class="row mx-auto" style="margin-top: 3%">' +
                '<label for="" class="font-weight-bold lead" style="color: #64C1C6">' +
                'Datos Usuario' +
                '</label>' +
                '</div>' +
                //'<form action="" id="forminicial">' +
                '<div class="form-row">' +
                '<div class="col">' +
                '<input type="text" name="nombre[]" id="" class="form-control" placeholder="Nombre*" required>' +
                '</div>' +
                '<div class="col">' +
                '<input type="text" name="apellido[]" id="" class="form-control" placeholder="Apellido*" required>' +
                '</div>' +
                '</div>' +
                '<div class="form-row my-3">' +
                '<div class="col">' +
                '<select name="segmento[]" id="" class="form-control" required>' +
                '<option value="" selected disabled>Segmento</option>' +
                '<option value="Focus">Focus</option>' +
                '<option value="Invest">Invest</option>' +
                '</select>' +
                '</div>' +
                '<div class="col">' +
                '<input type="number" name="telefono[]" class="form-control mt-1" id="" placeholder="Telefono*" required>' +
                '</div>' +
                '</div>' +
                '<div class="form-row">' +
                '<div class="col">' +
                '<input type="email" name="correo[]" class="form-control mt-1" placeholder="Correo electronico*" id="" required>' +
                '</div>' +
                '<div class="col">' +
                '<select name="especialidad[]" id="" class="form-control" required>' +
                '<option value="" selected disabled>Especialidad</option>' +
                '<option value="Especialista - Odontología">Especialista - Odontología</option>' +
                '<option value="Especialista - Endodoncia">Especialista - Endodoncia</option>' +
                '<option value="Especialista - Maxilofacial">Especialista - Maxilofacial</option>' +
                '<option value="Especialista - Ortodoncia">Especialista - Ortodoncia</option>' +
                '<option value="Especialista - Rehabilitación Oral">Especialista - Rehabilitación Oral</option>' +
                '<option value="Especialista - Operatoria Dental">Especialista - Operatoria Dental</option>' +
                '</select>' +
                '</div>' +
                '</div>';
            $("#campos").append(campo);

            $("#campos").css({
                'display': 'block'
            })

        }
    </script>

    <!-- ESTA ES LA LIBRERIA PARA EL PROGRESS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#Agregar').click(function() {
                $('#contenedor').show();
                $('#cont2').hide();
            })

            $('#importar').click(function() {
                $('#contenedor').hide();
                $('#cont2').show();
            })
        })
    </script>

    <script>
        /* AQUI CAPTURAMOS CUANDO ESCOJA UNA IMAGEN PARA LUEGO LANZAR EL PROGRESS */

        $(document).ready(function() {

            if ($('#showerror').val() == 1) {
                $('.invalid-feedback').css('display', 'block');
            }

            $('#progress').change(function() {
                /* SI NO ESCOGE UN ARCHIVO DA UN ERROR CON EL SWEET ALERT */
                var fileExtension = ['csv', 'xls', 'xlsx'];
                if (!$('#progress').val() || $.inArray($('#progress').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'No has escogido ningun archivo o no cumple con el formato correcto (xls, xlsx, csv)!'
                    });
                    return;
                }

                /* SI ESCOGE UN ARCHIVO SIGO EL PROCESO */
                $(".pie").css({
                    'display': 'block'
                });
                $(".excel").css({
                    'display': 'block'
                });
                $("#botoncargue").css({
                    'display': 'none'
                });
                $("#formSubmit").css({
                    'display': 'none'
                });
                $("#text1").css({
                    'display': 'none'
                });
                $("#text2").css({
                    'display': 'none'
                });
                $("#text4").css({
                    'display': 'none'
                });
                $("#text3").css({
                    'display': 'block'
                });
                $("#contenedor_b_p").css({
                    'margin-top': '0'
                });

                /* ESTE ES EL FUNCIONAMIENTO DEL PROGRESS */
                var game = {
                        score: 0
                    },
                    scoreDisplay = document.getElementById("score-1");

                function updateHandler() {
                    scoreDisplay.innerHTML = game.score;
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Archivo adjuntado',
                        showConfirmButton: false,
                        timer: 1800
                    }).then(function() {
                        //window.location.href = "{{ route('cliente.tabla_import') }}";
                        $("#formImport").submit();
                    });
                }

                var c1Section3 = new TimelineLite();

                c1Section3
                    .to("#progress_animate1", 3, {
                        "stroke-dasharray": "300 200",
                        ease: Expo.easeNone,
                    }, 0)
                    .to(game, 3, {
                        score: "+100",
                        roundProps: "score",
                        //LLEGO A 100 EL LOADER
                        onUpdate: updateHandler,
                        ease: Expo.easeNone,
                    }, 0);
            });
        });

        $(document).ready(function() {
            $('#importar').click(function() {
                $('#importar').hide();
                $('#Agregar').show();

            })

            $('#Agregar').click(function() {
                $('#importar').show();
                $('#Agregar').hide();

            })
        })
    </script>
    <style>
        @media (min-width: 500px) and (max-width: 890px) {
            #pasos {
                margin-bottom: 10%;
            }

        }

        .pie {
            width: 100%;

        }

        .pie .pie-holder {
            position: relative;
            width: 350px;
            height: 350px;
            margin: 0 auto 0 auto;
            overflow: hidden;
            border-radius: 260px;
        }

        .pie .pie-holder span {
            position: absolute;
            top: 50%;
            transform: translateY(-100%);
            left: 0;
            width: 100%;
            text-align: center;
            font-weight: bold;
            color: #F28D8D;
            font-size: 2rem;

        }
        .colr{
            background-color: #51A2A7 !important;
            color: white !important;
        }
    </style>
@endsection
