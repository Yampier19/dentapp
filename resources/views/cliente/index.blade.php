@extends('layouts.admin.app')
@push('scripts')
    <script src="{{ asset('js/doctor/doctor.js') }}"></script>
    <script src="{{ asset('js/doctor/confirmDelete.js') }}"></script>
@endpush
@section('content')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @if (Session::has('create'))
        <script>
            Swal.fire(
                '¡Buen trabajo!',
                '{{ Session::get('create') }}',
                'success'
            )
        </script>
    @endif

    @if (Session::has('delete'))
        <script>
            Swal.fire(
                '¡Buen trabajo!',
                '{{ Session::get('delete') }}',
                'success'
            )
        </script>
    @endif

    @if (Session::has('destroy'))
        <script>
            Swal.fire(
                '¡Buen trabajo!',
                '{{ Session::get('edit') }}',
                'success'
            )
        </script>
    @endif

    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
            <label for="" class="lead"
                style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todos los
                doctores</label>
        </div>
        <div class="col" style="display: flex;justify-content: flex-end !important">
            <button class="btn btn-round" style="background-color: #51A2A7" onclick="openmodalDoctor();">
                <i class="material-icons">add</i>AGREGAR DOCTOR
            </button>
            <a href="{{ route('cliente.import') }}">
                <button class="btn btn-round" style="background-color: #51A2A7">
                    <i class="material-icons">add</i>AGREGAR MASIVAMENTE
                </button>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-12 card py-3" style="border-radius: 30px">
            <div class="table-responsive shadow mt-5  mb-5 rounded" style="overflow-x:auto">
                <table id="example" class="table table-striped table-hover text-center table-sm mt-4 align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Registro</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Nivel</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Usuario</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Documento</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Tipo</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Segmento</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Distribuidor</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Telefono</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Puntos y facturación</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Posee consultorio</th>
                            <th class="text-uppercase text-center text-secondary text-xxs font-weight-bolder opacity-7">Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="">
                        @foreach ($cliente as $c)
                            <tr>
                                <td>{{ $c->created_at }}</td>
                                <td>
                                    <img width="40px" src="{{$c->level->getFirstMediaUrl('level-image')}}" alt="Imagen del nivel">
                                <span style="font-size: 10px"> {{ $c->level->nombre }}</span>
                                </td>
                                <td>
                                    {{ $c->nombre }} {{ $c->apellido }} <br>
                                    {{ $c->correo }}
                                </td>
                                <td>{{ $c->document_number ?? 'Sin datos' }}</td>
                                <td>
                                    {{ $c->rol->rol }}
                                    <hr>
                                    {{ $c->especialidad }}
                                </td>
                                <td>
                                    <span class="badge badge-success">{{ $c->login->segmento }}</span>
                                </td>
                                <td>{{ $c->distribuidor }}</td>
                                <td><span class="material-icons" style="font-size: 20px; color: #F28D8D">phone</span> {{ $c->telefono }}</td>
                                <td><span class="material-icons" style="font-size: 20px; color: #F28D8D">star</span>
                                    {{ $c->total_estrellas }} <br>
                                    ${{ $c->total_reunido }}
                                </td>
                                <td>
                                    @if (isset($c->consultorio->id_consultorio))
                                        <a class="btn btn-info btn-sm rounded" href="{{ url('/cliente/consultorio/', $c->consultorio->id_consultorio) }}">Ver</a>
                                    @else
                                        <span>No posee</span>
                                    @endif
                                </td>
                                <td class="mx-auto text-center">
                                    <a style="color: #fff; background-color: #51A2A7;" href="{{ url('cliente/detalle',$c->id_cliente) }}"
                                        class="btn rounded btn-sm btn-secondary mx-auto">
                                        <span class="iconify" data-icon="fa-regular:eye"></span>
                                    </a>
                                    <a class="btn btn-info btn-sm rounded" href="{{ route('cliente.edit', $c->id_cliente) }}">
                                        <span class="iconify" data-icon="akar-icons:edit"></span>
                                    </a>
                                    <form action="{{ route('cliente.destroy', $c->id_cliente) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('¿Deseas eliminar este usuario?')" class="btn btn-danger btn-sm rounded">
                                            <span class="iconify" data-icon="fa6-solid:trash-can"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <template id="doctores">
        <tr>
            <!--fecha de creacion-->
            <td>
                <span class="text-secondary text-xs font-weight-normal"></span>
            </td>
            <td>
                <span class="text-secondary text-xs font-weight-normal"></span>
            </td>
            <!--Usuario-->
            <td>
                <div class="d-flex px-2 py-1">
                    <div class="d-flex flex-column justify-content-center">
                        <h6 class="mb-0 text-xs"></h6>
                        <p class="text-xs text-secondary mb-0"></p>
                    </div>
                </div>
            </td>
            <!--documento-->
            <td class="align-middle text-center text-sm">
                <span class="text-secondary text-xs font-weight-normal"></span>
            </td>
            <!--tipo-->
            <td>
                <div class="d-flex px-2 py-1">
                    <div class="d-flex flex-column justify-content-center">
                        <h6 class="mb-0 text-xs"></h6>
                        <p class="text-xs text-secondary mb-0"></p>
                    </div>
                </div>
            </td>
            <!--segmento-->
            <td class="align-middle text-center text-sm">
                <span class="badge badge-sm badge-success"></span>
            </td>
            <!--distribuidor-->
            <td class="align-middle text-center text-sm">
                <span class="text-secondary text-xs font-weight-normal"></span>
            </td>
            <!--telefono-->
            <td class="align-middle text-center text-sm">
                <span class="iconify" data-icon="bi:phone-fill"></span> <span class="text-secondary text-xs font-weight-normal"></span>
            </td>
            <!--puntos acumulado-->
            <td class="align-middle text-center text-sm">
                <div class="row-reverse">
                    <div class="col">
                        <div class="row d-flex justify-content-center">
                            <div class="col-auto p-0"><span class="iconify d-inline-block" data-width="18" data-icon="emojione:star"></span></div>
                            <div class="col-auto p-0">
                                <h6 class="p mb-0 text-xs d-inline-block"></h6>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row d-flex justify-content-center">
                            <span class="text-xs text-secondary mb-0 mr-1">$</span>
                            <p class="text-xs text-secondary mb-0"></p>
                        </div>
                    </div>
                    <div class="col">

                    </div>
                </div>
            </td>
            <!--consultorio-->
            <td class="align-middle text-center text-sm">
                <span class="text-secondary text-xs font-weight-normal"></span>
            </td>
            <!--Botones-->
            <td class="align-middle">
                <a href="" class="editar btn btn-sm editRow btn-info rounded"><span class="iconify" data-width="15" data-icon="akar-icons:edit"></span></a>
                <button id="eliminar" data-id="" class="delete-doctor btn btn-sm  btn-danger rounded"><span class="iconify" data-width="15" data-icon="akar-icons:trash-can"></span></button>
                <a href="" class="ver btn btn-sm btn-dark rounded">
                    <span class="iconify" data-width="15" data-icon="emojione-monotone:eye-in-speech-bubble"></span></a>
            </td>
        </tr>
    </template>

    <!-- MODAL DE CREAR USUARIOS -->
    <div class="modal modal-doctor fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #51A2A7">
                    <h5 class="modal-title text-white font-weight-bold pb-2" id="exampleModalLabel">
                        <i class="fa fa-edit mr-2"></i>Crear Doctor
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-5">

                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <form class="p-4" {{-- action="{{ route('cliente.store') }}" method="post" --}}
                        enctype="multipart/form-data">
                        {{-- @csrf --}}
                        <label for="" style="color: #51A2A7; font-weight: bold">Datos login</label>
                        <div class="form-row">
                            <div class="col">
                                <input name="correo" id="correo" type="email" placeholder="Correo electronico"
                                    class="form-control @error('correo') is-invalid @enderror" value="{{ old('correo') }}">
                                <span id="invalid-correo" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <select required name="estado" id="estado"
                                    class="form-control @error('estado') is-invalid @enderror">
                                    <option value="" selected disabled>Seleccione el estado</option>
                                    <option value="activo" {{ old('estado') == 'activo' ? 'selected' : '' }}>Activo
                                    </option>
                                    <option value="inactivo" {{ old('estado') == 'inactivo' ? 'selected' : '' }}>Inactivo
                                    </option>
                                </select>
                                <span id="invalid-estado" class="text-danger"></span>
                            </div>
                            <div class="col">
                                <select required name="segmento" id="segmento"
                                    class="form-control @error('segmento') is-invalid @enderror">
                                    <option value="" selected disabled>Selecciona su Segmentación</option>
                                    <option value="Focus" {{ old('segmento') == 'Focus' ? 'selected' : '' }}>Focus
                                    </option>
                                    <option value="Invest" {{ old('segmento') == 'Invest' ? 'selected' : '' }}>Invest
                                    </option>
                                </select>
                                <span id="invalid-segmento" class="text-danger"></span>
                            </div>
                        </div>
                        <label for="" style="color: #51A2A7; font-weight: bold; margin-top: 5%">Datos
                            cliente</label>
                        <div class="form-row">
                            <div class="col">
                                <input required name="nombre" id="nombre" type="text" placeholder="Nombre"
                                    class="form-control @error('nombre') is-invalid @enderror"
                                    value="{{ old('nombre') }}">
                                <span id="invalid-nombre" class="text-danger"></span>
                            </div>
                            <div class="col">
                                <input required name="apellido" id="apellido" type="text" placeholder="Apellido"
                                    class="form-control @error('apellido') is-invalid @enderror"
                                    value="{{ old('apellido') }}">
                                <span id="invalid-apellido" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col-sm-6">
                                <label>Departamento</label>
                                <select class="form-control" name="departamento" id="departamento">
                                    <option value="" selected disabled>Selecciona una opción...</option>
                                    @foreach ($departaments as $departament)
                                        <option value="{{ $departament->id }}">{{ $departament->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="city_id">Ciudad</label>
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="" selected disabled>Seleccione una opción...</option>
                                </select>
                                <span id="invalid-city" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col-6">
                                <label for="name_user">Tipo de documento:</label>
                                <select required class="form-control @error('document_type_id') is-invalid @enderror"
                                    name="document_type_id" id="document_type_id">
                                    <option value="">Seleccione el tipo de documento...</option>
                                    @foreach ($document_types as $document_type)
                                        <option value="{{ $document_type->id }}">{{ $document_type->name }}</option>
                                    @endforeach
                                </select>
                                <span id="invalid-document_type" class="text-danger"></span>
                            </div>
                            <div class="col-6">
                                <label for="name_user">Numero de documento:</label>

                                <input required name="document_number" id="document_number" type="number"
                                    placeholder="Numero de identificación"
                                    class="form-control @error('document_number') is-invalid @enderror"
                                    value="{{ old('document_number') }}">
                                <span id="invalid-document_number" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="">Dirección (Opcional):</label>
                                <input name="direccion" id="direccion" type="text"
                                    placeholder="Bogotá, Colombia. Monserrate"
                                    class="form-control @error('direccion') is-invalid @enderror"
                                    value="{{ old('direccion') }}">
                                <span id="invalid-direccion" class="text-danger"></span>
                            </div>
                            <div class="col">
                                <label for="">Teléfono:</label>
                                <input required name="telefono" id="telefono" type="text"
                                    placeholder="Ejemplo: +5715800462"
                                    class="form-control @error('telefono') is-invalid @enderror"
                                    value="{{ old('telefono') }}">
                                <span id="invalid-telefono" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label for="">Distribuidor:</label>
                                <select required class="form-control" name="distribuidor" id="distribuidor">
                                    <option value="" selected disabled>Seleccione un distribuidor...</option>
                                    @foreach ($distribuidoras as $distribuidor)
                                        <option value="{{ $distribuidor->nombre_publico }}">
                                            {{ $distribuidor->nombre_publico }}</option>
                                    @endforeach
                                </select>
                                <span id="invalid-distribuidor" class="text-danger"></span>
                            </div>
                            <div class="col">
                                <label for="">Imagen de perfil:</label>
                                <input type="file" name="foto" id="foto" accept=".jpg,.jpeg,.png"
                                    class="form-control @error('foto') is-invalid @enderror">
                                <span id="invalid-imagen" class="text-danger"></span>
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <select required name="especialidad" id="especialidad"
                                    class="form-control @error('especialidad') is-invalid @enderror">
                                    <option value="" selected>Seleccione la especialidad</option>
                                    <option value="Especialista - Odontología">Especialista - Odontología
                                    </option>
                                    <option value="Especialista - Endodoncia">Especialista - Endodoncia</option>
                                    <option value="Especialista - Maxilofacial">Especialista - Maxilofacial
                                    </option>
                                    <option value="Especialista - Ortodoncia">Especialista - Ortodoncia</option>
                                    <option value="Especialista - Rehabilitación Oral">Especialista -
                                        Rehabilitación Oral</option>
                                    <option value="Especialista - Operatoria Dental">Especialista - Operatoria
                                        Dental</option>
                                </select>
                                <span id="invalid-especialidad" class="text-danger"></span>
                            </div>
                        </div>

                        <input name="total_estrellas" id="total_estrellas" type="hidden" value="0">

                        <div class="form-row my-3">
                            <div class="col-6">
                                <label for="">Tipo de Usuario:</label>
                                <select required name="loginrol_id" id="tipo" class="form-control">
                                    <option selected disabled value="">Seleccione una opción...</option>
                                    @foreach ($roles as $rol)
                                        <option value="{{ $rol->id_loginrol }}">{{ $rol->rol }}</option>
                                    @endforeach
                                </select>
                                <span id="invalid-tipo" class="text-danger"></span>
                            </div>
                            <div class="col-6">
                                <label for="">Posee consultorio:</label><br>
                                <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                    <label class="form-check-label">
                                        <input class="form-check-input posee-consultorio" type="radio" name="consultorio"
                                            id="consultorio" value="1"> Si
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                <div class="form-check form-check-radio form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input no-posee-consultorio" type="radio" name="consultorio"
                                            id="consultorio" value="0" checked> No
                                        <span class="circle">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div id="formconsultorio" style="display: none">
                            <label for="" style="color: #51A2A7; font-weight: bold">Datos Consultorio</label>
                            <div class="form-row">
                                <div class="col">
                                    <input name="nombre_c" id="nombre_c" type="text"
                                        placeholder="Nombre de Consultorio"
                                        class="form-control @error('nombre_c') is-invalid @enderror"
                                        value="{{ old('nombre_c') }}">
                                    <span id="invalid-nombre_c" class="text-danger"></span>
                                </div>
                                <div class="col">
                                    <input name="direccion_c" id="direccion_c" type="text"
                                        placeholder="Dirección de Consultorio"
                                        class="form-control @error('direccion_c') is-invalid @enderror"
                                        value="{{ old('direccion_c') }}">
                                    <span id="invalid-direccion_c" class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <input name="telefono_c" id="telefono_c" type="text" placeholder="Teléfono"
                                        class="form-control @error('telefono_c') is-invalid @enderror"
                                        value="{{ old('telefono_c') }}">
                                    <span id="invalid-telefono_c" class="text-danger"></span>
                                </div>
                                <div class="col">
                                    <input name="numero_pacientes" id="numero_pacientes" type="number"
                                        placeholder="Número de Pacientes"
                                        class="form-control @error('numero_pacientes') is-invalid @enderror"
                                        value="{{ old('numero_pacientes') }}">
                                    <span id="invalid-numero_pacientes" class="text-danger"></span>
                                </div>
                            </div>
                        </div>
                </div>

                <div class="modal-footer">
                    <button id="agregardoctor" class="btn mx-auto col-md-6" type="button">Agregar Usuario</button>
                </div>
                </form>

            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {

        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#tbody tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        $(".w_open").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=600,height=500");
        });

        $(".w_big_open").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank");
        });

        $('.posee-consultorio').click(function() {
            $('#formconsultorio').show();
        })

        $('.no-posee-consultorio').click(function() {
            $('#formconsultorio').hide();
            $('#nombre_c').val('');
            $('#direccion_c').val('');
            $('#telefono_c').val('');
            $('#numero_pacientes').val('');
        })
    });

    async function onSelectCountryChange(departamentoElement, cityElementId) {
        const country_id = departamentoElement.value;
        await $.get(`/api/country/${country_id}`, function(data) {
            var option = '<option value="" disabled selected>Seleccione una ciudad...</option>'
            for (var i = 0; i < data.length; ++i)
                option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            $(cityElementId).html(option)
        })
    }
    $(function() {
        $('#departamento').on('change', e => {
            onSelectCountryChange(e.target, "#city_id");
        });
    })
</script>
@if ($errors->any())
    <script>
        $(document).ready(function() {
            $('.invalid-feedback').css('display', 'block');
            $('.bd-example-modal-lg').modal('show');
        })
    </script>
@endif
