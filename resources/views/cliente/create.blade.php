@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">
            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <div class="row col-md-12">
                    <div class="row col-md-12">
                        <label for="" class="lead my-4">Por favor escoge el tipo de usuario que quieres crear:</label>
                    </div>
                    <button type="button" class="btn btn-primary col-md-4 mx-auto" data-toggle="modal"
                        data-target=".bd-example-modal-lg-1">Cliente</button>

                    <button type="button" class="btn btn-secondary col-md-4 mx-auto" data-toggle="modal"
                        data-target=".bd-example-modal-lg-2">Administrativo</button>
                </div>

                <!-- Modal CLIENTE -->
                <div class="modal fade bd-example-modal-lg-1" tabindex="-1" role="dialog"
                    aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Crear cliente</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form action="{{ route('cliente.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <h5 class="modal-title" id="exampleModalLongTitle">Datos Login</h5>
                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">Correo:</label>
                                            <input name="correo" id="categoria" type="email" class="form-control @error('correo') is-invalid @enderror">
                                            @error('correo')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name_user">Correo de Recuperación:</label>
                                            <input name="email_recuperacion" id="erecuperacion" type="email" class="form-control @error('email_recuperacion') is-invalid @enderror">
                                            @error('email_recuperacion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">Contraseña:</label>
                                            <input name="password" id="password" type="password" class="form-control @error('password') is-invalid @enderror">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name_user">Estado:</label>
                                            <select name="estado" id="" class="form-control @error('estado') is-invalid @enderror">
                                                <option value="" selected disabled>Seleccione...</option>
                                                <option value="activo">Activo</option>
                                                <option value="inactivo">Inactivo</option>
                                            </select>
                                            @error('estado')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <hr>

                                    <h5 class="modal-title" id="exampleModalLongTitle">Datos Cliente</h5>
                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">Nombre:</label>
                                            <input name="nombre" id="categoria" type="text" class="form-control @error('nombre') is-invalid @enderror">
                                            @error('nombre')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name_user">Apellido:</label>
                                            <input name="apellido" id="categoria" type="text" class="form-control @error('apellido') is-invalid @enderror">
                                            @error('apellido')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">Direccion:</label>
                                            <input name="direccion" id="categoria" type="text" class="form-control @error('direccion') is-invalid @enderror">
                                            @error('direccion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name_user">Fecha de Nacimiento:</label>
                                            <input name="fecha_nacimiento" id="SUBCATEGORIA" type="date"
                                                class="form-control @error('fecha_nacimiento') is-invalid @enderror">
                                            @error('fecha_nacimiento')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">Telefono:</label>
                                            <input name="telefono" id="categoria" type="text" class="form-control @error('telefono') is-invalid @enderror">
                                            @error('telefono')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Subir foto:</label>
                                            <div class="custom-file mb-3">
                                                <input type="file" class="custom-file-input @error('foto') is-invalid @enderror"
                                                id="upload" name="foto" accept=".jpg,.jpeg,.png" value="">
                                                <label class="custom-file-label" for="customFile">Escoge una Imagen</label>
                                                @error('foto')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">Especialidad:</label>
                                            <select name="especialidad" id="" class="form-control @error('especialidad') is-invalid @enderror">
                                                <option value="" selected>Seleccione...</option>
                                                <option value="Especialista - Odontología">Especialista - Odontología</option>
                                                <option value="Especialista - Endodoncia">Especialista - Endodoncia</option>
                                                <option value="Especialista - Maxilofacial">Especialista - Maxilofacial</option>
                                                <option value="Especialista - Ortodoncia">Especialista - Ortodoncia</option>
                                                <option value="Especialista - Rehabilitación Oral">Especialista - Rehabilitación Oral</option>
                                                <option value="Especialista - Operatoria Dental">Especialista - Operatoria Dental</option>
                                            </select>
                                            @error('especialidad')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name_user">Total de estrellas:</label>
                                            <input name="total_estrellas" id="categoria" type="number" class="form-control @error('total_estrellas') is-invalid @enderror">
                                            @error('total_estrellas')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">¿Posee Consultorio?:</label>
                                            <div class="form-check">
                                                <input value="1" class="form-check-input" type="radio" name="consultorio" id="flexRadioDefault1">
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Si
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input value="0" class="form-check-input" type="radio" name="consultorio" id="flexRadioDefault2" checked>
                                                <label class="form-check-label" for="flexRadioDefault2">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div id="formconsultorio" class="d-none">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Datos Consultorio</h5>
                                        <div class="row col-md-12 my-3">
                                            <div class="col-md-6">
                                                <label for="name_user">Nombre:</label>
                                                <input name="nombre_c" id="nomconsult" type="text" class="form-control @error('nombre_c') is-invalid @enderror">
                                                @error('nombre_c')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="name_user">Direccion:</label>
                                                <input name="direccion_c" id="diconsult" type="text" class="form-control @error('direccion_c') is-invalid @enderror">
                                                @error('direccion_c')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="row col-md-12 my-3">
                                            <div class="col-md-6">
                                                <label for="name_user">Telefono:</label>
                                                <input name="telefono_c" id="telconsult" type="text" class="form-control @error('telefono_c') is-invalid @enderror">
                                                @error('telefono_c')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label for="name_user">Número de Pacientes:</label>
                                                <input name="numero_pacientes" id="n_pacientes" type="number" class="form-control @error('numero_pacientes') is-invalid @enderror">
                                                @error('numero_pacientes')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col-md-12 my-3">
                                        <button type="submit" class="btn btn-primary form-control">Crear usuario</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- Modal ADMIN -->
                <div class="modal fade bd-example-modal-lg-2" tabindex="-1" role="dialog"
                    aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Crear administrativo</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form action="{{ route('admin.store') }}" method="post">
                                    @csrf

                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">Nombre:</label>
                                            <input name="nombre" id="categoria" type="text" class="form-control @error('nombre') is-invalid @enderror">
                                            @error('nombre')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name_user">Apellido:</label>
                                            <input name="apellido" id="categoria" type="text" class="form-control @error('apellido') is-invalid @enderror">
                                            @error('apellido')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row col-md-12">
                                        <div class="col-md-6">
                                            <label for="name_user">Correo electronico:</label>
                                            <input name="email" id="categoria" type="text" class="form-control @error('email') is-invalid @enderror">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name_user">Contraseña:</label>
                                            <input name="password" id="SUBCATEGORIA" type="password" class="form-control @error('password') is-invalid @enderror">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6">
                                            <label for="name_user">Estado:</label>
                                            <select name="estado" id="" class="form-control @error('estado') is-invalid @enderror">
                                                <option value="SIN INFORMACION" selected disabled>Seleccione...</option>
                                                <option value="activo">Activo</option>
                                                <option value="inactivo">Inactivo</option>
                                            </select>
                                            @error('estado')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name_user">Rol:</label>
                                            <select name="FK_id_loginrol" id="user_rol" class="form-control">
                                                @foreach ($rol as $i => $a)
                                                    <option value="{{ $a->id_loginrol }}">{{ $a->rol }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row col-md-12 my-3">
                                        <div class="col-md-6 d-none" id="base_permisos">
                                            <label for="name_user">Permisos de Base de Datos:</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_store" name="p_store">
                                                <label class="form-check-label" for="p_store">
                                                    Creación de datos
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_update" name="p_update">
                                                <label class="form-check-label" for="p_update">
                                                    Actualización de datos
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_destroy" name="p_destroy">
                                                <label class="form-check-label" for="p_destroy">
                                                    Eliminación de datos
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-md-6 d-none" id="modulos_permisos">
                                            <label for="name_user">Permisos de Módulos:</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_productos" name="p_productos">
                                                <label class="form-check-label" for="p_productos">
                                                    Productos
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_premios" name="p_premios">
                                                <label class="form-check-label" for="p_premios">
                                                    Premios
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_facturas" name="p_facturas">
                                                <label class="form-check-label" for="p_facturas">
                                                    Facturas
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_noticias" name="p_noticias">
                                                <label class="form-check-label" for="p_noticias">
                                                    Noticias
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_capacitaciones" name="p_capacitaciones">
                                                <label class="form-check-label" for="p_capacitaciones">
                                                    Capacitaciones
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" id="p_trivia" name="p_trivia">
                                                <label class="form-check-label" for="p_trivia">
                                                    Trivias y Encuestas
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="row col-md-12 my-3">
                                        <button type="submit" class="btn btn-primary form-control">Crear usuario</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $("input[name='consultorio']").change(function(){
            var radioValue = $("input[name='consultorio']:checked").val();
            if(radioValue == 1)
                $("#formconsultorio").removeClass("d-none");
            else
                $("#formconsultorio").addClass("d-none");
        });

        $("#user_rol").change(function(){
            var selectValue = $(this).val();
            //1 Super Admin
            //2 Call Center
            //3 Básico

            //base_permisos
            //modulos_permisos

            if(selectValue == 1){
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").addClass("d-none");
            }
            else if(selectValue == 2){
                $("#base_permisos").removeClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
            else{
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
        });
    });
</script>
