@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif

@if ($errors->any())
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    Error en la validación de los datos, por favor abra de nuevo el modal y corrija los datos...
</div>
@endif

<div class="row my-5">
    <div class="col">
        <label for="" class="font-weight-bold lead" style="color: #51A2A7">Todos los Clientes Por Aprobar</label>
    </div>
</div>

<!-- TABLA DE USUARIOS REGISTRADOS -->

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive shadow p-3 mb-5 rounded">
            <table id="example" class="table table-striped" cellspacing="0" width="100%">
                <thead style="background-color: #51A2A7; color: #fff;">
                    <tr>
                        <th>Nombres</th>
                        <th>Segmento</th>
                        <th>Distribuidor</th>
                        <th>Especialidad</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>Fecha Registro</th>
                        <th>Factura Adjuntada</th>
                        <th>Confirmacion</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="table-light" style="background-color: #fff; color: #000">
                @foreach($logins as $login)
                    <tr>
                        <td>{{ $login->cliente->nombre." ".$login->cliente->apellido }}</td>
                        <td id="seg{{ $login->id_clientelogin }}">{{ $login->segmento }}</td>
                        <td>
                            <span class="d-none" id="dist{{ $login->id_clientelogin }}">
                                {{ $login->cliente->distribuidor }}
                            </span>
                            {{ $login->cliente->distribuidor ? $login->cliente->distribuidor : "No posee adjunto" }}
                        </td>
                        <td id="esp{{ $login->id_clientelogin }}">{{ $login->cliente->especialidad }}</td>
                        <td id="ema{{ $login->id_clientelogin }}">{{ $login->email }}</td>
                        <td id="tel{{ $login->id_clientelogin }}">{{ $login->cliente->telefono }}</td>
                        <td>{{ date_format(date_create($login->created_at->modify('-5 hours')), 'F j, Y H:i:s') }}</td>
                        <td>
                            <a id="{{ route('cliente.archive', ['filename' => basename($login->validacion)]) }}"
                                href="" class="btn w_open_detail" style="background-color: #51A2A7;">
                                <i class="fas fa-file-archive text-white"></i>
                            </a>
                        </td>
                        <td>
                            @if($login->status == 'pendiente')
                                <div class="row-reverse mx-auto">
                                    <div class="col mx-auto">
                                        <a href="{{ route('cliente.registrados.status', ['id' => $login->id_clientelogin, 'status' => 'aceptado']) }}"
                                            class="btn" style="background-color: #51A2A7;">
                                            <i class="fas fa-check mr-1"></i>Aceptar
                                        </a>
                                    </div>
                                    <div class="col mx-auto">
                                        <a href="{{ route('cliente.registrados.status', ['id' => $login->id_clientelogin, 'status' => 'rechazado']) }}"
                                            class="btn" style="background-color: #F22738;">
                                            <i class="fas fa-times mr-1"></i>Rechazar
                                        </a>
                                    </div>
                                </div>
                            @else
                                <span>{{$login->status}}</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <a id="{{ $login->id_clientelogin }}" class="feditar" style="color: #51A2A7; cursor: pointer">
                                <i class="far fa-edit"></i>
                            </a>
                            <!-- <a href="javascript:;" style="color: #51A2A7;" onclick="delete_alert();"><i class="far fa-trash-alt"></i></a> -->
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
<!-- FIN TABLA DE USUARIOS REGISTRADOS -->

<!-- MODAL DE EDITAR USUARIOS -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                    Editar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('cliente.registrados.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="col mt-1">
                            <label for="">Correo electronico: (No editable)</label>
                            <input name="correo" id="pemail" type="email" placeholder="Correo electronico"
                                class="form-control" disabled
                                value="prueba@gmail.com">
                        </div>
                        <div class="col">
                            <label for="">Segmento:</label>
                            <select name="segmento" id="psegmento" class="form-control @error('segmento') is-invalid @enderror">
                                <option value="" selected disabled>Selecciona su Segmentación</option>
                                <option value="Focus">Focus</option>
                                <option value="Invest">Invest</option>
                                <option value="OTRO">Otro</option>
                            </select>
                            @error('segmento')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col">
                            <label for="">Distribuidor (Opcional):</label>
                            <select name="distribuidor" id="pdistribuidor"
                                class="form-control ">
                                <option value="" selected>Opcional (no lo conozco)...</option>
                                <option value="Bracket">Bracket</option>
                                <option value="Aldental" >Aldental</option>
                                <option value="Vertice Aliados">Vertice Aliados</option>
                                <option value="Dental Palermo">Dental Palermo</option>
                                <option value="Dentales Antioquía">Dentales Antioquía </option>
                                <option value="Casa Dental Gabriel Velasquez">Casa Dental Gabriel Velasquez </option>
                                <option value="Dentales Padilla">Dentales Padilla</option>
                                <option value="Orbidental">Orbidental</option>
                                <option value="Dentales y Acrilicos">Dentales y Acrilicos </option>
                                <option value="Dental 83">Dental 83</option>
                                <option value="Janer Distribuciones">Janer Distribuciones</option>
                                <option value="Dental Nader">Dental Nader</option>
                                <option value="Casa Odontológica">Casa Odontológica</option>
                                <option value="Dentales Market">Dentales Market</option>
                                <option value="Adental">Adental</option>
                            </select>

                            @error('distribuidor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col mt-1">
                            <label for="">Teléfono:</label>
                            <input name="telefono" id="ptelefono" type="text" placeholder="Ejemplo: +5715800462"
                                class="form-control"
                                value="234234">
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <label for="">Especialidad:</label>
                            <select name="especialidad" id="pespecialidad"
                                class="form-control @error('especialidad') is-invalid @enderror">
                                <option value="" selected>Seleccione la especialidad</option>
                                <option value="Sin Especialidad">Sin Especialidad</option>
                                <option value="Especialista - Odontología">Especialista - Odontología</option>
                                <option value="Especialista - Endodoncia">Especialista - Endodoncia</option>
                                <option value="Especialista - Maxilofacial">Especialista - Maxilofacial</option>
                                <option value="Especialista - Ortodoncia">Especialista - Ortodoncia</option>
                                <option value="Especialista - Rehabilitación Oral">Especialista - Rehabilitación Oral</option>
                                <option value="Especialista - Operatoria Dental">Especialista - Operatoria Dental</option>
                            </select>
                            @error('especialidad')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <input id="idperson" type="hidden" class="form-control" name="idperson" value="">
            </div>

            <div class="modal-footer">
                <input class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;" type="submit" value="EDITAR USUARIO">
            </div>
                </form>

        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR USUARIOS -->
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $( ".w_open_detail" ).click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=250,width=1000,height=1000");
        });

        $(".feditar").click(function() {
            var id = $(this).attr("id");
            var segmento = $("#seg"+id).text();
            var distribuidor = $("#dist"+id).text();
            var especialidad = $("#esp"+id).text();
            var email = $("#ema"+id).text();
            var telefono = $("#tel"+id).text();

            //Seteamos los valores
            $("#psegmento option[value='"+segmento+"']").attr("selected", true);
            $("#pespecialidad option[value='"+especialidad+"']").attr("selected", true);
            $("#pdistribuidor option[value='"+distribuidor+"']").attr("selected", true);
            $("#pemail").val(email);
            $("#ptelefono").val(telefono);
            $("#idperson").val(id);

            $('#exampleModal').modal('show');
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>
