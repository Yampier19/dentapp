@extends('layouts.admin.app')

@section('content')

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todas los clientes prueba</label>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th>Avatar</th>
                            <th>Nombres</th>
                            <th>Nivel</th>
                            <th>Tipo</th>
                            <th>Segmento</th>
                            <th>Distribuidor</th>
                            <th>Correo</th>
                            <th>Teléfono Personal</th>
                            <th>Puntos Acumulados</th>
                            <th>Valor Facturado</th>
                            <th>Especialidad</th>
                            <!-- <th>Datos de Consultorio</th>
                            <th>Acciones</th> -->
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach($clientes as $cliente)
                            <tr>
                                <td class="mx-auto text-center">
                                    <img style="width:100px;max-height:100px;"
                                        src="{{ route('cliente.icon', ['filename' => basename($cliente->foto)]) }}">
                                </td>
                                <td>{{ $cliente->nombre . ' ' . $cliente->apellido }}</td>
                                <td>
                                        <img style="width:100px;max-height:100px;" src="{{asset("img/niveles/".$cliente->nivel.".png")}}" alt="">
                                </td>
                                <td>{{ $cliente->tipo ? strtoupper($cliente->tipo) : 'No posee.' }}</td>
                                <td>{{ $cliente->login->segmento ? $cliente->login->segmento : 'No posee.' }}</td>
                                <td>{{ $cliente->distribuidor ? $cliente->distribuidor : 'No posee.' }}</td>
                                <td>{{ $cliente->correo }}</td>
                                <td><i class="fas fa-phone"></i> {{ $cliente->telefono }}</td>
                                <td><i class="fas fa-star text-warning"></i> {{ $cliente->total_estrellas }}</td>
                                <td>${{ number_format($cliente->total_reunido, 3, ',', '.') }}</td>
                                <td>{{ $cliente->especialidad }}</td>
                                <!-- <td>No Posee....</td>
                                <td class="text-center">
                                    <a href="javascript:;"
                                        style="color: #51A2A7; cursor: pointer"> <i class="far fa-eye"></i></a>
                                </td> -->
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>

@endsection
