@extends('layouts.admin.app')
@section('content')
    <div class="row p-3">
        <div class="col-sm-6 card p-5" style="border-radius:30px">
            <h2 class="text-center mb-5 font-weight-bold"style="color:#51A2A7">Petición de inhabilitar cuenta</h2>
            <div class="d-flex justify-content-center mb-3">
                <span style="font-size:5em" class="iconify" data-icon="ic:baseline-person-add-disabled"></span>
            </div>
            <table id="pendientes" class="table table-light">
                <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Telefono</th>
                        <th>Desactivar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $cliente)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $cliente->nombre }}</td>
                            <td>{{ $cliente->correo }}</td>
                            <td>{{ $cliente->telefono }}</td>
                            <td>
                                <form action="{{ route('deactivate.user', $cliente->id_cliente) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <button type="submit" class="btn rounded btn-sm btn-danger">Desactivar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-sm-6 card p-5" style="border-radius:30px">
            <h2 class="text-center mb-5 font-weight-bold"style="color:#51A2A7">Restaurar Usuario</h2>
            <div class="d-flex justify-content-center mb-3">
                <span style="font-size:5em" class="iconify" data-icon="ic:sharp-settings-backup-restore"></span>
            </div>
            <table id="inactivos" class="table table-light">
                <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Distribuidor</th>
                        <th>Fecha de eliminación</th>
                        <th>Restaurar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($inactiveClients as $inactiveClient)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $inactiveClient->nombre }}</td>
                            <td>{{ $inactiveClient->correo }}</td>
                            <td>{{ $inactiveClient->distribuidor }}</td>
                            <td>{{ $inactiveClient->deleted_at }}</td>
                            <td>
                                <form action="{{ route('cliente.restore', $inactiveClient->id_cliente) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <button type="submit" class="btn btn-sm rounded btn-success">Restaurar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <style>
        .paginate_button .page-item .next .disabled{
            background-color: yellow !important;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('#pendientes').DataTable({

            });
        });

        $(document).ready(function() {
            $('#inactivos').DataTable({

            });
        });
    </script>
@endsection
