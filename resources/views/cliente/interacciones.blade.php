@extends('layouts.admin.app')

@section('content')

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todas las interacciones</label>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th>Fecha de interacción</th>
                            <th>Nombre del usuario</th>
                            <th>Interacción realizada</th>
                            <th>Especialidad</th>
                            <th>Distribuidor</th>
                            <!-- <th>Acciones</th> -->
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach($interacciones as $interaccion)
                            <tr>
                                <td class="font-weight-bold">{{ date_format(date_create($interaccion->fecha_registro), "F j, Y") }}</td>
                                <td>{{ $interaccion->cliente->nombre." ".$interaccion->cliente->apellido }}</td>
                                <td>{{ $interaccion->accion }}</td>
                                <td>{{ $interaccion->cliente->especialidad }}</td>
                                <td>{{ $interaccion->cliente->distribuidor }}</td>
                                <!-- <td class="text-center">
                                    <a href="javascript:;"
                                        style="color: #51A2A7; cursor: pointer"> <i class="far fa-eye"></i></a>
                                </td> -->
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>

@endsection