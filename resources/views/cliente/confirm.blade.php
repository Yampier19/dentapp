@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (auth()->user()->FK_id_loginrol == 1)
            <div class="card col-sm-8 m-auto p-5">
                <div class="p-5">
                    <h2 class="mb-5 text-center font-weight-bold">¿Deseas eliminar el siguiente usuario? </h2>
                    <div class="text-center mb-5" style="color: tomato;font-size:7em">
                        <i class="fa fa-exclamation-triangle"></i>
                    </div>
                    <h4 class='font-weight-bold'>Nombre: </h4>
                    <p>{{ $cliente->nombre }}</p>
                    <h4 class='font-weight-bold'>Apellido: </h4>
                    <p>{{ $cliente->apellido }}</p>
                    <h4 class='font-weight-bold'>Correo: </h4>
                    <p>{{ $cliente->correo }}</p>
                    <h4 class='font-weight-bold'>Especialidad: </h4>
                    <p>{{ $cliente->especialidad }}</p>
                    <h4 class='font-weight-bold'>Segmento: </h4>
                    <p>{{ $cliente->login->segmento ? $cliente->login->segmento : 'No posee' }}</p>

                    <form method="post" enctype="multipart/form-data" action="{{ route('cliente.destroy', $cliente->id_cliente) }}">
                        @method('DELETE')
                        @csrf
                        <div class="modal-footer mt-4">
                            <button type="submit" class="redondo btn btn-danger rounded col-md-4 mx-auto">
                                <i class="fas fa-trash-alt"></i> <b>Eliminar</b>
                            </button>
                            <a style="background-color:#51A2A7" href="{{ route('cliente.index') }}" class="btn rounded col-md-4 mx-auto"><b>Volver</b></a>
                        </div>
                    </form>
                </div>
            </div>
        @endif


    </div>
@endsection
