@extends('layouts.admin.app')
@push('scripts')
{{-- <script src="{{asset('js/doctor/doctor.js')}}"></script> --}}
@endpush
@section('content')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <div class="container mt-5">

        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (session('edit'))
            <script>
                Swal.fire({
                    icon: 'success',
                    title: 'Usuario editado con exito',
                    showConfirmButton: false,
                    timer: 1000
                })
            </script>
        @endif
        @if ($errors->any())
            <div class="card p-4">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <script>
                Swal.fire({
                    icon: 'error',
                    title: 'Completa todos los campos',
                    showConfirmButton: false,
                    timer: 1000
                })
            </script>
        @endif

        <div class="row">
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <form method="post" action="{{route('cliente.update',$cliente->id_cliente)}}" class="p-5" id="asesor" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <h3 class="mb-5 text-primary font-weight-bold">Editar cliente - {{ $cliente->nombre }}
                        {{ $cliente->apellido }} </h3>
                    @if ($cliente->foto)
                        <div class="d-flex justify-content-center mb-5">
                            <img style="border-radius:100%;box-shadow: 0 0 40px rgba(58, 56, 56, 0.1);" width="200px"
                                src="{{ asset('storage') . '/' . $cliente->foto }}" alt="">
                        </div>
                    @endif
                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLongTitle">Datos Login</h5>
                    <div class="row col-md-12 my-3 p-4">
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Correo:</label>
                            <input required name="correo" id="correo" type="email"
                                class="form-control @error('correo') is-invalid @enderror"
                                value="{{ old('correo', $cliente->correo) }}">
                            @error('correo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Contraseña (Opcional):</label>
                            <input name="password" id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror"
                                placeholder="Dejar en vacio si no require ser editado...">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Estado:</label>
                            <select required name="estado" id="estado"
                                class="form-control @error('estado') is-invalid @enderror">
                                <option value="activo"
                                    {{ old('estado', $cliente->login->estado) == 'activo' ? 'selected' : '' }}>Activo
                                </option>
                                <option value="inactivo"
                                    {{ old('estado', $cliente->login->estado) == 'inactivo' ? 'selected' : '' }}>Inactivo
                                </option>
                            </select>
                            @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Segmento:</label>
                            <select required name="segmento" id="segmento"
                                class="form-control @error('segmento') is-invalid @enderror">
                                <option value="Focus"
                                    {{ old('segmento', $cliente->login->segmento) == 'Focus' ? 'selected' : '' }}>Focus
                                </option>
                                <option value="Invest"
                                    {{ old('segmento', $cliente->login->segmento) == 'Invest' ? 'selected' : '' }}>Invest
                                </option>
                            </select>
                            @error('segmento')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <hr class="mt-5">

                    <h5 class="modal-title text-primary font-weight-bold"id="exampleModalLongTitle">Datos Cliente</h5>
                    <div class="row col-md-12 my-3 p-3">
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Nombre:</label>
                            <input required name="nombre" id="nombre" type="text"
                                class="form-control @error('nombre') is-invalid @enderror"
                                value="{{ old('nombre', $cliente->nombre) }}">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Apellido:</label>
                            <input required name="apellido" id="apellido" type="text"
                                class="form-control @error('apellido') is-invalid @enderror"
                                value="{{ old('apellido', $cliente->apellido) }}">
                            @error('apellido')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm-6">
                            <label>Departamento</label>
                            <select class="form-control" name="departamento" id="departamento">
                                <option value="" selected disabled>Seleccione una opción...</option>
                                @foreach ($departaments as $departament)
                                    @if (isset($cliente->city->departamento_id))
                                        <option value="{{ $departament->id }}" @selected($cliente->city->departamento_id == $departament->id)>
                                            {{ $departament->name }}</option>
                                    @else
                                        <option value="{{ $departament->id }}">
                                            {{ $departament->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="city_id">Ciudad</label>
                            <select name="city_id" id="city_id" class="form-control">
                                <option value="{{ isset($cliente->city->id) ? $cliente->city->id : '' }}">
                                    {{ isset($cliente->city->name) ? $cliente->city->name : 'Seleccione una opción...' }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-6">
                            <label class="font-weight-bold" for="document_type_id">Tipo de documento:</label>
                            <select required class="form-control @error('document_type_id') is-invalid @enderror"
                                name="document_type_id" id="document_type_id">
                                <option value="{{ isset($cliente->documentType->id) ? $cliente->documentType->id : '' }}">
                                    {{ isset($cliente->documentType->name) ? $cliente->documentType->name : 'Seleccione una opción...' }}
                                </option>
                                @foreach ($document_types as $document_type)
                                    <option value="{{ $document_type->id }}">{{ $document_type->name }}</option>
                                @endforeach
                            </select>
                            @error('document_type_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label class="font-weight-bold" for="document_type_id">Numero de documento:</label>
                            <input required name="document_number" id="document_number" type="number"
                                placeholder="Numero de identificación"
                                class="form-control @error('document_number') is-invalid @enderror"
                                value="{{ old('document_number', $cliente->document_number) }}">
                            @error('document_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12 p-3">
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Dirección:</label>
                            <input name="direccion" id="direccion" type="text"
                                class="mt-1 form-control @error('direccion') is-invalid @enderror"
                                value="{{ old('direccion', $cliente->direccion) }}">
                            @error('direccion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Distribuidor:</label>
                            <select required class="form-control" name="distribuidor" id="distribuidor">
                                <option value="" disabled selected>Seleccione una opción...</option>
                                @foreach ($distribuidoras as $distribuidor)
                                    <option @if ($cliente->distribuidor == $distribuidor->nombre_publico) selected="selected" @endif
                                        value="{{ $distribuidor->nombre_publico }}">{{ $distribuidor->nombre_publico }}
                                    </option>
                                @endforeach
                            </select>
                            @error('distribuidor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12 my-3 p-3">
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Teléfono:</label>
                            <input required name="telefono" id="telefono" type="number"
                                class="form-control @error('telefono') is-invalid @enderror"
                                value="{{ old('telefono', $cliente->telefono) }}">
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="">Cambiar foto:</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('foto') is-invalid @enderror"
                                    id="foto" name="foto" accept=".jpg,.jpeg,.png" value="">
                                <label class="custom-file-label" for="customFile">Escoge una Imagen</label>
                                @error('foto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12 my-3 p-3">
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Especialidad:</label>
                            <select required name="especialidad" id="especialidad"
                                class="form-control @error('especialidad') is-invalid @enderror">
                                <option selected disabled value="">Seleccione una opción...</option>
                                <option value="Especialista - Odontología"
                                    {{ $cliente->especialidad == 'Especialista - Odontología' ? 'selected' : '' }}>
                                    Especialista - Odontología</option>
                                <option value="Especialista - Endodoncia"
                                    {{ $cliente->especialidad == 'Especialista - Endodoncia' ? 'selected' : '' }}>
                                    Especialista - Endodoncia</option>
                                <option value="Especialista - Maxilofacial"
                                    {{ $cliente->especialidad == 'Especialista - Maxilofacial' ? 'selected' : '' }}>
                                    Especialista - Maxilofacial</option>
                                <option value="Especialista - Ortodoncia"
                                    {{ $cliente->especialidad == 'Especialista - Ortodoncia' ? 'selected' : '' }}>
                                    Especialista - Ortodoncia</option>
                                <option value="Especialista - Rehabilitación Oral"
                                    {{ $cliente->especialidad == 'Especialista - Rehabilitación Oral' ? 'selected' : '' }}>
                                    Especialista - Rehabilitación Oral</option>
                                <option value="Especialista - Operatoria Dental"
                                    {{ $cliente->especialidad == 'Especialista - Operatoria Dental' ? 'selected' : '' }}>
                                    Especialista - Operatoria Dental</option>
                            </select>
                            @error('especialidad')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Total de estrellas:</label>
                            <input required name="total_estrellas" id="total_estrellas" type="number"
                                value="{{ old('total_estrellas', $cliente->total_estrellas) }}"
                                class="form-control @error('total_estrellas') is-invalid @enderror">
                            @error('total_estrellas')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3 p-3">
                        <div class="col-md-6">
                            <label class="font-weight-bold" for="name_user">Tipo de Usuario:</label>
                            <select required name="loginrol_id" id="tipo" class="form-control">
                                <option selected disabled value="">Seleccione una opción...</option>
                                @foreach ($roles as $rol)
                                <option {{$cliente->loginrol_id == $rol->id_loginrol ? 'selected' : '' }} value="{{$rol->id_loginrol}}">{{$rol->rol}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <div class="row col-md-12 my-3 {{ $cliente->consultorio ? 'd-none' : 'd-block' }}">
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="name_user">¿Posee Consultorio?:</label>
                                    <div class="form-check">
                                        <input value="1" class="" {{ $cliente->consultorio ? 'checked' : '' }}
                                            type="radio" name="consultorio" id="consultorio">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Si
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input value="0" class="" {{ $cliente->consultorio ? '' : 'checked' }}
                                            type="radio" name="consultorio" id="flexRadioDefault2">
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="formconsultorio" class="{{ $cliente->consultorio ? 'd-block' : 'd-none' }}">
                        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLongTitle">Datos Consultorio
                        </h5>

                        <div class="row col-md-12 my-3">
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="name_user">Nombre:</label>
                                <input name="nombre_c" id="nombre_c" type="text"
                                    value="{{ $cliente->consultorio ? old('nombre_c', $cliente->consultorio->nombre) : '' }}"
                                    class="form-control @error('nombre_c') is-invalid @enderror">
                                @error('nombre_c')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="name_user">Direccion:</label>
                                <input name="direccion_c" id="direccion_c" type="text"
                                    value="{{ $cliente->consultorio ? old('direccion_c', $cliente->consultorio->direccion) : '' }}"
                                    class="form-control @error('direccion_c') is-invalid @enderror">
                                @error('direccion_c')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row col-md-12 my-3">
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="name_user">Telefono:</label>
                                <input name="telefono_c" id="telefono_c" type="text"
                                    value="{{ $cliente->consultorio ? old('telefono_c', $cliente->consultorio->telefono) : '' }}"
                                    class="form-control @error('telefono_c') is-invalid @enderror">
                                @error('telefono_c')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold" for="name_user">Número de Pacientes:</label>
                                <input name="numero_pacientes" id="numero_pacientes" type="number"
                                    value="{{ $cliente->consultorio ? old('numero_pacientes', $cliente->consultorio->numero_pacientes) : '' }}"
                                    class="form-control @error('numero_pacientes') is-invalid @enderror">
                                @error('numero_pacientes')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer mt-4">
                        <button data-toggle="modal" data-target="#editar" style="background-color:teal" type="button"
                            class="btn col-3 btn-success form-control"><span class="iconify mr-2"
                                data-icon="akar-icons:edit"></span> Actualizar Cliente</button>
                        <a href="{{ route('cliente.index') }}" style="background-color: rgb(219, 208, 208)"
                            class="btn btn-secondary col-2 form-control font-weight-bolt"><b><span class="iconify mr-2"
                                    data-icon="akar-icons:arrow-back-thick"></span> Volver</b></a>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade text-dark" id="editar" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body text-center p-4">
                                    <span style="color: tomato" class="iconify" data-width='90'
                                        data-icon="bi:exclamation-circle-fill"></span>
                                    <h2 class="font-weight-bold">¿Está seguro?</h2>
                                    <h4 class="mb-4">¡No podrás revertir esto!</h4>
                                    <button type="submit" class="btn" style="background-color:#00a7d0">¡Sí,
                                        Editalo!</button>
                                    <button data-dismiss="modal" class="bg-danger btn ">Cancelar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $("input[name='consultorio']").change(function() {
            var radioValue = $("input[name='consultorio']:checked").val();
            if (radioValue == 1)
                $("#formconsultorio").removeClass("d-none");
            else
                $("#formconsultorio").addClass("d-none");
        });

        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }
    });


    async function onSelectCountryChange(departamentoElement, cityElementId) {
        const country_id = departamentoElement.value;
        await $.get(`/api/country/${country_id}`, function(data) {
            var option = '<option value="" disabled selected>Seleccione una ciudad...</option>'
            for (var i = 0; i < data.length; ++i)
                option += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            $(cityElementId).html(option)
        })
    }
    $(function() {
        $('#departamento').on('change', e => {
            onSelectCountryChange(e.target, "#city_id");
        });
    })
</script>
