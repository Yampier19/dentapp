@extends('layouts.admin.app')

@section('content')


@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif

@if ($errors->any())
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    Error en la validación de los datos, por favor abra de nuevo el modal y corrija los datos...
</div>
@endif

<a href="{{route('factura.noleidas')}}">
    <button class="btn btn-round" style="background-color: #51A2A7">
        VER FACTURAS NO LEIDAS
    </button>
</a>

<div class="row mt-3" >

    <!-------------------------------- CARDs CLIENTE --------------------------------->
    <div class="col-lg-6 col-md-6">
        <div class="row mx-auto">
            <label for="" class="lead" style="color: #51A2A7; font-weight: bold">Listado de clientes</label>
            <a href="{{route('factura.cliente')}}" class="ml-auto text-secondary">Ver todos</a>
        </div>
        @forelse ($clientes as $cliente)
            <div class="card">
                <div class="card-header">
                    <div class="row mx-auto">
                        <h4 class="card-title" style="color: #51A2A7; font-weight: bold">
                            {{ $cliente->nombre." ".$cliente->apellido }}
                        </h4>
                        <i class="material-icons ml-auto">star</i>{{ $cliente->total_estrellas }}
                    </div>
                    <p class="category" style="color: #51A2A7;">{{ $cliente->especialidad }}</p>
                </div>
                <div class="card-body">
                    <p>{{ $cliente->direccion }}</p>
                    <p>{{ $cliente->telefono }}</p>
                    <div class="row ml-auto mx-auto">
                        <p class="mx-2 ml-auto  my-auto" style="font-size: 15px; color: #51A2A7">
                            <a style="color: #51A2A7" href="{{ route('factura.listado', ['id' => $cliente->id_cliente, 'nombre' => $cliente->nombre.' '.$cliente->apellido]) }}">
                            Ver facturas</a>
                        </p>
                        <span class="material-icons" style="font-size: 20px; color: #51A2A7">edit</span>
                    </div>
                </div>
            </div>
        @empty
            <div class="card">
                <div class="card-header">
                    <div class="row mx-auto">
                        <h4 class="card-title" style="color: #51A2A7; font-weight: bold">
                            No hay clientes Registrados
                        </h4>
                    </div>
                </div>
            </div>
        @endforelse
    </div>

    <!-------------------------------- CARDs FACTURAS --------------------------------->
    <div class="col-lg-6 col-md-6">
        <div class="row mx-auto">
            <label class="lead" for="" style="color: #F28D8D; font-weight: bold">Listado de facturas</label>
            <a href="{{route('factura.listado')}}" class="ml-auto text-secondary
                ">Ver todos</a>
        </div>
        @forelse ($facturas as $factura)
            <div class="card">
                <div class="card-header">
                    <div class="row mx-auto">
                        <h4 id="ref{{ $factura->id_factura }}" class="card-title" style="color: #F28D8D; font-weight: bold">{{ $factura->referencia }}</h4>
                        <p class="ml-auto">{{ $factura->puntos }} puntos</p>
                    </div>
                </div>
                <div class="card-body">
                    <p class="font-weight-bold">
                        <span id="monto{{ $factura->id_factura }}" name="{{$factura->monto_total}}">${{ number_format($factura->monto_total, 2, ",", ".") }}</span> -
                        <span id="dist{{ $factura->id_factura }}">{{ $factura->distribuidor }}</span>
                    </p>
                    <p>{{ $factura->cliente->nombre." ".$factura->cliente->apellido }}</p>
                    <p id="fecha{{ $factura->id_factura }}" name="{{$factura->fecha_registro}}">{{ date_format(date_create($factura->fecha_registro), "F j, Y") }}</p>
                    <span class="d-none" id="tel{{ $factura->id_factura }}">{{ $factura->telefono }}</span>
                    <p class="font-weight-bold">
                        <span class="{{ $factura->estado == 'aceptado' ? 'text-success' : 'text-danger' }}">{{ $factura->estado }}</span>
                        @if($factura->foto)
                            <a style="cursor: pointer" id="{{ route('factura.archive', ['filename' => basename($factura->foto)]) }}" class="w_open">
                            ( Ver factura: <span class="material-icons" style="font-size: 20px; color: #F28D8D">edit</span> )
                            </a>
                        @endif
                    </p>
                    @if($factura->estado == 'pendiente')
                        <div class="row ml-auto mx-auto feditar" id="{{ $factura->id_factura }}" style="cursor: pointer"
                            data-toggle="modal" data-target="#modalEditFactura">
                            <p class="mx-2 ml-auto  my-auto" style="font-size: 15px; color: #F28D8D">Editar</p>

                            <i class="far fa-edit mx-1 my-auto" style="font-size: 20px; color: #F28D8D"></i>
                        </div>
                    @endif
                </div>
            </div>
        @empty
            <div class="card">
                <div class="card-header">
                    <div class="row mx-auto">
                        <h4 class="card-title" style="color: #F28D8D; font-weight: bold">
                            No hay Facturas Registradas
                        </h4>
                    </div>
                </div>
            </div>
        @endforelse

    </div>
</div>

<!-- MODAL DE EDITAR FACTURA -->
<div class="modal fade" id="modalEditFactura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Factura (<span id="facturaref"></span>)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('factura.updateData') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="col-12 mb-3">
                        <label class="d-block" for="referencia">Referencia o Identificador:</label>
                        <input name="referencia" id="referencia" type="text" value="referencia"
                            class="form-control @error('referencia') is-invalid @enderror">
                        @error('referencia')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12 mb-3">
                        <label for="distribuidor">Distribuidor:</label>
                        <input name="distribuidor" id="distribuidor" type="text" value="Distribuidor"
                            class="form-control @error('distribuidor') is-invalid @enderror">
                        @error('distribuidor')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12 mb-3 d-none">
                        <label for="telefono">Teléfono:</label>
                        <input name="telefono" id="telefono" type="text" value="telefono"
                            class="form-control @error('telefono') is-invalid @enderror">
                        @error('telefono')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12 mb-3">
                        <label for="monto_total">Valor de la Factura (Productos 3M):</label>
                        <input name="monto_total" id="monto_total" type="number" min="0" step=".001" value="monto_total"
                            class="form-control @error('monto_total') is-invalid @enderror">
                        @error('monto_total')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12">
                        <label for="fecha_registro">Fecha de Factura:</label>
                        <input name="fecha_registro" id="fecha_registro" type="date" value=""
                            class="form-control @error('fecha_registro') is-invalid @enderror">
                        @error('fecha_registro')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <input id="idfactura" type="hidden" class="form-control" name="id_factura" value="">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Actualizar Factura</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- FIN MODAL DE EDITAR FACTURA -->

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $( ".w_open" ).click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
        });

        $( ".feditar" ).click(function() {
            var id = $(this).attr("id");
            var ref = $("#ref"+id).text();
            var dist = $("#dist"+id).text();
            var tel = $("#tel"+id).text();
            tel ? "" : tel = 0;
            var monto = $("#monto"+id).attr("name");
            var fecha = $("#fecha"+id).attr("name");

            //Seteo el modal de editar con el input val (idfactura con el id) y el resto de inputs
            $("#facturaref").text(ref);
            $("#referencia").val(ref);
            $("#distribuidor").val(dist);
            $("#telefono").val(tel);
            $("#monto_total").val(monto);
            $("#fecha_registro").val(fecha);
            $("#idfactura").val(id);
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>
