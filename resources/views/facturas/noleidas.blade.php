@extends('layouts.admin.app')

@section('content')

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #F28D8D">Facturas no leidas por la IA</label>


<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table id="example" class="table table-striped " cellspacing="0" width="100%">
                <thead style="background-color: #FFFFFF; color: #F28D8D; font-weight: bold">
                    <tr>
                        <th>ID</th>
                        <th>Fecha</th>
                        <th>Documento</th>
                        <th>Factura</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody class="table-bordered" style="background-color: #fff; color: #000">
                    @foreach($facturas as $factura)
                        <tr>
                            <td class="align-middle">{{ $factura->id_factura_noleida }}</td>
                            <td class="align-middle" id="fecha{{ $factura->id_factura }}" name="{{$factura->fecha_registro}}">
                                {{ date_format(date_create($factura->fecha_registro), "F j, Y") }}
                            </td>
                            <td class="align-middle">{{ basename($factura->foto) }}</td>
                            <td class="align-middle text-center">
                                @if($factura->foto)
                                    <a id="{{ route('factura.archive', ['filename' => basename($factura->foto)]) }}" style="background-color: #00a7d0"  class="fa btn btn-sm text-white mx-auto text-white w_open">
                                        @switch(pathinfo($factura->foto)['extension'])
                                            @case('jpg') @case('jpeg') @case('png') <i style="color:white !important" class="fas fa-image"></i> @break
                                            @case('pdf') <i style="color:white !important" class="fas fa-file-pdf"></i> @break
                                        @endswitch
                                    </a>
                                @else
                                    <span>No posee...</span>
                                @endif
                            </td>
                            <td class="align-middle text-center">
                                <a  onclick="return confirm('Desea eliminar esta factura')" href="{{ route('factura.destroy', $factura->id_factura_noleida) }}"
                                    class="text-danger"><i style="font-size: 2em" class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        //Seteamos la edición de la respuesta
        $( ".w_open" ).click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
        });
    });
</script>
