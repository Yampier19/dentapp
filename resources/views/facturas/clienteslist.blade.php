@extends('layouts.admin.app')

@section('content')




<label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todos los doctores</label>


<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table id="example" class="table table-sm table-striped " cellspacing="2" width="100%">
                <thead style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                    <tr>
                        <th class="font-weight-bold">Nombres</th>
                        <th class="font-weight-bold">Apellidos</th>
                        <th class="font-weight-bold">Dirección</th>
                        <th class="font-weight-bold">Telefono</th>
                        <th class="font-weight-bold">Total estrellas</th>
                        <th class="font-weight-bold">Especialidad</th>
                        <th class="font-weight-bold">Ver facturas</th>
                    </tr>
                </thead>
                <tbody class="table-bordered" style="background-color: #fff; color: #000">
                    @foreach ($clientes as $c)
                        <tr>
                            <td>{{ $c->nombre}}</td>
                            <td>{{ $c->apellido }}</td>
                            <td>{{ $c->direccion }}</td>
                            <td><span class="material-icons" style="font-size: 20px; color: #F28D8D">phone</span> {{ $c->telefono }}</td>
                            <td><span class="material-icons" style="font-size: 20px; color: #F28D8D">star</span> {{ $c->total_estrellas }}</td>
                            <td>{{ $c->especialidad }}</td>
                            <td class="mx-auto text-center" >
                                <a style="color: #fff; background-color: #51A2A7;" href="{{ route('factura.listado', ['id' => $c->id_cliente, 'nombre' => $c->nombre.' '.$c->apellido]) }}"
                                            class="btn btn-sm btn-secondary mx-auto">
                                            <span class="material-icons" style="font-size: 20px; ">visibility</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>



@endsection


<!--

<div class="container mt-5">
    <div class="row mx-auto">
        <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

            <div class="card-header mt-3 mb-4" style="background-color: #e7344c;">
                <h5 class="text-white">Listado de clientes</h5>
            </div>

            <table class="table table-bordered " id="table1">
                <thead>
                    <tr>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Total estrellas</th>
                        <th>Especialidad</th>
                        <th>Ver Facturas</th>
                    </tr>
                </thead>
                <tbody class="table-light">
                    @foreach ($clientes as $c)
                        <tr>
                            <td>{{ $c->nombre}}</td>
                            <td>{{ $c->apellido }}</td>
                            <td>{{ $c->direccion }}</td>
                            <td>{{ $c->telefono }}</td>
                            <td>{{ $c->total_estrellas }}</td>
                            <td>{{ $c->especialidad }}</td>
                            <td>
                                <form action="" method="post">
                                    <div class="row">
                                        <a href="{{ route('factura.listado', ['id' => $c->id_cliente, 'nombre' => $c->nombre.' '.$c->apellido]) }}"
                                            class="btn btn-sm btn-secondary mx-auto">
                                            <i class="fas fa-clipboard"></i>
                                        </a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

-->

