@extends('layouts.admin.app')

@section('content')
    @if (Session::has('edit'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Factura actualizada correctamente',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif

    @if (Session::has('error'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: '{{ Session::get('error') }}',
            })
        </script>
        {{-- <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div> --}}
    @endif
    @if ($errors->any())
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            Error en la validación de los datos, por favor corrija los datos...
        </div>
    @endif

    <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #F28D8D">Listado
        de facturas</label>


    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="facturas" class="table table-sm table-striped " cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th class="font-weight-bold">Referencia</th>
                            <th class="font-weight-bold">Fecha Subida</th>
                            <th class="font-weight-bold">Cliente</th>
                            <th class="font-weight-bold">Distribuidor</th>
                            <th class="font-weight-bold">Teléfono</th>
                            <th class="font-weight-bold">Monto</th>
                            <th class="font-weight-bold">Puntos</th>
                            <th class="font-weight-bold">Fecha Factura</th>
                            <th class="font-weight-bold">Campaña</th>
                            <th class="font-weight-bold">Archivo</th>
                            <th class="font-weight-bold">Estado</th>
                            <th class="font-weight-bold">Descripción</th>
                            <th class="font-weight-bold">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-light text-center" style="background-color: #fff; color: #000">
                        @foreach ($facturas as $factura)
                            <tr>
                                <td id="ref{{ $factura->id_factura }}">{{ $factura->referencia }}</td>
                                <td>{{ $factura->created_at }}</td>
                                {{-- <td>{{$factura->cliente->nombre}} {{$factura->cliente->apellido}}</td> --}}
                                <td>
                                    <p>
                                        {{ isset($factura->cliente->nombre) ? $factura->cliente->nombre : 'Sin datos' }}
                                    {{ isset($factura->cliente->apellido) ? $factura->cliente->apellido : 'Sin datos' }}
                                    </p>

                                </td>
                                <td id="dist{{ $factura->id_factura }}">{{ $factura->distribuidor }}</td>

                                <td id="tel{{ $factura->id_factura }}">{{ $factura->telefono }}</td>
                                <td id="monto{{ $factura->id_factura }}" name="{{ $factura->monto_total }}">
                                    ${{ number_format($factura->monto_total, 2, ',', '.') }}</td>
                                <td>{{ number_format($factura->puntos) }}</td>

                                <td id="fecha{{ $factura->id_factura }}" name="{{ $factura->fecha_registro }}">
                                    {{ $factura->fecha_registro }}
                                </td>

                                <td class="{{ $factura->navidad ? 'text-success' : '' }}">
                                    @if ($factura->navidad)
                                        {{ $factura->navidad->tipo == 'normal' ? 'Navidad' : 'Navidad (Registro)' }}
                                    @else
                                        Factura Normal
                                    @endif
                                </td>

                                <td>
                                    @if ($factura->foto)
                                        <a id="{{ route('factura.archive', ['filename' => basename($factura->foto)]) }}"
                                            class="text-primary mx-auto w_open">
                                            @switch(pathinfo($factura->foto)['extension'])
                                                @case('jpg')
                                                @case('jpeg')

                                                @case('png')
                                                @case('pdf')
                                                    <span class="material-icons mr-2" style="font-size: 20px;">picture_as_pdf</span>
                                                @break
                                            @endswitch
                                        </a>
                                    @else
                                        <span>No posee...</span>
                                    @endif
                                </td>

                                <td>
                                    <form action="" method="post" class="p-0 m-0">
                                        @if ($factura->estado == 'pendiente')
                                            <div class="dropdown">
                                                <button class="btn btn-sm bg-secondary text-white dropdown-toggle rounded"
                                                    type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                    aria-expanded="false">
                                                    Estado
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a id="{{ route('factura.update', ['id' => $factura->id_factura, 'estado' => 'aceptado']) }}"
                                                        class="dropdown-item rejectInvoice"><i class="fa fa-check mr-2"></i>
                                                        Aceptar</a>
                                                    <a id="{{ route('factura.update', ['id' => $factura->id_factura, 'estado' => 'rechazado']) }}"
                                                        class="dropdown-item rejectInvoice"><i class="fa fa-times mr-2"></i>
                                                        Rechazar</a>
                                                </div>
                                            </div>
                                        @else
                                            <span class="text-capitalize">{{ $factura->estado }}</span>
                                        @endif
                                    </form>
                                </td>
                                <td>
                                    <a id="{{ route('factura.detail', $factura->id_factura) }}"
                                        class="btn btn-sm mx-auto w_open_detail text-white rounded"
                                        style="background: #51A2A7;">
                                        <span class="material-icons mr-2" style="font-size: 20px;">visibility</span>Ver
                                    </a>
                                </td>

                                <td>
                                    @if ($factura->estado == 'pendiente')
                                        <button title="Editar factura" style="background: #51A2A7;"
                                            class="feditar btn btn-sm  btn-info rounded" id="{{ $factura->id_factura }}"
                                            data-toggle="modal" data-target="#modalEditFactura">
                                            <i class="fa fa-edit mt-1 mb-1"></i>
                                        </button>
                                    @else
                                        <button disabled title="No se puede editar" class="feditar btn btn-sm rounded">
                                            <i class="fa fa-edit mt-1 mb-1"></i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- MODAL DE EDITAR FACTURA -->
    <div class="modal modal-edit fade" id="modalEditFactura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar Factura (<span id="facturaref"></span>)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('factura.updateData') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="col-12 mb-3">
                            <label class="d-block" for="referencia">Referencia o Identificador:</label>
                            <input name="referencia" id="referencia" type="text" value="referencia"
                                class="form-control @error('referencia') is-invalid @enderror">
                            @error('referencia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-12 mb-3">
                            <label for="distribuidor">Distribuidor:</label>
                            <input list="datadistribuidores" name="distribuidor" id="distribuidor" type="text"
                                value="Distribuidor" class="form-control @error('distribuidor') is-invalid @enderror">
                            <datalist id="datadistribuidores">
                                @foreach ($distribuidores as $distribuidor)
                                    <option id="{{ $distribuidor->id_distri }}" value='{{ $distribuidor->nombre }}'>
                                    </option>
                                @endforeach
                            </datalist>
                            @error('distribuidor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-12 mb-3">
                            <label for="telefono">Teléfono:</label>
                            <input name="telefono" id="telefono" type="text" value="telefono"
                                class="form-control @error('telefono') is-invalid @enderror">
                            @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-12 mb-3">
                            <label for="monto_total">Valor de la Factura (Productos 3M):</label>
                            <input name="monto_total" id="monto_total" type="number" min="0" step=".001"
                                value="monto_total" class="form-control @error('monto_total') is-invalid @enderror">
                            @error('monto_total')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col-12 mt-3">
                            <label for="">Producto multiplicador: (si vas a editar procura adjuntar todos los
                                productos)</label>
                        </div>
                        <div class="col-12">
                            <input list="dataopciones" id="valorOpciones" class="form-control" name="opciones"
                                placeholder="Selecciona una opción" onchange="cambioOpciones()">
                            <datalist id="dataopciones">
                                @foreach ($productos as $producto)
                                    <?php
                                    switch ($producto->id_distri3M) {
                                        case 1:
                                            $name_distribuidor = 'Dental 83';
                                            break;
                                        case 2:
                                            $name_distribuidor = 'Adental';
                                            break;
                                        case 3:
                                            $name_distribuidor = 'Aldental';
                                            break;
                                        case 4:
                                            $name_distribuidor = 'Audifarma';
                                            break;
                                        case 5:
                                            $name_distribuidor = 'Casa Dental Gabriel Velasquez';
                                            break;
                                        case 6:
                                            $name_distribuidor = 'Casa Odontológica';
                                            break;
                                        case 7:
                                            $name_distribuidor = 'Dental Nader';
                                            break;
                                        case 8:
                                            $name_distribuidor = 'Dentales Antioquía';
                                            break;
                                        case 9:
                                            $name_distribuidor = 'Dentales Market';
                                            break;
                                        case 10:
                                            $name_distribuidor = 'Dentales Padilla';
                                            break;
                                        case 11:
                                            $name_distribuidor = 'Janer Distribuciones';
                                            break;
                                        case 12:
                                            $name_distribuidor = 'Dental Palermo';
                                            break;
                                        case 13:
                                            $name_distribuidor = 'Orbidental';
                                            break;
                                        case 14:
                                            $name_distribuidor = 'Luis Fernando Posada Henao';
                                            break;
                                        case 15:
                                            $name_distribuidor = 'Suministros y Dotaciones Colom';
                                            break;
                                        case 16:
                                            $name_distribuidor = 'Dentales Padilla';
                                            break;
                                        case 17:
                                            $name_distribuidor = 'Bracket';
                                            break;
                                        case 18:
                                            $name_distribuidor = 'Vertice Aliados';
                                            break;
                                        case 19:
                                            $name_distribuidor = 'Dentales y Acrilicos';
                                            break;
                                    }
                                    ?>
                                    <option id="{{ $producto->id_pro }}" value='{{ $producto->distribuidor }}'>
                                        {{ $name_distribuidor . ' - ' . $producto->referencia }}
                                    </option>
                                @endforeach
                            </datalist>
                        </div>

                        <div class="container" id="datalist" style="display: none">
                            <div class="row my-3">
                                <input type="hidden" id="productID">
                                <div class="col-md-4 mx-auto">
                                    <input type="text" id='showId' placeholder="Nombre del producto"
                                        class="form-control" readonly>
                                </div>
                                <div class="col-md-4 mx-auto">
                                    <input type="number" id="count" placeholder="Cantidad" class="form-control">
                                </div>
                                <div class="col-md-3 ml-auto my-auto mr-2">
                                    <a href="javascript:;" class="add-row " style="color: #51A2A7; font-weight: bold"
                                        class="form-control">+ Agregar</a>
                                </div>
                            </div>

                            <table class="table table2">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nombre del producto</th>
                                        <th scope="col">Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody class="tbody2">

                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-3 ml-auto">
                                    <a href="javascript:;" class="delete-row">Eliminar</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <label for="fecha_registro" class="mt-3">Fecha de Factura:</label>
                            <input name="fecha_registro" id="fecha_registro" type="date" value=""
                                class="form-control @error('fecha_registro') is-invalid @enderror">
                            @error('fecha_registro')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <input id="idfactura" type="hidden" class="form-control" name="id_factura" value="">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Actualizar Factura</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- FIN MODAL DE EDITAR FACTURA -->

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

    <script>
        $(document).ready(function() {
            $(".add-row").click(function() {
                var name = $("#showId").val();
                var id = $("#productID").val();
                var cantidad = $("#count").val();
                if (!cantidad) cantidad = 1;

                var markup = "<tr>" +
                    "<input type='hidden' name='productId[]' value='" + id + "'>" +
                    "<input type='hidden' name='productName[]' value='" + name + "'>" +
                    "<input type='hidden' name='productCantidad[]' value='" + cantidad + "'>" +
                    "<td><input type='checkbox' name='record'></td>" +
                    "<td>" + name + "</td>" +
                    "<td>" + cantidad + "</td>" +
                    "</tr>";
                $(".table2 .tbody2").append(markup);
            });

            // Find and remove selected table rows
            $(".delete-row").click(function() {
                $(".table2 .tbody2").find('input[name="record"]').each(function() {
                    if ($(this).is(":checked")) {
                        $(this).parents("tr").remove();
                    }
                });
            });
        });
    </script>

    <script>
        function cambioOpciones() {
            var opciones = $("#valorOpciones").val();

            var idPro = $('#dataopciones').find("option[value='" + opciones + "']").attr('id');

            $("#showId").val(opciones);
            $("#productID").val(idPro);
            //document.getElementById('showId').value = document.getElementById('valorOpciones').value;
            $('#datalist').css({
                'display': 'block'
            })
        }
    </script>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $(".w_open").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
        });

        $(".w_open_detail").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=250,width=1000,height=1000");
        });

        $(".feditar").click(function() {
            //Elimino los tr del modal de los productos
            $(".table2 .tbody2").find('input[name="record"]').each(function() {
                $(this).parents("tr").remove();
            });

            var id = $(this).attr("id");
            var ref = $("#ref" + id).text();
            var dist = $("#dist" + id).text();
            var tel = $("#tel" + id).text();
            var monto = $("#monto" + id).attr("name");
            var fecha = $("#fecha" + id).attr("name");

            //Seteo el modal de editar con el input val (idfactura con el id) y el resto de inputs
            $("#facturaref").text(ref);
            $("#referencia").val(ref);
            $("#distribuidor").val(dist);
            $("#telefono").val(tel);
            $("#monto_total").val(monto);
            $("#fecha_registro").val(fecha);
            $("#idfactura").val(id);
        });

        //ALERTA AL MANDAR EL UPDATE DEL ESTADO DE LA FACTURA
        $('.acceptInvoice').click(function() {
            var url = $(this).attr('id');

            // const swal = Swal.mixin({
            //     customClass: {
            //         confirmButton: 'btn btn-success',
            //         cancelButton: 'btn btn-danger'
            //     },
            //     buttonsStyling: false
            // })

            // swal.fire({
            //     title: 'Estas Segur@ de aceptar la factura?',
            //     icon: 'warning',
            //     showCancelButton: true,
            //     confirmButtonText: 'Si, por favor!',
            //     cancelButtonText: 'No, cancelar!',
            //     reverseButtons: true
            // }).then((result) => {
            // if (result.isConfirmed) {
            // swal.fire(
            //     'Perfecto!',
            //     'Tu actualización de estado sera enviado.',
            //     'success'
            // );
            location.href = url;
            // } else if (result.dismiss === Swal.DismissReason.cancel) {
            // swal.fire(
            //     'Cancelado!',
            //     'Tu cambio de estado no sera procesado.',
            //     'error'
            // )
            // }
            // })
        });

        $('.rejectInvoice').click(function() {
            var url = $(this).attr('id');

            // const swal = Swal.mixin({
            //     customClass: {
            //         confirmButton: 'btn btn-success',
            //         cancelButton: 'btn btn-danger'
            //     },
            //     buttonsStyling: false
            // })

            // swal.fire({
            //     title: 'Estas Segur@ de aceptar la factura?',
            //     icon: 'warning',
            //     showCancelButton: true,
            //     confirmButtonText: 'Si, por favor!',
            //     cancelButtonText: 'No, cancelar!',
            //     reverseButtons: true
            // }).then((result) => {
            //     if (result.isConfirmed) {
            //         swal.fire(
            //             'Perfecto!',
            //             'Tu actualización de estado sera enviado.',
            //             'success'
            //         );
            location.href = url;
            //     } else if (result.dismiss === Swal.DismissReason.cancel) {
            //         swal.fire(
            //             'Cancelado!',
            //             'Tu cambio de estado no sera procesado.',
            //             'error'
            //         )
            //     }
            // })
        });

        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }
    });
</script>
