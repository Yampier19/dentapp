<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('img/apple-icon.png')}}">
    <link rel="icon" type="image/png" href="{{url('img/dentapp/favicon.ico')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        DentApp Web
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{url('datatables/main.css')}}">
    <link href="{{url('icons/css/uicons-regular-rounded.css')}}" rel="stylesheet">
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="{{url('datatables/datatables.min.css')}}"/>
    <!--datables estilo bootstrap 4 CSS-->
    <link rel="stylesheet"  type="text/css" href="{{url('datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}">
    <!-- CSS Files -->
    <link href="{{url('css/app.css')}}" rel="stylesheet" />
</head>

<body id="page-top">
<div class="container mt-5">

    <div class="row mb-5 mx-auto">
        <!-- LISTA DE PUNTOS -->
            <div id="puntos-table" class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">
                    Descripción de factura: {{ $factura->referencia }}
                </label>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped " cellspacing="0" width="100%">
                                <thead style="background-color: #51A2A7; color: #fff">
                                    <tr>
                                        <th>Factura</th>
                                        <th>Descripcion</th>
                                    </tr>
                                </thead>
                                <tbody class="table-bordered text-dark" style="background-color: #fff; color: #000">
                                   <tr>
                                    <td>{{ $factura->referencia }}</td>
                                    <td class="text-left">
                                        <?php
                                            $factura->descripcion = "• ".$factura->descripcion;
                                            $factura->descripcion = preg_replace('/[\n]/', '<br>• ', $factura->descripcion);
                                            echo substr($factura->descripcion, 0, strrpos($factura->descripcion, "•"));
                                        ?>
                                        {{-- $factura->descripcion --}}
                                    </td>
                                   </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <!-- FIN LISTA DE PUNTOS -->
    </div>
</div>
</body>

<!--   Core JS Files   -->
<script src="{{url('js/app.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

<script type="text/javascript"  src="{{url('datatables/datatables.min.js')}}"></script>

<!-- para usar botones en datatables JS -->
<script src="{{url('datatables/Buttons-1.5.6/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('datatables/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{url('datatables/pdfmake-0.1.36/pdfmake.min.js')}}"></script>
<script src="{{url('datatables/pdfmake-0.1.36/vfs_fonts.js')}}"></script>
<script src="{{url('datatables/Buttons-1.5.6/js/buttons.html5.min.js')}}"></script>

<!-- código JS propìo-->
<script type="text/javascript" src="{{url('datatables/main.js')}}"></script>

<script>
$(document).ready(function() {
    $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
            if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                $('.fixed-plugin .dropdown').addClass('open');
            }

        }

        $('.fixed-plugin a').click(function(event) {
            // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
            if ($(this).hasClass('switch-trigger')) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else if (window.event) {
                    window.event.cancelBubble = true;
                }
            }
        });

        $('.fixed-plugin .active-color span').click(function() {
            $full_page_background = $('.full-page-background');

            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            var new_color = $(this).data('color');

            if ($sidebar.length != 0) {
                $sidebar.attr('data-color', new_color);
            }

            if ($full_page.length != 0) {
                $full_page.attr('filter-color', new_color);
            }

            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.attr('data-color', new_color);
            }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            var new_color = $(this).data('background-color');

            if ($sidebar.length != 0) {
                $sidebar.attr('data-background-color', new_color);
            }
        });

        $('.fixed-plugin .img-holder').click(function() {
            $full_page_background = $('.full-page-background');

            $(this).parent('li').siblings().removeClass('active');
            $(this).parent('li').addClass('active');


            var new_image = $(this).find("img").attr('src');

            if ($sidebar_img_container.length != 0 && $(
                    '.switch-sidebar-image input:checked').length != 0) {
                $sidebar_img_container.fadeOut('fast', function() {
                    $sidebar_img_container.css('background-image', 'url("' +
                        new_image + '")');
                    $sidebar_img_container.fadeIn('fast');
                });
            }

            if ($full_page_background.length != 0 && $(
                    '.switch-sidebar-image input:checked').length != 0) {
                var new_image_full_page = $('.fixed-plugin li.active .img-holder').find(
                    'img').data('src');

                $full_page_background.fadeOut('fast', function() {
                    $full_page_background.css('background-image', 'url("' +
                        new_image_full_page + '")');
                    $full_page_background.fadeIn('fast');
                });
            }

            if ($('.switch-sidebar-image input:checked').length == 0) {
                var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr(
                    'src');
                var new_image_full_page = $('.fixed-plugin li.active .img-holder').find(
                    'img').data('src');

                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                $full_page_background.css('background-image', 'url("' +
                    new_image_full_page + '")');
            }

            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
            }
        });

        $('.switch-sidebar-image input').change(function() {
            $full_page_background = $('.full-page-background');

            $input = $(this);

            if ($input.is(':checked')) {
                if ($sidebar_img_container.length != 0) {
                    $sidebar_img_container.fadeIn('fast');
                    $sidebar.attr('data-image', '#');
                }

                if ($full_page_background.length != 0) {
                    $full_page_background.fadeIn('fast');
                    $full_page.attr('data-image', '#');
                }

                background_image = true;
            } else {
                if ($sidebar_img_container.length != 0) {
                    $sidebar.removeAttr('data-image');
                    $sidebar_img_container.fadeOut('fast');
                }

                if ($full_page_background.length != 0) {
                    $full_page.removeAttr('data-image', '#');
                    $full_page_background.fadeOut('fast');
                }

                background_image = false;
            }
        });

        $('.switch-sidebar-mini input').change(function() {
            $body = $('body');

            $input = $(this);

            if (md.misc.sidebar_mini_active == true) {
                $('body').removeClass('sidebar-mini');
                md.misc.sidebar_mini_active = false;

                $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

            } else {

                $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                setTimeout(function() {
                    $('body').addClass('sidebar-mini');

                    md.misc.sidebar_mini_active = true;
                }, 300);
            }

            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function() {
                window.dispatchEvent(new Event('resize'));
            }, 180);

            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function() {
                clearInterval(simulateWindowResize);
            }, 1000);

        });
    });
});
</script>

</html>
