@extends('layouts.admin.app')

@section('content')
<div class="container mt-5">

    @if (Session::has('error'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('error') }}
    </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row ">
        <div class="col-md-12">
            <a href="{{ route('trivia.index') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                <i class="fas fa-undo-alt"></i>
                Volver al inicio
            </a>
        </div>
    </div>

    <div class="col-md-12 card p-0 mx-auto mb-3">
        <div class="row col-md-12 p-3 mx-auto">
            <div class="col-md-12 mx-auto">
            <label for=""><strong>Trivia: {{ $trivia->nombre }} </strong></label><br>

            <!-- MODAL PARA ELIMINAR UNA PREGUNTA -->
                <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">¿Estas seguro de eliminar esta pregunta?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{ route('pregunta.destroy') }}" method="post">
                                @csrf
                                <div class="modal-body">
                                    <h5 id="dpregunta" class="Text-center">Pregunta 1</h5>
                                </div>
                                <input id="didpregunta" type="hidden" class="form-control" name="id_pregunta" value="">
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger">Eliminar pregunta</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <!-- FIN MODAL PARA ELIMINAR UNA PREGUNTA -->

            <!-- PREGUNTAS JUNTO SUS RESPUESTAS -->
                <div class="bs-example">
                    <!-- MODAL + BOTON DE AGREGAR PREGUNTA -->
                        <div class="row col-md-12">
                            <button style="background-color: #F28D8D" class="btn my-3" data-toggle="modal" data-target="#exampleModalPersonal">Agregar pregunta</button>
                        </div>
                        <!-- MODAL DE AGREGAR PREGUNTA -->
                        <div class="modal fade" id="exampleModalPersonal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Crear Pregunta</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{ route('pregunta.store') }}" method="post">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="col-12 my">
                                                <label for="">¿Cual es la pregunta? </label>
                                                <input type="text" class="form-control @error('informacion') is-invalid @enderror"
                                                    name="informacion" placeholder="Texto de la pregunta.">
                                                @error('informacion')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <input type="hidden" class="form-control" name="FK_id_trivia" value="{{ $trivia->id_trivia }}">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn" style="background-color: #F28D8D">Agregar Pregunta</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <!-- FIN MODAL + BUTTON PREGUNTA -->

                    <!-- TABLA DE LAS PREGUNTAS DE LA TRIVIA -->
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Preguntas</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($preguntas as $pregunta)
                            <tr>
                                <td class="align-middle text-center">{{ $pregunta->id_pregunta }}</td>
                                <td>
                                    <div class="accordion" id="accordionExample">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h2 class="mb-0">
                                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne">
                                                        <i class="fa fa-plus"></i>
                                                        <span id="pregunta{{ $pregunta->id_pregunta }}">{{ $pregunta->informacion }}</span> - "Click para ver las respuestas"
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <ol >
                                                        @if(!$pregunta->respuestas->isEmpty())
                                                            @foreach($pregunta->respuestas as $respuesta)
                                                                <li class="mb-1 {{ $respuesta->correcta ? 'alert alert-success' : 'alert alert-danger'  }}">
                                                                    {{ $respuesta->informacion }}
                                                                </li>
                                                            @endforeach
                                                        @else
                                                            <li class="mb-1">No posee respuestas...</li>
                                                        @endif
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <form action="" method="post">
                                        <div class="row my-3">
                                            <a href="{{ route('pregunta.edit', $pregunta->id_pregunta) }}" style="background-color: #64C2C8" class="btn btn-sm mx-auto text-white"><i class="far fa-edit"></i> Editar</a>
                                            <a id="{{ $pregunta->id_pregunta }}" data-toggle="modal" data-target="#exampleModalDelete" class="btn btn-sm btn-danger mx-auto text-white cdelete">
                                                <i class="far fa-trash-alt"></i> Eliminar
                                            </a>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- FIN TABLA DE LAS PREGUNTAS DE LA TRIVIA -->
                </div>
            <!-- FIN PREGUNTAS JUNTO SUS RESPUESTAS -->
            </div>
        </div>
    </div>


</div>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<script>
    $(document).ready(function() {
        // Add minus icon for collapse element which is open by default
        $(".collapse.show").each(function() {
            $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function() {
            $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function() {
            $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });

        //Setear el boton de eliminar al modal de la pregunta para que la elimine
        $( ".cdelete" ).click(function() {
            var id = $(this).attr("id");
            var pregunta = $("#pregunta"+id).text();

            //Seteo el modal de elimar con el input val (didpregunta con el id) y el h5 con el texto
            $("#didpregunta").val(id);
            $("#dpregunta").text(pregunta);
        });
    });
</script>
