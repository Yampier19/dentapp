@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">
            
            <div class="col-md-10 mx-auto">
                <a href="{{ url()->previous() }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                    <i class="fas fa-undo-alt"></i>
                    Volver atras
                </a>
            </div>

            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <form action="{{ route('trivia.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="text-center">
                        <label for="" class="lead"><strong>Registrar Trivia</strong></label><br>
                        <label for="" class="lead">tome en cuenta que esta trivia esta desactivada hasta que se le generen sus respectivas preguntas</label>
                    </div>
                    <hr>
                    <!-- input hidden con el FK de capacitacion -->
                    <input name="FK_id_capacitacion" id="" type="hidden" value="{{ $id_capacitacion }}">
                            
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Nombre:</label>
                            <input name="nombre" id="" type="text"
                                class="form-control @error('nombre') is-invalid @enderror">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Descripcion:</label>
                            <input name="descripcion" id="" type="text" 
                                class="form-control @error('descripcion') is-invalid @enderror">
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Inicio:</label>
                            <input name="fecha_inicio" id="" type="date" value="{{ date('Y-m-d') }}"
                                class="form-control @error('fecha_inicio') is-invalid @enderror">
                            @error('fecha_inicio')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Vencimiento:</label>
                            <input name="fecha_fin" id="" type="date"
                                class="form-control @error('fecha_fin') is-invalid @enderror">
                            @error('fecha_fin')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Puntos:</label>
                            <input name="puntuacion" id="" type="number" min="0" 
                            class="form-control @error('puntuacion') is-invalid @enderror">
                            @error('puntuacion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- <div class="col-md-6">
                            <label for="name_user">Estado:</label>
                            <div class="form-check">
                                <input value="1" class="form-check-input" type="radio" name="estado" id="flexRadioDefault1" checked>
                                <label class="form-check-label" for="flexRadioDefault1">
                                    Trivia Activada
                                </label>
                            </div>
                            <div class="form-check">
                                <input value="0" class="form-check-input" type="radio" name="estado" id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    Trivia Desactivada
                                </label>
                            </div>
                        </div> -->
                    </div>

                    <hr>
                    <div class="row col-md-12 my-3">
                        <input class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;" 
                                type="submit" value="Crear Trivia">
                    </div>
                </form>

            </div>
        </div>
    </div>
    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>