@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">

            <div class="col-md-12 mx-auto">
                <a href="{{ route('trivia.index') }}" class="btn text-white mb-3" style="background-color:#51A2A7">
                    <i class="fas fa-undo-alt"></i>
                    Volver atras
                </a>
            </div>

            <!-- Edición de Trivia -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <div class="card-header mt-3" style="background-color:#51A2A7">
                    <h5 class="text-white my-auto">Editar Trivia</h5>
                </div>

                <form action="{{ route('trivia.update',$trivia->id_trivia) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Nombre:</label>
                            <input name="nombre" id="" type="text" value="{{ old('nombre', $trivia->nombre) }}"
                                class="form-control @error('nombre') is-invalid @enderror">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Descripcion:</label>
                            <input name="descripcion" id="" type="text" value="{{ old('descripcion', $trivia->descripcion) }}"
                                class="form-control @error('descripcion') is-invalid @enderror">
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                    <!-- <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Inicio:</label>
                            <input name="fecha_inicio" id="" type="date" value="{{ $trivia->fecha_inicio }}"
                                class="form-control @error('fecha_inicio') is-invalid @enderror">
                            @error('fecha_inicio')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Vencimiento:</label>
                            <input name="fecha_fin" id="" type="date" value="{{ $trivia->fecha_fin }}"
                                class="form-control @error('fecha_fin') is-invalid @enderror">
                            @error('fecha_fin')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div> -->

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Puntos:</label>
                            <input name="puntuacion" id="" type="number" min="0" value="{{ old('puntuacion', $trivia->puntuacion) }}"
                            class="form-control @error('puntuacion') is-invalid @enderror">
                            @error('puntuacion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Estado:</label>
                            <div class="form-check">
                                <input value="1" class="" type="radio" name="estado" id="flexRadioDefault1" 
                                    {{ old('estado', $trivia->estado) ? "checked" : "" }}>
                                <label class="form-check-label" for="flexRadioDefault1">
                                    Trivia Activada
                                </label>
                            </div>
                            <div class="form-check">
                                <input value="0" class="" type="radio" name="estado" id="flexRadioDefault2"
                                    {{ old('estado', $trivia->estado) ? "" : "checked" }}>
                                <label class="form-check-label" for="flexRadioDefault2">
                                    Trivia Desactivada
                                </label>
                            </div>
                        </div>
                    </div>

                    @if($trainings)
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Capacitación:</label>
                            <select name="FK_id_capacitacion" id="" class="form-control @error('FK_id_capacitacion') is-invalid @enderror" required>
                                <option value="">Selecciona una Opción...</option>
                                @foreach($trainings as $training)
                                    <option value="{{ $training->id_capacitacion }}">{{ $training->nombre }}</option>
                                @endforeach
                            </select>
                            @error('FK_id_capacitacion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    @endif

                    <!-- PREGUNTAS Y RESPUESTAS QUE YA EXISTEN -->
                    @if($trivia->preguntas->count() > 0)
                    <div class="row col-md-12 my-3" id="">
                        <div class="col">
                            <label for="" style="color: #51A2A7;">Preguntas Actuales de la Trivia</label>
                            <br>
                            <!-- PREGUNTAS Y RESPUESTAS EXISTENTES -->
                            @foreach($trivia->preguntas as $pregunta)
                                <label for="name_user">{{$pregunta->informacion}} | Respuestas:</label>
                                <div class="row col-md-12 my-3">
                                    <div class="list-group text-dark">
                                        @foreach($pregunta->respuestas as $respuesta)
                                            <a class="list-group-item list-group-item-action disabled">
                                                {{$respuesta->informacion}} 
                                                <span class='{{$respuesta->correcta ? "text-success" : "text-danger"}}'>
                                                    ({{$respuesta->correcta ? "Correcta" : "Incorrecta"}})
                                                </span>
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                            <!-- PREGUNTAS Y RESPUESTAS EXISTENTES -->
                        </div>
                    </div>
                    @endif
                    <!-- FIN PREGUNTAS Y RESPUESTA EXISTENTES -->

                    <hr>
                    <div class="row col-md-12 my-3">
                        <div class="col">
                            <label style="font-size: 12px; color: #51A2A7;  margin-bottom: 0px;">Agregar / Editar Preguntas<br><br>Selecciona si deseas Hacerlo?</label>
                            <select name="selectPregunta" id="inputSelect" class="form-control" required="required">
                                <option value="" selected disabled>Seleccionar...</option>
                                <option value="1">Si</option>
                                <option value="No">No</option>
                                <!-- <option value="2">Falso - Verdadero</option>
                                <option value="3">Fecha</option>
                                <option value="4">Numero</option>
                                <option value="5">Abierta</option> -->
                            </select>
                        </div>
                    </div>
                    
                    <!-- OPCION MULTIPLE -->
                    <div style="display: none" class="divOculto" id="div1">
                        <div class="row field_wrapper mx-auto">
                            <!-- ANEXAMOS LAS PREGUNTAS Y SUS RESPUESTAS DE SER SI TIENEN CLARO -->
                            <?php $cantidad = 0; ?>
                            @foreach($trivia->preguntas as $pregunta)
                                <?php $cantidad++; ?>
                                <div class="row col-md-12 my-3" id="pregunta-{{ $cantidad }}">
                                    <div class="col-md-9 my-auto">
                                        <input type="text" name="multiple[{{ $cantidad }}][pregunta]" 
                                            class="form-control" value="{{ $pregunta->informacion }}" required>
                                    </div>
                                    <div class="col-md-3">
                                        <?php $cantidad_resp = 0; ?>
                                        @foreach($pregunta->respuestas as $respuesta)
                                        <div class="row" id="respuesta-{{ $cantidad }}{{ $cantidad_resp }}">
                                            <div class="form-check d-flex justify-content-end">
                                                <label class="form-check-label my-auto">
                                                <input class="form-check-input" name="multiple[{{ $cantidad }}][{{ $cantidad_resp }}][correcta]" 
                                                    type="checkbox" value="1" {{ $respuesta->correcta ? "checked" : "" }}><span class="form-check-sign">
                                                <span class="check"></span></span></label>
                                                <input name="multiple[{{ $cantidad }}][{{ $cantidad_resp }}][texto]" 
                                                    class="form-control" required placeholder="Opcion de respuesta" value="{{ $respuesta->informacion }}">
                                            </div>
                                        </div>
                                        <?php $cantidad_resp++; ?>
                                        @endforeach
                                        <div class="row d-flex justify-content-end">
                                            <a id="opcionadd-{{ $cantidad }}" name="{{ ($cantidad_resp-1) }}" class="mr-2 my-3" style="color: #F28D8D" onclick="addOption({{ $cantidad }})"><i class="fas fa-plus mr-2"></i></a>
                                            <a id="opcionrem-{{ $cantidad }}" name="{{ ($cantidad_resp-1) }}" class="mr-2 my-3" style="color: #F28D8D" onclick="removeOption({{ $cantidad }})"><i class="fas fa-trash mr-2"></i></a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <!-- FIN ANEXO DE PREGUNTAS Y RESPUESTAS -->
                        </div>

                        <div class="row ml-2 d-flex justify-content">
                            <a href="javascript:void(0);" class="add_button mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-plus mr-2"></i><!--  --></a>
                            <a href="javascript:void(0);" class="remove_button mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-trash mr-2"></i><!-- Eliminar Pregunta --></a>
                        </div>
                    </div>

                    <hr>
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 mx-auto">
                            <button onclick="return(confirm('¿Desea editar esta trivia?'))" type="submit" class="form-control text-white" style="background-color:#51A2A7; cursor: pointer"><b>Editar Trivia</b></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
    <input type="hidden" id="preguntasCant" value="{{ $cantidad }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<!-- OPCION MULTIPLE JS -->
<script type="text/javascript">
    $(document).ready(function() {
        
        var maxField = 6; //Input fields increment limitation
        var x = $('#preguntasCant').val();//0; //Initial field counter is 1
        var addButton = $('.add_button'); //Add button selector
        var removeButton = $('.remove_button'); //Delete button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        
        $(addButton).click(function() { //Once add button is clicked
            if (x < maxField) { //Check maximum number of input fields
                x++; //Increment field counter
                console.log(x);
                var j = 0;
        var fieldHTML = '<div class="row col-md-12 my-3" id="pregunta-'+x+'">'+
                            '<div class="col-md-9 my-auto">'+
                                '<input type="text" name="multiple['+x+'][pregunta]" class="form-control" placeholder="Pregunta" required>'+
                            '</div>'+
                            '<div class="col-md-3">'+
                                '<div class="row" id="respuesta-'+x+(j)+'">'+
                                    '<div class="form-check d-flex justify-content-end">' +
                                        '<label class="form-check-label my-auto">' +
                                        '<input class="form-check-input" name="multiple['+x+']['+j+'][correcta]" type="checkbox" value="1" ><span class="form-check-sign">' +
                                        '<span class="check"></span></span></label>' +
                                        '<input name="multiple['+x+']['+j+'][texto]" class="form-control" required placeholder="Opcion de respuesta">' +
                                    '</div>'+
                                '</div>'+
                                '<div class="row" id="respuesta-'+x+(j+1)+'">'+
                                    '<div class="form-check d-flex justify-content-end">' +
                                        '<label class="form-check-label my-auto">' +
                                        '<input class="form-check-input" name="multiple['+x+']['+(j+1)+'][correcta]" type="checkbox" value="1" ><span class="form-check-sign">' +
                                        '<span class="check"></span></span></label>' +
                                        '<input name="multiple['+x+']['+(j+1)+'][texto]" class="form-control" required placeholder="Opcion de respuesta">' +
                                    '</div>'+
                                '</div>'+
                                '<div class="row" id="respuesta-'+x+(j+2)+'">'+
                                    '<div class="form-check d-flex justify-content-end">' +
                                        '<label class="form-check-label my-auto">' +
                                        '<input class="form-check-input" name="multiple['+x+']['+(j+2)+'][correcta]" type="checkbox" value="1" ><span class="form-check-sign">' +
                                        '<span class="check"></span></span></label>' +
                                        '<input name="multiple['+x+']['+(j+2)+'][texto]" class="form-control" required placeholder="Opcion de respuesta">' +
                                    '</div>'+
                                '</div>'+
                                '<div class="row d-flex justify-content-end">'+
                                    '<a id="opcionadd-'+x+'" name="'+(j+2)+'" class="mr-2 my-3" style="color: #F28D8D" onclick="addOption('+x+')"><i class="fas fa-plus mr-2"></i></a>'+
                                    '<a id="opcionrem-'+x+'" name="'+(j+2)+'" class="mr-2 my-3" style="color: #F28D8D" onclick="removeOption('+x+')"><i class="fas fa-trash mr-2"></i></a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'; 
                $(wrapper).append(fieldHTML); // Add field html
            }
        });
        $(removeButton).click(function(){ //Once remove button is clicked
            //e.preventDefault();
            //$(this).parent('div').remove(); //Remove field html
            $('#pregunta-'+x).remove();
            x > 0 ? x-- : ""; //Decrement field counter
        });
    });

    function addOption(x){
        var j = $('#opcionadd-'+x).attr('name');
        var z = parseInt(j)+1;
        var optionHtml = '<div class="row" id="respuesta-'+x+z+'">'+
                            '<div class="form-check d-flex justify-content-end">' +
                                '<label class="form-check-label my-auto">' +
                                '<input class="form-check-input" name="multiple['+x+']['+z+'][correcta]" type="checkbox" value="1" ><span class="form-check-sign">' +
                                '<span class="check"></span></span></label>' +
                                '<input name="multiple['+x+']['+z+'][texto]" class="form-control" required placeholder="Opcion de respuesta">' +
                            '</div>'+
                        '</div>';

        $('#respuesta-'+x+j).after(optionHtml);
        j++;
        $('#opcionadd-'+x).attr('name',j);
        $('#opcionrem-'+x).attr('name',j);
    }

    function removeOption(x){
        var j = $('#opcionadd-'+x).attr('name');
        if(j > 1){
            $('#respuesta-'+x+j).remove();
            j--;
            $('#opcionadd-'+x).attr('name',j);
            $('#opcionrem-'+x).attr('name',j);
        }
    }
</script>

<script>
    $(function() {

        $("#inputSelect").on('change', function() {

            var selectValue = $(this).val();
            switch (selectValue) {

                case "1":
                    $("#div1").show();
                    $("#div2").hide();
                    $("#div3").hide();
                    $("#div4").hide();
                    $("#div5").hide();
                    break;

                case "2":
                    $("#div1").hide();
                    $("#div2").show();
                    $("#div3").hide();
                    $("#div4").hide();
                    $("#div5").hide();
                    break;

                case "3":
                    $("#div1").hide();
                    $("#div2").hide();
                    $("#div3").show();
                    $("#div4").hide();
                    $("#div5").hide();
                    break;

                case "4":
                    $("#div1").hide();
                    $("#div2").hide();
                    $("#div3").hide();
                    $("#div4").show();
                    $("#div5").hide();
                    break;

                case "5":
                    $("#div1").hide();
                    $("#div2").hide();
                    $("#div3").hide();
                    $("#div4").hide();
                    $("#div5").show();
                    break;
                
                default: 
                    $("#div1").hide();
                    $("#div2").hide();
                    $("#div3").hide();
                    $("#div4").hide();
                    $("#div5").hide();
                    break;
            }

        }).change();

    });
</script>

<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>