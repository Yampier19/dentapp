@extends('layouts.admin.app')

@section('content')
    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <label for="" class="lead" style="font-weight: bold; margin-top: 5%; color: #F28D8D">
        Listado de trivias
    </label><!-- <br>
    <label class="" style="font-weight: bold; margin-bottom: 5%; color: #51A2A7">
        Pulsa sobre la fila de la trivia para ver las preguntas y respuestas que tiene...
    </label> -->

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped" id="example" cellspacing="0" width="100%">
                    <thead style="background-color: #F28D8D; color: #fff">
                        <tr>
                            <th>Capacitación</th>
                            <th>Trivia</th>
                            <th>Descripción</th>
                            <th>Puntuacion</th>
                            <th>Estado</th>
                            <th>N° de preguntas</th>
                            <th>N° de respuestas</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach ($trivias as $trivia)
                            <tr id="{{ route('trivia.detail', $trivia->id_trivia) }}" class="table_click">
                                <td class="fila font-weight-bold">{{ $trivia->capacitacion ? $trivia->capacitacion->nombre : "No tiene" }}</td>
                                <td class="fila">{{ $trivia->nombre }}</td>
                                <td class="fila">{{ $trivia->descripcion }}</td>
                                <td class="fila"><span class="material-icons mr-2">star</span> {{ $trivia->puntuacion }}</td>
                                <td class="fila">
                                    <span class='{{ $trivia->estado ? "text-success" : "text-danger" }}'>
                                        {{ $trivia->estado ? "Activa" : "Desactiva" }}
                                    </span>
                                </td>
                                <td class="fila text-center">{{ $trivia->preguntas->count() }}</td>
                                <?php $resp = 0; ?>
                                @foreach($trivia->preguntas as $pregunta) 
                                    <?php $resp += $pregunta->respuestas->count(); ?>
                                @endforeach
                                <td class="fila text-center">{{ $resp }}</td>
                                <td>
                                    <div class="row container justify-content-end">
                                        <a href="{{ route('trivia.edit', $trivia->id_trivia) }}">
                                            <button class="btn" style="background-color: #51A2A7">
                                                <span class="material-icons mr-2">edit</span>Ver y Editar 
                                            </button>
                                        </a>
                                        <!-- <button class="btn" onclick="alert()" style="background-color: #da3a3a">
                                            <i class="far fa-trash mr-2"></i>Eliminar
                                        </button> -->
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<style>
    button#enviar:disabled {
        color: #ccc;
    }
</style>
@endsection