@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <h1 class="mb-5">¿Deseas eliminar la siguiente trivia? </h1>
        <h4>Nombre: </h4>
        <p>{{ $trivia->nombre }}</p>
        <h4>Descripcion: </h4>
        <p>{{ $trivia->descripcion }}</p>
        <h4>Puntuación: </h4>
        <p>{{ $trivia->puntuacion }}</p>
        <h4>Fecha de Inicio: </h4>
        <p>{{ $trivia->fecha_inicio }}</p>
        <h4>Fecha de Vencimiento: </h4>
        <p>{{ $trivia->fecha_fin }}</p>

        <form method="post" enctype="multipart/form-data" action="{{ route('trivia.destroy', $trivia->id_trivia) }}">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{route('trivia.index')}}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
@endsection
