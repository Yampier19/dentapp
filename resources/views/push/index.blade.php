@extends('layouts.admin.app')

@section('content')

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

@if (Session::has('error'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('error') }}
    </div>
@endif

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row-reverse">
                <div class="col d-flex justify-content-center mb-3">
                    <label for="" class="h3">Push Notification</label>
                </div>
                <div class="col">
                    <form action="{{ route('push.send') }}" method="post">
                        @csrf
                        <div class="row-reverse">
                            <div class="col mb-3">
                                <label for="">Selecciona un usuario o todos:</label>
                                <select class="js-example-basic-multiple fav_clr form-control @error('usuarios') is-invalid @enderror" 
                                        name="usuarios[]" multiple="multiple">
                                    <option value="all">Seleccionar todos</option>
                                    @foreach($usuarios as $usuario)
                                        <option value="{{ $usuario->id_cliente }}">
                                            {{ $usuario->nombre." ".$usuario->apellido }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('usuarios')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <input type="text" class="form-control @error('titulo') is-invalid @enderror" 
                                        placeholder="Titulo de la notificación" name="titulo" value="{{ old('titulo') }}">
                                @error('titulo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col my-3">
                                <textarea name="texto" class="form-control py-4 @error('texto') is-invalid @enderror" 
                                        placeholder="Descripción de la notificación" maxlength="255">{{ old('texto') }}</textarea>
                                @error('texto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col my-3 d-flex justify-content-center">
                                <button type="submit" class="btn btn-success">Enviar Push Notification</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

<script>
  $(document).ready(function() {
    $('.js-example-basic-multiple').select2();

    $('.fav_clr').on("select2:select", function (e) { 
        var data = e.params.data.text;

        if(data=='Seleccionar todos'){
            $(".fav_clr > option").prop("selected","selected");
            $(".fav_clr").trigger("change");
        }
    });

    if($('#showerror').val() == 1){
        $('.invalid-feedback').css('display','block');
        $('#exampleModal').modal('show');
    }
});
</script>

@endsection