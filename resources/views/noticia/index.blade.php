@extends('layouts.admin.app')

@section('content')

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif


    <div class="row-reverse">
        @if ($noticias->count())
            @foreach ($noticias as $noticia)
                <div class="col">
                    <div class="card p-0">
                        <div class="row p-0 m-0">
                            <div class="col-md-2 p-0">
                                <div style="width: 100%; height: 200px">

                                    <img src="{{ route('noticias.preview', ['filename' => basename($noticia->imagen)]) }}" alt="" width="100%" height="100%">

                                </div>

                            </div>
                            <div class="col-md-10 p-3">
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="">{{ $noticia->nombre }}</label>
                                    </div>
                                    <div class="col">
                                        <div class="row-reverse">
                                            <div class="col">
                                                <label for="">{{ $noticia->tipo }}</label>
                                            </div>
                                            <div class="col">
                                                <label for="">{{ $noticia->estado ? 'Activo' : 'Inactivo' }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p>{{ $noticia->informacion }}</p>
                                <div class="row">
                                    <div class="col">
                                        <div class="row d-flex justify-content-end mr-3">
                                            <a href="{{ url('informativo/inicio/listnoticias') }}" class="text-secondary">Ver opciones</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <script>
                $(document).ready(function() {
                    $('#alerta').modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                    $('#alerta').modal('show');
                })
            </script>
            <!-- Button trigger modal -->
            <!-- Modal -->
            <div class="modal fade" id="alerta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body p-5">
                            <div class="d-flex justify-content-center">
                                <i style="font-size:4em" class="fa fa-exclamation-triangle"></i>
                            </div>
                            <h4 class="text-center">No hay premios registrados</h4>
                            <div class="d-flex justify-content-center">
                            <button class="btn btn-round ml-3" style="background-color: #F28D8D" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <i class="material-icons">add</i>CREAR NOTICIA
                            </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif


        <nav aria-label="Page navigation example">
            <?php $limite_paginas = 10; ?>

            @if ($noticias->lastPage() > 1)
                <ul class="pagination justify-content-end">
                    <li class="page-item {{ $noticias->currentPage() == 1 ? ' disabled' : '' }}">
                        <a class="page-link" href="{{ $noticias->url(1) }}">Primera</a>
                    </li>
                    @for ($i = 1; $i <= $noticias->lastPage(); $i++)
                        <?php
                        $half_total_links = floor($limite_paginas / 2);
                        $from = $noticias->currentPage() - $half_total_links;
                        $to = $noticias->currentPage() + $half_total_links;
                        if ($noticias->currentPage() < $half_total_links) {
                            $to += $half_total_links - $noticias->currentPage();
                        }

                        if ($noticias->lastPage() - $noticias->currentPage() < $half_total_links) {
                            $from -= $half_total_links - ($noticias->lastPage() - $noticias->currentPage()) - 1;
                        }
                        ?>
                        @if ($from < $i && $i < $to)
                            <li class="page-item {{ $noticias->currentPage() == $i ? 'active disabled' : '' }}">
                                <a class="page-link" href="{{ $noticias->url($i) }}">{{ $i }}</a>
                            </li>
                        @endif
                    @endfor
                    <li class="page-item {{ $noticias->currentPage() == $noticias->lastPage() ? ' disabled' : '' }}">
                        <a class="page-link" href="{{ $noticias->url($noticias->lastPage()) }}">Ultima</a>
                    </li>
                </ul>
            @endif
        </nav>
        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                            Creacion de noticias</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form onsubmit="return checkSubmit();" action="{{ route('noticias.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="col">
                                    <input name="nombre" id="" type="text" placeholder="Nombre" value="{{ old('nombre') }}" class="form-control @error('nombre') is-invalid @enderror">
                                    @error('nombre')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col-12 mb-2">
                                    <label for="">Tipo de Noticia:</label>
                                    <input list="nottipo" name="tipo" value="{{ old('tipo') }}" class="form-control @error('tipo') is-invalid @enderror" />
                                    <datalist id="nottipo">
                                        @foreach ($tipos as $tipo)
                                            <option value="{{ $tipo->nombre }}">
                                        @endforeach
                                    </datalist>
                                    @error('tipo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <textarea name="informacion" id="" type="text" placeholder="Informacion" class="form-control @error('informacion') is-invalid @enderror">{{ old('informacion') }}</textarea>
                                    @error('informacion')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <label for="name_user">Fecha de Inicio:</label>
                                    <input name="fecha_inicio" id="" type="date" value="{{ old('fecha_inicio', date('Y-m-d')) }}" class="form-control @error('fecha_inicio') is-invalid @enderror">
                                    @error('fecha_inicio')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <div class="row mx-auto">
                                        <label for="name_user mx-auto">Fecha de Vencimiento:</label>
                                        <div class="form-check ml-auto">
                                            <label class="form-check-label ">
                                                ¿Noticia sin vencimiento?
                                                <input class="form-check-input" type="checkbox" value="1" id="checkboxDetermine" name="withoutDate">
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <input name="fecha_fin" id="checkboxConditioned" type="date" value="{{ old('fecha_fin') }}" class="form-control @error('fecha_fin') is-invalid @enderror">
                                    @error('fecha_fin')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <script>
                                        var determine = document.getElementById("checkboxDetermine");
                                        var disableCheckboxConditioned = function() {
                                            if (determine.checked) {
                                                document.getElementById("checkboxConditioned").disabled = true;
                                                document.getElementById("checkboxConditioned").value = null;
                                            } else {
                                                document.getElementById("checkboxConditioned").disabled = false;
                                            }
                                        }
                                        determine.onclick = disableCheckboxConditioned;
                                        disableCheckboxConditioned();
                                    </script>
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <label for="">Subir documento de noticia:</label>
                                    <input type="file" name="documento" accept=".jpg,.jpeg,.png,.pdf" class="form-control @error('documento') is-invalid @enderror" id="">
                                    @error('documento')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <label for="">Subir Imagen preview de noticia:</label>
                                    <input type="file" name="imagen" accept=".jpg,.jpeg,.png" class="form-control @error('imagen') is-invalid @enderror" id="">
                                    @error('imagen')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <label for="">Estado:</label><br>
                                    <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="estado" checked id="inlineRadio1" value="1"> Noticia Activada
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-radio form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="estado" id="inlineRadio1" value="0"> Noticia Desactivada
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="modal-footer">
                        <input class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;" type="submit" value="CREAR NOTICIA">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<style>
    .input-container {
        padding-bottom: 1em;
    }

    .left-inner-addon {
        position: relative;
    }

    .left-inner-addon input {
        padding-left: 35px !important;
    }

    .left-inner-addon i {
        position: absolute;
        padding: 11px 12px;
        pointer-events: none;
    }

    img {
        vertical-align: middle;
    }
</style>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('#exampleModal').modal('show');
        }
    });
</script>
