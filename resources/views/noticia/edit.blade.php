@extends('layouts.admin.app')
@push('scripts')
<script src="{{asset('js/noticia/noticia.js')}}"></script>
@endpush
@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <script>
                Swal.fire({
                    icon: 'success',
                    title: 'Noticia Actualizada',
                    showConfirmButton: false,
                    timer: 1000
                })
            </script>
        @endif

        <div class="row mb-5 mx-auto">
            <!-- Editar Noticia -->
            <div class="col-md-8 m-auto card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <div class="card-header mt-3" style="background-color:#64C2C8">
                    <h3 class="text-white mt-3"><span class="iconify mr-2" data-icon="akar-icons:edit"></span>Editar Noticia</h3>
                </div>
                <div class="p-5 ">
                    <div class="d-flex justify-content-center mt-3 mb-5">
                        <img style="width:200px;max-height:200px;border-radius:100%" src="{{ route('noticias.preview', ['filename' => basename($noticia->imagen)]) }}">
                    </div>
                    <br>
                    <form action="{{ route('noticias.update', $noticia->id_noticia) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row col-md-12 my-4">
                            <div class="col-md-6">
                                <label for="name_user">Nombre:</label>
                                <input required name="nombre" id="nombre" type="text" value="{{ old('nombre', $noticia->nombre) }}"
                                    class="form-control @error('nombre') is-invalid @enderror">
                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input type="hidden" name="id" id="id" value="{{$noticia->id_noticia}}">
                            </div>
                            <div class="col-md-6">
                                <label for="name_user">Información:</label>
                                <textarea required name="informacion" id="informacion" type="text" class="form-control @error('informacion') is-invalid @enderror">{{ old('informacion', $noticia->informacion) }}</textarea>
                                @error('informacion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row col-md-12 mb-3">
                            <div class="col-md-6">
                                <label for="">Tipo de Noticia:</label>
                                <input id="tipo" required list="nottipo" name="tipo" value="{{ old('tipo', $noticia->tipo) }}"
                                    class="form-control @error('tipo') is-invalid @enderror" />
                                <datalist id="nottipo">
                                    @foreach ($tipos as $tipo)
                                        <option value="{{ $tipo->nombre }}">
                                    @endforeach
                                </datalist>
                                @error('tipo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="name_user">Estado:</label>
                                <div class="form-check">
                                    <input id="estado" value="1" class="" type="radio" name="estado"
                                        {{ old('estado', $noticia->estado) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="estado">
                                        Noticia Activada
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input value="0" class="" type="radio" name="estado" id="estado"
                                        {{ old('estado', $noticia->estado) ? '' : 'checked' }}>
                                    <label class="form-check-label" for="estado">
                                        Noticia Desactivada
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row col-md-12 mb-3">
                            <div class="col-md-6">
                                <label for="name_user">Fecha de Inicio:</label>
                                <input min="{{$noticia->fecha_inicio}}" required name="fecha_inicio" id="fecha_inicio" type="date" value="{{ old('fecha_inicio', $noticia->fecha_inicio) }}"
                                    class="form-control @error('fecha_inicio') is-invalid @enderror">
                                @error('fecha_inicio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <div class="row mx-auto">
                                    <label for="name_user mx-auto">Fecha de Vencimiento:</label>
                                    <div class="form-check ml-auto">
                                        <label class="form-check-label ">
                                            ¿Noticia sin vencimiento?
                                            <input class="form-check-input" type="checkbox" value="1"
                                                id="withoutDate" name="withoutDate" {{ $noticia->fecha_fin ? '' : 'checked' }}>
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <input min=""  name="fecha_fin" id="fecha_fin" type="date" value="{{ old('fecha_fin', $noticia->fecha_fin) }}"
                                    class="form-control @error('fecha_fin') is-invalid @enderror">
                                @error('fecha_fin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <script>
                                    var determine = document.getElementById("withoutDate");
                                    if (determine.checked) {
                                        document.getElementById("fecha_fin").disabled = true;
                                        document.getElementById("fecha_fin").value = null;
                                    }
                                    var disablefecha_fin = function() {
                                        if (determine.checked) {
                                            document.getElementById("fecha_fin").disabled = true;
                                            document.getElementById("fecha_fin").value = null;
                                        } else {
                                            document.getElementById("fecha_fin").disabled = false;
                                        }
                                    }
                                    determine.onchange = disablefecha_fin;
                                    disablefecha_fin();
                                </script>
                            </div>
                        </div>

                        <div class="row col-md-12 mb-3">
                            <div class="col-md-6">
                                <label for="">Subir documento de noticia:</label>
                                <div class="custom-file mb-3">
                                    <input type="file" class="custom-file-input @error('documento') is-invalid @enderror"
                                        id="documento" name="documento" accept=".jpg,.jpeg,.png,.pdf" value="">
                                    <label class="custom-file-label" for="customFile">Escoger un Documento</label>
                                    @error('documento')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Subir Imagen preview de noticia:</label>
                                <div class="custom-file mb-3">
                                    <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror"
                                        id="imagen" name="imagen" accept=".jpg,.jpeg,.png" value="">
                                    <label class="custom-file-label" for="customImage">Escoger una Imagen</label>
                                    @error('imagen')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            {{-- <button id="updatenew" type="button" class="btn text-white" style="background-color: #64C2C8">Editar Noticia</button> --}}
                            <button type="submit" class="btn text-white" style="background-color: #64C2C8">Editar Noticia</button>

                            <a href="{{url('informativo/inicio/listnoticias') }}" class="btn" style="background-color:rgb(202, 197, 197)"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }
    });
</script>
