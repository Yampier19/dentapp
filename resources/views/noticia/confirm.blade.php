@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif
        <div class="card p-5 col-sm-8 m-auto">

            <h2 class="mb-5 text-center">¿Deseas eliminar la siguiente noticia? </h2>
            <div class="d-flex justify-content-center">
            <p><img style="width:200px;max-height:200px;border-radius: 100%" src="{{ route('noticias.preview', ['filename' => basename($noticia->imagen)]) }}"></p>
            </div>
            <h4 class="font-weight-bold">Fecha Inicio: </h4>
            <p>{{ date_format(date_create($noticia->fecha_inicio), 'F j, Y') }}</p>
            <h4>Fecha Fin: </h4>
            <p>{{ date_format(date_create($noticia->fecha_fin), 'F j, Y') }}</p>
            <h4 class="font-weight-bold">Nombre: </h4>
            <p>{{ $noticia->nombre }}</p>
            <h4 class="font-weight-bold">Información: </h4>
            <p>{{ $noticia->informacion }}</p>
            <h4 class="font-weight-bold">Tipo: </h4>
            <p>{{ $noticia->tipo }}</p>
            <h4 class="font-weight-bold">Documento: </h4>
            <p>
                @if ($noticia->documento)
                    <a href="{{ route('noticias.documento', ['filename' => basename($noticia->documento)]) }}" target="_blank" class="btn-sm col-2 btn mx-auto">
                        @switch(pathinfo($noticia->documento)['extension'])
                            @case('jpg')
                            @case('jpeg')

                            @case('png')
                                <i class="fas fa-image"></i>
                            @break

                            @case('pdf')
                                <i class="fa fa-file-pdf-o"></i>
                            @break

                            @case('mp4')
                            @case('wm')

                            @case('wmv')
                                <i class="fas fa-video"></i>
                            @break
                        @endswitch
                    </a>
                @else()
                    <span>No posee...</span>
                @endif
            </p>

            <form method="post" enctype="multipart/form-data" action="{{ route('noticias.destroy', $noticia->id_noticia) }}">
                @method('DELETE')
                @csrf
              <div class="modal-footer d-flex justify-content-end mt-4">
                <button type="submit" class="redondo btn col-md-4 mx-auto" style="background-color:#51A2A7">
                    <i class="fas fa-trash-alt"></i> <strong>Eliminar</strong>
                </button>
                <a href="{{ route('noticias.listnoticias') }}" class="btn col-md-4 mx-auto"><strong>Volver</strong></a>
              </div>
        </div>
    </div>
@endsection
