@extends('layouts.admin.app')

@section('content')
    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
            <label for="" class="lead mx-auto"
                style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todas las noticias</label>
        </div>
        <div class="col" style="display: flex;justify-content: flex-end !important">

            <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal"
                data-target=".bd-example-modal-lg">
                <i class="material-icons">add</i>CREAR NOTICIA
            </button>
            <a href="{{ url('informativo/inicio') }}" class="btn btn-round float-lg-right"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
        </div>
    </div>




    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="example" class="table table-striped " cellspacing="0" width="100%">
                    <thead style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                        <tr>
                            <th>Inicio</th>
                            <th>Fin</th>
                            <th>Nombre</th>
                            <th>Información</th>
                            <th>Tipo</th>
                            <th>Estado</th>
                            {{-- <th>Imagen Preview</th> --}}
                            <th>Documento</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered">
                        @foreach ($noticias as $noticia)
                            <tr>
                                <td>{{ date_format(date_create($noticia->fecha_inicio), 'F j, Y') }}</td>
                                <td>{{ $noticia->fecha_fin ? date_format(date_create($noticia->fecha_fin), 'F j, Y') : 'Sin Vencimiento' }}
                                </td>
                                <td>{{ $noticia->nombre }}</td>
                                <td>{{ $noticia->informacion }}</td>
                                <td>{{ $noticia->tipo }}</td>
                                <td>
                                    <span class='badge font-weight-bold {{ $noticia->estado ? 'text-white bg-success' : 'text-white bg-danger' }}'>
                                        {{ $noticia->estado ? 'Activo' : 'Inactivo' }}
                                    </span>
                                </td>
                                {{-- <td class="mx-auto text-center">
                                    <img style="width:100px;max-height:100px;"
                                        src="{{ route('noticias.preview', ['filename' => basename($noticia->imagen)]) }}">
                                </td> --}}
                                <td class="text-center">
                                    @if ($noticia->documento)
                                        <a href="{{ route('noticias.documento', ['filename' => basename($noticia->documento)]) }}"
                                            target="_blank">
                                            @switch(pathinfo($noticia->documento)['extension'])
                                                @case('jpg')
                                                @case('jpeg')

                                                @case('png')
                                                    <span class="material-icons text-danger">image</span>
                                                @break

                                                @case('pdf')
                                                    <span class="material-icons text-danger">picture_as_pdf</span>
                                                @break

                                                @case('mp4')
                                                @case('wm')

                                                @case('wmv')
                                                    <span class="material-icons text-danger">play_arrow</span>
                                                @break
                                            @endswitch
                                        </a>
                                    @else()
                                        <span>No posee...</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('noticias.edit', $noticia->id_noticia) }}" style="color: #51A2A7;"> <span class="material-icons">edit</span></a>
                                    <a href="{{ route('noticias.confirm', $noticia->id_noticia) }}" style="color:tomato;"><span class="material-icons">delete</span></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!-- Modal -->
                <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                                    Creacion de noticias</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body p-5">
                                <form onsubmit="return checkSubmit();" action="{{ route('noticias.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-row">
                                        <div class="col">
                                            <input required name="nombre" id="" type="text" placeholder="Nombre"
                                                value="{{ old('nombre') }}"
                                                class="form-control @error('nombre') is-invalid @enderror">
                                            @error('nombre')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row my-3">
                                        <div class="col-12 mb-2">
                                            <label for="">Tipo de Noticia:</label>
                                            <input required list="nottipo" name="tipo" value="{{ old('tipo') }}"
                                                class="form-control @error('tipo') is-invalid @enderror" />
                                            <datalist id="nottipo">
                                                @foreach ($tipos as $tipo)
                                                    <option value="{{ $tipo->nombre }}">
                                                @endforeach
                                            </datalist>
                                            @error('tipo')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col">
                                            <textarea required name="informacion" id="" type="text" placeholder="Informacion"
                                                class="form-control @error('informacion') is-invalid @enderror">{{ old('informacion') }}</textarea>
                                            @error('informacion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="name_user">Fecha de Inicio:</label>
                                            <input min="{{now()->format('Y-m-d')}}" required name="fecha_inicio" id="" type="date"
                                                value="{{ old('fecha_inicio', date('Y-m-d')) }}"
                                                class="form-control @error('fecha_inicio') is-invalid @enderror">
                                            @error('fecha_inicio')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col">
                                            <div class="row mx-auto">
                                                <label for="name_user mx-auto">Fecha de Vencimiento:</label>
                                                <div class="form-check ml-auto">
                                                    <label class="form-check-label ">
                                                        ¿Noticia sin vencimiento?
                                                        <input class="form-check-input" type="checkbox" value="1"
                                                            id="checkboxDetermine" name="withoutDate">
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <input min="{{now()->addday(1)->format('Y-m-d')}}" name="fecha_fin" id="checkboxConditioned" type="date"
                                                value="{{ old('fecha_fin') }}"
                                                class="form-control @error('fecha_fin') is-invalid @enderror">
                                            @error('fecha_fin')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <script>
                                                var determine = document.getElementById("checkboxDetermine");
                                                var disableCheckboxConditioned = function() {
                                                    if (determine.checked) {
                                                        document.getElementById("checkboxConditioned").disabled = true;
                                                        document.getElementById("checkboxConditioned").value = null;
                                                    } else {
                                                        document.getElementById("checkboxConditioned").disabled = false;
                                                    }
                                                }
                                                determine.onclick = disableCheckboxConditioned;
                                                disableCheckboxConditioned();
                                            </script>
                                        </div>
                                    </div>
                                    <div class="form-row my-3">
                                        <div class="col">
                                            <label for="">Subir documento de noticia:</label>
                                            <input type="file" name="documento" accept=".jpg,.jpeg,.png,.pdf"
                                                class="form-control @error('documento') is-invalid @enderror" id="">
                                            @error('documento')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col">
                                            <label for="">Subir Imagen preview de noticia:</label>
                                            <input type="file" name="imagen" accept=".jpg,.jpeg,.png"
                                                class="form-control @error('imagen') is-invalid @enderror" id="">
                                            @error('imagen')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-row my-3">
                                        <div class="col">
                                            <label for="">Estado:</label><br>
                                            <div class="form-check form-check-radio form-check-inline ml-3 mr-3">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="radio" name="estado" checked
                                                        id="inlineRadio1" value="1"> Noticia Activada
                                                    <span class="circle">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-radio form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="radio" name="estado"
                                                        id="inlineRadio1" value="0"> Noticia Desactivada
                                                    <span class="circle">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="modal-footer">
                                <input class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;"
                                    type="submit" value="CREAR NOTICIA">
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('#exampleModal').modal('show');
        }
    });
</script>
