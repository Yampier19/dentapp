@extends('layouts.admin.app')

@section('content')

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
            <label for="" class="lead"
                style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todos las facturas
                HalloDent</label>
        </div>
        <div class="col" style="display: flex;justify-content: flex-end !important">
            <a href="{{ route('campañas.list') }}">
                <button class="btn btn-round" style="background-color: #51A2A7">
                    <i class="material-icons mr-2">reply</i>IR Al PANEL HALLODENT
                </button>
            </a>
        </div>
    </div>

    
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th>Avatar</th>
                            <th>Nombres</th>
                            <th>Correo</th>
                            <th>Fecha participante</th>
                            <th>Ranking</th>
                            <th>Facturado</th>
                            <th>Campaña</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach ($participantes as $participante)
                            <tr>
                               <td>
                                    <img style="width:100px;max-height:100px; border-radius: 60px"
                                    src="{{ route('cliente.icon', ['filename' => basename($participante->cliente->foto)]) }}">
                               </td>
                               <td>{{ $participante->cliente->nombre." ".$participante->cliente->apellido }}</td>
                               <td>{{ $participante->cliente->correo }}</td>
                               <td>{{ $participante->fecha_participante ? $participante->fecha_participante : "Aun no ha conseguido los 280.000 puntos" }}</td>
                               <td>No. {{ $participante->ranking }}</td>
                               <td>${{ number_format($participante->facturado, 2, ',', '.') }}</td>
                               <td>HalloDent</td>
                               <td>
                                    <!-- AQUI QUE REDIRIJA A CLIENTE.DETAILS->$id  -->
                                    <a href="{{ route('cliente.detail', $participante->cliente->id_cliente) }}"
                                    style="color: #51A2A7; cursor: pointer"> <i class="far fa-eye"></i></a>
                               </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>

@endsection
