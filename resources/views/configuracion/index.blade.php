@extends('layouts.admin.app')

@section('content')

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif


    <div class="row my-5">
        <div class="col d-flex justify-content-center align-items-center">
            <img src="{{ url('Img/logo_hallodent.png') }}" alt="" width="20%">
        </div>
    </div>

    @if (Session::has('contacts'))
    <div class="row mx-auto">
        <div class="col-11 mx-auto"><button data-toggle="collapse" data-target="#demo" class="btn ml-3" style="background-color: #00a7d0">VER DETALLES</button></div>
    </div>
        

        <div id="demo" class="collapse">
            <div class="card card-body">
                <label for="" class="h4">Los siguientes contactos no pudieron ser agregados, esto se debe a que fueron eliminados en
                    mailchim (deben ser resubscribidos) o no poseen un formato correcto de correo para mailchimp: </label>
                <ul>
                    @foreach (Session::get('contacts') as $contact)
                        <li>Correo: {{ $contact }}</li>
                    @endforeach
                </ul>
        </div>
    </div>
    @endif

    <div class="row" id="fila1">
        <div class="col">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-9 col-md-9 mt-4 mb-4">
                    <div class="card z-index-2 ">
                        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 bg-transparent">
                            <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                                <form method="post" action="{{ route('campañas.notificacion') }}" id="push">
                                    @csrf
                                    <div class="chart" id="submitpush" style="cursor: pointer;">
                                        <img src="{{ url('Img/portadahallo.png') }}" alt="" width="100%" height="300">
                                    </div>
                                </form>
                                <script>
                                    $('#submitpush').click(function() {
                                        $('#push').submit();
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="card-body">
                            <h3 class="mb-0 mt-0">Enviar Notificación</h3>
                            <p class="h5">Aqui podras enviar los push para los correos y para la app movil</p>
                            <hr class="dark horizontal">
                            <div class="d-flex ">
                                <i class="fas fa-ghost mr-2" style="font-size: 20px"></i>
                                <p class="mb-0 text-sm">DentApp HalloDent</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-9 col-md-9 mt-4 mb-4">
                    <div class="card z-index-2 ">
                        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 bg-transparent">
                            <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                                <form method="post" action="{{ route('campañas.subscribe') }}" id="contact">
                                    @csrf
                                    <div class="chart" id="submitcontact" style="cursor: pointer;" >
                                        <img src="{{ url('Img/portada2hallo.png') }}" alt="" width="100%" height="300">
                                    </div>
                                </form>
                                <script>
                                    $('#submitcontact').click(function() {
                                        $('#contact').submit();
                                        $('#fila1').hide();
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="card-body">
                            <h3 class="mb-0 mt-0">Agregar contactos</h3>
                            <p class="h5">Esta seccion es para agregar los contactos de MailtChip a la campaña
                            </p>
                            <hr class="dark horizontal">
                            <div class="d-flex ">
                                <i class="fas fa-ghost mr-2" style="font-size: 20px"></i>
                                <p class="mb-0 text-sm">DentApp HalloDent</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
