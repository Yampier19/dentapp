@extends('layouts.admin.app')
<link rel="stylesheet" href="{{ asset('css/permisoDeModulos.css') }}">
@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-9 shadow-lg card card-body shadow p-3 mb-5 rounded mx-auto p-5">

                @if (Session::has('error'))
                    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if ($errors->any())
                    <script>
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: '¡Completa todos los campos!',
                        })
                    </script>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                @if (Session::has('edit'))
                    <script>
                        Swal.fire({
                            icon: 'success',
                            title: 'El Usuario ha sido editado exitosamente',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    </script>
                @endif

                <form class="p-5" action="{{ route('admin.update', $admin->id_useradmin) }}" method="post" id="asesor">

                    @csrf
                    {{ method_field('PUT') }}

                    <h3 style="color: #64C2C8" class="font-weight-bold">Editar usuario - {{ $admin->nombre }} {{ $admin->apellido }}</h3>
                    <hr>
                    <div class="row col-md-12 my-3 mt-5 p-3">
                        <div class="col-md-6">
                            <label for="name_user">Nombre:</label>
                            <input required name="nombre" id="categoria" type="text" class="form-control @error('nombre') is-invalid @enderror"
                                value="{{ old('nombre', $admin->nombre) }}">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Apellido:</label>
                            <input required name="apellido" id="categoria" type="text" class="form-control @error('apellido') is-invalid @enderror"
                                value="{{ old('apellido', $admin->apellido) }}">
                            @error('apellido')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12 p-3">
                        <div class="col-md-12">
                            <label for="name_user">Correo:</label>
                            <input required name="email" id="categoria" type="text" class="form-control @error('email') is-invalid @enderror"
                                value="{{ old('email', $admin->email) }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12 my-3 p-3">
                        <div class="col-md-6">
                            <label for="name_user">Estado:</label>
                            <select required name="estado" id="" class="form-control @error('estado') is-invalid @enderror">
                                <option value="activo" {{ old('estado', $admin->estado) == 'activo' ? 'selected' : '' }}>Activo</option>
                                <option value="inactivo" {{ old('estado', $admin->estado) == 'inactivo' ? 'selected' : '' }}>Inactivo</option>
                            </select>
                            @error('estado')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Rol:</label>
                            <select required name="FK_id_loginrol" id="user_rol" class="form-control">
                                @foreach ($rol as $i => $a)
                                    <option value="{{ $a->id_loginrol }}" {{ $admin->FK_id_loginrol == $a->id_loginrol ? 'selected' : '' }}>
                                        {{ $a->rol }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 d-none" id="base_permisos">
                            <label for="name_user">Permisos de Base de Datos:</label><br><br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_store" name="p_store"
                                    {{ $admin->p_store ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_store" class="d-inline-block mb-2">Creación de datos</label>
                            <br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_update" name="p_update"
                                    {{ $admin->p_update ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_update" class="d-inline-block mb-2">Actualización de datos</label>
                            <br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_destroy" name="p_destroy"
                                    {{ $admin->p_destroy ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_destroy" class="d-inline-block mb-2">Eliminación de datos</label>
                        </div>

                        <div class="col-md-6 d-none" id="modulos_permisos">
                            <label for="name_user">Permisos de Módulos:</label><br><br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_productos" name="p_productos"
                                    {{ $admin->p_productos ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_productos" class="d-inline-block mb-2">Productos</label>
                            <br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_premios" name="p_premios"
                                    {{ $admin->p_premios ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_premios" class="d-inline-block mb-2">Premios</label>
                            <br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_facturas" name="p_facturas"
                                    {{ $admin->p_facturas ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_facturas" class="d-inline-block mb-2">Facturas</label>
                            <br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_noticias" name="p_noticias"
                                    {{ $admin->p_noticias ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_noticias" class="d-inline-block mb-2">Noticias y Banners</label>
                            <br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_capacitaciones" name="p_capacitaciones"
                                    {{ $admin->p_capacitaciones ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_capacitaciones" class="d-inline-block mb-2">Capacitaciones</label>
                            <br>
                            <div class="switch d-inline-block">
                                <input class="" type="checkbox" value="1" id="p_trivia" name="p_trivia"
                                    {{ $admin->p_trivia ? 'checked' : '' }}>
                                <div></div>
                            </div>
                            <label for="p_trivia" class="d-inline-block mb-2">Trivias y Encuestas</label>
                        </div>
                    </div>
                    <div class="modal-footer mt-4">
                        <button type="submit" style="background-color:#64C2C8" class="btn form-control col-4">Guardar cambios</button>
                        <a href="{{ route('cliente.listadmin') }}" class="btn" style="background-color:rgb(187, 172, 172)"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $("#user_rol").change(function() {
            var selectValue = $(this).val();
            //1 Super Admin
            //2 Call Center
            //3 Básico

            //base_permisos
            //modulos_permisos

            if (selectValue == 1) {
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").addClass("d-none");
            } else if (selectValue == 2) {
                $("#base_permisos").removeClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            } else {
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
        });

        var rol = $("#user_rol").val();
        if (rol == 1) {
            $("#base_permisos").addClass("d-none");
            $("#modulos_permisos").addClass("d-none");
        } else if (rol == 2) {
            $("#base_permisos").removeClass("d-none");
            $("#modulos_permisos").removeClass("d-none");
        } else {
            $("#base_permisos").addClass("d-none");
            $("#modulos_permisos").removeClass("d-none");
        }

        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }
    });
</script>
