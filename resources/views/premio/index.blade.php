@extends('layouts.admin.app')

@section('content')


@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif

<div class="row" style="margin-top: 3%">

    <!-------------------------------- CARDs PREMIOS / NIVELES --------------------------------->
    <div class="col-lg-6 col-md-6">
        <div class="row mx-auto">
            <label class="lead" for="" style="color: #51A2A7; font-weight: bold">Control de premios</label>
            <a href="{{route('premio.control')}}" class="ml-auto text-secondary">Ver todos</a>
        </div>
        @forelse ($premios as $premio)
            <div class="card my-3">
                <div class="row">
                    <div class="col-md-4 p-3">
                        <div style="width: 100%; height: 120px">
                            @if( $premio->distribuidor == 'N/A' )
                                <img src="{{ route('premio.icon', ['filename' => basename($premio->foto)]) }}" width="100%" height="100%" alt="">
                            @else
                                <!-- <img src="{{ $premio->foto }}" width="100%" height="100%" alt=""> -->
                                <img src="{{ route('premio.icon', ['filename' => basename($premio->foto)]) }}" width="100%" height="100%" alt="">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-8 p-3">
                        <p style="color: #51A2A7; font-weight: bold">{{ $premio->nombre }}</p>
                        <p>Marca - <span class="font-weight-bold">{{ $premio->marca }}</span></p>
                        <p>Distribuidor - <span class="font-weight-bold">{{ $premio->distribuidor }}</span></p>
                        <div class="row">
                            <div class="col">
                                <p>
                                    <span class="font-weight-bold">
                                        @if (!$premio->premiolevel)
                                            No posee nivel
                                        @else
                                            Nivel {{ $premio->premiolevel->level->nivel->nombre }}
                                        @endif
                                    </span>
                                </p>
                            </div>
                            <div class="col">
                                <a href="{{route('premio.edit',$premio->id_premio)}}" class="float-right mr-3" style="color: #51A2A7">
                                    Ver detalle
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="card">
                <div class="card-header">
                    <div class="row mx-auto">
                        <h4 class="card-title" style="color: #51A2A7; font-weight: bold">
                            No hay Premios Registrados
                        </h4>
                    </div>
                </div>
            </div>
        @endforelse
        <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal" data-target=".bd-example-modal-lg2">
            <i class="material-icons">add</i>CREAR PREMIOS
        </button>

        <a href="{{route('premio.quantum')}}" class="btn btn-round" style="background-color: #51A2A7">
            <i class="material-icons">add</i>CREAR PREMIOS CON QUANTUM REWARDS
        </a>

        <div class="row mx-auto" style="margin-top: 10%">
            <a href="{{route('premio.nivel')}}" class="btn btn-round font-weight-bold" style="background-color: #fff; width: 100%; color: #51A2A7; border-width: 1px;
                        border-style: solid; border-color: #51A2A7;">
                <div class="row">
                    <div class="col">
                        Control de niveles / rangos
                    </div>
                    <div class="col" style="left: 20%">
                        <i class="material-icons my-auto">arrow_forward_ios</i>
                    </div>
                </div>
            </a>
        </div>

        <!-- MODAL DE CREAR PREMIO -->
        <div class="modal fade bd-example-modal-lg2" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                            Creacion de premio</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form onsubmit="return checkSubmit();" action="{{ route('premio.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="col">
                                    <input name="nombre" id="" type="text" placeholder="Nombre" value="{{ old('nombre') }}" class="form-control @error('nombre') is-invalid @enderror">
                                    @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <input name="marca" id="" type="text" placeholder="Marca" value="{{ old('marca') }}" class="form-control @error('marca') is-invalid @enderror">
                                    @error('marca')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <label for="">Categoría del Premio:</label>
                                    <input list="precat" name="tag" value="{{ old('tag') }}" class="form-control @error('tag') is-invalid @enderror" />
                                    <datalist id="precat">
                                        @foreach($categorias as $categoria)
                                        <option value="{{ $categoria->nombre }}">
                                            @endforeach
                                    </datalist>
                                    @error('tag')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <textarea name="descripcion" id="" type="text" placeholder="Descripción" class="form-control @error('descripcion') is-invalid @enderror">{{ old('descripcion') }}</textarea>
                                    @error('descripcion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row ">
                                <div class="col">
                                    <input name="numero_estrellas" id="" type="number" min="0" placeholder="Puntos de Redención" class="form-control @error('numero_estrellas') is-invalid @enderror" value="{{ old('numero_estrellas') }}">
                                    @error('numero_estrellas')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <input name="stock" id="" type="number" min="0" placeholder="Stock" value="{{ old('stock') }}" class="form-control @error('stock') is-invalid @enderror">
                                    @error('stock')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <input name="limite_usuario" id="" type="number" min="0" placeholder="Máximo de redención por usuario" class="form-control @error('limite_usuario') is-invalid @enderror" value="{{ old('limite_usuario') }}">
                                    @error('limite_usuario')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <select name="entrega" id="" class="form-control @error('entrega') is-invalid @enderror">
                                        <option value="">Tipo de Entrega...</option>
                                        <option value="Domicilio">Domicilio</option>
                                        <option value="Online">Online</option>
                                    </select>
                                    @error('entrega')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <label for="">Subir foto</label>
                                    <input type="file" accept=".jpg,.jpeg,.png" class="form-control @error('foto') is-invalid @enderror" name="foto">
                                    @error('foto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">CREAR PREMIO</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- FIN MODAL DE CREAR PREMIO -->

    </div>

    <!-------------------------------- CARDs CANJEADOS --------------------------------->
    <div class="col-lg-6 col-md-6">
        <div class="row mx-auto">
            <label for="" class="lead" style="color: #F28D8D; font-weight: bold">Premios canjeados</label>
            <a href="{{route('canjeado.index')}}" class="ml-auto text-secondary">Ver todos</a>
        </div>
        @forelse ($canjeados as $canjeado)
        <div class="card">
            <div class="card-header">
                <div class="row mx-auto">
                    <h4 class="card-title" style="color: #F28D8D; font-weight: bold">{{ $canjeado->nombre }}</h4>
                    <i class="material-icons ml-auto">star</i>{{ $canjeado->total_estrellas }}
                </div>
            </div>
            <div class="card-body">
                <p>Cliente: {{ $canjeado->cliente->nombre." ".$canjeado->cliente->apellido }}</p>
                <p>Fecha de rendicion: {{ date_format(date_create($canjeado->fecha_redencion), "F j, Y") }}</p>
            </div>
        </div>
        @empty
        <div class="card">
            <div class="card-header">
                <div class="row mx-auto">
                    <h4 class="card-title" style="color: #F28D8D; font-weight: bold">
                        No hay Premios Canjeados
                    </h4>
                </div>
            </div>
        </div>
        @endforelse
    </div>
</div>
<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

<style>
    .file-input>[type='file'] {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 10;
        cursor: pointer;
    }

    .file-input>.button {
        display: inline-block;
        cursor: pointer;
        background: #eee;
        padding: 4px 12px;
        border-radius: 2px;
        margin-right: 8px;
    }

    .file-input:hover>.button {
        background: dodgerblue;
        color: white;
    }

    .file-input>.label {
        color: #333;
        white-space: nowrap;
        opacity: .3;
    }

    .file-input.-chosen>.label {
        opacity: 1;
    }
</style>

<script>
    // Also see: https://www.quirksmode.org/dom/inputfile.html

    var inputs = document.querySelectorAll('.file-input')

    for (var i = 0, len = inputs.length; i < len; i++) {
        customInput(inputs[i])
    }

    function customInput(el) {
        const fileInput = el.querySelector('[type="file"]')
        const label = el.querySelector('[data-js-label]')

        fileInput.onchange =
            fileInput.onmouseout = function() {
                if (!fileInput.value) return

                var value = fileInput.value.replace(/^.*[\\\/]/, '')
                el.className += ' -chosen'
                label.innerText = value
            }
    }
</script>

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('#exampleModal').modal('show');
        }
    });
</script>
