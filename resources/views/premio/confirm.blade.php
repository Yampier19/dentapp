@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

     <div class="card col-sm-8 m-auto p-5">
        <h1 class="mb-5 text-center">¿Deseas eliminar el siguiente premio? </h1>
        <h4 class="font-weight-bold">Nombre: </h4>
        <p>{{ $premio->nombre }}</p>
        <h4 class="font-weight-bold">Descripcion: </h4>
        <p>{{ $premio->descripcion }}</p>
        <h4 class="font-weight-bold">Foto: </h4>
        <p class="d-flex justify-content-center"><img style="width:200px;max-height:200px;" src="{{ route('premio.icon', ['filename' => basename($premio->foto)]) }}" ></p>

        <form method="post" enctype="multipart/form-data" action="{{ route('premio.destroy', $premio->id_premio) }}">
            @method('DELETE')
            @csrf
            <div class="modal-footer mt-4">
                <button type="submit" class="rounden btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a style="background-color:#51A2A7" href="{{route('premio.control')}}" class="btn rounded col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
     </div>
@endsection
