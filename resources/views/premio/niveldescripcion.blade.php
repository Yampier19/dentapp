@extends('layouts.admin.app')

@section('content')
<div class="container mt-5">

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row ">
        <div class="col-md-12">
            <a href="{{ route('premio.nivel') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                <i class="fas fa-undo-alt"></i>
                Volver atras
            </a>
        </div>
    </div>

    <div class="col-md-12 card p-0 mx-auto mb-3">
        <div class="row col-md-12 p-3 mx-auto">
            <div class="card-header mt-3 mb-4 w-100" style="background-color: #e7344c;">
                <h5 class="text-white my-auto font-weight-bold">Nivel {{ $nivel->nombre }}</h5>
            </div>

            <!-- SECCIÓN DE DESCRIPCION -->
            <div class="card col-md-12 mx-auto p-3 shadow my-3">
                <div class="row col-md-12 mx-auto my-3">
                    <div class="row col-md-12 mx-auto">
                        <button data-toggle="modal" data-target="#exampleModalInsert" class="btn ml-auto text-white" style="background-color: #e7344c;">Agregar Descripción</button>
                    </div>
                </div>

                <!-- MODAL PARA AGREGAR DESCRIPCION -->
                    <div class="modal fade" id="exampleModalInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar nueva Descripción</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('descripcion.store') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="col-12">
                                            <label for="">Descripción: </label>
                                            <input type="text" class="form-control @error('descripcion') is-invalid @enderror" 
                                                    name="descripcion" placeholder="Texto de la descripción...">
                                                @error('descripcion')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        <input type="hidden" class="form-control" name="FK_id_nivel" value="{{ $nivel->id_nivel }}">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-round" style="background-color: #51A2A7">Ingresar descripción</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <!-- FIN MODAL PARA AGREGAR DESCRIPCION -->
                
                <!-- TABLA DE LAS DESCRIPCIONES -->
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Descripciones</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody style="background-color: #fff; color: #000">
                            @foreach($nivel->descripciones as $desc)
                                <tr class="text-center">
                                    <td class="align-middle" id="desc{{ $desc->id_nivel_descripcion }}">{{ $desc->descripcion }}</td>
                                    <td class="align-middle">
                                        <form action="" method="post">
                                            <div class="row my-3">
                                                <a id="{{ $desc->id_nivel_descripcion }}" data-toggle="modal" data-target="#exampleModalEdit" href="" style="background-color: #51A2A7" class="btn btn-sm mx-auto reditar"><i class="far fa-edit"></i> Editar</a>
                                                <a id="{{ $desc->id_nivel_descripcion }}" data-toggle="modal" data-target="#exampleModalDelete" href="" class="btn btn-sm btn-danger mx-auto rdelete"><i class="far fa-trash-alt"></i> Eliminar</a>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                <!-- FIN TABLA DE DESCRIPCIONES -->
                
                <!-- MODAL DE EDITAR DESCRIPCION -->
                    <div class="modal fade" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Editar Descripción</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('descripcion.update') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="col-12">
                                            <label for="">Contenido: </label>
                                            <input type="text" class="form-control @error('descripcion') is-invalid @enderror" 
                                                    name="descripcion" value="" id="etex_descripcion">
                                                @error('descripcion')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                    </div>
                                    <input id="eid_descripcion" type="hidden" class="form-control" name="id_nivel_descripcion" value="">
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-round" style="background-color: #51A2A7">Actualizar Descripción</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <!-- FIN MODAL DE EDITAR DESCRIPCION -->

                <!-- MODAL DE ELIMINAR DESCRIPCION -->
                    <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">¿Estas seguro de eliminar esta descripción?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('descripcion.destroy') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <h5 id="ddescripcion" class="Text-center">Descripción 1</h5>
                                    </div>
                                    <input id="did_descripcion" type="hidden" class="form-control" name="id_nivel_descripcion" value="">
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-round btn-danger">Eliminar Descripción</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <!-- FIN MODAL DE ELIMINAR DESCRIPCION -->
            </div>
            <!-- FIN SECCIÓN DE DESCRIPCION -->
        </div>
    </div>


</div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<script>
    $(document).ready(function() {
        //Seteamos la edición de la descripcion
        $( ".reditar" ).click(function() {
            var id = $(this).attr("id");
            var descripcion = $("#desc"+id).text();
            
            //Seteo el modal de editar con el input val
            $("#eid_descripcion").val(id);
            $("#etex_descripcion").val(descripcion);
        });

        //Seteamos la eliminacion de la descripcion
        $( ".rdelete" ).click(function() {
            var id = $(this).attr("id");
            var descripcion = $("#desc"+id).text();
            
            //Seteo el modal de elimar con el input val
            $("#did_descripcion").val(id);
            $("#ddescripcion").text(descripcion);
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            //$('.bd-example-modal-lg').modal('show');
        }
    });
</script>