@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <h1 class="mb-5">¿Deseas eliminar el siguiente level? </h1>
        <h4>Nombre: </h4>
        <p>xxxxx</p>
        <h4>Meta: </h4>
        <p>xxxxx</p>
        <h4>Nivel: </h4>
        <p>Bronce</p>
        
        <form method="post" enctype="multipart/form-data" action="">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{route('premio.nivel')}}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
@endsection
