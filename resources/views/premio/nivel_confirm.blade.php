@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <h1 class="mb-5">¿Deseas eliminar el siguiente nivel? </h1>
        <h4>Nombre: </h4>
        <p>Bronce</p>
        <h4>Foto: </h4>
        <p><img style="width:200px;max-height:200px;" src="https://cdn.pixabay.com/photo/2016/08/26/16/04/medal-1622549_640.png" ></p>
        
        <form method="post" enctype="multipart/form-data" action="">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{route('premio.nivel')}}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
@endsection
