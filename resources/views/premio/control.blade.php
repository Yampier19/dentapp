@extends('layouts.admin.app')

@section('content')
<script src="{{asset('js/premio/confirmDelete.js')}}"></script>
    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif




    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
            <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 2%; color: #51A2A7">Todos los
                premios</label>
        </div>
        <div class="col" style="display: flex;justify-content: flex-end !important">
            <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal"
                    data-target=".bd-example-modal-lg2">
                <i class="material-icons">add</i>CREAR PREMIOS
            </button>
            <a href="{{route('premio.quantum')}}" class="btn btn-round" style="background-color: #51A2A7">
                <i class="material-icons">add</i>CREAR PREMIOS CON QUANTUM REWARDS
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="example" class="table table-striped " cellspacing="0" width="100%">
                    <thead style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                        <tr>
                            <th class="font-weight-bold">Nombre</th>
                            <th class="font-weight-bold">Marca</th>
                            <th class="font-weight-bold">Distribuidor</th>
                            <th class="font-weight-bold">Nivel Minimo</th>
                            <th class="font-weight-bold">Descripción</th>
                            <th class="font-weight-bold">Entrega</th>
                            <th class="font-weight-bold">Tag</th>
                            <th class="font-weight-bold">Stock</th>
                            <th class="font-weight-bold">Foto</th>
                            <th class="font-weight-bold">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered" style="background-color: #fff; color: #000">
                        @foreach ($premios as $premio)
                            <tr>
                                <td>{{ $premio->nombre }}</td>
                                <td>{{ $premio->marca }}</td>
                                <td>{{ $premio->distribuidor }}</td>
                                <td class="text-center align-middle">
                                    @if (!$premio->premiolevel)
                                        No posee nivel
                                    @else
                                        <img style="width:100px;max-height:100px;"
                                        src="{{asset("img/niveles/".$premio->premiolevel->level->nivel->nombre.".png")}}" >
                                        <!-- <i class="material-icons my-auto" style="font-size: 50px; color: #51A2A7">military_tech</i> -->
                                        <br>{{ $premio->premiolevel->level->nivel->nombre }}
                                    @endif
                                </td>
                                <td>
                                    <textarea disabled class="form-control" name="" id="" cols="30" rows="10">{{ $premio->descripcion }}</textarea>
                                </td>
                                <td>{{ $premio->entrega }}</td>
                                <td>{{ $premio->tag }}</td>
                                <td>{{ $premio->stock }}</td>
                                <td class="mx-auto text-center" >
                                    @if( $premio->distribuidor == 'N/A' )
                                        <img style="width:100px;max-height:100px;"
                                            src="{{ route('premio.icon', ['filename' => basename($premio->foto)]) }}" >
                                    @else
                                        <!-- <img style="width:100px;max-height:100px;" src="{{ $premio->foto }}" > -->
                                        <img style="width:100px;max-height:100px;"
                                            src="{{ route('premio.icon', ['filename' => basename($premio->foto)]) }}" >
                                    @endif
                                </td>
                            <td class="text-center">
                                <a href="{{ route('premio.edit', $premio->id_premio) }}" style="color: #51A2A7;"> <span class="material-icons mr-2">edit</span></a>
                                <a class="delete-premio" data-id="{{$premio->id_premio}}" href="#" style="color: #51A2A7;"><span class="material-icons mr-2">delete</span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>


                <!-- MODAL DE CREAR PREMIO -->
                <div class="modal fade bd-example-modal-lg2" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                                    Creacion de premios</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <form onsubmit="return checkSubmit();" action="{{ route('premio.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="col">
                                        <input required name="nombre" id="" type="text" placeholder="Nombre" value="{{ old('nombre') }}"
                                            class="form-control @error('nombre') is-invalid @enderror">
                                        @error('nombre')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <input required name="marca" id="" type="text" placeholder="Marca" value="{{ old('marca') }}"
                                            class="form-control @error('marca') is-invalid @enderror">
                                        @error('marca')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col">
                                        <label for="">Categoría del Premio:</label>
                                        <input required list="precat" name="tag" value="{{ old('tag') }}"
                                            class="form-control @error('tag') is-invalid @enderror"/>
                                        <datalist id="precat">
                                            @foreach($categorias as $categoria)
                                                <option value="{{ $categoria->nombre }}">
                                            @endforeach
                                        </datalist>
                                        @error('tag')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col">
                                        <textarea required name="descripcion" id="" type="text" placeholder="Descripción"
                                            class="form-control @error('descripcion') is-invalid @enderror">{{ old('descripcion') }}</textarea>
                                        @error('descripcion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row ">
                                    <div class="col">
                                        <input required name="numero_estrellas" id="" type="number" min="0" placeholder="Puntos para Redimir"
                                            class="form-control @error('numero_estrellas') is-invalid @enderror" value="{{ old('numero_estrellas') }}">
                                        @error('numero_estrellas')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <input required name="stock" id="" type="number" min="0" placeholder="Stock"
                                            class="form-control @error('stock') is-invalid @enderror" value="{{ old('stock') }}">
                                        @error('stock')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col">
                                        <input required name="limite_usuario" id="" type="number" min="0" placeholder="Máximo de redención por usuario"
                                            class="form-control @error('limite_usuario') is-invalid @enderror" value="{{ old('limite_usuario') }}">
                                        @error('limite_usuario')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <select required name="entrega" id=""
                                            class="form-control @error('entrega') is-invalid @enderror">
                                            <option value="">Tipo de Entrega...</option>
                                            <option value="Domicilio">Domicilio</option>
                                            <option value="Online">Online</option>
                                        </select>
                                        @error('entrega')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>
                                </div>
                                <div class="form-row my-3">
                                    <div class="col">
                                        <label for="">Subir foto</label>
                                        <input type="file" accept=".jpg,.jpeg,.png" class="form-control @error('foto') is-invalid @enderror"
                                                name="foto">
                                        @error('foto')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                        <button type="submit" class="btn mx-auto col-md-6"
                            style="background-color: #51A2A7; border-radius: 40px;">CREAR PREMIO</button>
                    </div>
                    </form>
                        </div>
                    </div>
                </div>
                <!-- FIN MODAL -->
            </div>
        </div>
    </div>
    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">

    <style>
        .file-input>[type='file'] {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            z-index: 10;
            cursor: pointer;
        }

        .file-input>.button {
            display: inline-block;
            cursor: pointer;
            background: #eee;
            padding: 4px 12px;
            border-radius: 2px;
            margin-right: 8px;
        }

        .file-input:hover>.button {
            background: dodgerblue;
            color: white;
        }

        .file-input>.label {
            color: #333;
            white-space: nowrap;
            opacity: .3;
        }

        .file-input.-chosen>.label {
            opacity: 1;
        }

    </style>

    <script>
        // Also see: https://www.quirksmode.org/dom/inputfile.html

        var inputs = document.querySelectorAll('.file-input')

        for (var i = 0, len = inputs.length; i < len; i++) {
            customInput(inputs[i])
        }

        function customInput(el) {
            const fileInput = el.querySelector('[type="file"]')
            const label = el.querySelector('[data-js-label]')

            fileInput.onchange =
                fileInput.onmouseout = function() {
                    if (!fileInput.value) return

                    var value = fileInput.value.replace(/^.*[\\\/]/, '')
                    el.className += ' -chosen'
                    label.innerText = value
                }
        }

    </script>

@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('#exampleModal').modal('show');
        }
    });
</script>








