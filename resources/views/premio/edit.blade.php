@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <script>
                Swal.fire(
                    'Buen trabajo!',
                    '{{ Session::get('edit') }}',
                    'success'
                )
            </script>
        @endif

        @if ($errors->any())
            <script>
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '!Completa todos los campos!',
                })
            </script>
        @endif

        <div class="row mb-5 mx-auto">
            <!-- Editar Premios -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">


                <div class="card-header mt-3" style="background-color:#51A2A7">
                    <h5 class="text-white my-auto">Editar premio</h5>
                </div>


                <form class="p-5" action="{{ route('premio.update', $premio->id_premio) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row col-md-12 my-4">
                        <div class="col-md-6">
                            <label for="name_user">Nombre:</label>
                            <input required name="nombre" id="nombre" type="text" value="{{ old('nombre', $premio->nombre) }}"
                                class="form-control @error('nombre') is-invalid @enderror">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Marca:</label>
                            <input required name="marca" id="product_marca" type="text" value="{{ old('marca', $premio->marca) }}"
                                class="form-control @error('marca') is-invalid @enderror">
                            @error('marca')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-4">
                        <div class="col-md-12">
                            <label for="name_user">Descripcion:</label>
                            <textarea required class="form-control" cols="20" rows="6" name="descripcion" id="descripcion" type="text"
                                class="form-control @error('descripcion') is-invalid @enderror">{{ old('descripcion', $premio->descripcion) }}</textarea>
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="">Categoría del Premio:</label>
                            <input required list="precat" id="pre_cat" name="tag" value="{{ old('tag', $premio->tag) }}"
                                class="form-control @error('tag') is-invalid @enderror" />
                            <datalist id="precat">
                                @foreach ($categorias as $categoria)
                                    <option value="{{ $categoria->nombre }}">
                                @endforeach
                            </datalist>
                            @error('tag')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Puntos:</label>
                            <input required name="numero_estrellas" id="" type="number" min="0" value="{{ old('numero_estrellas', $premio->numero_estrellas) }}"
                                class="form-control @error('numero_estrellas') is-invalid @enderror">
                            @error('numero_estrellas')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Stock:</label>
                            <input required name="stock" id="" type="number" min="0" value="{{ old('stock', $premio->stock) }}"
                                class="form-control @error('stock') is-invalid @enderror">
                            @error('stock')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Limite de usuario:</label>
                            <input required name="limite_usuario" id="" type="number" min="0" value="{{ old('limite_usuario', $premio->limite_usuario) }}"
                                class="form-control @error('limite_usuario') is-invalid @enderror">
                            @error('limite_usuario')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Tipo de Entrega:</label>
                            <select required name="entrega" id="" class="form-control @error('entrega') is-invalid @enderror">
                                <option value="Domicilio" {{ $premio->entrega == 'Domicilio' ? 'selected' : '' }}>
                                    Domicilio</option>
                                <option value="Online" {{ $premio->entrega == 'Online' ? 'selected' : '' }}>
                                    Online</option>
                            </select>
                            @error('entrega')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <!-- <div id="imagenPremio"  class="col-md-6 {{ $premio->distribuidor != 'N/A' ? 'd-none' : '' }}"> -->
                        <div id="imagenPremio" class="col-md-6">
                            <label for="">Subir foto</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('foto') is-invalid @enderror"
                                    id="customFile" name="foto" accept=".jpg,.jpeg,.png" value="">
                                <label class="custom-file-label" for="customFile">Escoger una imagen</label>
                                @error('foto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Nivel Minimo para Redención:</label>
                            <select required name="level" id="" class="form-control @error('tag') is-invalid @enderror">
                                <option value="">Selecciona una opción...</option>
                                @foreach ($level as $one)
                                    <option value="{{ $one->id_level }}"
                                        @if ($premio->premiolevel) {{ $one->id_level == $premio->premiolevel->FK_id_level ? 'selected' : '' }} @endif>
                                        {{ $one->nivel->nombre }}
                                    </option>
                                @endforeach
                            </select>
                            @error('tag')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Distribuidor:</label>
                            <select required name="distribuidor" id="distribuidor" class="form-control @error('distribuidor') is-invalid @enderror">
                                <option value="N/A" {{ $premio->distribuidor == 'N/A' ? 'selected' : '' }}>
                                    Propia de DentApp</option>
                                <option value="Quantum" {{ $premio->distribuidor == 'Quantum' ? 'selected' : '' }}>
                                    Quantum</option>
                            </select>
                            @error('distribuidor')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div id="marcasAPI" class="col-md-6 {{ $premio->distribuidor == 'N/A' ? 'd-none' : '' }}">
                            <label for="">Seleccionar Marca</label>
                            <select name="brand_id" id="marcaselect" class="form-control @error('brand_id') is-invalid @enderror">
                                <option value="" selected disabled>Seleccionar marca...</option>
                            </select>
                            @error('brand_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            @error('imagen_quantum')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Debes seleccionar un producto para almacenar la imagen</strong>
                                </span>
                            @enderror

                            @error('producto_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>Debes seleccionar un producto para almacenar su referencia</strong>
                                </span>
                            @enderror
                        </div>

                        <!-- INPUT HIDDEN TIPO DE IMAGEN | MARCA | DISTRIBUIDOR-->
                        <input type="hidden" id="imageQuantum" name="imagen_quantum"
                            value="{{ $premio->distribuidor != 'N/A' ? $premio->foto : '' }}">
                        <input type="hidden" id="product_id" name="producto_id"
                            value="{{ $premio->distribuidor != 'N/A' ? $premio->producto_id : '' }}">
                    </div>

                    <!-- LISTA DE PREMIOS DE LA MARCA -->
                    <div class="row col-md-12 my-3" style="display: none;" id="itemlist">
                        <div class="col-10 mx-auto">
                            <a class="btn form-control" style="background-color: #51A2A7;" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample">
                                Lista de premios
                            </a>

                            <div class="collapse show" id="collapseExample">
                                <div id="list" class="card p-3" style="background: #F5F5F5;">
                                    <div class="row d-flex justify-content-center mb-3" id="itemrow">
                                        <!-- ESPACIO DONDE SALEN LOS PREMIOS CONSULTADOS -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="modal-footer">
                        <button type="submit" class="form-control text-white col-2" style="background-color: #64C2C8">Editar Premio</button>
                        <a href="{{ route('premio.control') }}" class="btn text-white" style="background-color: #64C2C8">Volver</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
    <input type="hidden" id="redirectIndex" value="{{ route('premio.index') }}">

    <!-- RUTAS PARA COGER LAS MARCAS Y PRODUCTOS -->
    <input type="hidden" id="getBrand" value="{{ $premio->brand_id }}">
    <input type="hidden" id="getBrandName" value="{{ $premio->marca }}">
    <input type="hidden" id="getMarcas" value="{{ route('quantum.marcas') }}">
    <input type="hidden" id="getProductos" value="{{ route('quantum.productos') }}">

    <style>
        #item {
            transition: 0.3s;
            cursor: pointer;
        }

        #item:hover {
            opacity: 0.7
        }

        .productoBack {
            background: 'green';
        }
    </style>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }

        //Consulta de las marcas para crear el select
        var distribuidor = $('#distribuidor').val();
        if (distribuidor != 'N/A') {
            marcas();
            /* var brand = $('#getBrand').val();
            $("#marcaselect").val(brand); */

            //Arreglamos el input de marca
            var texto = $("#getBrandName").val();
            $('#product_marca').val(texto);
        }

        $("#distribuidor").change(function() {
            var dist = $(this).val();

            if (dist != 'N/A') {
                $('#marcasAPI').removeClass('d-none');
                //$('#imagenPremio').addClass('d-none');
                var length = $('#marcaselect').children('option').length;
                if (length < 2)
                    marcas();
            } else {
                $('#marcasAPI').addClass('d-none');
                //$('#imagenPremio').removeClass('d-none');
                $('#itemlist').hide();
            }
        });

        //Selección de marca y display de los premios
        $("#marcaselect").change(function() {
            var id = $('#marcaselect').val();
            $('#itemrow').empty();

            //Borramos el id del product
            $('#product_id').val(null);

            //Arreglamos el input de marca
            var texto = $("#marcaselect option:selected").text();
            $('#product_marca').val(texto);

            var urlProductos = $('#getProductos').val();
            $.ajax({
                url: urlProductos + "/" + id,
                type: 'GET',
                success: function(response) {
                    if (response.premios) {
                        if (response.premios.length > 0) {
                            response.premios.forEach(function(element) {
                                console.log(element);

                                html = '<div class="col-3 card bg-secondary p-0 mr-3 mb-3" id="' + element.product_id + '" onclick="productTaken(this.id)">' +
                                    '<input class="product_cat" type="hidden" value="' + element.catl1 + '">' +
                                    '<img class="product_image" src="' + element.image + '"style="width: 100%; height :100%"></img>' +
                                    '<div class="prueba row-reverse w-100 mx-auto px-0 py-2" style="height: 40%; position: absolute; margin-top: 85%;background: rgb(0,150,146); background: linear-gradient(90deg, rgba(0,150,146,0.4990371148459384) 0%, rgba(9,94,121,0.49343487394957986) 35%);">' +
                                    '<div class="col">' +
                                    '<label for="" class="text-white font-weight-bold product_name">' + element.name + '</label>' +
                                    '</div>' +
                                    '<div class="col">' +
                                    '<label for="" class="text-white font-weight-bold product_desc">' + element.description + '</label>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                                $('#itemrow').append(html);
                            });
                        } else {
                            html = '<h3>No hay premios disponibles para esta marca por el momento, por favor selecciona otra</h3>';
                            $('#itemrow').append(html);
                        }

                        $('#itemlist').show();
                    } else {
                        Swal.fire({
                            title: response.message,
                            text: "Problemas de Conexión con Quantum, por favor intentalo más tarde",
                            icon: 'warning',
                            confirmButtonText: 'Aceptar'
                        })

                        $('#itemlist').hide();
                    }
                }
            });
        });
    });

    //Selección de producto y arreglo en el form
    function productTaken(id) {
        var nombre = $("#" + id + " .product_name").text();
        var categoria = $("#" + id + " .product_cat").val();
        var imagen = $("#" + id + " .product_image").attr("src");
        var descripcion = $("#" + id + " .product_desc").text();

        $('.product_select .prueba').css({
            'background': 'rgb(0,150,146)'
        });

        $("#" + id + " .prueba").css({
            'background': 'green'
        });

        $('#product_id').val(id);
        $('#nombre').val(nombre);
        $('#pre_cat').val(categoria);
        $('#imageQuantum').val(imagen);
        $('#descripcion').text(descripcion);
    }

    function marcas() {
        var urlMarcas = $('#getMarcas').val();
        $.ajax({
            url: urlMarcas,
            type: 'GET',
            success: function(response) {
                if (response.marcas) {
                    response.marcas.forEach(function(element) {
                        console.log(element);
                        var o = new Option(element.nombre, element.brand_id);
                        $(o).html(element.nombre);
                        $("#marcaselect").append(o);
                    });
                    var brand = $('#getBrand').val();
                    $("#marcaselect").val(brand);
                } else {
                    Swal.fire({
                        title: response.message,
                        text: "Problemas de Conexión con Quantum, por favor intentalo más tarde",
                        icon: 'warning',
                        allowOutsideClick: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            /* var redirect = $('#redirectIndex').val();
                            location.href = redirect; */
                            $('#btnSubmit').attr('disabled', true);
                            $('#btnSubmit').text('No esta disponible');
                        }
                    })
                }
            }
        });
    }
</script>
