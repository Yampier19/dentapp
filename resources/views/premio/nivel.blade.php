@extends('layouts.admin.app')

@section('content')
    <div class="row">

        <!-------------------------------- CARDs ADMINISTRADOR --------------------------------->
        <div class="col-lg-12 col-md-12">
            <div class="row mx-auto">
                <div class="col-sm-10">
                    <label class="lead" for="" style="color: #51A2A7; font-weight: bold">Control de niveles / rangos</label>
                <div class="row ml-auto mr-3">
                    <i class="fas fa-print ml-auto my-auto" style="font-size: 20px; color: #51A2A7"></i>
                    <i class="fas fa-file-csv mx-2 my-auto" style="font-size: 20px; color: #51A2A7"></i>
                    <i class="fas fa-file-pdf my-auto" style="font-size: 20px; color: #51A2A7"></i>
                </div>
                </div>
                <div class="col-sm-2">
                    <a href="{{url('admin/levels')}}" style="background-color:#51A2A7" class="btn rounded">Gestionar Niveles</a>
                </div>

                <hr style="color: #51A2A7; width: 75%">
            </div>
            <div class="row">
                @foreach ($niveles as $nivel)
                    <div class="col-sm-4 p-2">
                        <div style="border-radius:50px !important" class="card rounded font-weight-bold p-5 mb-2">
                            <p class="text-dark text-center">{{$nivel->nombre}}</p>
                            <div class="row mx-auto">
                                <img title="imagen del nivel" style="width:100px;max-height:100px;" src="{{ $nivel->getFirstMediaUrl('level-image') }}" alt="">
                            </div>

                            <div class="row my-3">
                                <hr class="m-0 mx-auto" style="width: 80%; color: #F28D8D; margin: auto;">
                            </div>
                            <div class="row mx-auto text-center">
                                <label for="" style="color: #F28D8D;">
                                    <strong>
                                        {{ $nivel->nombre ?  number_format($nivel->rango_inicio, 0, ',', '.') : 'No posee' }} /
                                        {{ $nivel->nombre ?  number_format($nivel->meta, 0, ',', '.') : 'No posee' }}
                                    </strong>
                                    <br>
                                </label>
                            </div>
                            <div class="d-flex justify-content-center">
                                <a disabled href="{{-- {{ route('nivel.descripcion', $nivel->id_level) }} --}}#" class="btn btn-round font-weight-bold p-2" style="background-color: #51A2A7">
                                    total de personas:
                                      {{count($nivel->cliente)}}
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
@endsection
