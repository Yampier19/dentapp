@extends('layouts.admin.app')
@section('content')

@if (Session::has('create'))
    <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('create') }}
    </div>
@endif

@if (Session::has('delete'))
    <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
        {{ Session::get('delete') }}
    </div>
@endif

@if (Session::has('edit'))
    <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
        {{ Session::get('edit') }}
    </div>
@endif

<div class="row-reverse">
    <div class="col">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 2%; color: #51A2A7">Crear premio con Quantum Rewards</label>
    </div>
</div>


<!-- DATOS DEL PREMIO SELECCIONADO -->
<div class="row mx-auto" id="itemform">
    <div class="modal-body">
        <form action="{{ route('premio.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="col">
                    <label for="">Nombre:</label>
                    <input name="nombre" id="nombre" type="text" placeholder="Nombre..." 
                    class="form-control @error('nombre') is-invalid @enderror">
                    @error('nombre')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col">
                    <label for="">Categoría:</label>
                    <input list="precat" id="pre_cat" name="tag" class="form-control @error('tag') is-invalid @enderror" />
                    <datalist id="precat">
                        @foreach($categorias as $categoria)
                            <option value="{{ $categoria->nombre }}">
                        @endforeach
                    </datalist>
                    @error('tag')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-row my-3">
                <div class="col">
                    <textarea name="descripcion" id="descripcion" type="text" placeholder="Descripción" 
                    class="form-control @error('descripcion') is-invalid @enderror"></textarea>
                    @error('descripcion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-row ">
                <div class="col">
                    <input name="numero_estrellas" id="" type="number" min="0" placeholder="Puntos de Redención" 
                    class="form-control @error('numero_estrellas') is-invalid @enderror">
                    @error('numero_estrellas')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col">
                    <input name="stock" id="" type="number" min="0" placeholder="Stock" 
                    class="form-control @error('stock') is-invalid @enderror">
                    @error('stock')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-row my-3">
                <div class="col">
                    <input name="limite_usuario" id="" type="number" min="0" placeholder="Máximo de redención por usuario" 
                    class="form-control mt-1 @error('limite_usuario') is-invalid @enderror">
                    @error('limite_usuario')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col">
                    <select name="entrega" id="" class="form-control @error('entrega') is-invalid @enderror">
                        <option value="">Tipo de Entrega...</option>
                        <option value="Domicilio">Domicilio</option>
                        <option value="Online">Online</option>
                    </select>
                    @error('entrega')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <!-- INPUT HIDDEN TIPO DE IMAGEN | MARCA | DISTRIBUIDOR-->
            <input type="hidden" id="imageQuantum" name="imagen_quantum" value="">
            <input type="hidden" id="product_marca" name="marca" value="">
            <input type="hidden" id="distribuidor" name="distribuidor" value="Quantum">

            <!-- MARCAS -->
            <div class="form-row my-3">
                <div class="col-md-6">
                    <select name="brand_id" id="marcaselect" class="form-control @error('brand_id') is-invalid @enderror">
                        <option value="" selected disabled>Seleccionar marca...</option>
                    </select>
                    @error('brand_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div id="imagenPremio" class="col-md-6">
                    <div class="custom-file mb-3">
                        <label class="" for="customFile">Escoge una imagen</label>
                        <input type="file" class="form-control @error('foto') is-invalid @enderror" 
                        id="customFile" name="foto" accept=".jpg,.jpeg,.png" value="">
                        @error('foto')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <!-- PRODUCTOS -->
            <input type="hidden" id="product_id" name="producto_id" value="">

            <!-- ERRORES POR LOS INPUT HIDDEN -->
            @error('imagen_quantum')
                <span class="invalid-feedback" role="alert">
                    <strong>Debes seleccionar un producto para almacenar la imagen</strong>
                </span>
            @enderror

            @error('producto_id')
                <span class="invalid-feedback" role="alert">
                    <strong>Debes seleccionar un producto para almacenar su referencia</strong>
                </span>
            @enderror

            <div class="form-row my-3">
                <div class="col">
                    <button type="submit" id="btnSubmit" class="btn" style="background-color: #51A2A7;" >Crear premio</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- LISTA DE PREMIOS DE LA MARCA -->
<div class="row mx-auto" style="display: none;" id="itemlist">
    <div class="col-10 mx-auto">
        <a class="btn form-control" style="background-color: #51A2A7;" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample">
            Lista de premios
        </a>

        <div class="collapse show" id="collapseExample">
            <div id="list" class="card p-3" style="background: #F5F5F5;">
                <div class="row d-flex justify-content-center mb-3" id="itemrow">
                    <!-- ESPACIO DONDE SALEN LOS PREMIOS CONSULTADOS -->

                </div>
                <!-- <div class="row">
                    <div class="col">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="javascript:;" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="javascript:;">1</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="javascript:;">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>



<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
<input type="hidden" id="redirectIndex" value="{{ route('premio.index') }}">
<!-- RUTAS PARA COGER LAS MARCAS Y PRODUCTOS -->
<input type="hidden" id="getMarcas" value="{{ route('quantum.marcas') }}">
<input type="hidden" id="getProductos" value="{{ route('quantum.productos') }}">

<style>
    #item {
        transition: 0.3s;
        cursor: pointer;
    }

    #item:hover {
        opacity: 0.7
    }

    .productoBack {
        background-color: 'green' !important;;
    }
</style>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
 
    $(document).ready(function() {

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('#exampleModal').modal('show');
        }
        else{
            //MENSAJE DE ALERTA INICIAL
            Swal.fire(
                'Premios con Quantum Rewards',
                'Para crear un premio sigue los siguientes pasos: <br /> 1. Selecciona una marca <br /> 2. Ve al "listado de premios" y escoge el que desees <br /> 3. Rellena los campos faltantes y dale en "Crear Premio"',
                'info'
            );
        }

        //Consulta de las marcas para crear el select
            var urlMarcas = $('#getMarcas').val();
            $.ajax({
                url: urlMarcas,
                type: 'GET',
                success: function(response){
                    if(response.marcas){
                        response.marcas.forEach(function(element){
                            console.log(element);
                            var o = new Option(element.nombre, element.brand_id);
                            $(o).html(element.nombre);
                            $("#marcaselect").append(o);
                        });
                    }
                    else{
                        Swal.fire({
                            title: response.message,
                            text: "Problemas de Conexión con Quantum, por favor intentalo más tarde",
                            icon: 'warning',
                            allowOutsideClick: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Aceptar'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    /* var redirect = $('#redirectIndex').val();
                                    location.href = redirect; */
                                    $('#btnSubmit').attr('disabled', true);
                                    $('#btnSubmit').text('No esta disponible');
                                }
                            }
                        )
                    }
                }
            });

        //Selección de marca y display de los premios
            $("#marcaselect").change(function() {
                var  id = $('#marcaselect').val();
                $('#itemrow').empty();
                
                //Borramos el id del product
                $('#product_id').val(null);

                //Arreglamos el input de marca
                var texto = $( "#marcaselect option:selected" ).text();
                $('#product_marca').val(texto);

                var urlProductos = $('#getProductos').val();
                $.ajax({
                    url: urlProductos+"/"+id,
                    type: 'GET',
                    success: function(response){
                        if(response.premios){
                            if(response.premios.length > 0){
                                response.premios.forEach(function(element){
                                    console.log(element);
                                    
                                    html = '<div class="product_select col-2 card d-flex justify-content-end p-0 mr-3 mb-3" style="height: 250px; cursor: pointer" id="' + element.product_id + '" onclick="productTaken(this.id)">' +
                                                '<input class="product_cat" type="hidden" value="'+ element.catl1 +'">'+
                                                '<img class="product_image" src="' + element.image + '"style="width: 100%; height :100%" id="imgChange"></img>' +
                                                '<div class="prueba row-reverse w-100 mx-auto px-0 py-2" style="height: 40%; position: absolute; margin-top: 85%;background: rgb(0,150,146); background: linear-gradient(90deg, rgba(0,150,146,0.4990371148459384) 0%, rgba(9,94,121,0.49343487394957986) 35%);">' +
                                                    '<div class="col">' +
                                                        '<label for="" class="text-white font-weight-bold product_name">' + element.name + '</label>' +
                                                    '</div>' +
                                                    '<div class="col">' +
                                                        '<label for="" class="text-white font-weight-bold product_desc">' + element.description + '</label>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>';
                                    $('#itemrow').append(html);
                                });
                            }
                            else{
                                html = '<h3>No hay premios disponibles para esta marca por el momento, por favor selecciona otra</h3>';
                                $('#itemrow').append(html);
                            }
                            
                            $('#itemlist').show();
                        }
                        else{
                            Swal.fire({
                                title: response.message,
                                text: "Problemas de Conexión con Quantum, por favor intentalo más tarde",
                                icon: 'warning',
                                confirmButtonText: 'Aceptar'
                            })

                            $('#itemlist').hide();
                        }
                    }
                });
            });

        //Selección de producto y arreglo en el form
            $('.product_selected').click(function (){
                var nombre = $(this+" .product_name").text();
                alert(nombre);
            });

    });
   

    //Selección de producto y arreglo en el form
    function productTaken(id){
        var nombre = $("#"+id+" .product_name").text();
        var categoria = $("#"+id+" .product_cat").val();
        var imagen = $("#"+id+" .product_image").attr("src");
        var descripcion = $("#"+id+" .product_desc").text();

        $('.product_select .prueba').css({
            'background' : 'rgb(0,150,146)'
        });

        $("#"+id+" .prueba").css({
            'background' : 'green'
        });

        $('#product_id').val(id);
        $('#nombre').val(nombre);
        $('#pre_cat').val(categoria);
        $('#imageQuantum').val(imagen);
        $('#descripcion').text(descripcion);
    }

</script>



@endsection