{{-- @dd($errors) --}}
@extends('layouts.auth.app')
@section('content')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <div class="wrapper wrapper-full-page">
        <div class="page-header login-page header-filter " filter-color="black">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="container">
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <div class="row-reverse animate__animated animate__fadeInLeft">
                            <div class="col title">
                                <h2 style="color: #C2C2C2">BIENVENIDO A</h2>
                            </div>
                            <div class="col logo_title">
                                <img src="{{ url('img/dentapp/logo.png') }}" alt="" class="w-75">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-8 ml-auto col_auth">

                        @if ($passwordReset)
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf

                                <input type="hidden" name="token" value="{{ $passwordReset->token }}">
                                <input type="hidden" name="tipo" value="{{ $passwordReset->tipo }}">

                                <div class="card card-login card-hidden pb-5 ">
                                    <div class="card-header card-header-rose text-center p-5">
                                        <img src="{{ url('img/dentapp/icon.png') }}" alt="" style="max-width: 40%" class="animate__animated animate__flipInX">
                                    </div>
                                    <div class="card-body ">
                                        <div class="row">
                                            <div class="col">
                                                @if (Session::has('error'))
                                                    <div class="text-center"
                                                        style="padding: 10px; background-color: #E7344C; color: #fff; margin-bottom: 1%;">
                                                        {{ Session::get('error') }}
                                                    </div>
                                                @endif

                                                @if (Session::has('edit'))
                                                    <div class="text-center"
                                                        style="padding: 10px; background-color: #00a7d0; color:#fff ; margin-bottom: 1%;">
                                                        {{ Session::get('edit') }}
                                                    </div>
                                                @endif
                                                @if (session('status'))
                                                    <div class="alert alert-success text-center" role="alert">
                                                        {{ session('status') }}
                                                    </div>
                                                @endif
                                            </div>

                                        </div>

                                        <p class="card-description text-center p-3 font-weight-bold">
                                            Por favor digita la siguiente información
                                        </p>
                                        @if ($errors->any())
                                            <div class="container pb-3">
                                                <p class="font-weight-bold">Corrige los siguientes errores:</p>
                                                <ul class="text-danger">
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                                <script>
                                                    Swal.fire({
                                                        icon: 'error',
                                                        title: 'Oops...',
                                                        text: 'Corrige los errores!',
                                                    })
                                                </script>
                                            </div>
                                        @endif
                                        <span class="bmd-form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="material-icons">email</i>
                                                    </span>
                                                </div>
                                                <input type="email"
                                                    class="form-control @error('email') is-invalid @enderror"
                                                    aria-describedby="emailHelp" placeholder="Digita tu correo electronico"
                                                    name="email" value="{{ $passwordReset->email }}" readonly>
                                            </div>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong class="text-dark">{{ $message }}</strong>
                                                </span>
                                            @enderror

                                        </span>
                                        <span class="bmd-form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="material-icons">lock_outline</i>
                                                    </span>
                                                </div>
                                                <input type="password"
                                                    class="form-control @error('password') is-invalid @enderror"
                                                    aria-describedby="emailHelp" placeholder="Contraseña" name="password"
                                                    required autocomplete="new-password">
                                            </div>

                                            @error('password')
                                                <span class="invalid-feedback text-dark font-weight-bold" role="alert">
                                                    <strong class="text-dark">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </span>
                                        <span class="bmd-form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="material-icons">lock_outline</i>
                                                    </span>
                                                </div>
                                                <input name="password_confirmation" type="password"
                                                    class="form-control  @error('password_confirmation') is-invalid @enderror"
                                                    aria-describedby="emailHelp" placeholder="Confirmar contraseña" required
                                                    autocomplete="new-password">
                                            </div>
                                            @error('password_confirmation')
                                                <span class="invalid-feedback text-dark font-weight-bold" role="alert">
                                                    <strong class="text-dark">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </span>

                                    </div>
                                    <div class="card-footer justify-content-center">
                                        <div class="row-reverse">
                                            <div class="col my-3">
                                                <button type="submit" class="btn btn-primary">
                                                    Restablecer contraseña
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @else
                            <!-- AQUI SON LOS MENSAJES DE ALERTA PARA LOS USUARIOS CLIENTES -->
                            <div class="text-center">
                                <p class="mb-4 text-dark font-weight-bold">{{ $mensaje }}</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
