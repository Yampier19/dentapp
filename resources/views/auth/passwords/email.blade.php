@extends('layouts.auth.app')
@section('content')
    <div class="wrapper wrapper-full-page">
        <div class="page-header login-page header-filter " filter-color="black">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="container">
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <div class="row-reverse animate__animated animate__fadeInLeft">
                            <div class="col title">
                                <h2 style="color: #C2C2C2">BIENVENIDO A</h2>
                            </div>
                            <div class="col logo_title">
                                <img src="{{ url('img/dentapp/logo.png') }}" alt="" class="w-75">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-8 ml-auto col_auth">
                        <form class="user mt-4" method="POST" action="{{ route('password.email', 'admin') }}">
                            <div class="card card-login card-hidden pb-5 ">
                                <div class="card-header card-header-rose text-center p-5">
                                    <img src="{{ url('img/dentapp/icon.png') }}" alt="" style="max-width: 40%" class="animate__animated animate__flipInX">
                                </div>
                                <div class="card-body ">
                                    @csrf

                                    <div class="row">
                                        <div class="col">
                                            <!-- Email Input -->
                                            @if (Session::has('error'))
                                                <div class="col bg-primary p-2 text-center text-white" style="border-radius: 10px;">
                                                    {{ Session::get('error') }}
                                                </div>
                                            @endif

                                            @if (Session::has('edit'))
                                                <div class="col bg-primary p-2 text-center text-white" style="border-radius: 10px;">
                                                    {{ Session::get('edit') }}
                                                </div>
                                            @endif
                                            @if (session('status'))
                                                <div class="alert alert-success text-center text-white" role="alert">
                                                    {{ session('status') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <p class="card-description text-center p-3 font-weight-bold">
                                        Por favor digita tu correo electronico. <br>
                                        Te enviaremos un link a tu email para recuperación de contraseña
                                    </p>

                                    <span class="bmd-form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">email</i>
                                                </span>
                                            </div>
                                            <input type="email" name="email"
                                                class="form-control form-control-user @error('email') is-invalid @enderror"
                                                id="email" placeholder="Digita tu correo electrónico"
                                                value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        </div>
                                        @error('email')
                                            <span class="invalid-feedback text-white" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </span>

                                    <div class="card-footer justify-content-center">
                                        <div class="row-reverse">
                                            <div class="col my-3">
                                                <button type="submit" class="btn btn-primary">
                                                Recuperar Contraseña
                                            </button>
                                            </div>
                                            <div class="col text-center font-weight-bold">
                                                <a href="{{ URL::to('/') }}">
                                                    <span class="text-dark ">Volver al Login</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
