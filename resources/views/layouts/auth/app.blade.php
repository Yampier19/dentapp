<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/dentapp/favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ url('img/dentapp/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        DentApp | Panel Administrativo
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <!-- CSS Files -->
    <link href="{{ url('login/app.css') }}" rel="stylesheet" />
</head>

<body class="off-canvas-sidebar p-0 m-0">
  @yield('content')
 
    <!--   Core JS Files   -->
    <script src="{{ url('js/app.js') }}"></script>
    <style>
       .login-page {
        background: rgb(255,255,255);
background: linear-gradient(168deg, rgba(255,255,255,1) 0%, rgba(100,193,198,0.2497373949579832) 100%);
            overflow: hidden;
        }

        input{
          border: solid 2px #64c1c698 !important;
        }

    </style>

</body>

</html>