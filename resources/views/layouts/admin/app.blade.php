<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ url('img/dentapp/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        DentApp | Panel Administrativo
    </title>

    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('datatables/datatables.min.css') }}" />
    <!--datables estilo bootstrap 4 CSS-->

    <link rel="stylesheet" type="text/css"
        href="{{ url('datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ url('datatables/main.css') }}"> --}}

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- CSS Files -->
    <link href="{{ url('css/app.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{url('css/paginate.css')}}">
</head>

<body class="" id="content">

    <div class="wrapper ">

        @include('layouts.admin.components.sidebar')

        <div class="main-panel p-0">
            <!-- Navbar -->
            @include('layouts.admin.components.navbar')
            <!-- End Navbar -->
            <div class="content p-0">
                <div class="container-fluid">
                    <!-- CONTENIDO PRINCIPAL -->
                    @yield('content')

                </div>
            </div>
            @include('layouts.admin.components.footer')
        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="{{ url('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.27.2/axios.min.js" integrity="sha512-odNmoc1XJy5x1TMVMdC7EMs3IVdItLPlCeL5vSUPN2llYKMJ2eByTTAIiiuqLg+GdNr9hF6z81p27DArRFKT7A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <!-- datatables JS -->
    <script type="text/javascript" src="{{ url('datatables/datatables.min.js') }}"></script>

    <!-- para usar botones en datatables JS -->
    <script src="{{ url('datatables/Buttons-1.5.6/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('datatables/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script src="{{ url('datatables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script src="{{ url('datatables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ url('datatables/Buttons-1.5.6/js/buttons.html5.min.js') }}"></script>

    <!-- código JS propìo-->
    <script type="text/javascript" src="{{ url('datatables/main.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/paginate/pagination.js') }}"></script>


    <style>
        .boton {
            background-color: Transparent !important;
            background-repeat: no-repeat !important;
            background: Transparent no-repeat !important;
            border: none !important;
            cursor: pointer !important;
            overflow: hidden !important;
            outline: none !important;
            padding: 0 !important;

        }
    </style>


    @stack('scripts')
</body>

</html>
