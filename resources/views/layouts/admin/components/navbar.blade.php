<nav class="navbar navbar-expand-lg sticky-top bg-white" id="nav">
    <div class="container-fluid">

    <div class="navbar-wrapper">
      <a class="navbar-brand" href="javascript:;">
          @if(request()->is('home'))
          @elseif(request()->is('escritorio'))<label for="" class="font-weight-bold lead">Portal principal</label>
          @elseif(request()->is('cliente/inicio' , 'admin/list'))<label for="" class="font-weight-bold lead">Administración de doctores</label>
          @elseif(request()->is('informativo/inicio'))<label for="" class="font-weight-bold lead">Panel Informativo</label>
          @elseif(request()->is('informativo/inicio/listbanners'))<label for="" class="font-weight-bold lead">Banners</label>
          @elseif(request()->is('noticia/*'))<label for="" class="font-weight-bold lead">Noticias</label>
          @elseif(request()->is('facturas/*'))<label for="" class="font-weight-bold lead">Facturas</label>
          @elseif(request()->is('training/*'))<label for="" class="font-weight-bold lead">Capacitaciones</label>
          @elseif(request()->is('trivias/*'))<label for="" class="font-weight-bold lead">Trivias</label>
          @elseif(request()->is('encuestas/*'))<label for="" class="font-weight-bold lead">Encuestas</label>
          @elseif(request()->is('premios/*'))<label for="" class="font-weight-bold lead">Premios</label>
          @elseif(request()->is('notificaciones/list'))<label for="" class="font-weight-bold lead">Historial Ultimas Acciones</label>
          @elseif(request()->is('cliente/import'))<label class="lead font-weight-bold lead">Registro Masivo</label>
          @elseif(request()->is('campañas/list'))<label class="lead font-weight-bold lead">Panel de HalloDent</label>
          @elseif(request()->is('campañas/historial'))<label class="lead font-weight-bold lead">HalloDent / Historial de factiras</label>
          @endif
      </a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
    <form id="searchUser" class="navbar-form ml-auto mr-3">
          <div class="input-group no-border">
            <input type="text" list="listusers" class="form-control" placeholder="Buscar" style="width: 300px !Important">
            <button type="submit"  class="btn btn-white btn-round btn-just-icon">
              <i class="material-icons">search</i>
              <div class="ripple-container"></div>
            </button>
          </div>
        </form>
        <datalist id="listusers">
          <?php $usuarios = \App\Models\Cliente::orderBy('nombre','asc')->get() ?>
          @foreach($usuarios as $usuario)
            <option data-value="{{ route('cliente.detail', $usuario->id_cliente) }}"
                    value="{{$usuario->nombre.' '.$usuario->apellido.' - Cod: '.$usuario->id_cliente}}">
            </option>
          @endforeach
        </datalist>
      <ul class="navbar-nav">

                <li class="nav-item dropdown">
                    <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">notifications</i>
                        <span class="notification"> {{ auth()->user()->unreadnotification->count() > 3? '3+': auth()->user()->unreadnotification->count() }}</span>
                        <p class="d-lg-none d-md-block">
                            Some Actions
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right p-3" aria-labelledby="navbarDropdownMenuLink">
                        <!-- NOTIFICACIONES NO LEIDAS -->
                        @if (auth()->user()->unreadnotification->count() > 0)
                            <div style="background-color: rgba(100,193,198,0.3); border-radius: 10px;">
                                <div class="row mx-3 p-2">
                                    <label for="" class="font-weight-bold mr-auto" style="color: #64C1C6; font-size: 12px">No leidas</label>
                                    <a href="{{ route('notificacion.marcartres') }}" style="background-color: transparent; cursor: pointer">
                                        <label class="font-weight-bold" style="color: #64C1C6; font-size: 12px;">Marcar como leidas</label>
                                    </a>
                                </div>

                                @foreach (auth()->user()->unreadnotification->take(3) as $not)
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <!-- INSERT -->
                                            @if ($not->notificacion->tipo == 'Insert')
                                                <div class="bg-secondary p-2 rounded-circle">
                                                    <i class="fas fa-book text-white"></i>
                                                </div>
                                                <!-- UPDATE -->
                                            @elseif ($not->notificacion->tipo == 'Update')
                                                <div class="bg-secondary p-2 rounded-circle">
                                                    <i class="fas fa-pencil-alt text-white"></i>
                                                </div>
                                                <!-- DELETE -->
                                            @elseif ($not->notificacion->tipo == 'Delete')
                                                <div class="bg-secondary p-2 rounded-circle">
                                                    <i class="fas fa-trash-alt text-white"></i>
                                                </div>
                                            @endif
                                        </div>
                                        <div>
                                            {{ $not->notificacion->descripcion }}
                                            <div class="small text-gray-500">
                                                {{ date_format(date_create($not->notificacion->created_at), 'F j, Y') }}
                                            </div>
                                        </div>
                                    </a>
                                    <hr class="mx-3">
                                @endforeach
                            </div>
                        @endif

                        <!-- NOTIFICACIONES LEIDAS -->
                        @if (auth()->user()->notificaciones->count() > 0)
                            <div>
                                <div class="row mx-3 p-2">
                                    <label for="" class="font-weight-bold mr-auto" style="color: #64C1C6; font-size: 12px">Leidas</label>
                                </div>
                                @foreach (auth()->user()->notificaciones->take(3) as $not)
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <!-- INSERT -->
                                            @if ($not->notificacion->tipo == 'Insert')
                                                <div class="bg-secondary p-2 rounded-circle">
                                                    <i class="fas fa-book text-white"></i>
                                                </div>
                                                <!-- UPDATE -->
                                            @elseif ($not->notificacion->tipo == 'Update')
                                                <div class="bg-secondary p-2 rounded-circle">
                                                    <i class="fas fa-pencil-alt text-white"></i>
                                                </div>
                                                <!-- DELETE -->
                                            @elseif ($not->notificacion->tipo == 'Delete')
                                                <div class="bg-secondary p-2 rounded-circle">
                                                    <i class="fas fa-trash-alt text-white"></i>
                                                </div>
                                            @endif
                                        </div>
                                        <div>
                                            {{ $not->notificacion->descripcion }}
                                            <div class="small text-gray-500">
                                                {{ date_format(date_create($not->notificacion->created_at), 'F j, Y') }}
                                            </div>
                                        </div>
                                    </a>
                                    <hr class="mx-3">
                                @endforeach
                            </div>
                        @endif

                        <div class="row">
                            <a class="dropdown-item ml-auto small" style="background-color: #F28D8D !Important; color: #fff !Important; border-radius: 20px;" href="{{ route('notificacion.index') }}">
                                Ver las Notificaciones
                            </a>
                        </div>
                </li>

                @if (auth()->user()->p_premios)
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">card_giftcard</i>
                            <!-- Counter - Messages -->
                            <span class="notification">
                                <?php $premiocanje = \App\Models\PremioCanjeado::where('estado', 'pendiente')->get(); ?>
                                {{ $premiocanje->count() > 3 ? '3+' : $premiocanje->count() }}
                            </span>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                            aria-labelledby="messagesDropdown">
                            <h6 class="dropdown-header">
                                Premios Canjeados Pendientes
                            </h6>
                            @foreach ($premiocanje->take(3) as $exchange)
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <div class="bg-danger p-2 rounded-circle">
                                            <i class="fas fa-gift text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="text-truncate">
                                            {{ $exchange->nombre }}
                                            <i class="fas fa-star text-warning"></i><strong>{{ $exchange->total_estrellas }}</strong>
                                        </div>
                                        <div class="small text-gray-500">
                                            <strong>{{ $exchange->cliente->nombre . ' ' . $exchange->cliente->apellido }}</strong>
                                            · {{ date_format(date_create($exchange->fecha_redencion), 'F j, Y') }}
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                            <a class="dropdown-item text-center small text-gray-500" href="{{ route('canjeado.index', 'pendiente') }}">
                                Ver los Premios Canjeados
                            </a>
                        </div>
                    </li>
                @endif

                @if (auth()->user()->p_facturas)
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">receipt_long</i>
                            <!-- Counter - Messages -->
                            <span class="notification">
                                <?php $invoices = \App\Models\Factura::where('estado', 'pendiente')->get(); ?>
                                {{ $invoices->count() > 3 ? '3+' : $invoices->count() }}
                            </span>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                            aria-labelledby="messagesDropdown">
                            <h6 class="dropdown-header">
                                Facturas con estado pendiente
                            </h6>
                            @foreach ($invoices->take(3) as $exchange)
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <div class="bg-secondary p-2 rounded-circle">
                                            <i class="fas fa-file-invoice text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="text-truncate">
                                            {{ $exchange->distribuidor . ' ' . $exchange->referencia }} <br>
                                            <i class="fas fa-money-bill-alt text-success"></i><strong> {{ number_format($exchange->monto_total, 3, ',', '.') }}</strong>
                                        </div>
                                        <div class="small text-gray-500">
                                            <strong>{{ $exchange->cliente->nombre . ' ' . $exchange->cliente->apellido }}</strong>
                                            · {{ date_format(date_create($exchange->fecha_registro), 'F j, Y') }}
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                            <a class="dropdown-item text-center small text-gray-500" href="{{ route('factura.listado') }}">
                                Ver las Facturas
                            </a>
                        </div>
                    </li>
                @endif

                <li class="nav-item dropdown">
                    <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                            {{ auth()->user()->nombre . ' ' . auth()->user()->apellido }}
                        </span>
                        <img class="img-​thumbnail rounded-circle" style="width:40px; height:40px"
                            src="{{ route('admin.icon', ['filename' => basename(auth()->user()->foto)]) }}">
                        <!-- <i class="material-icons">person</i> -->
                    </a>
                    <div class="dropdown-menu dropdown-menu-right p-3" aria-labelledby="navbarDropdownProfile" style="border-radius: 10px !Important">
                        <a class="dropdown-item" href="{{ route('modules.dashboard.perfil') }}"style="border-radius: 20px !Important; background-color: #64C1C6">
                            <label class="mx-auto my-auto text-white">Ver perfil</label>
                        </a>

                        @if (auth()->user()->roles->rol == 'Super Admin')
                            <a class="dropdown-item p-2 my-2" href="{{ route('notificacion.all') }}" style="border-radius: 20px !Important; background-color: #64C1C6">
                                <label class="mx-auto my-auto text-white">Administradores</label>
                            </a>

                            <a class="dropdown-item p-2 my-2 " href="{{ route('push.index') }}" style="border-radius: 20px !Important; background-color: #64C1C6">
                                <label class="mx-auto my-auto text-white d-flex align-items-center" style="cursor: pointer">Push Notification<span class="badge bg-danger ms-1 ml-2 p-1">New</span></label>
                            </a>
                        @endif

              <a class="dropdown-item p-2 my-2 " href="{{ route('push.index') }}" style="border-radius: 20px !Important; background-color: #64C1C6">
                <label class="mx-auto my-auto text-white d-flex align-items-center" style="cursor: pointer">Push Notification<span class="badge bg-danger ms-1 ml-2 p-1">New</span></label>
              </a>
            @endif

            <div class="dropdown-divider"></div>
            <a class="dropdown-item bg-secondary" href="{{route('login.logout')}}"style="border-radius: 20px !Important;">
              <label class="mx-auto my-auto text-white">Cerrar sesion</label>
            </a>
          </div>
        </li>
      </ul>
    </div>
</nav>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $("#searchUser").submit(function(e) {
            e.preventDefault();
            var option = $('#searchUser input').val();
            var url = $('#listusers [value="' + option + '"]').data('value');
            if (url)
                location.href = url;
            else
                alert('No hay usuarios coincidentes');
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#resetPoints").on('submit', function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Estás seguro de reiniciar los puntos y niveles?',
                text: "Esta informacion sera restaurada permanentemente",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Aceptar!'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Reiniciando!',
                        'Puntos y los niveles reiniciados',
                        'success'
                    );
                    this.submit();
                }
            });
        });

        $("#resetTrazabilidad").on('submit', function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Estás seguro de reiniciar los datos?',
                text: "Esta informacion sera restaurada permanentemente",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Aceptar!'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Reiniciando!',
                        'Datos reiniciados',
                        'success'
                    );
                    this.submit();
                }
            });
        });

    });
</script>
<style>
    @media (max-width: 1140px) {
        #searchUser {
            display: none;
        }

        #listusers {
            display: none;
        }
    }
</style>
