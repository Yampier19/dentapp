<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
        <body class="mx-auto" onload="mueveReloj()">
    <input style="border: 0; font-size: 15px" class=" ml-3 mt-3" disabled id="fechaout" />
  </body>
        </nav>
        
        <div class="copyright float-right">
            &copy;
            <script>
            document.write(new Date().getFullYear())
            </script> All rights reserved by
            <a href="https://www.peoplemarketing.com" target="_blank">People Marketing S.A.S</a>
        </div>
    </div>
</footer>

<script>
function mueveReloj(){

momentoActual = new Date()
dia = momentoActual.getDate()
año = momentoActual.getFullYear()
mes = momentoActual.getMonth() + 1
hora = momentoActual.getHours()
minuto = momentoActual.getMinutes()
segundo = momentoActual.getSeconds()

if (hora < 10 ){
hora = "0" + hora
}
if (minuto < 10 ){
minuto = "0" + minuto
}
if (segundo < 10 ){
segundo = "0" + segundo
}

if (dia < 10 ){
dia = "0" + dia
}
if (mes < 10 ){
mes = "0" + mes
}

horaImprimible = año + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo

document.getElementById('fechaout').value = horaImprimible

timeout = setTimeout("mueveReloj()",1000)
}
</script>