<div class="sidebar" id="sidebar">
    <div class="logo mb-3">
        <button id="minimizeSidebar" class="btn btn-just-icon btn-fab btn-round"
            style="background: #fafafa; margin-top: 28%;  margin-left: 93%; z-index: 10002; position: absolute;">
            <img src="{{ url('img/flecha.png') }}" class="visible-on-sidebar-regular mr-1" alt="">
            <img src="{{ url('img/flecha2.png') }}" class="visible-on-sidebar-mini ml-1" alt="">
            <div class="ripple-container"></div>
        </button>
        <a href="{{ route('home') }}" class="simple-text logo-normal my-3 text-center">
            <img src="{{ url('img/dentapp/logo.png') }}" style="width: 60%" alt="">
        </a>

    </div>

    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ request()->is('home', 'escritorio') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.index') }}">
                    <i class="material-icons">home</i>
                    <p>Inicio</p>
                </a>
            </li>

            @if (auth()->user()->roles->rol == 'Super Admin')
                <li class="{{ request()->is('cliente/*', 'admin/*') ? 'active' : '' }}">
                    <a class="nav-link" data-toggle="collapse" href="#componentsExamples" aria-expanded="false">
                        <i class="material-icons">group</i>

                        <p id="texto">Usuarios
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="componentsExamples">
                        <ul class="nav">
                            <li class="nav-item {{ request()->is('notificaciones/all') ? 'active' : '' }}">
                                <a class="nav-link " href="{{ route('notificacion.all') }}">
                                    <i class="material-icons" style="color: #F22738">person</i>
                                    <span class="sidebar-normal">Usuarios administrativos</span>
                                </a>
                            </li>
                            <li class="nav-item {{ request()->is('cliente/inicio') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('cliente.index') }}">
                                    <i class="material-icons" style="color: #F22738">person</i>
                                    <span class="sidebar-normal">Todos los doctores</span>
                                </a>
                            </li>
                            <li class="nav-item {{ request()->is('cliente/  ') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('cliente.request') }}">
                                    <i class="material-icons" style="color: #F22738">person</i>
                                    <span class="sidebar-normal">Solicitud de usuarios</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
            @endif
            <li class="">
                <a class="nav-link" data-toggle="collapse" href="#componentsExamples2" aria-expanded="false">
                    <i class="material-icons">star</i>
                    <p id="texto">Gestor de niveles
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="componentsExamples2">
                    <ul class="nav">
                        <li class="nav-item {{ request()->is('') ? 'active' : '' }}">
                            <a class="nav-link " href="{{ route('level.index') }}">
                                <i class="material-icons" style="color: #F22738">military_tech</i>
                                <span class="sidebar-normal">Mis Niveles</span>
                            </a>
                        </li>
                        {{-- <li class="nav-item {{ request()->is('') ? 'active' : '' }}">
                            <a class="nav-link" href="">
                                <i class="material-icons" style="color: #F22738">person</i>
                                <span class="sidebar-normal">Todos los doctores</span>
                            </a>
                        </li> --}}

                    </ul>
                </div>
            </li>
            @if (auth()->user()->p_noticias)
                <li class="nav-item {{ request()->is('informativo', 'informativo/inicio') ? 'active' : '' }}">
                    <a class="nav-link" data-toggle="collapse" href="#componentsExamples5" aria-expanded="false">
                        <i class="material-icons">library_books</i>

                        <p id="texto">Informativo
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="componentsExamples5">
                        <ul class="nav">
                            <li class="nav-item {{ request()->is('informativo/inicio') ? 'active' : '' }}">
                                <a class="nav-link " href="{{ route('informativo.index') }}">
                                    <i class="material-icons" style="color: #F22738">library_books</i>
                                    <span class="sidebar-normal">Panel informativo</span>
                                </a>
                            </li>
                            <li
                                class="nav-item {{ request()->is('informativo/inicio/listbanners') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('informativo.listbanners') }}">
                                    <i class="material-icons" style="color: #F22738">library_books</i>
                                    <span class="sidebar-normal">Banners</span>
                                </a>
                            </li>
                            <li class="nav-item {{ request()->is('noticia/*') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('noticias.index') }}">
                                    <i class="material-icons" style="color: #F22738">library_books</i>
                                    <span class="sidebar-normal">Noticias</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endif

            <li
                class="nav-item {{ request()->is('distribuidores/list', 'distribuidores/product', 'distribuidores/client') ? 'active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#componentsExamples3" aria-expanded="false">
                    <i class="material-icons">manage_accounts</i>

                    <p id="texto">Distribuidores
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="componentsExamples3">
                    <ul class="nav">
                        <li class="nav-item {{ request()->is('distribuidores/list') ? 'active' : '' }}">
                            <a class="nav-link " href="{{ route('distribuidor.list') }}">
                                <i class="material-icons" style="color: #F22738">manage_accounts</i>
                                <span class="sidebar-normal">Distribuidores</span>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->is('distribuidores/product') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('distribuidor.product') }}">
                                <i class="material-icons" style="color: #F22738">manage_accounts</i>
                                <span class="sidebar-normal">Productos especiales</span>
                            </a>
                        </li>
                        {{-- <li class="nav-item {{ request()->is('distribuidores/client') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('distribuidor.client') }}">
                                <i class="material-icons" style="color: #F22738">manage_accounts</i>
                                <span class="sidebar-normal">Usuarios por distribuidor</span>
                            </a>
                        </li> --}}
                        {{-- <li class="nav-item {{ request()->is('distribuidores/facturados') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('distribuidor.facturados') }}">
                                <i class="material-icons" style="color: #F22738">manage_accounts</i>
                                <span class="sidebar-normal">Productos facturados</span>
                            </a>
                        </li> --}}
                    </ul>
                </div>
            </li>

            @if (auth()->user()->p_facturas)
                <li class="{{ request()->is('facturas/*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('factura.index') }}">
                        <i class="material-icons">receipt_long</i>
                        <p>Facturas de Usuarios</p>
                    </a>
                </li>
            @endif

            @if (auth()->user()->p_capacitaciones)
                <li class="nav-item {{ request()->is('training/*') ? 'active' : '' }}">
                    <a class="nav-link" data-toggle="collapse" href="#componentsExamples7" aria-expanded="false">
                        <i class="material-icons">school</i>

                        <p id="texto">Capacitaciones
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="componentsExamples7">
                        <ul class="nav">
                            <li class="nav-item {{ request()->is('training/index') ? 'active' : '' }}">
                                <a class="nav-link " href="{{ route('training.index') }}">
                                    <i class="material-icons" style="color: #F22738">school</i>
                                    <span class="sidebar-normal">Panel Capacitaciones</span>
                                </a>
                            </li>
                            <li class="nav-item {{ request()->is('training/list/user') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('training.list_user') }}">
                                    <i class="material-icons" style="color: #F22738">school</i>
                                    <span class="sidebar-normal">Capacitaciones por usuario</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endif

            @if (auth()->user()->p_trivia)
                <li class="nav-item {{ request()->is('encuestas/*') ? 'active' : '' }}">
                    <a class="nav-link" data-toggle="collapse" href="#componentsExamples6" aria-expanded="false">
                        <i class="material-icons">rate_review</i>

                        <p id="texto">Encuestas
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="componentsExamples6">
                        <ul class="nav">
                            <li class="nav-item {{ request()->is('encuestas/index') ? 'active' : '' }}">
                                <a class="nav-link " href="{{ route('encuesta.index') }}">
                                    <i class="material-icons" style="color: #F22738">rate_review</i>
                                    <span class="sidebar-normal">Encuestas</span>
                                </a>
                            </li>
                            <li class="nav-item {{ request()->is('encuestas/list/user') ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('encuesta.list_user') }}">
                                    <i class="material-icons" style="color: #F22738">rate_review</i>
                                    <span class="sidebar-normal">Encuestas por usuario</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endif

            @if (auth()->user()->p_premios)
                <li class="{{ request()->is('premios/*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('premio.index') }}">
                        <i class="material-icons">redeem</i>
                        <p>Premios</p>
                    </a>
                </li>
            @endif

            {{-- <li class="nav-item {{ request()->is('reportes', 'reportes/filtros') ? 'active' : '' }}"
                style="margin-bottom: 20%">
                <a class="nav-link" data-toggle="collapse" href="#componentsExamples4" aria-expanded="false">
                    <i class="material-icons">analytics</i>
                    <p>Reportes
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="componentsExamples4" style="margin-bottom: 20%">
                    <ul class="nav">
                        <li class="nav-item {{ request()->is('reportes/graficas') ? 'active' : '' }}"">
                            <a class=" nav-link" href="{{ route('report.graficas') }}">
                                <i class="material-icons" style="color: #F22738">analytics</i>
                                <span class="sidebar-normal">Graficos</span>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->is('reportes/tablas') ? 'active' : '' }}"">
                            <a class=" nav-link" href="{{ route('report.tablas') }}">
                                <i class="material-icons" style="color: #F22738">analytics</i>
                                <span class="sidebar-normal">Filtros por tabla</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li> --}}

            {{-- <li class="nav-item {{ request()->is('reportes', 'reportes/filtros') ? 'active' : '' }}"
                style="margin-bottom: 20%">
                <a class="nav-link" data-toggle="collapse" href="#componentsExamples4" aria-expanded="false">
                    <i class="material-icons">analytics</i>
                    <p>Temporada
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="componentsExamples4" style="margin-bottom: 20%">
                    <ul class="nav">
                        <li class="nav-item {{ request()->is('reportes/graficas') ? 'active' : '' }}"">
                            <a class=" nav-link" href="{{route('season.index')}}">
                                <i class="material-icons" style="color: #F22738">analytics</i>
                                <span class="sidebar-normal">Gestor de temporadas</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li> --}}
        </ul>

    </div>

</div>

<style>
    #sidebar {
        background: rgb(255, 255, 255);
        background: linear-gradient(180deg, rgba(255, 255, 255, 1) 39%, rgba(100, 193, 198, 0.19371498599439774) 100%);
    }
</style>
