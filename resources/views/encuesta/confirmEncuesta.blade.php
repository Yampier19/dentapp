@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

       <div class="card col-sm-8 p-5 m-auto">
        <h1 class="mb-5 text-center font-weight-bold">¿Deseas eliminar la siguiente encuesta? </h1>
        <h4 class="font-weight-bold">Nombre: </h4>
        <p>{{ $trivia->nombre }}</p>
        <h4 class="font-weight-bold">Descripcion: </h4>
        <p>{{ $trivia->descripcion }}</p>
        <h4 class="font-weight-bold">Fecha de Inicio: </h4>
        <p>{{ $trivia->fecha_inicio }}</p>
        <h4 class="font-weight-bold">Fecha de Vencimiento: </h4>
        <p>{{ $trivia->fecha_fin }}</p>

        <form method="post" enctype="multipart/form-data" action="{{ route('trivia.destroy', $trivia->id_trivia) }}">
            @method('DELETE')
            @csrf
            <div class="modal-footer d-flex justify-content-center mt-4">
                <button type="submit" class="redondo btn btn-danger">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{route('encuesta.index')}}" class="btn ">Volver</a>
            </div>
        </form>
       </div>
    </div>
@endsection
