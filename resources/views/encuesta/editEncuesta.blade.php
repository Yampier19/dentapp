@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif
        <div class="row mb-5 mx-auto">
            <!-- Edición de Trivia -->
            <div class="col-md-12 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">

                <div class="card-header mt-3" style="background-color:#64C2C8;">
                    <h5 class="text-white my-auto"><strong>Editar Encuesta</strong></h5>
                </div>

                <form action="{{ route('trivia.update', $trivia->id_trivia) }}" method="post" enctype="multipart/form-data">

                    @csrf

                    <!-- input hidden con el tipo de encuesta | trivia -->
                    <input name="tipo" id="" type="hidden" value="{{ $trivia->tipo }}">

                    <!-- input hidden con el la puntuación 0 -->
                    <input name="puntuacion" id="" type="hidden" value="{{ $trivia->puntuacion }}">

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Nombre:</label>
                            <input name="nombre" id="" type="text" value="{{ old('nombre', $trivia->nombre) }}"
                                class="form-control @error('nombre') is-invalid @enderror">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Descripcion:</label>
                            <input name="descripcion" id="" type="text"
                                value="{{ old('descripcion', $trivia->descripcion) }}"
                                class="form-control @error('descripcion') is-invalid @enderror">
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Inicio:</label>
                            <input max="{{$trivia->fecha_inicio}}" name="fecha_inicio" id="" type="date"
                                value="{{ old('fecha_inicio', $trivia->fecha_inicio) }}"
                                class="form-control @error('fecha_inicio') is-invalid @enderror" required>
                            @error('fecha_inicio')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Vencimiento:</label>
                            <input min="{{$trivia->fecha_inicio}}" name="fecha_fin" id="" type="date"
                                value="{{ old('fecha_fin', $trivia->fecha_fin) }}"
                                class="form-control @error('fecha_fin') is-invalid @enderror" required>
                            @error('fecha_fin')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6 {{ $trivia->preguntas->isEmpty() ? 'd-none' : '' }}">
                            <label for="name_user">Estado:</label>
                            <div class="form-check">
                                <input value="1" class="" type="radio" name="estado" id="flexRadioDefault1"
                                    {{ $trivia->estado ? 'checked' : '' }}>
                                <label class="form-check-label" for="flexRadioDefault1">
                                    Encuesta Activada
                                </label>
                            </div>
                            <div class="form-check">
                                <input value="0" class="" type="radio" name="estado" id="flexRadioDefault2"
                                    {{ $trivia->estado ? '' : 'checked' }}>
                                <label class="form-check-label" for="flexRadioDefault2">
                                    Encuesta Desactivada
                                </label>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row d-flex justify-content-end col-md-12 my-3">
                        <input class="btn  d-inline-block" style="background-color: #64C2C8; border-radius: 40px;"
                            type="submit" value="Editar Encuesta">
                        <a href="{{ route('encuesta.index') }}" class="btn text-white"
                            style="border-radius:40px">
                            <i class="fas fa-undo-alt"></i>
                            Volver
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }
    });
</script>
