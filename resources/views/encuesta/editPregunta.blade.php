@extends('layouts.admin.app')

@section('content')
<div class="container mt-5">

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <div class="row ">
        <div class="col-md-12">
            <a href="{{ route('encuesta.detail', $pregunta->trivia->id_trivia) }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                <i class="fas fa-undo-alt"></i>
                Volver atras
            </a>
        </div>
    </div>

    <div class="col-md-12 card p-0 mx-auto mb-3">
        <div class="row col-md-12 p-3 mx-auto">
            <!-- SECCIÓN DE EDITAR PREGUNTA -->
                <div class="card col-md-12 mx-auto p-3 shadow">
                    <form action="{{ route('pregunta.update') }}" method="post">
                        @csrf
                        <div class="row col-md-12 mx-auto">
                            <div class="col-md-12 mx-auto my-2">
                                <h4 class="mb-3">Editar Pregunta</h4>
                            </div>
                        </div>
                        <div class="row col-md-12 mx-auto">
                            <div class="col-md-12">
                                <label for="">Pregunta: </label>
                                <input type="text" name="informacion" class="form-control mx-auto" value="{{ old('informacion', $pregunta->informacion) }}">
                                @error('informacion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="row col-md-12 mt-3">
                                <div class="col-md-6">
                                    <label for="">Puntos de la Pregunta: </label>
                                    <input name="puntuacion" id="" type="number" min="1" 
                                    class="form-control @error('puntuacion') is-invalid @enderror" value="{{ old('puntuacion', $pregunta->puntuacion) }}">
                                    @error('puntuacion')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label for="">Tipo de Pregunta: </label>
                                    <select name="tipo" id="" class="form-control @error('tipo') is-invalid @enderror">
                                        <option value="date" {{ $pregunta->tipo == "date" ? "selected" : "" }}>Fecha</option>
                                        <option value="text" {{ $pregunta->tipo == "text" ? "selected" : "" }}>Texto</option>
                                        <option value="radio" {{ $pregunta->tipo == "radio" ? "selected" : "" }}>Selección</option>
                                    </select>
                                    @error('tipo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id_pregunta" value="{{ $pregunta->id_pregunta }}">
                        <div class="row col-md-12 mx-auto my-4">
                            <div class="col-md-12">
                                <button class="btn" style="background-color: #F28D8D;">Actualizar pregunta</button>
                            </div>
                        </div>
                    </form>
                </div>
            <!-- FIN SECCIÓN DE EDITAR PREGUNTA -->

            <!-- SECCIÓN DE OPCIONES SI ES TIPO RADIO -->
            @if($pregunta->tipo == "radio")
            <div class="card col-md-12 mx-auto p-3 shadow my-3">
                <div class="row col-md-12 mx-auto my-3">
                    <div class="row col-md-12 mx-auto">
                        <h4 class="mb-3">Opciones</h4>
                        <button data-toggle="modal" data-target="#exampleModalInsert" class="btn ml-auto text-white" style="background-color: #e7344c;">Agregar opciones</button>
                    </div>
                </div>
                <!-- MODAL PARA AGREGAR OPCIONES -->
                    <div class="modal fade" id="exampleModalInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar Respuesta</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('respuesta.store') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="col-12">
                                            <label for="">Contenido de la posible opción: </label>
                                            <input type="text" class="form-control @error('informacion') is-invalid @enderror" 
                                                    name="informacion" placeholder="Texto de la opción...">
                                                @error('informacion')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        <input type="hidden" name="correcta" value="1">
                                        <input type="hidden" class="form-control" name="FK_id_pregunta" value="{{ $pregunta->id_pregunta }}">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success">Ingresar Respuesta</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <!-- FIN MODAL PARA AGREGAR RESPUESTAS -->
                
                <!-- TABLA DE LAS OPCIONES -->
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Posibles respuestas del selector</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody style="background-color: #fff; color: #000">
                            @foreach($pregunta->respuestas as $respuesta)
                            <tr class="text-center">
                                <td class="align-middle" id="rtexto{{ $respuesta->id_respuesta }}">{{ $respuesta->informacion }}</td>
                                <td class="align-middle">
                                    <form action="" method="post">
                                        <div class="row my-3">
                                            <a id="{{ $respuesta->id_respuesta }}" data-toggle="modal" data-target="#exampleModalEdit" href="" style="background-color: #64C2C8" class="btn btn-sm mx-auto reditar"><i class="far fa-edit"></i> Editar</a>
                                            <a id="{{ $respuesta->id_respuesta }}" data-toggle="modal" data-target="#exampleModalDelete" href="" class="btn btn-sm btn-danger mx-auto rdelete"><i class="far fa-trash-alt"></i> Eliminar</a>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                <!-- FIN TABLA DE OPCIONES -->
                
                <!-- MODAL DE EDITAR OPCIONES -->
                    <div class="modal fade" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Editar Respuesta</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('respuesta.update') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="col-12">
                                            <label for="">Contenido de la Opción: </label>
                                            <input type="text" class="form-control @error('informacion') is-invalid @enderror" 
                                                    name="informacion" value="" id="etex_respuesta">
                                                @error('informacion')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                                        <input value="1" type="hidden" name="correcta">
                                    </div>
                                    <input id="eidrespuesta" type="hidden" class="form-control" name="id_respuesta" value="">
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success">Actualizar Respuesta</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <!-- FIN MODAL DE EDITAR OPCIONES -->

                <!-- MODAL DE ELIMINAR OPCIONES -->
                    <div class="modal fade" id="exampleModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">¿Estas seguro de eliminar esta respuesta?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('respuesta.destroy') }}" method="post">
                                    @csrf
                                    <div class="modal-body">
                                        <h5 id="drespuesta" class="Text-center">Respuesta 1</h5>
                                    </div>
                                    <input id="didrespuesta" type="hidden" class="form-control" name="id_respuesta" value="">
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Eliminar respuesta</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <!-- FIN MODAL DE ELIMINAR OPCIONES -->
            </div>
            <!-- FIN SECCIÓN DE OPCIONES -->
            @endif
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
</div>
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<script>
    $(document).ready(function() {
        // Add minus icon for collapse element which is open by default
        $(".collapse.show").each(function() {
            $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function() {
            $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function() {
            $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });

        //Seteamos la edición de la respuesta
        $( ".reditar" ).click(function() {
            var id = $(this).attr("id");
            var respuesta = $("#rtexto"+id).text();
            var correcta = $("#rcorrecto"+id).attr('name');
            
            //Seteo el modal de editar con el input val (dierespuesta con el id) y el resto de inputs
            $("#eidrespuesta").val(id);
            $("#etex_respuesta").val(respuesta);
            
            if(correcta){
                $("#flexradioedit2").prop('checked', false);
                $("#flexradioedit1").prop('checked', false);
                $("#flexradioedit1").prop('checked', true);
            }
            else{
                $("#flexradioedit1").prop('checked', false);
                $("#flexradioedit2").prop('checked', false);
                $("#flexradioedit2").prop('checked', true);
            }
        });

        //Seteamos la eliminacion de la pregunta
        $( ".rdelete" ).click(function() {
            var id = $(this).attr("id");
            var respuesta = $("#rtexto"+id).text();
            
            //Seteo el modal de elimar con el input val (didrespuesta con el id) y el h5 con el texto
            $("#didrespuesta").val(id);
            $("#drespuesta").text(respuesta);
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
        }
    });
</script>