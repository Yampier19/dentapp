@extends('layouts.admin.app')

@section('content')
    <script src="{{ asset('js/encuesta/confirmDelete.js') }}"></script>
    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <label for="" class="lead" style="font-weight: bold; margin-top: 5%; color: #F28D8D">
        Listado de Encuestas Home
    </label>
    <!-- <br>
            <label class="" style="font-weight: bold; margin-bottom: 5%; color: #F28D8D">
                Pulsa sobre la fila de la trivia para ver las preguntas y respuestas que tiene...
            </label> -->

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">

                <div class="table row conteiner justify-content-end">
                    <a href="{{ route('encuesta.create') }}">
                        <button class="btn btn-round" style="background-color: #F28D8D">
                            <i class="material-icons">add</i>CREAR ENCUESTA
                        </button>
                    </a>
                </div>

                <table class="table table-striped" id="example" cellspacing="0" width="100%">
                    <thead style="background-color: #F28D8D; color: #fff">
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Estado</th>
                            <th>Inicio</th>
                            <th>Fin</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach ($trivias as $trivia)
                            <tr class="table_click">
                                <td class="fila">{{ $trivia->nombre }}</td>
                                <td class="fila">{{ $trivia->descripcion }}</td>
                                <td class="fila">{{ $trivia->estado ? 'Activa' : 'Inactiva' }}</td>
                                <td class="fila">{{ $trivia->fecha_inicio }}</td>
                                <td class="fila">{{ $trivia->fecha_fin }}</td>
                                <td class="">
                                    <div class="row container justify-content-end">
                                        <a href="{{ route('encuesta.edit', $trivia->id_trivia) }}"
                                            class="btn btn-sm rounded" style="background-color: #51A2A7"><span class="material-icons">edit</span></a>
                                        <a href="{{ route('trivia.detail', $trivia->id_trivia) }}"
                                            class="btn rounded btn-sm">Preguntas <span class="material-icons">help</span></a>

                                        <form action="{{ route('trivia.destroy', $trivia->id_trivia) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button onclick="return confirm('¿Desea eliminar este registro?')" type="submit" {{-- data-id="{{ $trivia->id_trivia }}" --}}  class="btn btn-danger"><span class="material-icons">delete</span></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
