@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">
            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <form action="{{ route('trivia.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="text-center">
                        <label for="" class="lead"><strong>Registrar Encuesta</strong></label><br>
                        <!-- <label for="" class="lead">
                                tome en cuenta que esta encuesta estara desactivada hasta que se le generen sus respectivas preguntas
                            </label> -->
                    </div>
                    <hr>

                    <!-- input hidden con el tipo de encuesta | trivia -->
                    <input name="tipo" id="" type="hidden" value="home">

                    <!-- input hidden con el la puntuación 0 -->
                    <input name="puntuacion" id="" type="hidden" value="0">

                    <!-- input hidden con el FK de capacitacion -->
                    <input name="FK_id_capacitacion" id="" type="hidden" value="0">

                    <!-- input hidden con el estado de la capacitación -->
                    <input name="estado" id="" type="hidden" value="1">

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Nombre:</label>
                            <input required name="nombre" id="nombre" type="text" value="{{ old('nombre') }}"
                                class="form-control @error('nombre') is-invalid @enderror">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Descripcion:</label>
                            <input required name="descripcion" id="descripcion" type="text" value="{{ old('descripcion') }}"
                                class="form-control @error('descripcion') is-invalid @enderror">
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Inicio:</label>
                            <input min="{{ now()->format('Y-m-d') }}" max="{{ now()->format('Y-m-d') }}" name="fecha_inicio" id="fecha_inicio" type="date" value="{{ old('fecha_inicio', date('Y-m-d')) }}"
                                class="form-control @error('fecha_inicio') is-invalid @enderror">
                            @error('fecha_inicio')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Fecha de Vencimiento:</label>
                            <input min="{{ now()->addday(1)->format('Y-m-d') }}" required name="fecha_fin" id="fecha_fin" type="date" value="{{ old('fecha_fin') }}"
                                class="form-control @error('fecha_fin') is-invalid @enderror">
                            @error('fecha_fin')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3 d-flex justify-content-center">
                        <button id="" type="submit" {{-- href="{{ route('encuesta.index') }}" --}} class="btn text-white mb-3" style="border-radius: 40px;background-color:#F28D8D">
                            <i class="fas fa-undo-alt"></i>
                            Crear encuesta
                        </button>
                        <a href="{{ route('encuesta.index') }}" class="btn text-white mb-3" style="border-radius: 40px;background-color:cadetblue">
                            <i class="fas fa-undo-alt"></i>
                            Volver atras
                        </a>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
        }

        $('#agregarencuesta').click(function(e) {
            // IsValid();
            e.preventDefault();
            let data = new FormData();
            data.append('nombre', $('#nombre').val());
            data.append('descripcion', $('#descripcion').val());
            data.append('fecha_inicio', $('#fecha_inicio').val());
            data.append('fecha_fin', $('#fecha_fin').val());

            axios.post("/trivias/store", data)
                .then(function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: '¡Buen trabajo!',
                        text: 'Noticia registrada con exito!',
                    });
                })
                .catch(function(error) {
                    Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'No se pudo crear la noticia!',
                        })
                });
        });
    });
</script>
