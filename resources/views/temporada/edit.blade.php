  <!-- Modal -->
  <div class="modal fade" id="edittemporada" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Editar temporada</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <form id="form-temporada"  method="post">
                      @csrf
                      @method('PATCH')
                      @include('temporada.form')
                      <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                          <button type="submit" class="btn" style="background-color:#51A2A7">Guardar cambios</button>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
