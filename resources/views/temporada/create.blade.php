<!-- Button trigger modal -->
<button type="button" style="background-color: #51A2A7" class="btn btn-sm rounded" data-toggle="modal" data-target="#exampleModal">
    Crear temporada
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-capitalize font-weight-bold" id="exampleModalLabel">Crear nueva temporada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="was-validated" class="p-4" action="{{ route('temporada.store') }}" method="post">
                    @csrf
                    @include('temporada.form')
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button style="background-color:#51A2A7" type="submit" class="btn text-capitalize">Agregar temporada</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
