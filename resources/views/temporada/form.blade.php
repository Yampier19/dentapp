<label class="font-weight-bold text-capitalize form-label" for="name">Nombre de la temporada</label>
<input required class="form-control" type="text" name="name" id="name" placeholder="Digite el nombre de la temporada">
@error('name')
    <div class="text-danger">
        <p>{{ $message }}</p>
    </div>
@enderror
<br>
<label class="font-weight-bold text-capitalize form-label" for="start_date">Fecha de inicio</label>
<input required min="{{ now()->format('Y-m-d') }}" class="form-control" type="date" name="start_date" id="start_date">
@error('start_date')
    <div class="text-danger">
        <span>{{ $message }}</span>
    </div>
@enderror
<br>
<label class="font-weight-bold text-capitalize form-label" for="finish_date">Fecha de finalización</label>
<input required min="{{ now()->addday()->format('Y-m-d') }}" class="form-control" type="date" name="finish_date" id="finish_date">
@error('finish_date')
    <div class="text-danger">
        <span>{{ $message }}</span>
    </div>
@enderror
