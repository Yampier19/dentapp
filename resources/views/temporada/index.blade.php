@extends('layouts.admin.app')
@section('content')
    @if (session('store'))
        <script>
            Swal.fire(
                '¡Buen trabajo!',
                ' {{ session('store') }}',
                'success'
            )
        </script>
    @endif

    @if (session('update'))
        <script>
            Swal.fire(
                '¡Buen trabajo!',
                'Temporada editada correctamente',
                'success'
            )
        </script>
    @endif
    @if (session('delete'))
        <script>
            Swal.fire(
                '¡Buen trabajo!',
                'Temporada eliminada correctamente',
                'success'
            )
        </script>
    @endif

    @if (session('fail'))
    <script>
        Swal.fire(
            '¡Opsss!',
            'Ya hay una temporada con este rango de fechas',
            'error'
        )
    </script>
@endif
    <div class="">
        <div class="card col-sm-11 m-auto">
            <div class="card-body p-5">
                <h2 class="text-center text-capitalize font-weight-bold">
                    Gestor de temporadas
                </h2>
                <div class="row">
                    @include('temporada.edit')
                    @include('temporada.create')
                </div>
                <br>
                <table id="example" class="table table-light text-center">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Fecha de inicio</th>
                            <th>Fecha de Finalización</th>
                            <th>Opciónes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($temporadas as $temporada)
                            <tr>
                                <td class="font-weight-bold">{{ $loop->iteration }}.</td>
                                <td>{{ $temporada->name }}</td>
                                <td>{{ $temporada->start_date }}</td>
                                <td>{{ $temporada->finish_date }}</td>
                                <td>
                                    <button data-id="{{ $temporada->id }}" class="btn btn-sm edit" style="background-color:#51A2A7">
                                        <span class="iconify" data-width="15" data-icon="bxs:calendar-edit"></span>
                                    </button>
                                    <form class="d-inline-block" action="{{ url('season/' . $temporada->id . '/destroy') }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('¿Desea eliminar está temporada?')" class="btn btn-danger btn-sm">
                                            <span class="iconify" data-width="15" data-icon="fa6-solid:trash-can"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        const form = document.getElementById('form-temporada')
        $('.edit').click(function(e) {
            e.preventDefault();
            $('#edittemporada').modal('show');
            const id = e.target.closest('.edit').dataset.id;
            form.action = '/season/' + id + '/update';
            axios.get('/season/' + id + '/edit')
                .then(function(res) {
                    $('#name').val(res.data.data.name),
                        $('#start_date').val(res.data.data.start_date),
                        $('#finish_date').val(res.data.data.finish_date)
                })
        })
    </script>
@endsection
