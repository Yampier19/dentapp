<!DOCTYPE html>
<html>
<head>
  <title>Pusher Test</title>
  <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('577986b8e0866841eb12', {
      cluster: 'us2'
    });
    
    var email = "cliente@cliente.com";
    var channel = pusher.subscribe('puntos.'+email);
    channel.bind('puntosState', function(data) {
      alert(JSON.stringify(data));
    });
  </script>
</head>
    <body>
    <h1>Pusher Test</h1>
    <p>
        Try publishing an event to channel <code>premio.email</code>
        with event name <code>premioState</code>.
    </p>
    </body>
</html>