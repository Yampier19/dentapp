@extends('layouts.admin.app')

@section('content')




        <div class="row">
            <div class="col-md-12">

                
@if (Session::has('import'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('import') }}
</div>
@endif

<div class="card-header">
    <label for="">Listado de productos</label>
</div>
                <div class="card-body">

                    <table class="table table-bordered" id="example">
                        <thead>
                        <tr>
                            <th>Familia</th>
                            <th>Categoria</th>
                            <th>Subcategoria</th>
                            <th>Stock</th>
                            <th>Descripción</th>
                            <th>Estado</th>
                            <th>Precio</th>
                            <th>Estrellas</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody class="table-light">
                            @foreach($products as $product)
                                <tr>
                                    <td >{{ $product->familia ? $product->familia->descripcion_familia : "No posee" }}</td>
                                    <td >{{ $product->categoria }}</td>
                                    <td >{{ $product->subcategoria }}</td>
                                    <td >{{ $product->stock }}</td>
                                    <td >{{ $product->descripcion }}</td>
                                    <td >{{ $product->estado ? 'Activo' : 'Inactivo' }}</td>
                                    <td >{{ '$'.$product->precio }}</td>
                                    <td >{{ $product->numero_estrellas }}</td>
                                    <td>
                                        <form action="" method="post">
                                       <div class="row">
                                            <a href="{{ route('product.edit',$product->id_producto_categoria) }}" class="btn btn-sm btn-warning mx-auto"><i class="far fa-edit"></i></a>
                                            <a href="{{ route('product.confirm',$product->id_producto_categoria) }}" class="btn btn-sm btn-danger mx-auto"><i class="far fa-trash-alt"></i></a>
                                        </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>



            </div>
        </div>



@endsection
