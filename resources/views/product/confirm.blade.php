@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif
        
        <h1 class="mb-5">¿Deseas eliminar el siguiente producto? </h1>
        <h4>Categoria: </h4>
        <p>{{ $product->categoria }}</p>
        <h4>Subcategoria: </h4>
        <p>{{ $product->subcategoria }}</p>
        <h4>Descripcion: </h4>
        <p>{{ $product->descripcion }}</p>
        <form method="post" enctype="multipart/form-data" action="{{ route('product.destroy', $product->id_producto_categoria) }}">
            @method('DELETE')
            @csrf
            <div class="row col-md-12 mt-5 mx-auto">
                <button type="submit" class="redondo btn btn-danger col-md-4 mx-auto">
                    <i class="fas fa-trash-alt"></i> Eliminar
                </button>
                <a href="{{ route('product.list') }}" class="btn btn-primary col-md-4 mx-auto">Volver</a>
            </div>
        </form>
    </div>
@endsection
