@extends('layouts.admin.app')

@section('content')


@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif

    <div class="row ">
        <div class="col-md-12">

            <a href="{{ route('product.list') }}" id="" class="btn btn-danger">Listado de productos</a>

            <a href="{{ route('product.create') }}" class="btn btn-danger">Registrar productos</a>

            <a href="{{ route('product.import_csv') }}" class="btn btn-danger">Subida masiva por CSV</a>



        </div>
    </div>



@endsection
