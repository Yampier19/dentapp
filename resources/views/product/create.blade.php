@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">
            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="text-center">
                        <label for="" class="lead">Registrar producto</label>
                    </div>
                    <hr>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-12">
                            <label for="name_user">Familia Producto:</label>
                            <select name="FK_id_familia" id="" class="form-control @error('FK_id_familia') is-invalid @enderror">
                                @foreach ($familia as $i => $fam)
                                    <option value="{{ $fam->id_producto_familia }}">{{ $fam->descripcion_familia }}</option>
                                @endforeach
                            </select>
                            @error('FK_id_familia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-6">
                            <label for="name_user">Categoria:</label>
                            <input name="categoria" id="categoria" type="text" class="form-control @error('categoria') is-invalid @enderror">
                            @error('categoria')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Subcategoria:</label>
                            <input name="subcategoria" id="subcategoria" type="text" class="form-control @error('subcategoria') is-invalid @enderror">
                            @error('subcategoria')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Stock:</label>
                            <input name="stock" id="stock" type="number" min="0" class="form-control @error('stock') is-invalid @enderror">
                            @error('stock')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Descripcion:</label>
                            <input name="descripcion" id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror">
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Precio Unidad:</label>
                            <input name="precio" id="precio" type="number" min="0" step=".001" class="form-control @error('precio') is-invalid @enderror">
                            @error('precio')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Estado:</label>
                            <div class="form-check">
                                <input value="1" class="form-check-input" type="radio" name="estado" id="flexRadioDefault1" checked>
                                <label class="form-check-label" for="flexRadioDefault1">
                                    Producto Activado
                                </label>
                            </div>
                            <div class="form-check">
                                <input value="0" class="form-check-input" type="radio" name="estado" id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    Producto Desactivado
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="name_user">Numero de estrelllas:</label>
                            <input name="numero_estrellas" id="numero_estrellas" type="number" min="0" 
                                    class="form-control @error('numero_estrellas') is-invalid @enderror">
                            @error('numero_estrellas')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Subir foto:</label>
                            <input type="file" name="foto" id="upload" class="upload-box @error('foto') is-invalid @enderror" 
                            placeholder="Upload File" accept=".jpg,.jpeg,.png">
                            @error('foto')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <hr>
                    <div class="row col-md-12 my-3">
                        <button type="submit" class="btn btn-primary form-control">Guardar Producto</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
