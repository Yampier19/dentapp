@extends('layouts.admin.app')

@section('content')

    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-10 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">
                <form action="{{ route('product.update',$product->id_producto_categoria) }}" method="post"
                enctype="multipart/form-data">

                    @csrf
                    {{method_field('PATCH')}}

                    <div class="form-group">
                        <label for="name_user">Familia Producto:</label>
                        <select name="FK_id_familia" id="familia" class="form-control @error('FK_id_familia') is-invalid @enderror">
                            @foreach ($familia as $i => $fam)
                                <option value="{{ $fam->id_producto_familia }}" 
                                        {{ $fam->id_producto_familia == $product->FK_id_familia ? "selected" : "" }}>
                                    {{ $fam->descripcion_familia }}
                                </option>
                            @endforeach
                        </select>
                        @error('FK_id_familia')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="documento">Categoria:</label>
                        <input name="categoria" id="categoria" type="text" class="form-control @error('categoria') is-invalid @enderror" 
                                value="{{$product->categoria}}">
                        @error('categoria')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="primer_nombre">Subcategoria:</label>
                        <input name="subcategoria" id="subcategoria" type="text" class="form-control @error('subcategoria') is-invalid @enderror" 
                                value="{{$product->subcategoria}}">
                        @error('subcategoria')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="segundo_nombre">Stock:</label>
                        <input name="stock" id="stock" type="number" min="0" class="form-control @error('stock') is-invalid @enderror" 
                                value="{{$product->stock}}">
                        @error('stock')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="apellido">Descripcion:</label>
                        <input name="descripcion" id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" 
                                value="{{$product->descripcion}}">
                        @error('descripcion')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="direccion">Precio Unidad:</label>
                        <input name="precio" id="precio" type="number" min="0" step=".001" class="form-control @error('precio') is-invalid @enderror" 
                                value="{{$product->precio}}">
                        @error('precio')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="ciudad">Estado:</label>
                        <div class="form-check">
                            <input value="1" class="form-check-input" type="radio" name="estado" id="flexRadioDefault1" 
                                {{ $product->estado ? "checked" : "" }}>
                            <label class="form-check-label" for="flexRadioDefault1">
                                Producto Activado
                            </label>
                        </div>
                        <div class="form-check">
                            <input value="0" class="form-check-input" type="radio" name="estado" id="flexRadioDefault2"
                                {{ $product->estado ? "" : "checked" }}>
                            <label class="form-check-label" for="flexRadioDefault2">
                                Producto Desactivado
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ciudad">Numero de estrelllas:</label>
                        <input name="numero_estrellas" id="TIPO_PRODUCTO" type="number" min="0" 
                            class="form-control @error('numero_estrellas') is-invalid @enderror" value="{{$product->numero_estrellas}}">
                        @error('numero_estrellas')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="name_user">Subir foto:</label>
                        <input type="file" name="foto" id="upload" class="upload-box @error('foto') is-invalid @enderror" 
                        placeholder="Upload File" accept=".jpg,.jpeg,.png">
                        @error('foto')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <input class="btn btn-success my-3" type="submit" value="Actualizar Producto">

                </form>

            </div>
        </div>
    </div>

@endsection
