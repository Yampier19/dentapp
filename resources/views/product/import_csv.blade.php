@extends('layouts.admin.app')

@section('content')


<div class="container my-5">
    <div class="card-header" style="background:#e7344c">
       <label for="" class="lead text-white">Importar Excel</label>
    </div>
    <div class="card-body">
        @if(isset($errores))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                @foreach ($errores as $i => $error)
                    <p>
                        <span><strong><u>Fila</u></strong>: {{ $error->row() }}</span>
                        <span><strong><u>Columna</u></strong>: {{ $error->attribute() }}</span>
                        <span><strong><u>Error</u></strong>: {{ $error->errors()[0] }}</span>
                    </p>
                @endforeach
            </div>
        @endif
        <div class="alert alert-warning" role="alert">
            <h5>Por favor, antes de importar tu archivo Excel. Lee las instrucciones:</h5>
            <ol>
                <li value="1">Descarga la plantilla: <a href="{{url('csv/1.xlsx')}}"  download="plantilla_dentapp.xlsx" download>Click aqui para descargar el archivo</a></li>
                <li>No cambies el formato del archivo</li>
                <li>No pongas titulos o rotulos</li>
                <li>No pongas diseños o formatos</li>
                <li>Despues de importar tu archivo, recuerda validar la informacion en la lista de productos</li>
                <li>Lista de productos: <a href="{{route('product.list')}}">Ver productos</a> </li>
            </ol>
          </div>
        <form action="{{route('product.import')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row col-md-12">
                <input type="file" name="file" id="" accept=".xls,.xlsx,.csv">
                <button type="submit" class="btn btn-success ml-auto">Importar</button>
                @error('file')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </form>
    </div>
</div>

@endsection