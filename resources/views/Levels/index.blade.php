@extends('layouts.admin.app')
@push('scripts')
    <script src="{{ asset('js/level/level.js') }}"></script>
@endpush
@section('content')
    @if (session('crear'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Nivel creado con exito',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    @if (session('edit'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Nivel editado con exito',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
    @if (session('destroy'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Nivel eliminado con exito',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif

    @if (session('fail'))
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Ya existe un nivel con  este rango',
                showConfirmButton: false,
                timer: 2000
            })
        </script>
    @endif
    <div class="container-fluid">
        <div class="d-flex justify-content-end">
            @include('Levels.create')
            <a class="btn" href="{{ url('nivel/index') }}" style="background-color:##51A2A7">Ver niveles</a>
        <a class="btn btn-dark" href="{{route('level.actualizar')}}">Actualizar</a>
        </div>
        <div class="card p-4" style="border-radius:50px">
            <div class="card-body p-5">
                {{-- <div class="row mb-5">
                    <button id="btn1" class="btn color"><span class="iconify mr-2" data-icon="fontisto:doctor"></span> Doctor</button>
                    <button id="btn2" class="btn bg-gray"><span class="iconify mr-2" data-icon="bi:hospital"></span> Clinica</button>
                    <button id="btn3" class="btn bg-gray"><span class="iconify mr-2" data-icon="carbon:user-activity"></span> Asistente</button>
                </div> --}}
                <table id="example" class="table table-light table-hover table1">
                    <thead class="thead-light text-center">
                        <tr>
                            <th>#</th>
                            <th class="d-flex justify-content-center">Imagen</th>
                            <th>Nombre</th>
                            <th>Rango inicial</th>
                            <th>Meta</th>
                            {{-- <th>Clasificación</th> --}}
                            <th class="text-center">Opciónes</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($levels as $level)
                            <tr {{-- class="{{ $level->rol->rol }}" --}}>
                                <td class="font-weight-bold">{{ $loop->iteration }}.</td>
                                <td class="d-flex justify-content-center">
                                    <img width="100px" src="{{ $level->getFirstMediaUrl('level-image') }}" alt="">
                                </td>
                                <td class="font-weight-">{{ $level->nombre }}</td>
                                <td>{{ number_format($level->rango_inicio) }}</td>
                                <td><span class="iconify" data-icon="fa-solid:medal"></span> {{ number_format($level->meta) }}</td>
                                {{-- <td>
                                    <span class="badge @if ($level->rol->rol == 'Doctor') badge-success @else badge-warning @endif">{{ $level->rol->rol }}</span>
                                </td> --}}
                                <td class="text-center">
                                    <form class="d-inline-block" action="{{ route('level.destroy', $level->id_level) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button onclick="return confirm('¿Desea eliminar este nivel?')" type="submit" class="btn rounded btn-danger delete-level"><span class="iconify" data-icon="fontisto:trash"></span></button>
                                    </form>
                                    <button data-id="{{ $level->id_level }}" class="btn editando rounded btn" style="background-color:#51A2A7"><span class="iconify" data-icon="akar-icons:edit"></span></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
    <style>
        .color {
            background-color: #51A2A7 !important;
            color: white !important;
        }
    </style>
    <script>
        $(document).ready(function() {
            // $('.Clinica').hide();
            // $('.Asistente').hide();
            $('#btn1').click(function() {
                $('#btn1').addClass('color');
                $('#btn2').removeClass('color');
                $('#btn3').removeClass('color');

                $('.Clinica').hide();
                $('.Asistente').hide();
                $('.Doctor').show();
            })

            $('#btn2').click(function() {
                $('#btn2').addClass('color');
                $('#btn3').removeClass('color');
                $('#btn1').removeClass('color');

                $('.Clinica').show();
                $('.Doctor').hide();
                $('.Asistente').hide();
            })

            $('#btn3').click(function() {
                $('#btn3').addClass('color');
                $('#btn2').removeClass('color');
                $('#btn1').removeClass('color');

                $('.Clinica').hide();
                $('.Doctor').hide();
                $('.Asistente').show();
            })
        })
    </script>
@endsection
