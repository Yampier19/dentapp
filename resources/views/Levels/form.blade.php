<label class="form-label" for="nombre">Nombre</label>
<input required type="text" name="nombre" id="nombre" value="{{ old('nombre') }}" class="form-control @error('nombre') is-invalid @enderror" placeholder="Digite el nombre del nivel">
@error('nombre')
    <div class="invalid-feedback"></div>
    <span class="text-danger">{{ $message }}</span>
@enderror
<br>
<div class="row">
    <div class="col-sm-6">
        <label class="form-label" for="rango_inicio">Rango Inicio</label>
        <input required type="number" name="rango_inicio" value="{{ old('rango_inicio') }}" id="rango_inicio" class="form-control @error('rango_inicio') is-invalid @enderror" placeholder="Digite el rango del nivel">
        @error('rango_inicio')
            <div class="invalid-feedback"></div>
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-sm-6">
        <label class="form-label" for="meta">Meta</label>
        <input type="number" name="meta" id="meta" value="{{ old('meta') }}" class="form-control @error('meta') is-invalid @enderror" placeholder="Digite la meta del nivel">
        @error('meta')
            <div class="invalid-feedback"></div>
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
</div>
{{-- <div class="row mt-2"> --}}
    {{-- <div class="col-sm-12">
        <label class="form-label" for="meta">Tipo de usuario</label>
        <select name="loginrol_id" id="loginrol_id" class="form-control">
            <option value="" selected disabled>--Seleccione una opción--</option>
            @foreach ($roles as $rol)
                <option value="{{ $rol->id_loginrol }}">{{ $rol->rol }}</option>
            @endforeach
        </select>
        @error('loginrol_id')
            <div class="invalid-feedback"></div>
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div> --}}
    {{-- <div class="col-sm-6">
        <label class="form-label" for="meta">Valor por peso</label>
        <input type="number" name="value_weight" id="value_weight" value="{{ old('value_weight') }}" class="form-control @error('value_weight') is-invalid @enderror" placeholder="Digite el valor por peso">
        @error('value_weight')
            <div class="invalid-feedback"></div>
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div> --}}
{{-- </div> --}}
<br>
<label class="form-label" for="level-image">Imagen del nivel</label>
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="level-image">Subir</span>
    </div>
    <div class="custom-file">
        <input accept="image/*" type="file" class="custom-file-input @error('level-image') is-invalid @enderror" name="level-image" id="level-image" aria-describedby="inputGroupFileAddon01">
        <label class="custom-file-label" for="inputGroupFile01">Seleccionar archivo...</label>
    </div>
</div>
@error('level-image')
    <div class="invalid-feedback"></div>
    <span class="text-danger">{{ $message }}</span>
@enderror
