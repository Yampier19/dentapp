<!-- Button trigger modal -->
<button id="creando" type="button" class="btn rounded" style="background-color:#51A2A7">
    <span class="iconify" data-icon="akar-icons:circle-plus-fill"></span> Crear Nivel
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-level" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    @include('Levels.form')
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button id="agregar" type="submit" class="btn btn-info">Agregar</button>
                        <button id="guardando" type="submit" class="btn btn-info">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
