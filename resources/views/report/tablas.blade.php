@extends('layouts.admin.app')

@section('content')


@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif


<iframe src="{{url('https://app-peoplemarketing.com/react/ScriptCase/Tablas/DentApp_Tablas/dentappweb.filtros/')}}" width="100%" height="700"></iframe>


@endsection
