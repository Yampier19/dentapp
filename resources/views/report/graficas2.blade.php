@extends('layouts.admin.app')

@section('content')


@if (Session::has('create'))
<div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('create') }}
</div>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif

<!-- <iframe src="{{url('https://app-peoplemarketing.com/react/ScriptCase/Graficas/DentApp_Graficas/menu/')}}" width="100%" height="700"></iframe>
 -->
 
<style>
    @media (max-width: 800px) {
        .total-block{
            font-size: 12px;
        }
    }
</style>

<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

<div>

    <!-- BLOQUE ESTADISTICO DEL INICIO DE TOTALES -->
    <div class="row justify-content-center my-3 px-2">
        <!-- TOTAL DE USUARIOS -->
        <div class="row mx-2">
            <div class="col text-center container" style="box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2); min-height: 100px">
                <div class="row justify-content-center h-100 total-block">
                    <div class="col-5 h-100 container" style="display: flex; background-color: #51A2A7">
                        <div class="text-center m-auto">
                            <i class="fas fa-3x fa-users fa-lg ic text-white"></i>
                        </div>
                    </div>
                    <div class="col-7 my-auto total-block-text">
                        <span class="font-weight-bold">Total Usuarios</span>
                        <span class="d-block text-muted display-4">{{ $users }}</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- REGISTRO DE USUARIOS (QUE HAYAN HECHO EL PROCESO COMPLETO) -->
        <div class="row mx-2">
            <div class="col text-center container" style="box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2); min-height: 100px">
                <div class="row justify-content-center h-100 total-block">
                    <div class="col-5 h-100 container" style="display: flex; background-color: #51A2A7">
                        <div class="text-center m-auto">
                            <i class="fas fa-3x fa-user fa-lg ic text-white"></i>
                        </div>
                    </div>
                    <div class="col-7 my-auto total-block-text">
                        <span class="font-weight-bold">Registros</span>
                        <span class="d-block text-muted display-4">{{ $registros }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mx-2">
            <div class="col text-center container" style="box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2); min-height: 100px">
                <div class="row justify-content-center h-100 total-block">
                    <div class="col-5 h-100 container" style="display: flex; background-color: #51A2A7">
                        <div class="text-center m-auto">
                            <i class="fas fa-3x fa-list fa-lg ic text-white"></i>
                        </div>
                    </div>
                    <div class="col-7 my-auto total-block-text">
                        <span class="font-weight-bold">Interacciones</span>
                        <span class="d-block text-muted display-4">{{ $interacciones }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center my-3 px-2">
        <div class="row mx-2">
            <div class="col text-center container" style="box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2); min-height: 100px">
                <div class="row justify-content-center h-100 total-block">
                    <div class="col-5 h-100 container" style="display: flex; background-color: #F28D8D">
                        <div class="text-center m-auto">
                            <i class="fas fa-3x fa-pencil fa-lg ic text-white"></i>
                        </div>
                    </div>
                    <div class="col-7 my-auto total-block-text">
                        <span class="font-weight-bold">Retos Realizados</span>
                        <span class="d-block text-muted display-4">{{ $retos }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mx-2">
            <div class="col text-center container" style="box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2); min-height: 100px">
                <div class="row justify-content-center h-100 total-block">
                    <div class="col-5 h-100 container" style="display: flex; background-color: #F28D8D">
                        <div class="text-center m-auto">
                            <i class="fas fa-3x fa-file-invoice fa-lg ic text-white"></i>
                        </div>
                    </div>
                    <div class="col-7 my-auto total-block-text">
                        <span class="font-weight-bold">Facturas</span>
                        <span class="d-block text-muted display-4">{{ $factura }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mx-2">
            <div class="col text-center container" style="box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.2); min-height: 100px">
                <div class="row justify-content-center h-100 total-block">
                    <div class="col-5 h-100 container" style="display: flex; background-color: #F28D8D">
                        <div class="text-center m-auto">
                            <i class="fas fa-3x fa-money fa-lg ic text-white"></i>
                        </div>
                    </div>
                    <div class="col-7 my-auto total-block-text">
                        <span class="font-weight-bold">Valor Facturas</span>
                        <span class="d-block text-muted display-4">${{ $monto }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN BLOQUE ESTADISTICO DEL INICIO DE TOTALES -->

    <!-- LISTADO DE GRAFICAS -->

        <!-- GRAFICA DE PRODUCTOS ESPECIALES LLEVADOS EN FACTURACION -->
        <div class="col-11 card p-3 mx-auto">
            <label for="" class="lead font-weight-bold text-dark ml-3">Productos vs Cantidad en Facturación</label>
            <div id="chart0"></div>
            @foreach($productos as $a)
                <input type="hidden" class="chartC0Label" value="{{ $a->producto }}">
                <input type="hidden" class="chartC0Data" value="{{ $a->total }}">
            @endforeach

            <script>
                var data = new Array(); 
                var xAsysLabels = new Array();

                $(".chartC0Label").each(function(indice,elemento){
                    xAsysLabels.push($(this).val());
                });
                $(".chartC0Data").each(function(indice,elemento){
                    data.push(parseInt($(this).val()));
                });

                var options8 = {
                series: [
                    {
                    name: 'Puntos',
                    data: data
                    }
                ],
                chart: {
                    height: 350,
                    type: 'bar',
                },
                plotOptions: {
                    bar: {
                    borderRadius: 10,
                    columnWidth: '50%',
                    }
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    width: 2
                },
                grid: {
                    row: {
                    colors: ['#fff', '#f2f2f2']
                    }
                },
                xaxis: {
                    labels: {
                    rotate: -45
                    },
                    categories: xAsysLabels,
                    tickPlacement: 'on'
                },
                yaxis: {
                    title: {
                    text: 'Puntos',
                    },
                },
                fill: {
                    type: 'gradient',
                    gradient: {
                    shade: 'light',
                    type: "horizontal",
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 0.85,
                    opacityTo: 0.85,
                    stops: [50, 0, 100]
                    },
                },
                legend: {
                    show: true
                },
                };

                var chart0 = new ApexCharts(document.querySelector("#chart0"), options8);
                chart0.render();
            </script>
        </div>
        <!-- FIN GRAFICA -->

        <!-- GRAFICA PIE ESPECIALIDADES -->
            <div class="col-6 mx-auto" id="chartC1"></div>
            @foreach($pie as $a)
                <input type="hidden" class="chartC1Label" value="{{ $a->especialidad }}">
                <input type="hidden" class="chartC1Data" value="{{ $a->total }}">
            @endforeach

            <script>
                var data = new Array(); 
                var xAsysLabels = new Array();

                $(".chartC1Label").each(function(indice,elemento){
                    xAsysLabels.push($(this).val());
                });
                $(".chartC1Data").each(function(indice,elemento){
                    data.push(parseInt($(this).val()));
                });

                var options = {
                    series: data,
                    chart: {
                        width: '100%',
                        type: 'pie',
                    },
                    labels: xAsysLabels,
                    theme: {
                        monochrome: {
                            enabled: false
                        }
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                            offset: -5
                            }
                        }
                    },/* 
                    title: {
                        text: "Total de Especialistas"
                    }, */
                    colors:['#F44336', '#E91E63', '#9C27B0'],
                    /* dataLabels: {
                        formatter(val, opts) {
                            const name = opts.w.globals.labels[opts.seriesIndex]
                            return [name, val.toFixed(1) + '%']
                        }
                    }, */
                    legend: {
                        show: true
                    }
                };

                var chart = new ApexCharts(document.querySelector("#chartC1"), options);
                chart.render();
            </script>
        <!-- FIN GRAFICA -->
        
        <!-- GRAFICA BARRA HORIZONTAL USUARIOS POR NIVEL -->
            <div class="col-6 mx-auto" id="chartC2"></div>
            @foreach($bar as $a)
                <input type="hidden" class="chartC2Label" value="{{ $a->nivel }}">
                <input type="hidden" class="chartC2Data" value="{{ $a->total }}">
            @endforeach

            <script>
                var data = new Array(); 
                var xAsysLabels = new Array();

                $(".chartC2Label").each(function(indice,elemento){
                    xAsysLabels.push($(this).val());
                });
                $(".chartC2Data").each(function(indice,elemento){
                    data.push(parseInt($(this).val()));
                });

                var options = {
                    series: [{
                        data: data
                    }],
                    chart: {
                        type: 'bar',
                        height: 350
                    },
                    colors:['#F44336', '#E91E63', '#9C27B0'],
                    title: {
                        text: 'Número de Usuarios por Nivel',
                        align: 'left'
                    },
                    plotOptions: {
                        bar: {
                            borderRadius: 4,
                            horizontal: true,
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    xaxis: {
                        categories: xAsysLabels,
                    },
                };

                var chart = new ApexCharts(document.querySelector("#chartC2"), options);
                chart.render();
            </script>
        <!-- FIN GRAFICA -->

        <!-- GRAFICA DE PASTEL TOTAL DE SEGMENTO DE USUARIOS -->
            <div class="col-6 mx-auto" id="chartC3"></div>
            @foreach($pastel as $a)
                <input type="hidden" class="chartC3Label" value="{{ $a->segmento }}">
                <input type="hidden" class="chartC3Data" value="{{ $a->total }}">
            @endforeach

            <script>
                var data = new Array(); 
                var xAsysLabels = new Array();
                var total = 0;

                $(".chartC3Label").each(function(indice,elemento){
                    xAsysLabels.push($(this).val());
                });
                $(".chartC3Data").each(function(indice,elemento){
                    data.push(parseInt($(this).val()));
                    total+=parseInt($(this).val());
                });

                var values = data.map(function(num) {
                    return Math.round((num*100)/total);
                });

                var options = {
                    series: values,
                    chart: {
                        height: 350,
                        type: 'radialBar',
                    },
                    plotOptions: {
                        radialBar: {
                            dataLabels: {
                                name: {
                                    fontSize: '22px',
                                },
                                value: {
                                    fontSize: '16px',
                                    formatter: function (val) {
                                        return Math.round((val*total)/100)
                                    }
                                },
                                total: {
                                    show: true,
                                    label: 'Total',
                                    formatter: function (w) {
                                        return total
                                    }
                                }
                            }
                        }
                    },
                    labels: xAsysLabels,
                    colors:['#F44336', '#E91E63', '#9C27B0'],
                    legend: {
                        show: true,
                    },
                };

                var chart = new ApexCharts(document.querySelector("#chartC3"), options);
                chart.render();
            </script>
        <!-- FIN GRAFICA -->

        <!-- GRAFICA DE LINEA | RELACION USUARIOS CARGANDO FACTURAS VS FACTURA CARGADAS TOTAL -->
            <div class="col-6 mx-auto" id="chartC4"></div>
            @foreach($lineal as $a)
                <input type="hidden" class="chartC4Label" value="{{ $a->mes }}">
                <input type="hidden" class="chartC4DataUser" value="{{ $a->total_user }}">
                <input type="hidden" class="chartC4DataFact" value="{{ $a->total_fact }}">
                <input type="hidden" class="chartC4Data" value="{{ $a->total_fact }}" name="{{ $a->total_user }}" id="{{ $a->mes }}">
            @endforeach

            <script>
                var dataUser = [0,0,0,0,0,0,0,0,0,0,0,0];
                var dataFact = [0,0,0,0,0,0,0,0,0,0,0,0]; 
                var xAsysLabels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
                
                $(".chartC4Data").each(function(indice,elemento){
                    var mes = parseInt($(this).attr('id'));
                    var users = parseInt($(this).attr('name'));
                    var facts = parseInt($(this).val());

                    dataUser[mes] = users;
                    dataFact[mes] = facts;
                });

                var options = {
                    series: [
                        { name: 'Usuarios', data: dataUser },
                        { name: 'Facturas', data: dataFact }
                    ],
                    chart: {
                        height: 500,
                        type: 'line',
                        zoom: {
                            enabled: true
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'straight'
                    },
                    title: {
                        text: 'Facturas Cargadas vs Usuarios Cargando Facturas',
                        align: 'left'
                    },
                    grid: {
                        row: {
                            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                            opacity: 0.5
                        },
                    },
                    xaxis: {
                        categories: xAsysLabels,
                    }
                };

                var chart = new ApexCharts(document.querySelector("#chartC4"), options);
                chart.render();
            </script>
        <!-- FIN GRAFICA -->

        <!-- GRAFICA DE AREA | VALOR FACTURA VS VALOR PRODUCTOS EXTRAS -->
            <div class="col-6 mx-auto" id="chartC5"></div>
            @foreach($lineal as $a)
                <input type="hidden" class="chartC5FactLabel" value="{{ $a->mes }}">
                <input type="hidden" class="chartC5DataFact" value="{{ $a->total_puntos }}">
                <input type="hidden" class="chartC5Data1" value="{{ $a->total_puntos }}" id="{{ $a->mes }}">
            @endforeach

            @foreach($especiales as $a)
                <input type="hidden" class="chartC5ProLabel" value="{{ $a->mes }}">
                <input type="hidden" class="chartC5DataPro" value="{{ $a->total_puntos }}">
                <input type="hidden" class="chartC5Data2" value="{{ $a->total_puntos }}" id="{{ $a->mes }}">
            @endforeach

            <script>
                var dataFact = [0,0,0,0,0,0,0,0,0,0,0,0]; 
                var dataProd = [0,0,0,0,0,0,0,0,0,0,0,0];
                var xAsysLabels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

                $(".chartC5Data1").each(function(indice,elemento){
                    var mes = parseInt($(this).attr('id'));
                    var values = parseInt($(this).val());
                    dataFact[mes] = values;
                });

                $(".chartC5Data2").each(function(indice,elemento){
                    var mes = parseInt($(this).attr('id'));
                    var values = parseInt($(this).val());
                    dataProd[mes] = values;
                    dataFact[mes] -= values;
                });

                var options = {
                    series: [
                        {
                            name: 'Facturas',
                            data: dataFact
                        }, 
                        {
                            name: 'Productos Espexiales',
                            data: dataProd
                        }
                    ],
                    title: {
                        text: 'Puntos Facturas vs Puntos Productos',
                        align: 'left'
                    },
                    chart: {
                        height: 350,
                        type: 'area'
                    },
                    dataLabels: {
                        enabled: true,
                    },
                    stroke: {
                        curve: 'smooth'
                    },
                    xaxis: {
                        categories: xAsysLabels
                    },
                    tooltip: {
                        x: {
                            format: 'dd/MM/yy HH:mm'
                        },
                    },
                };

                var chart = new ApexCharts(document.querySelector("#chartC5"), options);
                chart.render();
            </script>
        <!-- FIN GRAFICA -->
        
        <!-- GRAFICA DE TREEMAP | RELACIÓN DE VALOR FACTURAS POR ESPECIALIDAD DE USUARIOS -->
            <div class="col-6 mx-auto" id="chartC6"></div>
            @foreach($treemap as $a)
                <input type="hidden" class="chartC6Label" value="{{ $a->especialidad }}">
                <input type="hidden" class="chartC6Data" name="{{ $a->especialidad }}" value="{{ $a->total }}">
            @endforeach

            <script>
                var data = new Array(); 
                var xAsysLabels = new Array();

                $(".chartC6Label").each(function(indice,elemento){
                    xAsysLabels.push($(this).val());
                });

                $(".chartC6Data").each(function(indice,elemento){
                    var label = $(this).attr('name');
                    var value = parseInt($(this).val());
                    var json = {
                        x: label,
                        y: value
                    };

                    data.push(json);
                });

                var options = {
                    series: [
                        {
                            name: 'Especialidades',
                            data: data
                        },
                    ],
                    legend: {
                        show: true
                    },
                    chart: {
                        height: 350,
                        type: 'treemap'
                    },
                    title: {
                        text: 'Valor de Factura por Especialidades',
                        align: 'left'
                    }
                };

                var chart = new ApexCharts(document.querySelector("#chartC6"), options);
                chart.render();
            </script>
        <!-- FIN LISTADO DE GRAFICAS -->

        <!-- GRAFICA BARRA HORIZONTAL STACKED PUNTOS GANADOS DISTINTAS FORMAS POR MES -->
            <div class="col-6 mx-auto" id="chartC7"></div>
            @foreach($stacked as $a)
                <input type="hidden" class="chartC7Mes" value="{{ $a->mes }}">
                <input type="hidden" class="chartC7Tipo" value="{{ $a->tipo }}">
                <input type="hidden" class="chartC7Data" value="{{ $a->total }}">
                <input type="hidden" class="chartC7All" value="{{ $a->total }}" name="{{ $a->mes }}" id="{{ $a->tipo }}">
            @endforeach

            <script>
                var data = [
                        {
                            name: 'Trivia',
                            data: [0,0,0,0,0,0,0,0,0,0,0,0]
                        }, 
                        {
                            name: 'Encuesta',
                            data: [0,0,0,0,0,0,0,0,0,0,0,0]
                        }, 
                        {
                            name: 'Factura',
                            data: [0,0,0,0,0,0,0,0,0,0,0,0]
                        }, 
                    ];
                
                var xAsysLabels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

                $(".chartC7All").each(function(indice,elemento){
                    var tipo = $(this).attr('id');
                    var mes = parseInt($(this).attr('name'));
                    var value = parseInt($(this).val());

                    //console.log(data[tipo].data[mes]);
                    //0 = Trivia
                    //1 = Encuesta
                    //2 = Factura

                    if(tipo == "Trivia") tipo = 0;
                    if(tipo == "Encuesta") tipo = 1;
                    if(tipo == "Factura") tipo = 2;

                    data[tipo].data[mes-1] = value;
                });

                var options = {
                    series: data,
                    chart: {
                        type: 'bar',
                        height: 350,
                        stacked: true,
                    },
                    plotOptions: {
                        bar: {
                            horizontal: true,
                        },
                    },
                    stroke: {
                        width: 1,
                        colors: ['#fff']
                    },
                    title: {
                        text: 'Puntos Generadors por Retos'
                    },
                    xaxis: {
                        categories: xAsysLabels,
                    },
                    yaxis: {
                        title: {
                            text: undefined
                        },
                    },
                    tooltip: {
                        y: {
                            formatter: function (val) {
                                return val + " Puntos"
                            }
                        }
                    },
                    fill: {
                        opacity: 1
                    },
                    legend: {
                        position: 'top',
                        horizontalAlign: 'left',
                        offsetX: 40
                    }
                };

                var chart = new ApexCharts(document.querySelector("#chartC7"), options);
                chart.render();
            </script>
        <!-- FIN GRAFICA -->

        <!-- GRAFICA BARRA HORIZONTAL USUARIOS POR NIVEL -->
            <div class="col-6 mx-auto" id="chartC8"></div>
            @foreach($column as $a)
                <input type="hidden" class="chartC8Label" value="{{ $a->tipo }}">
                <input type="hidden" class="chartC8Data" value="{{ $a->total }}">
            @endforeach

            <script>
                var data = new Array(); 
                var xAsysLabels = new Array();

                $(".chartC8Label").each(function(indice,elemento){
                    xAsysLabels.push($(this).val());
                });
                $(".chartC8Data").each(function(indice,elemento){
                    data.push(parseInt($(this).val()));
                });
                
                var options = {
                    series: [
                        {
                            data: data
                        }
                    ],
                    chart: {
                        height: 350,
                        type: 'bar',
                    },
                    colors: ['#F44336', '#E91E63', '#9C27B0'],
                    plotOptions: {
                        bar: {
                            columnWidth: '45%',
                            distributed: true,
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    legend: {
                        show: true
                    },
                    xaxis: {
                        categories: xAsysLabels,
                        labels: {
                            style: {
                                colors: ['#F44336', '#E91E63', '#9C27B0'],
                                fontSize: '12px'
                            }
                        }
                    },
                    title: {
                        text: 'Puntos por distintas Interacciones',
                        align: 'left',
                        style: {
                            color: '#444'
                        }
                    }
                };

                var chart = new ApexCharts(document.querySelector("#chartC8"), options);
                chart.render();
            </script>
        <!-- FIN GRAFICA -->
    <!-- FIN DE TODAS LAS GRAFICAS -->
</div>
@endsection

