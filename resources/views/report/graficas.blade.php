@extends('layouts.admin.app')

@section('content')

<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<style>
  .divOculto {
    display: none;
  }
</style>

<div class="col-md-12">
    <div class="row my-3">
      <div class="col-md-12">
        @if($filter)
          <div class="row mb-2">
              <div class="col-md-2">
                <a href="{{ route('report.graficas')}}">
                  <input class="btn form-control text-white" style="background-color: #F28D8D; border-radius: 40px;" value="Limpiar Filtro">
                </a>
              </div>
          </div>
        @endif
        <!-- FORMULARIO PARA FILTRAR LOS DATOS -->
        <form action="{{ route('report.graficas.filter')}}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-2">
                    <select name="mes" id="" class="form-control text-secondary">
                        <option value="" selected>MES</option>
                        <option value="1" {{ $mes == 1 ? "selected" : "" }}>ENERO</option>
                        <option value="2" {{ $mes == 2 ? "selected" : "" }}>FEBRERO</option>
                        <option value="3" {{ $mes == 3 ? "selected" : "" }}>MARZO</option>
                        <option value="4" {{ $mes == 4 ? "selected" : "" }}>ABRIL</option>
                        <option value="5" {{ $mes == 5 ? "selected" : "" }}>MAYO</option>
                        <option value="6" {{ $mes == 6 ? "selected" : "" }}>JUNIO</option>
                        <option value="7" {{ $mes == 7 ? "selected" : "" }}>JULIO</option>
                        <option value="8" {{ $mes == 8 ? "selected" : "" }}>AGOSTO</option>
                        <option value="9" {{ $mes == 9 ? "selected" : "" }}>SEPTIEMBRE</option>
                        <option value="10" {{ $mes == 10 ? "selected" : "" }}>OCTUBRE</option>
                        <option value="11" {{ $mes == 11 ? "selected" : "" }}>NOVIEMBRE</option>
                        <option value="12" {{ $mes == 12 ? "selected" : "" }}>DICIEMBRE</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="segmento" id="" class="form-control text-secondary">
                        <option value="" selected>SEGMENTO</option>
                        <option value="invest" {{ $segmento == "invest" ? "selected" : "" }}>INVEST</option>
                        <option value="focus" {{ $segmento == "focus" ? "selected" : "" }}>FOCUS</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select name="nivel" id="" class="form-control text-secondary">
                        <option value="" selected>NIVEL</option>
                        <option value="bronce" {{ $nivel == "bronce" ? "selected" : "" }}>BRONCE</option>
                        <option value="plata" {{ $nivel == "plata" ? "selected" : "" }}>PLATA</option>
                        <option value="oro" {{ $nivel == "oro" ? "selected" : "" }}>ORO</option>
                        <option value="diamante" {{ $nivel == "diamante" ? "selected" : "" }}>DIAMANTE</option>
                        <option value="zafiro" {{ $nivel == "zafiro" ? "selected" : "" }}>ZAFIRO</option>

                    </select>
                </div>
                <div class="col-md-2">
                  <input class="btn form-control text-white" style="background-color: #51A2A7; border-radius: 40px;" type="submit" value="FILTRAR">
                </div>
            </div>
        </form>
        <!-- FIN FORMULARIO -->
      </div>
    </div>
    <div class="row">
      <div class="col">
          <div class="card card-stats">
            <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon w-100 text-center mb-2">
                  <span class="material-icons mr-2">person</span>

                </div>
                <div class="text-center">
                  <p class="card-category">Total de usuarios</p>
                  <h3 class="card-title">{{ $registros + $users }}</h3>
                </div>
            </div>
          </div>
      </div>
      <div class="col">
        <a href="{{route('cliente.userdemo')}}">
          <div class="card card-stats">
            <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon w-100 text-center mb-2">
                  <span class="material-icons mr-2">engineering</span>

                </div>
                <div class="text-center">
                  <p class="card-category">Usuarios demo</p>
                  <h3 class="card-title">{{ $registros }}</h3>
                </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col">
        <a href="{{route('cliente.index')}}">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon w-100 text-center mb-2">
                  <span class="material-icons mr-2">group</span>
                </div>
                <div class="text-center">
                  <p class="card-category">Total doctores</p>
                  <h3 class="card-title">{{ $users }}</h3>
                </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col" >
        <a href="{{route('cliente.interacciones')}}">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
              <div class="card-icon w-100 text-center mb-2">
                <span class="material-icons mr-2">layers</span>
              </div>
              <div class="text-center">
                <p class="card-category">Interacciones</p>
                <h3 class="card-title">{{ $interacciones }}</h3>
              </div>
          </div>
        </div>
      </div>
      <div class="col">
        <a href="{{route('training.list_user')}}">
        <div class="card card-stats">
          <div class="card-header card-header-info card-header-icon">
              <div class="card-icon w-100 text-center mb-2">
                <span class="material-icons mr-2">school</span>
              </div>
              <div class="text-center">
                <p class="card-category">Capacitaciones realizadas</p>
                <h3 class="card-title">{{ $retos }}</h3>
              </div>
          </div>
        </div>
      </a>
      </div>
      <div class="col">
        <a href="{{route('factura.listado')}}">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon w-100 text-center mb-2">
                  <span class="material-icons mr-2">receipt</span>
                </div>
                <div class="text-center">
                  <p class="card-category">Facturas</p>
                  <h3 class="card-title">{{ $factura }}</h3>
                </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col">
        <a href="{{route('canjeado.index')}}">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon w-100 text-center mb-2">
                  <span class="material-icons mr-2">redeem</span>
                </div>
                <div class="text-center">
                  <p class="card-category">Premios Canjeados</p>
                  <h3 class="card-title">{{ $canjeados }}</h3>
                </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  <div class="row">
    <div class="col">
      <div class="card card-stats">
        <div class="card-header card-header-info card-header-icon">
            <div class="card-icon w-100 text-center mb-2">
              <i class="fi fi-rr-dollar"></i><i class="fi fi-rr-receipt"></i>
            </div>
            <div class="text-center">
              <p class="card-category">Valor facturas</p>
              <h3 class="card-title">${{ $monto }} COP</h3>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

    <div class="col-md-12 mx-auto">
    
        <div id="div0">
            <div class="row">
              <!-- GRAFICA COLUMNA PRODUCTOS ESPECIALES -->
                <div class="col-11 card p-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Productos vs Cantidad en Facturación</label>
                    <div id="chart0"></div>
                    @foreach($productos as $a)
                        <input type="hidden" class="chartC0Label" value="{{ $a->producto }}">
                        <input type="hidden" class="chartC0Data" value="{{ $a->total }}">
                    @endforeach

                    <script>
                      var data = new Array(); 
                      var xAsysLabels = new Array();

                      $(".chartC0Label").each(function(indice,elemento){
                          xAsysLabels.push($(this).val());
                      });
                      $(".chartC0Data").each(function(indice,elemento){
                          data.push(parseInt($(this).val()));
                      });

                      var options8 = {
                        series: [
                          {
                            name: 'Total',
                            data: data
                          }
                        ],
                        chart: {
                          height: 350,
                          type: 'bar',
                        },
                        plotOptions: {
                          bar: {
                            borderRadius: 10,
                            columnWidth: '50%',
                          }
                        },
                        dataLabels: {
                          enabled: false
                        },
                        stroke: {
                          width: 2
                        },
                        grid: {
                          row: {
                            colors: ['#fff', '#f2f2f2']
                          }
                        },
                        xaxis: {
                          labels: {
                            rotate: -45
                          },
                          categories: xAsysLabels,
                          tickPlacement: 'on'
                        },
                        yaxis: {
                          title: {
                            text: 'Total',
                          },
                        },
                        fill: {
                          type: 'gradient',
                          gradient: {
                            shade: 'light',
                            type: "horizontal",
                            shadeIntensity: 0.25,
                            gradientToColors: undefined,
                            inverseColors: true,
                            opacityFrom: 0.85,
                            opacityTo: 0.85,
                            stops: [50, 0, 100]
                          },
                        },
                        legend: {
                          show: true
                        },
                      };

                      var chart0 = new ApexCharts(document.querySelector("#chart0"), options8);
                      chart0.render();
                    </script>
                </div>
              <!-- FIN GRAFICA COLUMNAS -->
            </div>
        </div>

        <div id="div1" class="my-2">
            <div class="row">
              <!-- GRAFICA PIE ESPECIALIDADES -->
                <div class="col-md-5 card pt-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Especializacion</label>
                    <div id="chart" class="mx-auto"></div>
                    @foreach($pie as $a)
                        <input type="hidden" class="chartC1Label" value="{{ $a->especialidad }}">
                        <input type="hidden" class="chartC1Data" value="{{ $a->total }}">
                    @endforeach

                    <script>
                        var data = new Array(); 
                        var xAsysLabels = new Array();

                        $(".chartC1Label").each(function(indice,elemento){
                            xAsysLabels.push($(this).val());
                        });

                        $(".chartC1Data").each(function(indice,elemento){
                            data.push(parseInt($(this).val()));
                        });

                        if(data.length == 0 && xAsysLabels.length == 0){
                          xAsysLabels.push("Sin Coincidencias");
                          data.push(1);
                        }

                        

                        var options = {
                          series: data,
                          chart: {
                            width: 600,
                            type: 'pie',
                          },
                          colors:['#F22738', '#F28D8D', '#156A6E', '#519EA3', '#64C1C6', '#40010D'],
                          labels: xAsysLabels,
                          responsive: [
                            { breakpoint: 1899, options: { chart: { width: 500 }, legend: { position: 'bottom' } } },
                            { breakpoint: 1400, options: { chart: { width: 400 }, legend: { position: 'bottom' } } },
                            { breakpoint: 1270, options: { chart: { width: 350 }, legend: { position: 'bottom' } } },
                            { breakpoint: 1165, options: { chart: { width: 300 }, legend: { position: 'bottom' } } }]
                        };

                        var chart = new ApexCharts(document.querySelector("#chart"), options);
                        chart.render();
                    </script>
                </div>
              <!-- FIN GRAFICAS DE PIE -->

              <!-- GRAFICA BARRA HORIZONTAL USUARIOS POR NIVEL -->
                <div class="col-md-5 card p-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Usuarios por nivel</label>
                    <div id="chart2"></div>
                    @foreach($bar as $a)
                        <input type="hidden" class="chartC2Label" value="{{ $a->nivel }}">
                        <input type="hidden" class="chartC2Data" value="{{ $a->total }}" name="{{ $a->nivel }}">
                    @endforeach

                    <script>
                      var data = [0, 0, 0, 0, 0]; 
                      var xAsysLabels = ['Oro', 'Plata', 'Bronce', 'Diamante', 'Zafiro'];

                      $(".chartC2Data").each(function(indice,elemento){
                          var value = parseInt($(this).val());
                          var nivel = $(this).attr('name');
                          
                          //0 = Oro
                          //1 = Plata
                          //2 = Bronce
                          //3 = Diamante
                          //4 = Zafiro

                          if(nivel == "Oro") nivel = 0;
                          if(nivel == "Plata") nivel = 1;
                          if(nivel == "Bronce") nivel = 2;
                          if(nivel == "Diamante") nivel = 3;
                          if(nivel == "Zafiro") nivel = 4;

                          data[nivel] = value;
                      });
                      
                      var options2 = {
                          series: [{
                              data: data
                          }],
                          chart: {
                              type: 'bar',
                              height: 350
                          },
                          colors:['#F22738', '#F28D8D', '#156A6E', '#519EA3', '#64C1C6'],
                          plotOptions: {
                              bar: {
                                  borderRadius: 4,
                                  horizontal: true,
                              }
                          },
                          dataLabels: {
                              enabled: false
                          },
                          xaxis: {
                              categories: xAsysLabels
                          }
                      };

                      var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
                      chart2.render();
                    </script>
                </div>
              <!-- FIN GRAFICAS DE BARRA -->
            </div>
        </div>

        <div id="div2">
            <div class="row ">
              <!-- GRAFICA DE PASTEL TOTAL DE SEGMENTO DE USUARIOS -->
                <div class="col-md-5 card p-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Segmentos - Cantidad de usuarios <br> Invest - Focus</label>
                    <div id="chart3"></div>
                    @foreach($pastel as $a)
                        <input type="hidden" class="chartC3Label" value="{{ $a->segmento }}">
                        <input type="hidden" class="chartC3Data" value="{{ $a->total }}">
                    @endforeach

                    <script>
                      var data = new Array(); 
                      var xAsysLabels = new Array();
                      var total = 0;

                      $(".chartC3Label").each(function(indice,elemento){
                          xAsysLabels.push($(this).val());
                      });
                      $(".chartC3Data").each(function(indice,elemento){
                          data.push(parseInt($(this).val()));
                          total+=parseInt($(this).val());
                      });

                      if(data.length == 0 && xAsysLabels.length == 0){
                          xAsysLabels.push("Sin Coincidencias");
                          data.push(1);
                      }

                      var values = data.map(function(num) {
                          return Math.round((num*100)/total);
                      });

                      var options3 = {
                          series: values,
                          chart: {
                              height: 350,
                              type: 'radialBar',
                          },
                          plotOptions: {
                              radialBar: {
                                  dataLabels: {
                                      name: {
                                          fontSize: '22px',
                                      },
                                      value: {
                                          fontSize: '16px',
                                          formatter: function (val) {
                                              return Math.round((val*total)/100)
                                          }
                                      },
                                      total: {
                                          show: true,
                                          label: xAsysLabels.length == 1 ? xAsysLabels[0] : 'Total',
                                          formatter: function(w) {
                                              // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                                              return total
                                          }
                                      }
                                  }
                              }
                          },
                          labels: xAsysLabels,
                      };

                      var chart3 = new ApexCharts(document.querySelector("#chart3"), options3);
                      chart3.render();
                    </script>
                </div>
              <!-- FIN GRAFICA DE PASTEL -->

              <!-- GRAFICA DE LINEA | RELACION USUARIOS CARGANDO FACTURAS VS FACTURA CARGADAS TOTAL -->
                <div class="col-md-5 card p-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Usuarios subiendo facturas vs Facturas cargadas</label>
                    <div id="chart4"></div>
                    @foreach($lineal as $a)
                        <input type="hidden" class="chartC4Label" value="{{ $a->mes }}">
                        <input type="hidden" class="chartC4DataUser" value="{{ $a->total_user }}">
                        <input type="hidden" class="chartC4DataFact" value="{{ $a->total_fact }}">
                        <input type="hidden" class="chartC4Data" value="{{ $a->total_fact }}" name="{{ $a->total_user }}" id="{{ $a->mes }}">
                    @endforeach

                    <script>
                      var dataUser = [0,0,0,0,0,0,0,0,0,0,0,0];
                      var dataFact = [0,0,0,0,0,0,0,0,0,0,0,0]; 
                      var xAsysLabels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
                      
                      $(".chartC4Data").each(function(indice,elemento){
                          var mes = parseInt($(this).attr('id')) - 1;
                          var users = parseInt($(this).attr('name'));
                          var facts = parseInt($(this).val());

                          dataUser[mes] = users;
                          dataFact[mes] = facts;
                      });

                      var options4 = {
                        series: [
                              { name: 'Usuarios', data: dataUser },
                              { name: 'Facturas', data: dataFact }
                        ],
                        chart: {
                          height: 350,
                          type: 'line',
                          zoom: {
                            enabled: false
                          }
                        },
                        dataLabels: {
                          enabled: false
                        },
                        stroke: {
                          curve: 'straight'
                        },
                        /* title: {
                          text: 'Product Trends by Month',
                          align: 'left'
                        }, */
                        grid: {
                          row: {
                            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                            opacity: 0.5
                          },
                        },
                        xaxis: {
                          categories: xAsysLabels,
                        }
                      };

                      var chart4 = new ApexCharts(document.querySelector("#chart4"), options4);
                      chart4.render();
                    </script>
                </div>
              <!-- FIN GRAFICA DE LINEA -->
            </div>
        </div>

        <div id="div3" class="my-2">
            <div class="row">
              <!-- GRAFICA DE AREA | VALOR FACTURA VS VALOR PRODUCTOS EXTRAS -->
                <div class="col-md-5 card p-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Valor factura vs Valor productos extra</label>
                    <div id="chart5"></div>
                    @foreach($lineal as $a)
                        <input type="hidden" class="chartC5FactLabel" value="{{ $a->mes }}">
                        <input type="hidden" class="chartC5DataFact" value="{{ $a->total_puntos }}">
                        <input type="hidden" class="chartC5Data1" value="{{ $a->total_puntos }}" id="{{ $a->mes }}">
                    @endforeach

                    @foreach($especiales as $a)
                        <input type="hidden" class="chartC5ProLabel" value="{{ $a->mes }}">
                        <input type="hidden" class="chartC5DataPro" value="{{ $a->total_puntos }}">
                        <input type="hidden" class="chartC5Data2" value="{{ $a->total_puntos }}" id="{{ $a->mes }}">
                    @endforeach

                    <script>
                      var dataFact = [0,0,0,0,0,0,0,0,0,0,0,0]; 
                      var dataProd = [0,0,0,0,0,0,0,0,0,0,0,0];
                      var xAsysLabels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

                      $(".chartC5Data1").each(function(indice,elemento){
                          var mes = parseInt($(this).attr('id')) - 1;
                          var values = parseInt($(this).val());
                          dataFact[mes] = values;
                      });

                      $(".chartC5Data2").each(function(indice,elemento){
                          var mes = parseInt($(this).attr('id')) - 1;
                          var values = parseInt($(this).val());
                          dataProd[mes] = values;
                          dataFact[mes] -= values;
                      });

                      var options5 = {
                        series: [
                          {
                              name: 'Facturas',
                              data: dataFact
                          }, 
                          {
                              name: 'Productos Especiales',
                              data: dataProd
                          }
                        ],
                        chart: {
                          height: 350,
                          type: 'area'
                        },
                        dataLabels: {
                          enabled: false
                        },
                        stroke: {
                          curve: 'smooth'
                        },
                        xaxis: {
                          categories: xAsysLabels
                        },
                        tooltip: {
                          x: {
                            format: 'dd/MM/yy HH:mm'
                          },
                        },
                      };

                      var chart5 = new ApexCharts(document.querySelector("#chart5"), options5);
                      chart5.render();
                    </script>
                </div>
              <!-- FIN GRAFICA DE AREA -->
              
              <!-- GRAFICA DE TREEMAP | RELACIÓN DE VALOR FACTURAS POR ESPECIALIDAD DE USUARIOS -->
                <div class="col-md-5 card p-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Valor de facturas cargadas por especialidad</label>
                    <div id="chart6" ></div>
                    @foreach($treemap as $a)
                        <input type="hidden" class="chartC6Label" value="{{ $a->especialidad }}">
                        <input type="hidden" class="chartC6Data" name="{{ $a->especialidad }}" value="{{ $a->total }}">
                    @endforeach

                    <script>
                      var data = new Array(); 
                      var xAsysLabels = new Array();

                      $(".chartC6Label").each(function(indice,elemento){
                          xAsysLabels.push($(this).val());
                      });

                      $(".chartC6Data").each(function(indice,elemento){
                          var label = $(this).attr('name');
                          var value = parseInt($(this).val());
                          var json = {
                              x: label,
                              y: value
                          };

                          data.push(json);
                      });

                      if(data.length == 0 && xAsysLabels.length == 0){
                          xAsysLabels.push("Sin Coincidencias");
                          var json = {
                              x: "Sin Coincidencias",
                              y: 1
                          };
                          data.push(json);
                      }

                      var options6 = {
                        series: [
                            {
                                name: 'Especialidades',
                                data: data
                            },
                        ],
                        legend: {
                          show: true
                        },
                        chart: {
                          height: 350,
                          type: 'treemap'
                        },
                        /* title: {
                          text: 'Distibuted Treemap (different color for each cell)',
                          align: 'center'
                        }, */
                        colors: [
                          '#3B93A5',
                          '#F7B844',
                          '#ADD8C7',
                          '#EC3C65',
                          '#CDD7B6',
                          '#C1F666',
                          '#D43F97',
                          '#1E5D8C',
                          '#421243',
                          '#7F94B0',
                          '#EF6537',
                          '#C0ADDB'
                        ],
                        plotOptions: {
                          treemap: {
                            distributed: true,
                            enableShades: false
                          }
                        }
                      };

                      var chart6 = new ApexCharts(document.querySelector("#chart6"), options6);
                      chart6.render();
                    </script>
                </div>
              <!-- FIN GRAFICA DE TREEMAP -->
            </div>
        </div>

        <div id="div4" class="mb-2">
            <div class="row">
              <!-- GRAFICA BARRA HORIZONTAL STACKED PUNTOS GANADOS DISTINTAS FORMAS POR MES -->
                <div class="col-md-5 card p-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Puntos otorgados vs meses - Puntos extra</label>
                    <div id="chart7"></div>
                    @foreach($stacked as $a)
                        <input type="hidden" class="chartC7Mes" value="{{ $a->mes }}">
                        <input type="hidden" class="chartC7Tipo" value="{{ $a->type }}">
                        <input type="hidden" class="chartC7Data" value="{{ $a->total }}">
                        <input type="hidden" class="chartC7All" value="{{ $a->total }}" name="{{ $a->mes }}" id="{{ $a->type }}">
                    @endforeach

                    <script>
                      var data = [
                              {
                                  name: 'Trivia',
                                  data: [0,0,0,0,0,0,0,0,0,0,0,0]
                              }, 
                              {
                                  name: 'Encuesta',
                                  data: [0,0,0,0,0,0,0,0,0,0,0,0]
                              }, 
                              {
                                  name: 'Factura',
                                  data: [0,0,0,0,0,0,0,0,0,0,0,0]
                              }, 
                          ];
                      
                      var xAsysLabels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

                      $(".chartC7All").each(function(indice,elemento){
                          var tipo = $(this).attr('id');
                          var mes = parseInt($(this).attr('name'));
                          var value = parseInt($(this).val());

                          //console.log(data[tipo].data[mes]);
                          //0 = Trivia
                          //1 = Encuesta
                          //2 = Factura

                          if(tipo == "Trivia") tipo = 0;
                          if(tipo == "Encuesta") tipo = 1;
                          if(tipo == "Factura") tipo = 2;

                          data[tipo].data[mes-1] = value;
                      });

                      var options7 = {
                        series: data,
                        chart: {
                          type: 'bar',
                          height: 350,
                          stacked: true,
                        },
                        plotOptions: {
                          bar: {
                            horizontal: true,
                          },
                        },
                        stroke: {
                          width: 1,
                          colors: ['#fff']
                        },
                        /* title: {
                          text: 'Fiction Books Sales'
                        }, */
                        xaxis: {
                          categories: xAsysLabels,
                          /* labels: {
                            formatter: function (val) {
                              return val + "K"
                            }
                          } */
                        },
                        yaxis: {
                          title: {
                            text: undefined
                          },
                        },
                        tooltip: {
                          y: {
                            formatter: function (val) {
                              return val + " Puntos"
                            }
                          }
                        },
                        fill: {
                          opacity: 1
                        },
                        legend: {
                          position: 'top',
                          horizontalAlign: 'left',
                          offsetX: 40
                        }
                      };

                      var chart7 = new ApexCharts(document.querySelector("#chart7"), options7);
                      chart7.render();
                    </script>
                </div>
              <!-- FIN GRAFICA BARRA HORIZONTAL STACKED -->

              <!-- GRAFICA COLUMNA PUNTOS OTORGADOS -->
                <div class="col-md-5 card p-3 mx-auto">
                    <label for="" class="lead font-weight-bold text-dark ml-3">Interacción vs puntos otorgados</label>
                    <div id="chart8"></div>
                    @foreach($column as $a)
                        <input type="hidden" class="chartC8Label" value="{{ $a->type }}">
                        <input type="hidden" class="chartC8Data" value="{{ $a->total }}">
                    @endforeach

                    <script>
                      var data = new Array(); 
                      var xAsysLabels = new Array();

                      $(".chartC8Label").each(function(indice,elemento){
                          xAsysLabels.push($(this).val());
                      });
                      $(".chartC8Data").each(function(indice,elemento){
                          data.push(parseInt($(this).val()));
                      });

                      var options8 = {
                        series: [
                          {
                            name: 'Puntos',
                            data: data
                          }
                        ],
                        chart: {
                          height: 350,
                          type: 'bar',
                        },
                        plotOptions: {
                          bar: {
                            borderRadius: 10,
                            columnWidth: '50%',
                          }
                        },
                        dataLabels: {
                          enabled: false
                        },
                        stroke: {
                          width: 2
                        },
                        grid: {
                          row: {
                            colors: ['#fff', '#f2f2f2']
                          }
                        },
                        xaxis: {
                          labels: {
                            rotate: -45
                          },
                          categories: xAsysLabels,
                          tickPlacement: 'on'
                        },
                        yaxis: {
                          title: {
                            text: 'Puntos',
                          },
                        },
                        fill: {
                          type: 'gradient',
                          gradient: {
                            shade: 'light',
                            type: "horizontal",
                            shadeIntensity: 0.25,
                            gradientToColors: undefined,
                            inverseColors: true,
                            opacityFrom: 0.85,
                            opacityTo: 0.85,
                            stops: [50, 0, 100]
                          },
                        },
                        legend: {
                          show: true
                        },
                      };

                      var chart8 = new ApexCharts(document.querySelector("#chart8"), options8);
                      chart8.render();
                    </script>
                </div>
              <!-- FIN GRAFICA COLUMNAS -->
            </div>
        </div>
    </div>
@endsection
