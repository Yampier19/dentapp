@extends('layouts.auth.app')
@section('content')

    <div class="wrapper wrapper-full-page">
        <div class="page-header login-page header-filter ">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="container">
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <div class="row-reverse animate__animated animate__fadeInLeft">
                            <div class="col title">
                                <h2 style="color: #C2C2C2">BIENVENIDO A</h2>
                            </div>
                            <div class="col logo_title">
                                <img src="{{ url('img/dentapp/logo.png') }}" alt="" class="w-75">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-8 ml-auto col_auth">
                        <form class="user" method="post" action="{{ route('login.auth') }}">
                            <div class="card card-login card-hidden pb-5 ">
                                <div class="card-header card-header-rose text-center p-5">
                                    <img src="{{ url('img/dentapp/icon.png') }}" alt="" style="max-width: 40%"
                                        class="animate__animated animate__flipInX">
                                </div>
                                <div class="card-body ">
                                    @csrf

                                    <div class="row">
                                        <div class="col">
                                            <!-- Email Input -->
                                            @if (Session::has('message'))
                                                <div class="text-center"
                                                    style="border-radius: 5px; padding: 10px; background-color: #E7344C; color: #ffffff; margin-bottom: 3%;">
                                                    {{ Session::get('message') }}
                                                </div>
                                            @endif

                                            @if (Session::has('edit'))
                                                <div class="text-center"
                                                    style="border-radius: 5px; padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                                                    {{ Session::get('edit') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <p class="card-description text-center p-3 font-weight-bold">

                                        Por favor ingresa tus
                                        credenciales
                                    </p>

                                    <span class="bmd-form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">email</i>
                                                </span>
                                            </div>
                                            <input type="email" class="form-control  @error('email') is-invalid @enderror"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                value="{{ old('email') }}" autocomplete="email" autofocus
                                                placeholder="Correo electronico" name="email" required>
                                        </div>
                                        @error('email')
                                            <span class="invalid-feedback text-white" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </span>
                                    <span class="bmd-form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <i class="material-icons">lock_outline</i>
                                                </span>
                                            </div>
                                            <input type="password"
                                                class="form-control @error('password') is-invalid @enderror"
                                                id="exampleInputPassword" placeholder="Contraseña" name="password" required
                                                autocomplete="current-password">
                                        </div>
                                        @error('password')
                                            <span class="invalid-feedback text-white" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </span>
                                </div>
                                <div class="card-footer justify-content-center">
                                    <div class="row-reverse">
                                        <div class="col my-3">
                                            <input type="submit" class="btn btn-primary" value="Iniciar Sesión">
                                        </div>
                                        <div class="col">
                                            <a href="{{ route('password.request') }}">
                                                <span class="text-dark">¿Olvidaste tu contraseña?</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
