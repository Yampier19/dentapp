@extends('layouts.admin.app')

@section('content')
    @if (Session::has('edit'))
        <script>
            Swal.fire(
                '!Buen trabajo!',
                'Perfil editado correctamente',
                'success'
            )
        </script>
    @endif

    <div class="row" style="margin-top: 5%">
        <div class="container-fluid">
            <form action="{{ route('modules.update.perfil') }}" method="post"
                enctype="multipart/form-data" class="col-md-12">
                @csrf
                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <div class="card card-profile">
                            <div class="card-avatar">
                                <a href="javascript:;"">
                                    <img class="img" id="previewImage"
                                        src="{{ route('admin.icon', ['filename' => basename(auth()->user()->foto)]) }}">
                                </a>
                            </div>
                            <div class="card-body">
                                <h3 class="card-title font-weight-bold" style="color: #64C1C6">{{ $admin->nombre . ' ' . $admin->apellido }}</h3>
                                <h4 class="card-description">Rol: {{ $admin->roles->rol }} <br> {{ $admin->estado }} </h4>
                                <h4 class="card-description">Correo electronico <br> {{ $admin->email }} </h4>

                                <div class="my-5" id="primero">
                                    <div class="form-group col-md-6 my-5 mx-auto">
                                        <label for="exampleInputPassword" class="pl-3" style="color: #64C1C6">Contraseña</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña" value="**********************" disabled>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 ml-auto">
                                            <i class="far fa-edit" style="color: #64C1C6; font-size: 18px"></i>
                                            <a onclick="myFunctionB2()"> <label for="" style="color: #64C1C6; font-size: 18px">Editar</label></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5" id="segundo" style="display: none;">
                                    <a onclick="myFunctionB1()"> <label for="" style="color: #64C1C6; font-size: 18px">Volver</label></a>
                                    <div class="form-group col-md-6 my-5 mx-auto">
                                        <label for="upload" class="pl-3" style="color: #64C1C6">Cambiar Avatar</label>
                                        <input type="file" class="form-control @error('foto') is-invalid @enderror"
                                            id="upload" placeholder="Contraseña" name="foto" accept=".png,.jpg,.jpeg">
                                        @error('foto')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 my-5 mx-auto">
                                        <label for="exampleInputPassword" class="pl-3" style="color: #64C1C6">Nueva contraseña (Opcional)</label>
                                        <input type="password" class="form-control @error('password') is-invalid @enderror"
                                            id="exampleInputPassword1" placeholder="Contraseña" name="password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 my-5 mx-auto">
                                        <label for="exampleInputPassword" class="pl-3" style="color: #64C1C6">Repite contraseña</label>
                                        <input type="password" class="form-control @error('confirmacion') is-invalid @enderror"
                                            id="exampleInputPassword1" placeholder="Contraseña" name="confirmacion">
                                        @error('confirmacion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 mx-auto">
                                            <button class="btn col-md-6" style="background-color: #64C1C6" type="submit">ACTUALIZAR PERFIL</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        function init() {
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            x.style.display = "block";
            y.style.display = "none";

        }

        function myFunctionB1() {
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            if (x.style.display === "none") {
                x.style.display = "block";
                y.style.display = "none";
            } else {
                x.style.display = "none";
                y.style.display = "none";
            }

        }

        function myFunctionB2() {
            var x = document.getElementById("primero");
            var y = document.getElementById("segundo");
            if (y.style.display === "none") {
                y.style.display = "block";
                x.style.display = "none";
            } else {
                x.style.display = "none";
                y.style.display = "none";
            }
        }

        init();
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#pass2').keyup(function() {
                var pass1 = $('#pass1').val();
                var pass2 = $('#pass2').val();
                if (pass1 == pass2) {

                    $('#check').removeClass('fas fa-exclamation').addClass('fas fa-check')
                } else {

                    $('#check').removeClass('fas fa-check').addClass('fas fa-exclamation')
                }
                if (pass1 === '' || pass2 === '') {
                    $('#check').removeClass('fas fa-check').removeClass('fas fa-exclamation')
                }
            });

            //Para enviar el preview de la imagen al cargar
            function readImage(input) {
                var url = input.value;
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#previewImage').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                } else {
                    $('#previewImage').attr('src', null);
                    input.value = null;
                }
            }

            $("#upload").change(function() {
                readImage(this);
            });
        });
    </script>
    @if ($errors->any())
        <script>
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Completa todos los campos',
            })
            myFunctionB2();
        </script>
    @endif
@endsection

<style>
    .button-wrapper {
        position: relative;
        width: 150px;
        text-align: center;
        margin: 10% auto;
    }

    .button-wrapper span.label {
        position: relative;
        z-index: 0;
        display: inline-block;
        width: 100%;
        background: #00bfff;
        cursor: pointer;
        color: #fff;
        padding: 10px 0;
        text-transform: uppercase;
        font-size: 12px;
    }

    #upload {
        display: inline-block;
        position: absolute;
        z-index: 1;
        width: 100%;
        height: 50px;
        top: 0;
        left: 0;
        opacity: 0;
        cursor: pointer;
    }
</style>
