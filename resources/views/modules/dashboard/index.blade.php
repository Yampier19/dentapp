@extends('layouts.admin.app')
@section('content')
    <div class="row">
        <div class="col col_card_home">
            <div class="row-reverse">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <h4 class="font-weight-bold mt-3" style="color: #F28D8D">Admin. de Usuarios</h4>
                        </div>
                        <div class="col-auto d-flex align-items-center">
                            <a href="{{ route('cliente.index') }}">
                                <i class="material-icons fa-2x" style="color: #F28D8D">add_circle</i>
                            </a>
                        </div>
                    </div>
                </div>
               
                @forelse($clientes as $cliente)
                    <div class="col">
                        <div class="card mt-4 rounded-card">
                            <div class="card-body">
                                <h4 class="font-weight-normal mt-3">{{ $cliente->nombre . ' ' . $cliente->apellido }}</h4>
                                <p class="card-text">{{ $cliente->direccion }}</p>
                                <p class="card-text">{{ $cliente->telefono }}</p>
                                <a href="{{ route('cliente.edit', $cliente->id_cliente) }}">
                                    <div class="row" style="float: right; color: #F28D8D;">
                                        <p style="margin-top: 2px; margin-right: 2px;">Editar</p>
                                        <i class="material-icons mr-2">edit_calendar</i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @empty
                    <span class="category">No hay clientes registrados...</span>
                @endforelse
            </div>
        </div>
        <div class="col col_card_home">
            <div class="row-reverse">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <h4 class="font-weight-bold mt-3" style="color: #64C1C6">Facturas de Usuarios</h4>
                        </div>
                        <div class="col-auto d-flex align-items-center">
                            <a href="{{ route('factura.index') }}">
                                <i class="material-icons fa-2x"style="color: #64C1C6">add_circle</i>
                            </a>
                        </div>
                    </div>
                </div>
                @forelse($facturas as $factura)
                    <div class="col">
                        <div class="card mt-4 rounded-card">
                            <div class="card-body">
                                <h4 class="font-weight-normal mt-3">{{ $factura->referencia }}</h4>
                                <p class="card-text mb-4">
                                    {{ $factura->cliente->nombre . ' ' . $factura->cliente->apellido }}</p>
                                <p class="card-text mb-4">{{ $factura->distribuidor }}</p>
                                <a href="{{ route('factura.index') }}">
                                    <div class="row" style="float: right; color: #64C1C6;">
                                        <p style="margin-top: 2px; margin-right: 2px;">Ver Facturas</p>
                                        <i class="material-icons mr-1">edit_calendar</i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @empty
                    <span class="category">No hay facturas registradas...</span>
                @endforelse
            </div>
        </div>
        <div class="col col_card_home">
            <div class="row-reverse">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <h4 class="font-weight-bold mt-3" style="color: #FFB053">Capacitaciones</h4>
                        </div>
                        <div class="col-auto d-flex align-items-center">
                            <a href="{{ route('training.index') }}">
                                <i class="material-icons fa-2x" style="color: #FFB053">add_circle</i>
                            </a>
                        </div>
                    </div>
                </div>
                @forelse($trainings as $training)
                    <div class="col">
                        <div class="card mt-4 rounded-card">
                            <div class="card-body">
                                <h4 class="font-weight-normal mt-3">{{ $training->nombre }}</h4>
                                <p class="card-text mb-4">{{ $training->tipo }}</p>
                                <p class="card-text mb-4">{{ $training->descripcion }}</p>
                            </div>
                        </div>
                    </div>
                @empty
                    <span class="category">No hay capacitaciones registradas...</span>
                @endforelse
            </div>

        </div>
        <div class="col col_card_home">
            <div class="row-reverse">
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <h4 class="font-weight-bold mt-3" style="color: #515AA7">Noticias</h4>
                        </div>
                        <div class="col-auto d-flex align-items-center">
                            <a href="{{ route('noticias.index') }}">
                                <i class="material-icons fa-2x"style="color: #515AA7">add_circle</i>
                            </a>
                        </div>
                    </div>

                </div>
                @forelse($noticias as $noticia)
                    <div class="col">
                        <div class="card mt-4 rounded-card">
                            <div class="card-body">
                                <h4 class="font-weight-normal mt-3">{{ $noticia->nombre }}</h4>
                                <p class="card-text mb-4">{{ $noticia->tipo }}</p>
                                <div style="min-height: 20vh; max-height: 20vh; background: url({{ route('noticias.preview', ['filename' => basename($noticia->imagen)]) }}); background-position: center; background-size: cover; background-repeat: no-repeat">
                                </div>
                                <a href="{{ route('noticias.edit', $noticia->id_noticia) }}">
                                    <div class="row mt-3" style="float: right; color: #515AA7;">
                                        <p style="margin-top: 2px; margin-right: 2px;">Editar</p><i
                                            class="material-icons mr-1">edit_calendar</i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @empty
                    <span class="category">No hay noticias registradas...</span>
                @endforelse
            </div>

        </div>
    </div>


    <style>
        .rounded-card {
            border-radius: 20px;
        }
    </style>
@endsection
