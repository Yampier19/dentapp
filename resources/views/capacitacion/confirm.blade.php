@extends('layouts.admin.app')
@section('content')
    <div class="container py-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif
<div class="card col-sm-7 p-5 m-auto">

    <div class="text-center">
        <h2 class="mb-5 font-weight-bold">¿Deseas eliminar la siguiente capacitación? </h2>
    </div>
   <div class="text-center mb-3">
    <i style="font-size: 4em;color:tomato" class="fa phpdebugbar-fa-exclamation-triangle"></i>
   </div>
    <h4 class="font-weight-bold">Nombre: </h4>
    <p>{{ $training->nombre }}</p>
    <h4 class="font-weight-bold">Descripcion: </h4>
    <p>{{ $training->descripcion }}</p>
    <h4 class="font-weight-bold">Tipo: </h4>
    <p>{{ $training->tipo }}</p>
    <h4 class="font-weight-bold">Información: </h4>
    <p>{{ $training->informacion }}</p>
    <h4 class="font-weight-bold">Documento: </h4>
    <p>
        @if($training->archivo)
            <a href="{{ route('training.archive', ['filename' => basename($training->archivo)]) }}" target="_blank" class="btn btn-sm btn-dark mx-auto">
                @switch(pathinfo($training->archivo)['extension'])
                    @case('jpg') @case('jpeg') @case('png') <i class="fas fa-image"></i>Imagen @break
                    @case('pdf') <i class="fa fa-file-pdf-o"></i> @break
                    @case('mp4') @case('wm') @case('wmv') <i class="fas fa-video"></i>Video @break
                @endswitch
            </a>
        @else()
            <span>No posee...</span>
        @endif
    </p>

    <form class="d-inline-block" method="post" enctype="multipart/form-data" action="{{ route('training.destroy', $training->id_capacitacion) }}">
        @method('DELETE')
        @csrf
        <div class="row col-md-12 mt-5 mx-auto d-flex justify-content-center">
            <button type="submit" class="d-inline-block redondo btn" style="background-color: teal">
                <i class="fas fa-trash-alt"></i> <strong>Eliminar</strong>
            </button>
            <a href="{{route('capacitacion.list')}}" class="btn d-inline-block"><strong>Volver</strong></a>
        </div>
    </form>
</div>
    </div>
@endsection
