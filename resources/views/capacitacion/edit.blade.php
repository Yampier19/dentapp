@extends('layouts.admin.app')

@section('content')
    <div class="container mt-5">
        @if (Session::has('error'))
            <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
                {{ Session::get('error') }}
            </div>
        @endif

        @if (Session::has('edit'))
            <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
                {{ Session::get('edit') }}
            </div>
        @endif

        <div class="row mb-5 mx-auto">
            <!-- Editar Capacitación -->
            <div class="col-md-8 card card-body shadow p-3 mb-5 bg-white rounded mx-auto">


                <div class="card-header mt-3" style="background-color:#64C2C8">
                    <h5 class="text-white my-auto font-weight-bold">Editar Capacitación</h5>
                </div>

                <form id="tupdate" action="{{ route('training.update', $training->id_capacitacion) }}" method="post" enctype="multipart/form-data">

                    @csrf
                    <div class="row pt-5 pl-5 pr-5 col-md-12 my-4">
                        <div class="col-md-6">
                            <label for="name_user">Nombre:</label>
                            <input required name="nombre" id="" type="text" value="{{ $training->nombre }}"
                                class="form-control @error('nombre') is-invalid @enderror">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="name_user">Descripcion:</label>
                            <input required name="descripcion" id="" type="text" value="{{ $training->descripcion }}"
                                class="form-control @error('descripcion') is-invalid @enderror">
                            @error('descripcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row col-md-12 pl-5 pr-5">
                        <div class="col">
                            <label for="name_user">Tipo de Capacitación:</label>
                            <input required list="ctipo" name="tipo" id="captipo" value="{{ $training->tipo }}"
                                class="form-control @error('tipo') is-invalid @enderror" />
                            <datalist id="ctipo">
                                @foreach ($categorias as $categoria)
                                    <option value="{{ $categoria->nombre }}">
                                @endforeach
                            </datalist>
                            @error('tipo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row pl-5 pr-5 col-md-12 my-3">
                        <div class="col">
                            <label for="name_user">Información:</label>
                            <input required name="informacion" id="" type="text" value="{{ $training->informacion }}"
                                class="form-control @error('informacion') is-invalid @enderror">
                            @error('informacion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row pl-5 pr-5 col-md-12 my-3">
                        <div class="col-md-6">
                            <label for="">Imagen Preview de Capacitación:</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('imagen') is-invalid @enderror"
                                    id="customFile" name="imagen" accept=".jpg,.jpeg,.png" value="">
                                <label class="custom-file-label" for="customFile">Escoger una Imagen</label>
                                @error('imagen')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Subir archivo de capacitación:</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input @error('archivo') is-invalid @enderror"
                                    id="customFile" name="archivo" accept=".jpg,.jpeg,.png,.pdf,.mp4" value="">
                                <label class="custom-file-label" for="customFile">Escoger un Documento</label>
                                @error('archivo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row col-md-12 mx-auto d-block">
                        <div class="progress mb-3">
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                                aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%" role="progressbar">
                            </div>
                        </div>
                        <div id="uploadStatus" style="display:none"></div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn text-white" style="background-color: #64C2C8">Editar Capacitación</button>
                        <a class="btn" href="{{ url('training/list/capacitacion') }}">Volver</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
    <input type="hidden" id="redirect" value="{{ Request::url() }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {

        $("#tupdate").on('submit', function(e) {
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $(this).attr('action');
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                type: type,
                url: url,
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".progress-bar").width('0%');
                    jQuery('#uploadStatus').hide();
                    jQuery('#uploadStatus').html('');
                },
                error: function() {
                    $('#uploadStatus').html('<span class="invalid-feedback d-block" role="alert"><strong>No se pudo editar de forma exitosa la capacitación, error de conexión</strong></span>');
                },
                success: function(resp) {
                    console.log(resp);
                    jQuery('#uploadStatus').show();
                    jQuery.each(resp.errors, function(key, value) {
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + value + '</strong></span></p>');
                    });

                    if (resp.success) {
                        $('#uploadStatus').append('<p><span style="color:#28A74B;" class="invalid-feedback d-block" role="alert"><strong>' + resp.success + '</strong></span></p>');
                        alert(resp.success);
                        location.href = $('#redirect').val();
                    }

                    if (resp.fallo)
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + resp.fallo + '</strong></span></p>');
                },
            });
        });
    });
</script>
