@extends('layouts.admin.app')

@section('content')

    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a65a; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    @if (Session::has('delete'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('delete') }}
        </div>
    @endif

    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    <?php $trivias = collect(); ?>
    <div class="row" style="margin-top: 5%">

        <!-------------------------------- CARDs CAPACITACIONES --------------------------------->
        <div class="col-lg-6 col-md-6">
            <div class="row mx-auto">
                <label for="" class="lead" style="color: #51A2A7; font-weight: bold">Capacitaciones</label>
                <a href="{{ route('capacitacion.list') }}" class="ml-auto text-secondary">Ver todos</a>
            </div>
            @foreach ($trainings as $training)
                @if ($training->trivia)
                    <?php $trivias->push($training->trivia); ?>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="row ">
                            <div class="col-md-8">
                                <h4 class="card-title" style="color: #51A2A7; font-weight: bold">
                                    {{ $training->nombre }}
                                </h4>
                            </div>
                            <div class="col-md-4">
                                <p class="ml-5 text-right" style="color: #51A2A7;">{{ $training->tipo }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0 px-4">
                        <p>{{ $training->descripcion }}</p>
                        <div class="row ml-auto mx-auto mb-2">
                            <p>{{ $training->informacion }}</p>
                            @if ($training->archivo)
                                <p class="mx-2 ml-auto my-auto" style="font-size: 15px; color: #51A2A7; cursor: pointer">
                                    <a class="w_open" style="color: #51A2A7" id="{{ route('training.archive', ['filename' => basename($training->archivo)]) }}">
                                        Ver Capacitación
                                        @switch(pathinfo($training->archivo)['extension'])
                                            @case('jpg')
                                            @case('jpeg')

                                            @case('png')
                                                <i class="fas fa-image mx-1 my-auto" style="font-size: 20px; color: #51A2A7"></i>
                                            @break

                                            @case('pdf')
                                                <i class="fas fa-file-pdf mx-1 my-auto" style="font-size: 20px; color: #51A2A7"></i>
                                            @break

                                            @case('mp4')
                                            @case('wm')

                                            @case('wmv')
                                                <i class="fas fa-video mx-1 my-auto" style="font-size: 20px; color: #51A2A7"></i>
                                            @break
                                        @endswitch
                                    </a>
                                </p>
                            @endif
                            <!-- <i class="fas fa-print ml-auto my-auto" style="font-size: 20px; color: #51A2A7"></i>
                            <i class="fas fa-file-csv mx-2 my-auto" style="font-size: 20px; color: #51A2A7"></i>
                            <i class="fas fa-file-pdf my-auto" style="font-size: 20px; color: #51A2A7"></i> -->
                        </div>
                    </div>
                </div>
            @endforeach
            <button class="btn btn-round" style="background-color: #51A2A7" data-toggle="modal" data-target=".bd-example-modal-lg">
                <i class="material-icons">add</i>CREAR CAPACITACION
            </button>

            <!-- PAGINACION -->
            <nav aria-label="Page navigation example">
                <?php $limite_paginas = 10; ?>

                @if ($trainings->lastPage() > 1)
                    <ul class="pagination justify-content-end">
                        <li class="page-item {{ $trainings->currentPage() == 1 ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $trainings->url(1) }}">Primera</a>
                        </li>
                        @for ($i = 1; $i <= $trainings->lastPage(); $i++)
                            <?php
                            $half_total_links = floor($limite_paginas / 2);
                            $from = $trainings->currentPage() - $half_total_links;
                            $to = $trainings->currentPage() + $half_total_links;
                            if ($trainings->currentPage() < $half_total_links) {
                                $to += $half_total_links - $trainings->currentPage();
                            }

                            if ($trainings->lastPage() - $trainings->currentPage() < $half_total_links) {
                                $from -= $half_total_links - ($trainings->lastPage() - $trainings->currentPage()) - 1;
                            }
                            ?>
                            @if ($from < $i && $i < $to)
                                <li class="page-item {{ $trainings->currentPage() == $i ? 'active disabled' : '' }}">
                                    <a class="page-link" href="{{ $trainings->url($i) }}">{{ $i }}</a>
                                </li>
                            @endif
                        @endfor
                        <li class="page-item {{ $trainings->currentPage() == $trainings->lastPage() ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $trainings->url($trainings->lastPage()) }}">Ultima</a>
                        </li>
                    </ul>
                @endif
            </nav>
        </div>

        <!-- CARD TRIVIAS -->
        <div class="col-md-6">
            <div class="row mx-auto">
                <label for="" class="lead" style="color: #F28D8D; font-weight: bold">Trivias</label>
                <a href="{{ route('trivia.index') }}" class="ml-auto text-secondary">Ver todas</a>
            </div>
            @foreach ($trivias as $trivia)
                <div class="card card-body">
                    <div class="row d-flex justify-content-between ">
                        <div class="col-md-9">
                            <div class="col">
                                <label for="" style="color: #F28D8D; font-weight: bold">Trivia - {{ $trivia->capacitacion->nombre }}</label>
                            </div>
                            <div class="col">
                                <label for="" style="color: #F28D8D;">Número de preguntas ({{ $trivia->preguntas->count() }})</label>
                            </div>
                            <div class="col">
                                <label for="">Información: <br>{{ $trivia->descripcion }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

    <!-- Modal de Agregar Capacitación-->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                        Creacion de capacitaciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form onsubmit="return checkSubmit();" id="tstore" action="{{ route('training.store') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <!-- CAPACITACION -->
                        <div class="form-row">
                            <div class="col">
                                <input required name="nombre" id="" type="text" placeholder="Nombre" class="form-control @error('nombre') is-invalid @enderror">
                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <input required name="descripcion" id="" type="text" placeholder="Descripción" class="form-control @error('descripcion') is-invalid @enderror">
                                @error('descripcion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="">Tipo de Capacitación:</label>
                                <input required list="ctipo" name="tipo" id="captipo" class="form-control @error('tipo') is-invalid @enderror" />
                                <datalist id="ctipo">
                                    @foreach ($categorias as $categoria)
                                        <option value="{{ $categoria->nombre }}">
                                    @endforeach
                                </datalist>
                                @error('tipo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="">Información:</label>
                                <input name="informacion" id="" type="text" placeholder=""
                                    class="form-control @error('informacion') is-invalid @enderror">
                                @error('informacion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row my-3">
                            <div class="col">
                                <label for="">Imagen Preview de Capacitación:</label>
                                <input type="file" name="imagen" accept=".jpg,.jpeg,.png" class="form-control @error('imagen') is-invalid @enderror">
                                @error('imagen')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col">
                                <label for="">Subir archivo de capacitación:</label>
                                <input type="file" name="archivo" accept=".jpg,.jpeg,.png,.pdf,.mp4" class="form-control @error('archivo') is-invalid @enderror">
                                @error('archivo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- APARTADO DE LA TRIVIA -->
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                                Trivias</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="form-row my-3">
                            <div class="col">
                                <label style="font-size: 12px; color: #51A2A7;  margin-bottom: 0px;">Descripción</label>
                                <input required name="descripcion_trivia" id="" type="text" placeholder="Descripción" class="form-control @error('descripcion_tri') is-invalid @enderror">
                                @error('descripcion_tri')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row my-3">
                            <div class="col">
                                <label style="font-size: 12px; color: #51A2A7;  margin-bottom: 0px;">Seleccione el tipo de pregunta</label>
                                <select name="select" id="inputSelect" class="form-control" required="required">
                                    <option value="" selected disabled>Seleccionar...</option>
                                    <option value="1">Opcion Multiple</option>
                                    <!-- <option value="2">Falso - Verdadero</option>
                                    <option value="3">Fecha</option>
                                    <option value="4">Numero</option>
                                    <option value="5">Abierta</option> -->
                                </select>
                            </div>
                        </div>

                        <!-- OPCION MULTIPLE -->
                        <div style="display: none" class="divOculto" id="div1">
                            <div class="row field_wrapper mx-auto">

                            </div>

                            <div class="row ml-2 d-flex justify-content">
                                <a href="javascript:void(0);" class="add_button mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-plus mr-2"></i><!--  --></a>
                                <a href="javascript:void(0);" class="remove_button mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-trash mr-2"></i><!-- Eliminar Pregunta --></a>
                            </div>
                        </div>

                        <!-- FALSO - VERDADERO -->
                        <!-- <div style="display: none" class="divOculto " id="div2">
                            <div class="form-row d-flex justify-content-between field_wrapper2">

                            </div>
                            <div class="row d-flex justify-content-end">
                                <a href="javascript:void(2);" class="add_button2 mr-2 my-3" style="color: #51A2A7" title="Add field"><i class="fas fa-plus mr-2"></i>Agregar afirmaciones</a>
                            </div>
                        </div> -->

                        <!-- FECHA -->
                        <!-- <div style="display: none" class="divOculto " id="div3">
                            <div class="form-row">
                               <div class="col">
                                   <input type="text" class="form-control" name="" id="" placeholder="Pregunta">
                               </div>
                               <div class="col">
                                   <input type="date" name="" class="form-control" id="">
                               </div>
                            </div>
                        </div> -->

                        <!-- NUMERO -->
                        <!-- <div style="display: none" class="divOculto " id="div4">
                            <div class="form-row">
                               <div class="col">
                                   <input type="text" class="form-control" name="" id="" placeholder="Pregunta">
                               </div>
                               <div class="col">
                                   <input type="number" placeholder="Respuesta de numero" name="" class="form-control" id="">
                               </div>
                            </div>
                        </div> -->

                        <!-- ABIERTA -->
                        <!-- <div style="display: none" class="divOculto " id="div5">
                            <div class="form-row">
                               <div class="col">
                                   <input type="text" class="form-control" name="" id="" placeholder="Pregunta">
                               </div>
                            </div>
                            <div class="form-row my-3">
                               <div class="col">
                                   <textarea class="form-control" placeholder="Escriba aquí que debe decir el participante de la Trivia.... (opcional)" name="" id="" cols="30" rows="10"></textarea>
                               </div>
                            </div>
                        </div> -->

                        <!-- FINAL DEL FORM -->
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" style="color: #51A2A7; font-weight: bold">
                                Agregar el valor de puntos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="form-group my-3">
                            <input type="number" class="form-control" name="puntuacion" id="" placeholder="Puntos 0">
                        </div>

                        <div class="progress my-5">
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%" role="progressbar">
                            </div>
                        </div>
                        <div id="uploadStatus" style="display:none"></div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn mx-auto col-md-6" style="background-color: #51A2A7; border-radius: 40px;">CREAR CAPACITACION</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- OPCION MULTIPLE JS -->
    <script type="text/javascript">
        $(document).ready(function() {

            var maxField = 6; //Input fields increment limitation
            var x = 0; //INICIAL DE PREGUNTAS MULTIPLE

            var addButton = $('.add_button'); //Add button selector
            var removeButton = $('.remove_button'); //Delete button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper

            $(addButton).click(function() { //Once add button is clicked
                if (x < maxField) { //Check maximum number of input fields
                    x++; //Increment field counter
                    var j = 0; //INICIAL DE OPCIONES MUTIPLE
                    console.log(x);
                    var fieldHTML = '<div class="row col-md-12 my-3" id="pregunta-' + x + '">' +
                        '<div class="col-md-9 my-auto">' +
                        '<input type="text" name="multiple[' + x + '][pregunta]" class="form-control" placeholder="Pregunta" required>' +
                        '</div>' +
                        '<div class="col-md-3">' +
                        '<div class="row" id="respuesta-' + x + (j) + '">' +
                        '<div class="form-check d-flex justify-content-end">' +
                        '<label class="form-check-label my-auto">' +
                        '<input class="form-check-input" name="multiple[' + x + '][' + j + '][correcta]" type="checkbox" value="1" ><span class="form-check-sign">' +
                        '<span class="check"></span></span></label>' +
                        '<input name="multiple[' + x + '][' + j + '][texto]" class="form-control" required placeholder="Opcion de respuesta">' +
                        '</div>' +
                        '</div>' +
                        '<div class="row" id="respuesta-' + x + (j + 1) + '">' +
                        '<div class="form-check d-flex justify-content-end">' +
                        '<label class="form-check-label my-auto">' +
                        '<input class="form-check-input" name="multiple[' + x + '][' + (j + 1) + '][correcta]" type="checkbox" value="1" ><span class="form-check-sign">' +
                        '<span class="check"></span></span></label>' +
                        '<input name="multiple[' + x + '][' + (j + 1) + '][texto]" class="form-control" required placeholder="Opcion de respuesta">' +
                        '</div>' +
                        '</div>' +
                        '<div class="row" id="respuesta-' + x + (j + 2) + '">' +
                        '<div class="form-check d-flex justify-content-end">' +
                        '<label class="form-check-label my-auto">' +
                        '<input class="form-check-input" name="multiple[' + x + '][' + (j + 2) + '][correcta]" type="checkbox" value="1" ><span class="form-check-sign">' +
                        '<span class="check"></span></span></label>' +
                        '<input name="multiple[' + x + '][' + (j + 2) + '][texto]" class="form-control" required placeholder="Opcion de respuesta">' +
                        '</div>' +
                        '</div>' +
                        '<div class="row d-flex justify-content-end">' +
                        '<a id="opcionadd-' + x + '" name="' + (j + 2) + '" class="mr-2 my-3" style="color: #F28D8D" onclick="addOption(' + x + ')"><i class="fas fa-plus mr-2"></i></a>' +
                        '<a id="opcionrem-' + x + '" name="' + (j + 2) + '" class="mr-2 my-3" style="color: #F28D8D" onclick="removeOption(' + x + ')"><i class="fas fa-trash mr-2"></i></a>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                    $(wrapper).append(fieldHTML); // Add field html
                }
            });
            /* $(wrapper).on('click', '.remove_button', function(e) { //Once remove button is clicked
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            }); */
            $(removeButton).click(function() { //Once remove button is clicked
                $('#pregunta-' + x).remove();
                x > 0 ? x-- : ""; //Decrement field counter
            });

        });

        function addOption(x) {
            var j = $('#opcionadd-' + x).attr('name');
            var z = parseInt(j) + 1;
            var optionHtml = '<div class="row" id="respuesta-' + x + z + '">' +
                '<div class="form-check d-flex justify-content-end">' +
                '<label class="form-check-label my-auto">' +
                '<input class="form-check-input" name="multiple[' + x + '][' + z + '][correcta]" type="checkbox" value="1" ><span class="form-check-sign">' +
                '<span class="check"></span></span></label>' +
                '<input name="multiple[' + x + '][' + z + '][texto]" class="form-control" required placeholder="Opcion de respuesta">' +
                '</div>' +
                '</div>';

            $('#respuesta-' + x + j).after(optionHtml);
            j++;
            $('#opcionadd-' + x).attr('name', j);
            $('#opcionrem-' + x).attr('name', j);
        }

        function removeOption(x) {
            var j = $('#opcionadd-' + x).attr('name');
            if (j > 1) {
                $('#respuesta-' + x + j).remove();
                j--;
                $('#opcionadd-' + x).attr('name', j);
                $('#opcionrem-' + x).attr('name', j);
            }
        }
    </script>


    <!-- FALSO O VERDADERO -->
    <script type="text/javascript">
        $(document).ready(function() {
            var maxField2 = 6; //Input fields increment limitation
            var addButton2 = $('.add_button2'); //Add button selector
            var wrapper2 = $('.field_wrapper2'); //Input field wrapper
            var fieldHTML2 = '<div class="row">' +
                '<div class="col">' +
                '<input type="text" name="" class="form-control" placeholder="Afirmacion" id="">' +
                '</div>' +
                '<div class="col">' +
                '<div class="row d-flex justify-content-end mt-3">' +
                '<div class="form-check">' +
                '<label class="form-check-label">' +
                '<input name="field_name2[]" class="form-check-input" type="checkbox" value="">Falso' +
                '<span class="form-check-sign"><span class="check"></span></span>' +
                '</label>' +
                '</div>' +
                '<div class="form-check">' +
                '<label class="form-check-label">' +
                '<input class="form-check-input" type="checkbox" value="">Verdadero' +
                '<span class="form-check-sign"><span class="check"></span></span>' +
                '</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<a href="javascript:void(2);" class="remove_button2" title="Remove field 2"><i class="fas fa-trash text-danger mt-3 mr-2"></i></a>' +
                '</div>';
            var x = 1; //Initial field counter is 1
            $(addButton2).click(function() { //Once add button is clicked
                if (x < maxField2) { //Check maximum number of input fields
                    x++; //Increment field counter
                    $(wrapper2).append(fieldHTML2); // Add field html
                }
            });
            $(wrapper2).on('click', '.remove_button2', function(e) { //Once remove button is clicked
                e.preventDefault();
                $(this).parent('div').remove(); //Remove field html
                x--; //Decrement field counter
            });
        });
    </script>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
    <input type="hidden" id="redirect" value="{{ route('training.index') }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(function() {
        $("#inputSelect").on('change', function() {
            var selectValue = $(this).val();
            switch (selectValue) {

                case "1":
                    $("#div1").show();
                    $("#div2").hide();
                    $("#div3").hide();
                    $("#div4").hide();
                    $("#div5").hide();
                    break;

                case "2":
                    $("#div1").hide();
                    $("#div2").show();
                    $("#div3").hide();
                    $("#div4").hide();
                    $("#div5").hide();
                    break;

                case "3":
                    $("#div1").hide();
                    $("#div2").hide();
                    $("#div3").show();
                    $("#div4").hide();
                    $("#div5").hide();
                    break;

                case "4":
                    $("#div1").hide();
                    $("#div2").hide();
                    $("#div3").hide();
                    $("#div4").show();
                    $("#div5").hide();
                    break;

                case "5":
                    $("#div1").hide();
                    $("#div2").hide();
                    $("#div3").hide();
                    $("#div4").hide();
                    $("#div5").show();
                    break;
            }

        }).change();

    });
</script>
<script>
    $(document).ready(function() {
        $(".w_open").click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
        });

        if ($('#showerror').val() == 1) {
            $('.invalid-feedback').css('display', 'block');
            $('#exampleModal').modal('show');
        }

        $("#tstore").on('submit', function(e) {
            e.preventDefault();
            var type = $(this).attr('method');
            var url = $(this).attr('action');
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                type: type,
                url: url,
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".progress-bar").width('0%');
                    jQuery('#uploadStatus').hide();
                    jQuery('#uploadStatus').html('');
                },
                error: function() {
                    $('#uploadStatus').html('<span class="invalid-feedback d-block" role="alert"><strong>No se pudo cargar de forma exitosa la capacitación, error de conexión</strong></span>');
                },
                success: function(resp) {
                    console.log(resp);
                    jQuery('#uploadStatus').show();
                    jQuery.each(resp.errors, function(key, value) {
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + value + '</strong></span></p>');
                    });

                    if (resp.success) {
                        $('#uploadStatus').append('<p><span style="color:#28A74B;" class="invalid-feedback d-block" role="alert"><strong>' + resp.success + '</strong></span></p>');
                        alert(resp.success);
                        location.href = $('#redirect').val();
                    }

                    if (resp.fallo)
                        jQuery('#uploadStatus').append('<p><span class="invalid-feedback d-block" role="alert"><strong>' + resp.fallo + '</strong></span></p>');
                },
            });
        });
    });
</script>
