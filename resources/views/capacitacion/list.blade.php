@extends('layouts.admin.app')

@section('content')
<script src="{{asset('js/capacitaciones/confirmDelete.js')}}"></script>
    <div class="col-md-12 mx-auto">

        <h5 class="font-weight-bold my-3" style="color: #51A2A7">Todas las Capacitaciones</h5>

        <table class="table table-striped " id="example">
            <thead style="background-color: #51A2A7; color: #fff">
                <tr>
                    <th>Imagen</th>
                    <th>Nombre</th>
                    <th>Categoría</th>
                    <th>Capacitación</th>
                    <th>Trivia - N° de preguntas</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody class="table-light" style="background-color: #fff; color: #000">
                @foreach ($trainings as $training)
                    <tr>
                        <td class="mx-auto text-center">
                            <img style="width:100px;max-height:100px;"
                                src="{{ route('training.preview', ['filename' => basename($training->imagen)]) }}">
                        </td>
                        <td class="align-middle">{{ $training->nombre }}</td>
                        <td class="align-middle">{{ $training->tipo }}</td>
                        <td class="align-middle text-center">
                            @if ($training->archivo)
                                <a href="{{ route('training.archive', ['filename' => basename($training->archivo)]) }}"
                                    target="_blank" class="btn btn-sm rounded btn-danger mx-auto">
                                    @switch(pathinfo($training->archivo)['extension'])
                                        @case('jpg')
                                        @case('jpeg')

                                        @case('png')
                                            <span class="material-icons">image</span>
                                        @break

                                        @case('pdf')
                                            <span class="material-icons">picture_as_pdf</span>
                                        @break

                                        @case('mp4')
                                        @case('wm')

                                        @case('wmv')
                                            <span class="material-icons">play_arrow</span>
                                        @break
                                    @endswitch
                                </a>
                            @else()
                                <span>No posee...</span>
                            @endif
                        </td>
                        <td class="align-middle">
                            {{ $training->trivia ? $training->trivia->nombre . ' | (' . $training->trivia->preguntas->count() . ')' : 'No posee' }}
                        </td>
                        <td>
                            <a href="{{ route('training.edit', $training->id_capacitacion) }}"
                                class="btn text-white btn-sm rounded" style="background-color: #51A2A7">
                                <span class="iconify" data-icon="akar-icons:edit"></span>
                            </a>
                            <form class="d-inline-block" action="{{route('training.destroy',$training->id_capacitacion)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button onclick="return confirm('¿Desea eliminar esta capacitación?')" class="btn btn-danger btn-sm rounded">
                                <span class="iconify" data-icon="fa:trash"></span>
                            </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
