@extends('layouts.admin.app')

@section('content')
    @if (Session::has('error'))
        <div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
            {{ Session::get('error') }}
        </div>
    @endif

    @if (Session::has('create'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('create') }}
        </div>
    @endif

    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
            <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todas los clientes</label>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a style="background-color: #51A2A7" class="btn float-right rounded font-weight-bold" href="{{url('notificaciones/all')}}"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped " cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <!-- <th>Avatar</th> -->
                            <th>Nombre</th>
                            <th>Fecha</th>
                            <th>Descripcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (auth()->user()->allnotifications->take(10) as $not)
                            <tr>
                                <td class="text-center">{{ $not->notificacion->nombre }}</td>
                                <td class="text-center">{{ date_format(date_create($not->notificacion->created_at), 'F j, Y h:m:s') }}</td>
                                <td class="text-center">{{ $not->notificacion->descripcion }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
