@extends('layouts.admin.app')

@section('content')

@if (Session::has('create'))
<script>
    Swal.fire(
        '!Buen trabajo!',
        '{{ Session::get('create') }}',
        'success'
    )
</script>
@endif

@if (Session::has('delete'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('delete') }}
</div>
@endif

@if (Session::has('error'))
<div style="padding: 10px; background-color: #ac2925; color: #ffffff; margin-bottom: 1%;">
    {{ Session::get('error') }}
</div>
@endif

@if (Session::has('edit'))
<div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
    {{ Session::get('edit') }}
</div>
@endif

<div class="row" style="margin-top: 5%">

    <!-------------------------------- Ultimas Acciones --------------------------------->
    <div class="col-lg-8 col-md-8">
        <label for="" class="lead pl-5" style="color: #51A2A7; font-weight: bold">Ultimas acciones</label>
        <div class="float-right pr-5">
            <a href="{{ route('notificacion.list') }}" class="ml-auto text-secondary font-weight-bold"><i class="fa fa-eye mr-2"></i> Ver todos</a>
        </div>

        <div class="p-5">
                <table class='table p-3 table-hover'>
                    <thead>
                        <tr>
                            <th scope="col" class="text-center font-weight-bold" style="color: #51A2A7">Nombre</th>
                            <th scope="col" class="text-center font-weight-bold" style="color: #51A2A7">Fecha</th>
                            <th scope="col" class="text-center font-weight-bold" style="color: #51A2A7">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(auth()->user()->allnotifications->take(10) as $not)
                            <tr>
                                <td class="text-center">{{ $not->notificacion->nombre }}</td>
                                <td class="text-center">{{ date_format(date_create($not->notificacion->created_at), 'F j, Y h:m:s') }}</td>
                                <td class="text-center">{{ $not->notificacion->descripcion }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>

    <!-------------------------------- Usuarios Administrativos --------------------------------->
    <div class="col-lg-4 col-md-4">
        <div class="row mx-auto">
            <label for="" class="lead" style="color: #F28D8D; font-weight: bold">Usuarios Administrativos</label>
            <a href="{{ route('cliente.listadmin') }}" class="ml-auto text-secondary font-weight-bold"><i class="fa fa-eye mr-2"></i>Ver todos</a>
        </div>

        @forelse ($admins as $admin)
            <div class="card card-body mt-5">
                <div class="row d-flex justify-content-between ">
                    <div class="col-md-9">
                        <div class="col">
                            <label for="" class="font-weight-bold" style="color: #F28D8D;">{{ $admin->nombre." ".$admin->apellido }}</label>
                        </div>
                        <div class="flex-column">
                            <div class="col">
                                <label for="" style="color: #F28D8D">Especialidad: {{ $admin->roles->rol }}</label>
                            </div>
                            <div class="col">
                                <label for="">Estado: {{ $admin->estado }}</label>
                            </div>
                            <div class="col">
                                <label for="">Email: {{ $admin->email }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="card card-body">
                <div class="card-header">
                    <div class="row mx-auto">
                        <h4 class="card-title" style="color: #F28D8D; font-weight: bold">No hay Admins Registrados</h4>
                    </div>
                </div>
            </div>
        @endforelse

        @if(auth()->user()->roles->rol == "Super Admin")
            <div class="d-flex justify-content-center">
                <button class="btn btn-round font-weight-bold" style="background-color: #F28D8D" data-toggle="modal"
                data-target=".bd-example-modal-lg">
                <i class="material-icons">add</i>AGREGAR ADMINISTRADOR
            </button>
            </div>
        @endif

        <!-- MODAL ADMIN-->
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style="color: #F28D8D; font-weight: bold">
                            Crear administrador</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('admin.store') }}" method="post">
                    @csrf
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col">
                                    <input name="nombre" id="categoria" type="text" placeholder="Nombre"
                                        class="form-control @error('nombre') is-invalid @enderror" value="{{ old('nombre') }}">
                                    @error('nombre')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <input name="apellido" id="categoria" type="text" placeholder="Apellido"
                                        class="form-control @error('apellido') is-invalid @enderror" value="{{ old('apellido') }}">
                                    @error('apellido')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <input name="email" id="categoria" type="text" placeholder="Correo electronico"
                                        class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <input name="password" id="SUBCATEGORIA" type="password" placeholder="Contraseña"
                                        class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col">
                                    <select name="estado" id="" class="form-control @error('estado') is-invalid @enderror">
                                        <option value="" selected disabled>Seleccione su estado</option>
                                        <option value="activo" {{ old('estado') == "activo" ? "selected" : "" }}>Activo</option>
                                        <option value="inactivo" {{ old('estado') == "inactivo" ? "selected" : "" }}>Inactivo</option>
                                    </select>
                                    @error('estado')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col">
                                    <select name="FK_id_loginrol" id="user_rol" class="form-control">
                                        @foreach ($rol as $i => $a)
                                            <option value="{{ $a->id_loginrol }}">{{ $a->rol }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row my-3">
                                <div class="col-md-6 d-none" id="base_permisos">
                                    <label for="name_user">Permisos de Base de Datos:</label>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_store" name="p_store">
                                        <label class="form-check-label" for="p_store">
                                            Creación de datos
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_update" name="p_update">
                                        <label class="form-check-label" for="p_update">
                                            Actualización de datos
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_destroy" name="p_destroy">
                                        <label class="form-check-label" for="p_destroy">
                                            Eliminación de datos
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6 d-none" id="modulos_permisos">
                                    <label for="name_user">Permisos de Módulos:</label>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_productos" name="p_productos">
                                        <label class="form-check-label" for="p_productos">
                                            Productos
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_premios" name="p_premios">
                                        <label class="form-check-label" for="p_premios">
                                            Premios
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_facturas" name="p_facturas">
                                        <label class="form-check-label" for="p_facturas">
                                            Facturas
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_noticias" name="p_noticias">
                                        <label class="form-check-label" for="p_noticias">
                                            Noticias
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_capacitaciones" name="p_capacitaciones">
                                        <label class="form-check-label" for="p_capacitaciones">
                                            Capacitaciones
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="" type="checkbox" value="1" id="p_trivia" name="p_trivia">
                                        <label class="form-check-label" for="p_trivia">
                                            Trivias y Encuestas
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input class="btn mx-auto col-md-6" style="background-color: #F28D8D; border-radius: 40px;"
                            type="submit" value="CREAR USUARIO">
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
</div>

<style>
    .borderless td,
    .borderless th {
        border: none;
    }
</style>

@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $("#user_rol").change(function(){
            var selectValue = $(this).val();
            //1 Super Admin
            //2 Call Center
            //3 Básico

            //base_permisos
            //modulos_permisos

            if(selectValue == 1){
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").addClass("d-none");
            }
            else if(selectValue == 2){
                $("#base_permisos").removeClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
            else{
                $("#base_permisos").addClass("d-none");
                $("#modulos_permisos").removeClass("d-none");
            }
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('.bd-example-modal-lg').modal('show');
        }
    });
</script>
