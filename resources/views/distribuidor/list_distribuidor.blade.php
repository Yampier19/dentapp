@extends('layouts.admin.app')

@section('content')
    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col">
            <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Todos los
                Distribuidores</label>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th>Nombre del distribuidor</th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                            <th>Identificación</th>
                            <th>Opciónes</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach ($distribuidores as $distribuidor)
                            <tr>
                                <td>{{ $distribuidor->nombre_publico }}</td>
                                <td>{{ $distribuidor->correo }}</td>
                                <td>{{ $distribuidor->num_contacto }}</td>
                                <td>{{ $distribuidor->identificacion }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button style="background-color: #51A2A7" class="btn btn-sm rounded text-white ROUNDED btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                            <span class="iconify" data-icon="fa:eye"></span>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{ route('facturados.distribuidor', $distribuidor->id_distri) }}">Productos Facturados</a>
                                            <a class="dropdown-item" href="{{ route('list.distribuidor.client', $distribuidor->id_distri) }}">Clientes {{ $distribuidor->distribuidor }}</a>
                                            <a class="dropdown-item" href="{{ route('distribuidor.product', $distribuidor->id_distri) }}">Productos {{ $distribuidor->distribuidor }}</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
