@extends('layouts.admin.app')

@section('content')
    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col p-3">
            @if ($distribuidor)
            <div class="row">
                <div class="col-sm-8">
                    <label for="" class="lead font-weight-bold" style="color: #51A2A7">
                        Productos: <u>{{ $distribuidor->distribuidor }}</u>
                    </label>
                </div>
                <div class="col-sm-4">
                    <a href="{{ route('distribuidor.product') }}" class="btn text-white rounded" style="background-color: #51A2A7">
                        Ver todos los productos
                    </a>
                    <a class="btn" href="{{ url('distribuidores/list') }}"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
                </div>
            </div>

            @else
                <div class="row">
                    <div class="col-sm-10">
                        <label for="" class="lead font-weight-bold" style="color: #51A2A7">
                            Todos los productos
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <a style="background-color: #51A2A7" class="btn" href="{{ url('distribuidores/list') }}"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th>Referencia</th>
                            <th>Distribuidor</th>
                            <th>Stock Código</th>
                            <th>Familia</th>
                            <th>Detalles Producto</th>
                            <th>Tipo</th>
                            <th>Puntos Extra</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach ($productos as $producto)
                            <tr>
                                <?php
                                $nombre = 'N/A';
                                switch ($producto->id_distri3M) {
                                    case 1:
                                        $nombre = 'Dental 83';
                                        break;
                                    case 2:
                                        $nombre = 'Adental';
                                        break;
                                    case 3:
                                        $nombre = 'Aldental';
                                        break;
                                    case 4:
                                        $nombre = 'Audifarma';
                                        break;
                                    case 5:
                                        $nombre = 'Casa Dental Gabriel Velasquez';
                                        break;
                                    case 6:
                                        $nombre = 'Casa Odontológica';
                                        break;
                                    case 7:
                                        $nombre = 'Dental Nader';
                                        break;
                                    case 8:
                                        $nombre = 'Dentales Antioquía';
                                        break;
                                    case 9:
                                        $nombre = 'Dentales Market';
                                        break;
                                    case 10:
                                        $nombre = 'Dentales Padilla';
                                        break;
                                    case 11:
                                        $nombre = 'Janer Distribuciones';
                                        break;
                                    case 12:
                                        $nombre = 'Dental Palermo';
                                        break;
                                    case 13:
                                        $nombre = 'Orbidental';
                                        break;
                                    case 14:
                                        $nombre = 'Luis Fernando Posada Henao';
                                        break;
                                    case 15:
                                        $nombre = 'Suministros y Dotaciones COLOM';
                                        break;
                                    case 16:
                                        $nombre = 'Dentales Padilla';
                                        break;
                                    case 17:
                                        $nombre = 'Bracket';
                                        break;
                                    case 18:
                                        $nombre = 'Vertices Aliados';
                                        break;
                                    case 19:
                                        $nombre = 'Dentales Acrilicos';
                                        break;
                                }
                                ?>
                                <td class="font-weight-bold">{{ $nombre }}</td>
                                <td>{{ $producto->referencia }}</td>
                                <td>{{ $producto->stock_3m }}</td>
                                <td>{{ $producto->familia }}</td>
                                <td>{{ $producto->distribuidor }}</td>
                                <td>{{ $producto->tipo }}</td>
                                <td>{{ $producto->puntos_extra }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
