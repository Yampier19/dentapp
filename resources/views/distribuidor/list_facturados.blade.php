@extends('layouts.admin.app')

@section('content')
    <div class="row my-3 ml-2 mt-4">
        <div class="col-6">
            <label for="" class="lead font-weight-bold" style="font-weight: bold; color: #51A2A7">
                @if (!$nombre)
                    Productos facturados por distribuidor
                @else
                    Productos facturados: <u>{{ $nombre }}</u>
                @endif
            </label>
        </div>
        <div class="col">
            <div class="row float-right">
                <a style="background-color: #51A2A7" class="btn btn rounded font-weight-bold" href="{{ url('distribuidores/list') }}"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
            </div>
        </div>
    </div>


    <div class="row" style="display: block" id="tabla">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-sm table-striped table-hover" cellspacing="0" width="100%">
                    <thead class="font-weight-bold" style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th>Distribuidor</th>
                            <th>Producto</th>
                            <th>Cantidad Facturada</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @if ($productos)
                            @foreach ($productos as $producto)
                                <tr>
                                    <td class="font-weight-bold">{{ $nombre }}</td>
                                    <td class="font-weight-bold">{{ $producto->producto }}</td>
                                    <td class="font-weight-bold">{{ $producto->total }}</td>
                                    <td class="font-weight-bold">
                                        {{ \Carbon\Carbon::parse($producto->created_at)->formatLocalized('%B %Y') }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
