@extends('layouts.admin.app')

@section('content')


    <div class="row mb-3 mx-auto" style="display: flex; align-items: center; justify-content: space-between !important">
        <div class="col-10">
        <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 5%; color: #51A2A7">Listado de clientes: <u>{{$distribuidor->distribuidor}}</u></label>
        </div>
        <div class="col-2">
            <a style="background-color: #51A2A7" class="btn" href="{{url('distribuidores/list')}}"><span class="iconify mr-2" data-icon="akar-icons:arrow-back-thick"></span>Volver</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive shadow p-3 mb-5 rounded">
                <table id="example" class="table table-striped" cellspacing="0" width="100%">
                    <thead style="background-color: #51A2A7; color: #fff;">
                        <tr>
                            <th>Distribuidor</th>
                            <th>Nombre Cliente</th>
                            <th>Correo cliente</th>
                            <th>Teléfono cliente</th>
                            <th>Numero de Facturas cargadas</th>
                        </tr>
                    </thead>
                    <tbody class="table-light" style="background-color: #fff; color: #000">
                        @foreach($clientes as $cliente)
                            <tr>
                                <td>{{ $cliente->distribuidor ? $cliente->distribuidor : "No posee" }}</td>
                                <td>{{ $cliente->nombre." ".$cliente->apellido }}</td>
                                <td>{{ $cliente->correo }}</td>
                                <td>{{ $cliente->telefono }}</td>
                                <td>{{ $cliente->facturas->count() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
