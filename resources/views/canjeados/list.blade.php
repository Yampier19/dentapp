@extends('layouts.admin.app')

@section('content')


    @if (Session::has('edit'))
        <div style="padding: 10px; background-color: #00a7d0; color:#ffffff ; margin-bottom: 1%;">
            {{ Session::get('edit') }}
        </div>
    @endif

    @if ($estado)
        <div class="row ">
            <div class="col-md-12">
                <a href="{{ route('canjeado.index') }}" class="btn text-white mb-3" style="background-color: #e7344c;">
                    <i class="fas fa-undo-alt"></i>
                    Ver Cualquier Estado
                </a>
            </div>
        </div>
    @endif

    <!-- MODAL DE EDITAR PREMIO -->
        <div class="modal fade" id="modalEditFactura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar Premio Canjeado</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('canjeado.update') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="col-12 mb-3">
                                <label for="referencia">Referencia del Premio:</label>
                                <input name="referencia" id="referencia" type="text" value=""
                                    class="form-control @error('referencia') is-invalid @enderror">
                                @error('referencia')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-12 mb-3">
                                <label for="cedula">Cédula de Identidad:</label>
                                <input name="cedula" id="cedula" type="number" value=""
                                    class="form-control @error('cedula') is-invalid @enderror">
                                @error('cedula')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-12 mb-3">
                                <label for="name_user">Estado de la Entrega:</label>
                                <select name="estado" id="estadoselect" class="form-control @error('estado') is-invalid @enderror">
                                    <option value="pendiente">Pendiente</option>
                                    <option value="empacando" class="domi">Empacando</option>
                                    <option value="saliendo" class="domi">Saliendo</option>
                                    <option value="entregado">Entregado</option>
                                </select>
                                @error('estado')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <input id="idcanjeado" type="hidden" class="form-control" name="id_premio_canjeado" value="">
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-round btn-success">Actualizar Premio Canjeado</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- FIN MODAL DE EDITAR PREMIO -->

    <label for="" class="lead" style="font-weight: bold; margin-top: 5%; margin-bottom: 2%; color: #51A2A7">
        Lista de premios canjeados {{ $estado ? "( ".$estado." )" : "" }}
    </label>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="premiosCanjeados" class="table table-striped " cellspacing="0" width="100%">
                    <thead class="text-center" style="background-color: #FFFFFF; color: #51A2A7; font-weight: bold">
                        <tr>
                            <th>Cliente</th>
                            <th>Cédula</th>
                            <th>Premio</th>
                            <th>Marca</th>
                            <th>Cantidad</th>
                            <th>Estrellas</th>
                            <th>Fecha Redención</th>
                            <th>Referencia</th>
                            <th>Estado</th>
                            <th>Receptor</th>
                            <th>Dirección</th>
                            <th>Teléfono</th>
                            <!-- <th>Datos de Entrega</th> -->
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="table-bordered text-capitalize text-center" style="background-color: #fff; color: #000">
                        @foreach($premios as $premio)
                            <tr class="text-center">
                                <td class="align-middle">{{ $premio->cliente->nombre." ".$premio->cliente->apellido }}</td>
                                <td class="align-middle" id="ced{{ $premio->id_premio_canjeado }}">{{ $premio->cedula }}</td>
                                <td class="align-middle">{{ $premio->nombre }}</td>
                                <td class="align-middle">{{ $premio->marca }}</td>
                                <td class="align-middle">{{ $premio->cantidad }}</td>
                                <td class="align-middle">{{ $premio->total_estrellas }}</td>
                                <td class="align-middle">{{ $premio->fecha_redencion }}</td>
                                @if($premio->direccion == "Quantum")
                                    <td class="align-middle" id="ref{{ $premio->id_premio_canjeado }}">
                                        <a class="text-success" href="https://api.activarpromo.com/productos/viewpdf/{{ $premio->referencia }}"
                                            style="cursor: pointer; text-decoration: underline">
                                            <span class="iconify" data-icon="fa:download"></span>
                                        </a>
                                    </td>
                                @else
                                    <td class="align-middle" id="ref{{ $premio->id_premio_canjeado }}">{{ $premio->referencia }}</td>
                                @endif
                                <td class="align-middle" id="est{{ $premio->id_premio_canjeado }}">{{ $premio->estado }}</td>
                                <td class="align-middle">{{ $premio->receptor }}</td>
                                <td class="align-middle">{{ $premio->direccion ? $premio->direccion : "Canje Online" }}</td>
                                <td class="align-middle">{{ $premio->telefono }}</td>
                                <!-- <td class="align-middle">
                                    <a id="{{ route('canjeado.datos', $premio->id_premio_canjeado) }}"
                                        class="btn btn-sm btn-danger mx-auto w_open text-white">
                                        Ver Datos <i class="fas fa-eye"></i>
                                    </a>
                                </td> -->
                                <td class="align-middle">
                                    @if($premio->estado != "entregado")
                                        <a  class=" mx-auto text-white feditar" id="{{ $premio->id_premio_canjeado }}"
                                            data-toggle="modal" data-target="#modalEditFactura" data-placement="right" title="Editar">
                                            <span class="material-icons mr-2" style="color: #51A2A7">edit</span>
                                        </a>
                                    @endif
                                </td>
                                <input type="hidden" id="tipo{{ $premio->id_premio_canjeado }}" value="{{ $premio->premio->entrega }}">
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<input type="hidden" id="showerror" value="{{ $errors->any() ? 1 : 0 }}">
@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $( ".w_open" ).click(function() {
            var id = $(this).attr("id");
            window.open(id, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=600,height=500");
        });

        $( ".feditar" ).click(function() {
            var id = $(this).attr("id");
            var ref = $("#ref"+id).text();
            var est = $("#est"+id).text();
            var ced = $("#ced"+id).text();
            var tipo = $("#tipo"+id).val();

            if(tipo == "Online"){
                $(".domi").addClass('d-none');
            }
            else{
                $(".domi").removeClass('d-none');
            }

            $("#referencia").val(ref);
            $("#cedula").val(ced);
            $("#estadoselect").val(est);
            $("#idcanjeado").val(id);
        });

        if($('#showerror').val() == 1){
            $('.invalid-feedback').css('display','block');
            $('#modalEditFactura').modal('show');
        }
    });
</script>
