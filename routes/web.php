<?php

use App\Http\Controllers\AdminController;
//Controladores
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ClienteLoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DistribuidorController;
use App\Http\Controllers\EncuestaController;
use App\Http\Controllers\FacturaController;
use App\Http\Controllers\InformativoController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\NivelController;
use App\Http\Controllers\NoticiaController;
use App\Http\Controllers\NotificacionAdmin;
use App\Http\Controllers\PasswordResetController;
use App\Http\Controllers\PremioController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PushController;
use App\Http\Controllers\QuantumController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TemporadaController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\TriviaPRController;
use App\Models\Temporada;
use Illuminate\Support\Facades\Route;

//RUTAS DE LOGIN | LOGOUT
Route::get('/', [LoginController::class, 'form_login'])->name('login'); //mostrar el login o dashboard
Route::get('login', [LoginController::class, 'form_login']); //->name('login'); //mostrar el login
Route::post('auth', [LoginController::class, 'login'])->name('login.auth'); //metodo de login
Route::get('logout', [LoginController::class, 'logout'])->name('login.logout'); //metodo de logout

// RUTAS RESET PASSWORD
Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('password/email/{tipo?}', [PasswordResetController::class, 'create'])->name('password.email');
Route::get('password/find/{token}/{tipo}', [PasswordResetController::class, 'find'])->name('password.find');
Route::post('password/reset', [PasswordResetController::class, 'reset'])->name('password.update');

//RUTAS DEL DASHBOARD Y PERFIL
Route::middleware(['auth:admin', 'state'])->get('home', [DashboardController::class, 'index'])->name('home');
Route::middleware(['auth:admin', 'state'])->get('escritorio', [DashboardController::class, 'index'])->name('dashboard.index');
Route::prefix('/perfil')->group(function () {
    Route::get('/getImage/{filename?}', [DashboardController::class, 'getImage'])->name('admin.icon');
    Route::middleware(['auth:admin', 'state'])->get('', [DashboardController::class, 'perfil'])->name('modules.dashboard.perfil');
    Route::middleware(['auth:admin', 'state'])->post('/update', [DashboardController::class, 'update'])->name('modules.update.perfil');
});

// RUTAS INFORMATIVO
Route::middleware('noticia')->prefix('/informativo')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('/inicio', [InformativoController::class, 'index'])->name('informativo.index');
    Route::middleware(['auth:admin', 'state'])->get('/listnews', [InformativoController::class, 'listnew'])->name('informativo.listnew');
    Route::middleware(['auth:admin', 'state'])->get('/inicio/listnoticias', [InformativoController::class, 'listnoticias'])->name('informativo.listnoticias');
    Route::middleware(['auth:admin', 'state'])->get('/inicio/listbanners', [InformativoController::class, 'listbanners'])->name('informativo.listbanners');
});

//RUTAS DEL CLIENTE
Route::get('/getImage/{filename?}', [ClienteController::class, 'getImage'])->name('cliente.icon');
Route::get('/getArchive/{filename?}', [ClienteController::class, 'getArchive'])->name('cliente.archive');
Route::middleware('superadmin')->prefix('/cliente')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('/inicio', [ClienteController::class, 'index'])->name('cliente.index');
    Route::middleware(['auth:admin', 'state'])->get('/request-user', [ClienteController::class, 'requestUser'])->name('cliente.request');
    Route::middleware(['auth:admin', 'state'])->patch('/{cliente}/desactivate', [ClienteController::class, 'desabilitarCliente'])->name('deactivate.user');
    Route::middleware(['auth:admin', 'state'])->patch('/{cliente}/restore', [ClienteController::class, 'restoreUser'])->name('cliente.restore');
    Route::middleware(['auth:admin', 'state'])->get('/listado', [ClienteController::class, 'ClientList'])->name('clientes.list');
    Route::middleware(['auth:admin', 'state'])->get('/list', [ClienteController::class, 'listclient'])->name('cliente.listclient');
    Route::middleware(['auth:admin', 'state'])->get('/import', [ClienteController::class, 'import_csv'])->name('cliente.import');
    Route::middleware(['auth:admin', 'state'])->get('/import/tabla', [ClienteController::class, 'tabla_import'])->name('cliente.tabla_import');
    Route::middleware(['auth:admin', 'state'])->get('/consultorio/{id}', [ClienteController::class, 'getConsultorio'])->name('cliente.consultorio');
    Route::middleware(['auth:admin', 'state'])->get('/detalle/{cliente}', [ClienteController::class, 'getDetail'])->name('cliente.detail');
    Route::middleware(['auth:admin', 'state'])->get('/interacciones', [ClienteController::class, 'interacciones'])->name('cliente.interacciones');
    Route::middleware(['auth:admin', 'state'])->get('/userdemo', [ClienteController::class, 'userdemo'])->name('cliente.userdemo');
    Route::middleware(['auth:admin', 'state'])->get('users/export/', [ClienteController::class, 'export'])->name('cliente.export_excel');

    //Apartado de clientes registrados por la App
    Route::middleware(['auth:admin', 'state'])->get('/registrados', [ClienteController::class, 'listapp'])->name('cliente.register.c_register');
    Route::middleware(['auth:admin', 'state'])->get('/editado/{cliente}', [ClienteController::class, 'editapp'])->name('cliente.registrados.edit');
    Route::middleware(['auth:admin', 'state'])->post('/change', [ClienteController::class, 'updateapp'])->name('cliente.registrados.update');
    Route::middleware(['auth:admin', 'state'])->get('/status/{id}/{status}', [ClienteController::class, 'statusapp'])->name('cliente.registrados.status');
    //Fin apartado de clientes registrados por la App

    Route::middleware(['auth:admin', 'state'])->get('/create', [ClienteController::class, 'create'])->name('cliente.create');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/store', [ClienteController::class, 'store'])->name('cliente.store');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import_form', [ClienteController::class, 'import_form'])->name('cliente.importform');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import_csv', [ClienteController::class, 'import'])->name('cliente.importcsv');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/import_store', [ClienteController::class, 'import_store'])->name('cliente.importstore');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/{cliente}', [ClienteController::class, 'destroy'])->name('cliente.destroy');
    Route::middleware(['auth:admin', 'state'])->get('/{cliente}/edit', [ClienteController::class, 'edit'])->name('cliente.edit');
    Route::middleware(['auth:admin', 'state', 'update'])->patch('/update/{cliente}', [ClienteController::class, 'update'])->name('cliente.update');
    Route::middleware(['auth:admin', 'state'])->get('/confirm/{cliente}', [ClienteController::class, 'confirm'])->name('cliente.confirm');
});

//RUTAS DEL ADMIN
Route::prefix('/admin')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('/cliente/create', [AdminController::class, 'create'])->name('cliente.create');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/store', [AdminController::class, 'store'])->name('admin.store');
    Route::middleware(['auth:admin', 'state'])->get('/list', [AdminController::class, 'listadmin'])->name('cliente.listadmin');
    Route::middleware(['auth:admin', 'state'])->get('/list-user', [AdminController::class, 'adminlist'])->name('admin.list');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/{admin}', [AdminController::class, 'destroy'])->name('admin.destroy');
    Route::middleware(['auth:admin', 'state'])->get('/confirm/{id}', [AdminController::class, 'confirm'])->name('admin.confirm');
    Route::middleware(['auth:admin', 'state'])->get('/{admin}/edit', [AdminController::class, 'edit'])->name('admin.edit');
    Route::middleware(['auth:admin', 'state'])->put('/update/{admin}', [AdminController::class, 'update'])->name('admin.update');

    Route::middleware(['auth:admin', 'state'])->get('/levels', [LevelController::class, 'index'])->name('level.index');
    Route::middleware(['auth:admin', 'state'])->post('/levels/create', [LevelController::class, 'store'])->name('level.create');
    Route::middleware(['auth:admin', 'state'])->get('/levels/{level}/edit', [LevelController::class, 'edit'])->name('level.edit');
    Route::middleware(['auth:admin', 'state'])->patch('/levels/{level}/update', [LevelController::class, 'update'])->name('level.update');
    Route::middleware(['auth:admin', 'state'])->delete('/levels/{level}/destroy', [LevelController::class, 'destroy'])->name('level.destroy');

    Route::middleware(['auth:admin', 'state'])->get('actualizar', [LevelController::class, 'actualizar'])->name('level.actualizar');
});

//RUTAS DE temporada
Route::middleware('superadmin')->prefix('/season')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('/inicio', [TemporadaController::class, 'index'])->name('season.index');
    Route::middleware(['auth:admin', 'state'])->post('store', [TemporadaController::class, 'store'])->name('temporada.store');
    Route::middleware(['auth:admin', 'state'])->get('{temporada}/edit', [TemporadaController::class, 'edit'])->name('temporada.edit');
    Route::middleware(['auth:admin', 'state'])->patch('{temporada}/update', [TemporadaController::class, 'update'])->name('temporada.update');
    Route::middleware(['auth:admin', 'state'])->delete('{temporada}/destroy', [TemporadaController::class, 'destroy'])->name('temporada.destroy');
});

//RUTAS PRODUCTOS
Route::prefix('/product')->group(function () {
    Route::middleware(['auth:admin', 'state', 'producto'])->get('/inicio', [ProductController::class, 'index'])->name('product.index');
    Route::middleware(['auth:admin', 'state', 'producto'])->get('/list', [ProductController::class, 'list'])->name('product.list');
    Route::middleware(['auth:admin', 'state', 'producto'])->get('/create', [ProductController::class, 'create'])->name('product.create');
    Route::middleware(['auth:admin', 'state', 'producto', 'store'])->post('/store', [ProductController::class, 'store'])->name('product.store');
    Route::middleware(['auth:admin', 'state', 'producto', 'destroy'])->delete('/{id}', [ProductController::class, 'destroy'])->name('product.destroy');
    Route::middleware(['auth:admin', 'state', 'producto'])->get('/confirm/{id}', [ProductController::class, 'confirm'])->name('product.confirm');
    Route::middleware(['auth:admin', 'state', 'producto'])->get('/{id}/edit', [ProductController::class, 'edit'])->name('product.edit');
    Route::middleware(['auth:admin', 'state', 'producto', 'update'])->patch('/update/{id}', [ProductController::class, 'update'])->name('product.update');
    Route::middleware(['auth:admin', 'state', 'producto'])->get('product/import', [ProductController::class, 'import_csv'])->name('product.import_csv');
    Route::middleware(['auth:admin', 'state', 'producto', 'store'])->post('product/csv', [ProductController::class, 'import'])->name('product.import');

    Route::get('/getImage/{filename?}', [ProductController::class, 'getImage'])->name('product.icon');
});

//RUTAS PARA LAS FACTURAS
Route::prefix('/facturas')->group(function () {
    Route::middleware(['auth:admin', 'state', 'factura'])->get('index', [FacturaController::class, 'index'])->name('factura.index');
    Route::middleware(['auth:admin', 'state', 'factura'])->get('/clientes', [FacturaController::class, 'listcliente'])->name('factura.cliente');

    //NO LEIDAS POR BOT
    Route::middleware(['auth:admin', 'state', 'factura'])->get('/noleidas', [FacturaController::class, 'listbot'])->name('factura.noleidas');
    Route::middleware(['auth:admin', 'state', 'factura', 'destroy'])->get('/destroy/{id}', [FacturaController::class, 'destroy'])->name('factura.destroy');

    //PRODUCTOS ESPECIALES
    Route::middleware(['auth:admin', 'state', 'factura'])->get('/detail/{id}', [FacturaController::class, 'getDetail'])->name('factura.detail');

    Route::middleware(['auth:admin', 'state', 'factura'])->get('/listado/{id?}/{nombre?}', [FacturaController::class, 'listfactura'])->name('factura.listado');
    Route::middleware(['auth:admin', 'state', 'factura', 'update'])->get('/update/{id}/{estado}', [FacturaController::class, 'update'])->name('factura.update');
    Route::middleware(['auth:admin', 'state', 'factura', 'update'])->post('/updateData', [FacturaController::class, 'updateDatos'])->name('factura.updateData');

    Route::get('/getArchive/{filename?}', [FacturaController::class, 'getArchive'])->name('factura.archive');
});

//RUTAS PARA LOS PREMIOS
Route::prefix('/premios')->group(function () {
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/index', [PremioController::class, 'index'])->name('premio.index');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/quantum', [PremioController::class, 'quantum'])->name('premio.quantum');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/control', [PremioController::class, 'create'])->name('premio.control');

    Route::middleware(['auth:admin', 'state', 'premio'])->get('/list', [PremioController::class, 'listpremios'])->name('premio.list');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/list/cambios', [PremioController::class, 'listpremioscambios'])->name('premio.list.cambios');
    Route::middleware(['auth:admin', 'state', 'premio', 'store'])->post('/store', [PremioController::class, 'store'])->name('premio.store');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/{id}/edit', [PremioController::class, 'edit'])->name('premio.edit');
    Route::middleware(['auth:admin', 'state', 'premio', 'update'])->post('/update/{id}', [PremioController::class, 'update'])->name('premio.update');
    Route::middleware(['auth:admin', 'state', 'premio', 'destroy'])->delete('/{id}', [PremioController::class, 'destroy'])->name('premio.destroy');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/confirm/{id}', [PremioController::class, 'confirm'])->name('premio.confirm');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/import', [PremioController::class, 'import_csv'])->name('premio.import_csv');
    Route::middleware(['auth:admin', 'state', 'premio', 'store'])->post('/csv', [PremioController::class, 'import'])->name('premio.import');

    Route::get('/getImage/{filename?}', [PremioController::class, 'getImage'])->name('premio.icon');
});

//AJAX PARA SOLICITAR DATOS DE QUANTUM
Route::prefix('/quantum')->group(function () {
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/marcas', [QuantumController::class, 'QuantumBrands'])->name('quantum.marcas');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/productos/{id?}', [QuantumController::class, 'QuantumProducts'])->name('quantum.productos');
    Route::get('/documento/{filename?}', [QuantumController::class, 'QuantumDocument'])->name('quantum.pdf');
});

//RUTAS PARA LOS PREMIOS CANJEADOS
Route::prefix('/canjeados')->group(function () {
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/list/{pendiente?}', [PremioController::class, 'listcanjeados'])->name('canjeado.index');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/entrega/{id}', [PremioController::class, 'entrega'])->name('canjeado.datos');
    Route::middleware(['auth:admin', 'state', 'premio', 'update'])->post('/update', [PremioController::class, 'updateCanjeado'])->name('canjeado.update');
});

//RUTAS PARA LOS NIVELES | LEVELS
Route::prefix('/nivel')->group(function () {
    Route::middleware(['auth:admin', 'state', 'premio'])->get('index', [NivelController::class, 'nivel'])->name('premio.nivel');
    Route::middleware(['auth:admin', 'state', 'premio', 'update'])->post('/update', [NivelController::class, 'updateNivel'])->name('nivel.update');
    Route::middleware(['auth:admin', 'state', 'premio', 'store'])->post('/level/store', [NivelController::class, 'levelStore'])->name('level.store');
    Route::middleware(['auth:admin', 'state', 'premio', 'update'])->post('/level/update', [NivelController::class, 'updateLevel'])->name('level.update');

    Route::middleware(['auth:admin', 'state', 'premio'])->get('/confirm', [NivelController::class, 'nivel_confirm'])->name('premio.nivel_confirm');
    Route::middleware(['auth:admin', 'state', 'premio'])->get('/level/confirm', [NivelController::class, 'level_confirm'])->name('premio.level_confirm');

    Route::middleware(['auth:admin', 'state', 'premio'])->get('/descripcion/{id}', [NivelController::class, 'descripcion'])->name('nivel.descripcion');
    Route::middleware(['auth:admin', 'state', 'premio', 'store'])->post('/descripcion/store', [NivelController::class, 'storeDesc'])->name('descripcion.store');
    Route::middleware(['auth:admin', 'state', 'premio', 'update'])->post('/descripcion/update', [NivelController::class, 'updateDesc'])->name('descripcion.update');
    Route::middleware(['auth:admin', 'state', 'premio', 'destroy'])->post('/descripcion/delete', [NivelController::class, 'destroyDesc'])->name('descripcion.destroy');
});

//RUTAS PARA LAS NOTIFICACIONES
Route::prefix('/notificaciones')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('all', [NotificacionAdmin::class, 'all'])->name('notificacion.all');
    Route::middleware(['auth:admin', 'state'])->get('list', [NotificacionAdmin::class, 'list'])->name('notificacion.list');
    Route::middleware(['auth:admin', 'state'])->get('index', [NotificacionAdmin::class, 'index'])->name('notificacion.index');
    Route::middleware(['auth:admin', 'state'])->get('/change/{id}', [NotificacionAdmin::class, 'marcarLeido'])->name('notificacion.marcar');
    Route::middleware(['auth:admin', 'state'])->get('/marcar', [NotificacionAdmin::class, 'marcartodo'])->name('notificacion.marcartodas');
    Route::middleware(['auth:admin', 'state'])->get('/marcartres', [NotificacionAdmin::class, 'marcartres'])->name('notificacion.marcartres');

    Route::middleware(['auth:admin', 'state'])->get('/delete/{id}', [NotificacionAdmin::class, 'deleteone'])->name('notificacion.delete');
    Route::middleware(['auth:admin', 'state'])->get('/borrar', [NotificacionAdmin::class, 'deletetodo'])->name('notificacion.deleteall');
});

//RUTAS PARA LAS ENCUESTAS CORTAS EN HOME
Route::middleware('trivia')->prefix('/encuestas')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('index', [EncuestaController::class, 'listencuestas'])->name('encuesta.index');
    Route::middleware(['auth:admin', 'state'])->get('list/user', [EncuestaController::class, 'list_user'])->name('encuesta.list_user');
    Route::middleware(['auth:admin', 'state'])->get('/create', [EncuestaController::class, 'createEncuesta'])->name('encuesta.create');
    Route::middleware(['auth:admin', 'state'])->get('/detail/{id}', [TriviaPRController::class, 'preguntas'])->name('encuesta.detail');
    Route::middleware(['auth:admin', 'state'])->get('/{id?}/edit', [EncuestaController::class, 'edit'])->name('encuesta.edit');
    Route::middleware(['auth:admin', 'state'])->get('/confirm/{id}', [EncuestaController::class, 'confirm'])->name('encuesta.confirm');

    Route::middleware(['auth:admin', 'state'])->post('/encuesta/cliente/{id?}', [EncuestaController::class, 'clienteDetail'])->name('encuesta.cliente');
});

//RUTAS PARA LAS TRIVIAS
Route::middleware('trivia')->prefix('/trivias')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('index', [EncuestaController::class, 'index'])->name('trivia.index');
    Route::middleware(['auth:admin', 'state'])->get('/list/cambios', [EncuestaController::class, 'listcambios'])->name('trivia.list.cambios');
    Route::middleware(['auth:admin', 'state'])->get('/create/{id}', [EncuestaController::class, 'create'])->name('trivia.create');
    Route::middleware(['auth:admin', 'state', 'store'])->post('/store', [EncuestaController::class, 'store'])->name('trivia.store');
    Route::middleware(['auth:admin', 'state'])->get('/{id?}/edit', [EncuestaController::class, 'edit'])->name('trivia.edit');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/update/{id}', [EncuestaController::class, 'update'])->name('trivia.update');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/{id}', [EncuestaController::class, 'destroy'])->name('trivia.destroy');
    Route::middleware(['auth:admin', 'state'])->get('/confirm/{id}', [EncuestaController::class, 'confirm'])->name('trivia.confirm');
});

//RUTAS PARA LAS PREGUNTAS Y RESPUESTAS DE LAS TRIVIAS
Route::middleware('trivia')->prefix('/tpreguntas')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('/detail/{id}', [TriviaPRController::class, 'preguntas'])->name('trivia.detail');
    Route::middleware(['auth:admin', 'state'])->get('/edit/{id}', [TriviaPRController::class, 'edit'])->name('pregunta.edit');

    Route::middleware(['auth:admin', 'state', 'store'])->post('/store', [TriviaPRController::class, 'storePregunta'])->name('pregunta.store');
    Route::middleware(['auth:admin', 'state', 'destroy'])->delete('/delete/{id}', [TriviaPRController::class, 'destroyPregunta'])->name('pregunta.destroy');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/update/', [TriviaPRController::class, 'updatePregunta'])->name('pregunta.update');

    Route::middleware(['auth:admin', 'state', 'store'])->post('/respuesta/store', [TriviaPRController::class, 'storeRepuesta'])->name('respuesta.store');
    Route::middleware(['auth:admin', 'state', 'destroy'])->post('/respuesta/delete', [TriviaPRController::class, 'destroyRespuesta'])->name('respuesta.destroy');
    Route::middleware(['auth:admin', 'state', 'update'])->post('/respuesta/update/', [TriviaPRController::class, 'updateRespuesta'])->name('respuesta.update');
});

//RUTAS PARA LAS TRAINING - CAPACITACIONES
Route::prefix('/training')->group(function () {
    Route::middleware(['auth:admin', 'state', 'capacitacion'])->get('index', [TrainingController::class, 'index'])->name('training.index');
    Route::middleware(['auth:admin', 'state', 'capacitacion'])->get('list/user', [TrainingController::class, 'list_user'])->name('training.list_user');
    Route::middleware(['auth:admin', 'state', 'capacitacion'])->get('/list/capacitacion', [TrainingController::class, 'list_capacitacion'])->name('capacitacion.list');

    Route::middleware(['auth:admin', 'state', 'capacitacion'])->get('/list', [TrainingController::class, 'list'])->name('training.list');
    Route::middleware(['auth:admin', 'state', 'capacitacion'])->get('/list/cambios', [TrainingController::class, 'listcambios'])->name('training.list.cambios');
    Route::middleware(['auth:admin', 'state', 'capacitacion', 'store'])->post('/store', [TrainingController::class, 'store'])->name('training.store');
    Route::middleware(['auth:admin', 'state', 'capacitacion'])->get('/{id}/edit', [TrainingController::class, 'edit'])->name('training.edit');
    Route::middleware(['auth:admin', 'state', 'capacitacion', 'update'])->post('/update/{id}', [TrainingController::class, 'update'])->name('training.update');
    Route::middleware(['auth:admin', 'state', 'capacitacion', 'destroy'])->delete('/{id}', [TrainingController::class, 'destroy'])->name('training.destroy');
    Route::middleware(['auth:admin', 'state', 'capacitacion'])->get('/confirm/{id}', [TrainingController::class, 'confirm'])->name('training.confirm');

    Route::middleware(['auth:admin', 'state', 'capacitacion'])->post('/trivias/cliente/{id?}', [TrainingController::class, 'clienteDetail'])->name('training.cliente');

    Route::get('/getArchive/{filename?}', [TrainingController::class, 'getArchive'])->name('training.archive');
    Route::get('/getImage/{filename?}', [TrainingController::class, 'getImage'])->name('training.preview');
    Route::get('/video/{filename?}', [TrainingController::class, 'ApiTestVideo'])->name('stream.video');
});

//RUTAS PARA LAS NOTICIAS
Route::prefix('/noticia')->group(function () {
    Route::middleware(['auth:admin', 'state', 'noticia'])->get('index', [NoticiaController::class, 'index'])->name('noticias.index');

    Route::middleware(['auth:admin', 'state', 'noticia'])->get('/listnoticias', [NoticiaController::class, 'list'])->name('noticias.listnoticias');
    Route::middleware(['auth:admin', 'state', 'noticia'])->get('/list/cambios', [NoticiaController::class, 'listcambios'])->name('noticias.list.cambios');
    Route::middleware(['auth:admin', 'state', 'noticia', 'store'])->post('/store', [NoticiaController::class, 'store'])->name('noticias.store');
    Route::middleware(['auth:admin', 'state', 'noticia'])->get('/{id}/edit', [NoticiaController::class, 'edit'])->name('noticias.edit');
    Route::middleware(['auth:admin', 'state', 'noticia', 'update'])->post('/update/{id}', [NoticiaController::class, 'update'])->name('noticias.update');
    Route::middleware(['auth:admin', 'state', 'noticia', 'destroy'])->delete('/{id}', [NoticiaController::class, 'destroy'])->name('noticias.destroy');
    Route::middleware(['auth:admin', 'state', 'noticia'])->get('/confirm/{id}', [NoticiaController::class, 'confirm'])->name('noticias.confirm');

    Route::get('/getDocument/{filename?}', [NoticiaController::class, 'getDocument'])->name('noticias.documento');
    Route::get('/getImage/{filename?}', [NoticiaController::class, 'getImage'])->name('noticias.preview');

    //BANNER
    Route::middleware(['auth:admin', 'state', 'noticia', 'store'])->post('/storeBanner', [NoticiaController::class, 'storeBanner'])->name('banner.store');
    Route::middleware(['auth:admin', 'state', 'noticia', 'update'])->post('/updateBanner', [NoticiaController::class, 'updateBanner'])->name('banner.update');
    Route::middleware(['auth:admin', 'state', 'noticia', 'destroy'])->delete('/destroyBanner/{id}', [NoticiaController::class, 'destroyBanner'])->name('banner.destroy');
    Route::get('/banner/getImage/{filename?}', [NoticiaController::class, 'getBannerImage'])->name('banner.preview');
    Route::get('/banner/getArchive/{filename?}', [NoticiaController::class, 'getBannerArchive'])->name('banner.archive');
    Route::get('/banner/video/{filename?}', [NoticiaController::class, 'ApiTestVideo'])->name('banner.video');
});

//RUTAS PARA LAS REPORTES
Route::prefix('/reportes')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('tablas', [ReportController::class, 'tablas'])->name('report.tablas');
    Route::middleware(['auth:admin', 'state'])->get('graficas', [ReportController::class, 'graficas'])->name('report.graficas');
    Route::middleware(['auth:admin', 'state'])->post('graficas/filtradas', [ReportController::class, 'graficasFilter'])->name('report.graficas.filter');
});

//RUTAS PARA LOS DISTRIBUIDORES
Route::prefix('/distribuidores')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('/list', [DistribuidorController::class, 'list_distribuidor'])->name('distribuidor.list');
    Route::middleware(['auth:admin', 'state'])->get('/product/{id?}', [DistribuidorController::class, 'list_product'])->name('distribuidor.product');
    Route::middleware(['auth:admin', 'state'])->get('/client', [DistribuidorController::class, 'list_client'])->name('distribuidor.client');
    Route::middleware(['auth:admin', 'state'])->get('/facturados', [DistribuidorController::class, 'list_facturados'])->name('distribuidor.facturados');
    Route::middleware(['auth:admin', 'state'])->post('/facturados/distribuidor', [DistribuidorController::class, 'list_facturados'])->name('distribuidor.facturados.product');

    Route::middleware(['auth:admin', 'state'])->get('{distribuidor}/clientes', [DistribuidorController::class, 'list_client_distribuidor'])->name('list.distribuidor.client');
    Route::middleware(['auth:admin', 'state'])->get('/{distribuidor}/facturados', [DistribuidorController::class, 'list_facturados_distribuidor'])->name('facturados.distribuidor');
});

//RUTAS PARA LOS PUSH
Route::prefix('/push')->group(function () {
    Route::middleware(['auth:admin', 'state'])->get('/index', [PushController::class, 'index'])->name('push.index');
    Route::middleware(['auth:admin', 'state'])->post('/send', [PushController::class, 'sendNotificacion'])->name('push.send');
});

Route::get('/email-verified/{clienteLogin}', [ClienteLoginController::class, 'emailVerified'])->name('email.verified');
