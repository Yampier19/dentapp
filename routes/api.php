<?php

use App\Http\Controllers\CityController;
use App\Http\Controllers\ClienteController;
//Controladores
use App\Http\Controllers\ClienteLoginController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\EncuestaController;
use App\Http\Controllers\FacturaController;
use App\Http\Controllers\NivelController;
use App\Http\Controllers\NoticiaController;
use App\Http\Controllers\NotificacionCliente;
use App\Http\Controllers\PasswordResetController;
use App\Http\Controllers\PremioController;
use App\Http\Controllers\QuantumController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\TriviaPRController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/cliente', function (Request $request) {
    return $request->user();
});

Route::prefix('/auth')->group(function () {
    Route::post('/login', [ClienteLoginController::class, 'login']);
    Route::post('/registro', [ClienteLoginController::class, 'register']);
    Route::post('/change/password', [ClienteLoginController::class, 'changePassword']);
    Route::post('/change/distribuidor', [ClienteLoginController::class, 'updateDist']);
    Route::middleware('auth:api')->post('/logout', [ClienteLoginController::class, 'logout']);
    Route::middleware('auth:api')->post('/getuser', [ClienteLoginController::class, 'autenthicated']);

    // RUTAS RESET PASSWORD
    Route::post('/password/email/{tipo?}', [PasswordResetController::class, 'create'])->name('password.email');
    Route::get('/password/find/{token}/{tipo}', [PasswordResetController::class, 'find'])->name('password.find');
    Route::post('/password/reset', [PasswordResetController::class, 'reset'])->name('password.update');
});

Route::prefix('/cliente')->group(function () {
    Route::middleware('auth:api')->post('/InfoUser', [ClienteLoginController::class, 'InfoUser']);
    Route::middleware('auth:api')->post('/getuser', [ClienteLoginController::class, 'usuario']);
    Route::middleware('auth:api')->get('/perfil', [ClienteLoginController::class, 'perfil']);
    Route::middleware('auth:api')->post('/puntos', [ClienteLoginController::class, 'clientpoints']);
    Route::middleware('auth:api')->post('/storeip', [ClienteLoginController::class, 'storeIp']);
    Route::middleware('auth:api')->post('/desactivate', [ClienteController::class, 'desabilitarClienteApi']);
    Route::middleware('auth:api')->post('/updatePerfil', [ClienteController::class, 'ApiUpdatePerfil']);
    Route::middleware('auth:api')->post('/updatePassword', [ClienteLoginController::class, 'updatePassword']);
    Route::middleware('auth:api')->post('/changeAvatar', [ClienteLoginController::class, 'changeAvatar']);
    Route::middleware('auth:api')->post('/avatar/{filename?}', [ClienteLoginController::class, 'useravatar']);

    Route::middleware('auth:api')->get('/nivel', [ClienteLoginController::class, 'clientlevel']);
});

Route::prefix('/niveles')->group(function () {
    Route::middleware('auth:api')->get('/getImage/{filename?}', [NivelController::class, 'ApigetImage']);
    Route::middleware('auth:api')->get('/list', [NivelController::class, 'ApiList']);
    Route::middleware('auth:api')->get('/{id}', [NivelController::class, 'ApiNivel']);
});

Route::prefix('/training')->group(function () {
    Route::middleware('auth:api')->get('/categories', [TrainingController::class, 'ApigetCategories']);
    Route::middleware('auth:api')->get('/getVideo/{filename?}', [TrainingController::class, 'ApiTestVideo']);
    Route::middleware('auth:api')->get('/getArchive/{filename?}', [TrainingController::class, 'ApigetArchive']);
    Route::middleware('auth:api')->get('/getImage/{filename?}', [TrainingController::class, 'ApigetImage']);
    Route::middleware('auth:api')->get('/list/{registros?}', [TrainingController::class, 'ApiListado']);
    Route::middleware('auth:api')->get('/buscador/{parametro}/{registros?}', [TrainingController::class, 'ApiBuscador']);
    Route::middleware('auth:api')->get('/fechas/{registros?}', [TrainingController::class, 'ApiNew']);
    Route::middleware('auth:api')->get('/categoria/{categoria}/{registros?}', [TrainingController::class, 'ApiListCategory']);
    Route::middleware('auth:api')->get('/{id}', [TrainingController::class, 'ApiTraining']);
});

Route::prefix('/encuesta')->group(function () {
    //Trivias y Encuestas
    Route::middleware('auth:api')->get('/list/{registros?}', [EncuestaController::class, 'ApiListado']);
    Route::middleware('auth:api')->get('/listhome/{registros?}', [EncuestaController::class, 'ApiListadoEncuestas']);
    Route::middleware('auth:api')->get('/{id}', [EncuestaController::class, 'ApiTrivia']);

    Route::middleware('auth:api')->get('/skip/{id}', [TriviaPRController::class, 'ApiEncuestaSkip']);
    Route::middleware('auth:api')->get('/preguntas/{id}', [TriviaPRController::class, 'ApiPreguntas']);
    Route::middleware('auth:api')->post('/respuestas/{id}', [TriviaPRController::class, 'ApiTriviaRespuesta']);
    Route::middleware('auth:api')->post('/encuestarespuestas/{id}', [TriviaPRController::class, 'ApiEncuestaRespuesta']);
});

Route::prefix('/factura')->group(function () {
    Route::middleware('auth:api')->post('/store', [FacturaController::class, 'store']);
    Route::middleware('auth:api')->post('/confirm', [FacturaController::class, 'confirm']);
    Route::middleware('auth:api')->post('/revision', [FacturaController::class, 'sendRevision']);
    Route::middleware('auth:api')->get('/list/{registros?}', [FacturaController::class, 'ApiList']);
    Route::middleware('auth:api')->get('/{id}', [FacturaController::class, 'ApiFactura']);
    Route::middleware('auth:api')->get('/getArchive/{filename?}', [FacturaController::class, 'ApigetArchive']);
});

Route::prefix('/premio')->group(function () {
    Route::middleware('auth:api')->get('/categories', [PremioController::class, 'ApigetCategories']);
    Route::middleware('auth:api')->get('/getImage/{filename?}', [PremioController::class, 'ApigetImage']);
    Route::middleware('auth:api')->get('/list/{registros?}', [PremioController::class, 'ApiList']);
    Route::middleware('auth:api')->get('/nuevos/{registros?}', [PremioController::class, 'ApiNew']);
    Route::middleware('auth:api')->get('/canjeados', [PremioController::class, 'ApiCanjeados']);
    Route::middleware('auth:api')->get('/canjeados/{id}', [PremioController::class, 'ApiCanje']);
    Route::middleware('auth:api')->post('/store/{id}', [PremioController::class, 'canjear']);
    Route::middleware('auth:api')->get('/categoria/{categoria}/{registros?}', [PremioController::class, 'ApiListCategory']);
    Route::middleware('auth:api')->get('/{id}', [PremioController::class, 'ApiPremio']);
});

Route::prefix('/noticias')->group(function () {
    Route::middleware('auth:api')->get('/categories', [NoticiaController::class, 'ApigetCategories']);
    Route::middleware('auth:api')->get('/getDocument/{filename?}', [NoticiaController::class, 'ApigetDocument']);
    Route::middleware('auth:api')->get('/getImage/{filename?}', [NoticiaController::class, 'ApigetImage']);
    Route::middleware('auth:api')->get('/list/{registros?}', [NoticiaController::class, 'ApiListado']);
    Route::middleware('auth:api')->get('/new/{registros?}', [NoticiaController::class, 'ApiNew']);
    Route::middleware('auth:api')->get('/categoria/{categoria}/{registros?}', [NoticiaController::class, 'ApiListCategory']);
    Route::middleware('auth:api')->get('/{id}', [NoticiaController::class, 'ApiNoticia']);
});

Route::prefix('/banners')->group(function () {
    Route::middleware('auth:api')->get('/list', [NoticiaController::class, 'ApiBanners']);
    Route::middleware('auth:api')->get('/getImage/{filename?}', [NoticiaController::class, 'ApigetImageBanner']);
    Route::middleware('auth:api')->get('/getVideo/{filename?}', [NoticiaController::class, 'ApiTestVideo']);
});

Route::prefix('/notificaciones')->group(function () {
    Route::middleware('auth:api')->get('/total', [NotificacionCliente::class, 'index']);
    Route::middleware('auth:api')->get('/list', [NotificacionCliente::class, 'ApiListado']);
    Route::middleware('auth:api')->get('/read/{id}', [NotificacionCliente::class, 'marcarLeido']);
    Route::middleware('auth:api')->get('/readall', [NotificacionCliente::class, 'marcartodo']);

    Route::middleware('auth:api')->get('/delete/{id}', [NotificacionCliente::class, 'deleteone']);
    Route::middleware('auth:api')->get('/deleteall', [NotificacionCliente::class, 'deletetodo']);
});

Route::prefix('/quantum')->group(function () {
    //Apartado de localizar el producto, marca, ciudad etc etc
    Route::middleware('auth:api')->get('/marcas', [QuantumController::class, 'QuantumBrands']);
    Route::middleware('auth:api')->get('/productos/{id}', [QuantumController::class, 'QuantumProducts']);
    Route::middleware('auth:api')->get('/departamentos/{id}', [QuantumController::class, 'QuantumBrandDepartment']);
    Route::middleware('auth:api')->get('/ciudades/{id_brand}/{id_department}', [QuantumController::class, 'QuantumBrandCities']);
    Route::middleware('auth:api')->get('/sitios/{id_brand}/{id_department}/{id_city}', [QuantumController::class, 'QuantumBrandSites']);

    //Apartado ya de canje como tal
    Route::middleware('auth:api')->post('/redencion/{id}', [QuantumController::class, 'QuantumRedeem']);
    Route::middleware('auth:api')->post('/existencia', [QuantumController::class, 'QuantumPreCheck']);
    Route::middleware('auth:api')->post('/verificar', [QuantumController::class, 'QuantumVerify']);

    //RUTA PARA PROBAR LA REDENCION
    Route::middleware('auth:api')->post('/testeo', [QuantumController::class, 'QuantumTesting']);
    Route::middleware('auth:api')->post('/redemtest', [QuantumController::class, 'QuantumTestRedeem']);

    //Llamar una imagen del canje con el PDF
    Route::get('/documento/{filename?}', [QuantumController::class, 'QuantumDocument']);
});

//PRUEBA DE STREAM #TODO
Route::prefix('/testvideo')->group(function () {
    Route::middleware('auth:api')->get('/video/{filename?}', [TrainingController::class, 'ApiTestVideo']);
});

// Route::post('/doctors/store', [DoctorController::class, 'store']);

Route::post('/doctors', [DoctorController::class, 'store']);
Route::get('/doctors/index', [DoctorController::class, 'index']);
Route::get('/country/{id}', [CityController::class, 'bydepartament']);
// Route::resource('doctors',DoctorController::class);
// Route::post('/{cliente}/desactivate', [ClienteController::class, 'desabilitarClienteApi']);
