<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TriviaRespuesta extends Model
{
    use HasFactory;

    protected $table = 'dentapp_trivia_respuesta';

    protected $primaryKey = 'id_respuesta';

    protected $fillable = [
        'id_respuesta',
        'informacion',
        'correcta',
        'FK_id_pregunta',
    ];

    public function pregunta()
    {
        return $this->belongsTo('App\Models\TriviaPregunta', 'FK_id_pregunta');
    }
}
