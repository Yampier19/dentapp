<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioCambio extends Model
{
    use HasFactory;

    protected $table = 'dentapp_premios_gestion_cambio';

    protected $primaryKey = 'id_premio_cambio';

    protected $fillable = [
        'id_premio_cambio',
        'accion',
        'nombre',
        'marca',
        'descripcion',
        'entrega', //
        'tag',
        'stock',
        'limite_usuario',
        'numero_estrellas',
        'FK_id_useradmin',
        'FK_id_premios',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin')->withTrashed();
    }

    public function premio()
    {
        return $this->belongsTo('App\Models\Premio', 'FK_id_premios');
    }
}
