<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NavidadCanjeado extends Model
{
    use HasFactory;

    protected $table = 'dentapp_navidad_canjeados';

    protected $primaryKey = 'id_navidad_canjeado';

    protected $fillable = [
        'id_navidad_canjeado',
        'forma',
        'tipo',
        'nombre',
        'marca',
        'descripcion',
        'referencia',
        'completado',
        'FK_id_navidad',
        'FK_id_premio',
    ];

    public function navidad()
    {
        return $this->belongsTo('App\Models\Navidad', 'FK_id_navidad');
    }

    public function premio()
    {
        return $this->belongsTo('App\Models\Premio', 'FK_id_premio')->withTrashed();
    }
}
