<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticiaCambio extends Model
{
    use HasFactory;

    protected $table = 'dentapp_noticias_cambios';

    protected $primaryKey = 'id_noticia_cambio';

    protected $fillable = [
        'id_noticia_cambio',
        'accion',
        'nombre',
        'informacion',
        'tipo',
        'estado',
        'fecha_inicio',
        'fecha_fin',
        'FK_id_useradmin',
        'FK_id_noticia',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin')->withTrashed();
    }

    public function noticia()
    {
        return $this->belongsTo('App\Models\Noticia', 'FK_id_noticia');
    }
}
