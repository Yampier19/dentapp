<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    use HasFactory;

    protected $table = 'dentapp_noticias';

    protected $primaryKey = 'id_noticia';

    protected $fillable = [
        'id_noticia',
        'nombre',
        'informacion',
        'tipo',
        'estado',
        'imagen',
        'documento',
        'fecha_inicio',
        'fecha_fin',
    ];

    public function cambios()
    {
        return $this->hasMany('App\Models\NoticiaCambio', 'FK_id_noticia');
    }
}
