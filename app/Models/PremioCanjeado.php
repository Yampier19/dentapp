<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioCanjeado extends Model
{
    use HasFactory;

    protected $table = 'dentapp_premios_canjeado';

    protected $primaryKey = 'id_premio_canjeado';

    protected $fillable = [
        'id_premio_canjeado',
        'cedula', //cedula de la persona que canjea el premio o receptor como tal
        'nombre', //viene del premio enviado
        'marca', //viene del premio enviado
        'cantidad', //se envia al canjear
        'total_estrellas', //viene del premio enviado
        'receptor', //se envia al canjear
        'telefono', //se envia al canjear
        'direccion', //se envia al canjear
        'adicional', //se envia al canjear
        'estado', //graba por defecto pendiente al canjear
        'fecha_redencion', //se graba directamente por el sistema
        'referencia', //se modifica por parte del superadmin
        'FK_id_premio',
        'FK_id_cliente',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function premio()
    {
        return $this->belongsTo('App\Models\Premio', 'FK_id_premio')->withTrashed();
    }

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }
}
