<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formulario extends Model
{
    use HasFactory;

    protected $table = 'dentapp_formularios';

    protected $primaryKey = 'id_formulario';

    protected $fillable = [
        'nombre_form',
        'estado',
        'descripcion',
        'fecha_inicio',
        'fecha_fin',
        'ubicacion_movil',
        'FK_id_useradmin',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin');
    }
}
