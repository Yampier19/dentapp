<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estrella extends Model
{
    use HasFactory;

    protected $table = 'dentapp_estrellas';

    protected $primaryKey = 'id_estrellas';

    protected $fillable = [
        'id_estrellas',
        'descripcion',
        'cantidad',
        'fecha_registro',
        'expiration_date',
        'estado',
        'FK_id_cliente',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }
}
