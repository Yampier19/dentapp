<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'is_active',
        'city_id',
        'speciality_id',
        'distribuidor_id',
        'cliente_id',
        'created_at',
        'updated_at',
    ];
}
