<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HalloDentFactura extends Model
{
    use HasFactory;

    protected $table = 'dentapp_hallodent_facturas';

    protected $primaryKey = 'id_hallodent_facturas';

    protected $fillable = [
        'id_hallodent_facturas',
        'FK_id_factura',
        'FK_id_hallodent',
    ];

    public function campaña()
    {
        return $this->belongsTo('App\Models\HalloDent', 'FK_id_hallodent');
    }

    public function factura()
    {
        return $this->belongsTo('App\Models\Factura', 'FK_id_factura');
    }
}
