<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSeason extends Model
{
    use HasFactory;

    protected $table = 'user_point_seasons';

    protected $fillable = [
        'temporada_id',
        'cliente_id',
        'points',
        'expired_at',
    ];

    public function client(){
        return $this->belongsTo(Cliente::class,'cliente_id');
    }
}
