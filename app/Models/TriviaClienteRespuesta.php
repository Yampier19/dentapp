<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TriviaClienteRespuesta extends Model
{
    use HasFactory;

    protected $table = 'dentapp_trivia_cliente_respuesta';

    protected $primaryKey = 'id_trivia_cliente_respuesta';

    protected $fillable = [
        'id_trivia_cliente_respuesta',
        'informacion',
        'correcta',
        'pregunta',
        'FK_id_trivia_cliente',
    ];

    /* public function pregunta(){
        return $this->belongsTo('App\Models\TriviaPregunta', 'FK_id_pregunta');
    } */

    public function clientetrivia()
    {
        return $this->belongsTo('App\Models\TriviaCliente', 'FK_id_trivia_cliente');
    }
}
