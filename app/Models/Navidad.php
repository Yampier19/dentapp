<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Navidad extends Model
{
    use HasFactory;

    protected $table = 'dentapp_navidad';

    protected $primaryKey = 'id_navidad';

    protected $fillable = [
        'id_navidad',
        'tipo',
        'nombre', //
        'apellido', //
        'correo',
        'fecha_participante', //
        'tickets',
        'tickets_especial',
        'tickets_usados',
        'FK_id_cliente',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }

    public function facturas()
    {
        return $this->hasMany('App\Models\NavidadFactura', 'FK_id_navidad');
    }

    public function canjeados()
    {
        return $this->hasMany('App\Models\NavidadCanjeado', 'FK_id_navidad');
    }
}
