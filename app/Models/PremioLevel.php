<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioLevel extends Model
{
    use HasFactory;

    protected $table = 'dentapp_premios_level';

    protected $primaryKey = 'id_premio_level';

    protected $fillable = [
        'id_premio_level',
        'FK_id_premio',
        'FK_id_level',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function premio()
    {
        return $this->belongsTo('App\Models\Premio', 'FK_id_premio')->withTrashed();
    }

    public function level()
    {
        return $this->belongsTo('App\Models\Level', 'FK_id_level');
    }

    public function levelapi()
    {
        return $this->belongsTo('App\Models\Level', 'FK_id_level')->with('nivel');
    }
}
