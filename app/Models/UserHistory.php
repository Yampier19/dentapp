<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserHistory extends Model
{
    use HasFactory;

    protected $table = 'dentapp_history_user';

    protected $primaryKey = 'id_history_user';

    protected $fillable = [
        'id_history_user',
        'accion',
        'email',
        'password',
        'FK_id_cliente',
        'FK_id_useradmin',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin')->withTrashed();
    }
}
