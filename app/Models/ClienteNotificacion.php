<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteNotificacion extends Model
{
    use HasFactory;

    protected $table = 'dentapp_cliente_notificaciones';

    protected $primaryKey = 'id_cliente_notificaciones';

    protected $fillable = [
        'id_cliente_notificaciones',
        'FK_id_cliente',
        'FK_id_notificaciones',
        'read_at',
    ];

    protected $hidden = [
        'FK_id_notificaciones',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }

    public function notificacion()
    {
        return $this->belongsTo('App\Models\Notificaciones', 'FK_id_notificaciones');
    }
}
