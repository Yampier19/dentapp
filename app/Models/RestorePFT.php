<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RestorePFT extends Model
{
    use HasFactory;

    protected $table = 'dentapp_restorepft';

    protected $primaryKey = 'id_restore';

    protected $fillable = [
        'id_restore',
        'nombre',
        'fecha_cambio',
    ];

    protected $hidden = [
        'id_restore',
        'created_at',
        'updated_at',
    ];
}
