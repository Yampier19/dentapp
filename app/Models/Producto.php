<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $table = 'dentapp_producto_categoria';

    protected $primaryKey = 'id_producto_categoria';

    protected $fillable = [
        'id_producto_categoria',
        'categoria',
        'subcategoria',
        'stock',
        'descripcion',
        'precio',
        'estado',
        'foto',
        'numero_estrellas',
        'FK_id_familia',
    ];

    public function familia()
    {
        return $this->belongsTo('App\Models\ProductoFamilia', 'FK_id_familia');
    }

    public function cambios()
    {
        return $this->hasMany('App\Models\ProductoCambio', 'FK_id_producto_categoria');
    }

    public function pedidos()
    {
        return $this->hasMany('App\Models\ProductoPedido', 'FK_id_producto_categoria');
    }

    public function multiplicadores()
    {
        return $this->hasMany('App\Models\Multiplicador', 'FK_id_producto_categoria');
    }
}
