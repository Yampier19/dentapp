<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HalloDent extends Model
{
    use HasFactory;

    protected $table = 'dentapp_hallodent';

    protected $primaryKey = 'id_hallodent';

    protected $fillable = [
        'id_hallodent',
        'nombre', //
        'apellido', //
        'correo',
        'fecha_participante', //
        'ranking',
        'facturado',
        'FK_id_cliente',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }

    public function facturas()
    {
        return $this->hasMany('App\Models\HalloDentFactura', 'FK_id_hallodent');
    }
}
