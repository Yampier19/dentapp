<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Model;
use Illuminate\Notifications\Notifiable;

class Admin extends Model
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $table = 'dentapp_useradmin';

    protected $primaryKey = 'id_useradmin';

    protected $guarded = ['id_useradmin'];

    protected $fillable = [
        'id_useradmin',
        'nombre',
        'apellido',
        'email',
        'foto',
        'estado',
        'email_verified_at',
        'password',
        //PERMISOS
        'p_store',
        'p_update',
        'p_destroy',

        'p_productos',
        'p_premios',
        'p_facturas',
        'p_noticias',
        'p_capacitaciones',
        'p_trivia',
        //FIN PERMISOS
        'remember_token',
        'FK_id_loginrol',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getAuthPassword()
    {
        return $this->password;
    }

    /* APARTADO DE LAS NOTIFICACIONES */
    public function allnotifications()
    {
        return $this->hasMany('App\Models\AdminNotificacion', 'FK_id_useradmin')->orderBy('created_at', 'desc');
    }

    public function notificaciones()
    {
        return $this->hasMany('App\Models\AdminNotificacion', 'FK_id_useradmin')->whereNotNull('read_at')->orderBy('created_at', 'desc');
    }

    public function unreadnotification()
    {
        return $this->hasMany('App\Models\AdminNotificacion', 'FK_id_useradmin')->whereNull('read_at')->orderBy('created_at', 'desc');
    }

    public function ownotification()
    {
        return $this->hasMany('App\Models\Notificaciones', 'FK_id_useradmin');
    }
    /* FIN APARTADO DE LAS NOTIFICACIONES */

    public function roles()
    {
        return $this->belongsTo('App\Models\Rol', 'FK_id_loginrol');
    }

    public function login_cambio()
    {
        return $this->hasMany('App\Models\UserHistory', 'FK_id_useradmin');
    }

    public function premios_cambio()
    {
        return $this->hasMany('App\Models\PremioCambio', 'FK_id_useradmin');
    }

    public function prodcutos_cambio()
    {
        return $this->hasMany('App\Models\ProductoCambio', 'FK_id_useradmin');
    }

    public function capacitacion_cambio()
    {
        return $this->hasMany('App\Models\CapacitacionCambio', 'FK_id_useradmin');
    }

    public function trivia_cambio()
    {
        return $this->hasMany('App\Models\TriviaCambio', 'FK_id_useradmin');
    }

    public function noticia_cambio()
    {
        return $this->hasMany('App\Models\NoticiaCambio', 'FK_id_useradmin');
    }
}
