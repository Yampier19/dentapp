<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DistribuidorProducto extends Model
{
    use HasFactory;

    protected $table = 'dentapp_distribuidores_product_3M';

    protected $primaryKey = 'id_distri';

    protected $fillable = [
        'id_pro',
        'id_distri3M',
        'referencia',
        'stock_3m',
        'familia',
        'distribuidor',
        'puntos_extra',
    ];
}
