<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Codigo extends Model
{
    use HasFactory;

    protected $table = 'dentapp_codigo';

    protected $primaryKey = 'id_codigo';

    protected $fillable = [
        'id_codigo',
        'referencia',
        'nombre_proveedor',
        'correo',
    ];
}
