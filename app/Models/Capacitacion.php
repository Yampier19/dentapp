<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capacitacion extends Model
{
    use HasFactory;

    protected $table = 'dentapp_capacitacion';

    protected $primaryKey = 'id_capacitacion';

    protected $fillable = [
        'id_capacitacion',
        'nombre',
        'descripcion',
        'tipo',
        'informacion',
        'imagen',
        'archivo',
    ];

    public function cambios()
    {
        return $this->hasMany('App\Models\CapacitacionCambio', 'FK_id_capacitacion');
    }

    public function clientes()
    {
        return $this->hasMany('App\Models\CapacitacionCliente', 'FK_id_capacitacion');
    }

    public function trivia()
    {
        return $this->hasOne('App\Models\Trivia', 'FK_id_capacitacion');
    }
}
