<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Multiplicador extends Model
{
    use HasFactory;

    protected $table = 'dentapp_multiplicador';

    protected $primaryKey = 'id_multiplicador';

    protected $fillable = [
        'id_multiplicador',
        'multiplo',
        'FK_id_level',
        'FK_id_producto_categoria',
    ];

    public function level()
    {
        return $this->belongsTo('App\Models\Level', 'FK_id_level');
    }

    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'FK_id_producto_categoria');
    }
}
