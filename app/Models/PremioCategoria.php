<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioCategoria extends Model
{
    use HasFactory;

    protected $table = 'dentapp_premio_categoria';

    protected $primaryKey = 'id_premio_subcategoria';

    protected $fillable = [
        'id_premio_subcategoria',
        'nombre',
    ];

    protected $hidden = [
        'id_premio_subcategoria',
        'created_at',
        'updated_at',
    ];
}
