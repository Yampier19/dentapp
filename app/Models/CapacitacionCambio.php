<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapacitacionCambio extends Model
{
    use HasFactory;

    protected $table = 'dentapp_capacitacion_gestion_cambio';

    protected $primaryKey = 'id_capacitacion';

    protected $fillable = [
        'id_capacitacion_cambio',
        'accion',
        'nombre',
        'descripcion',
        'tipo',
        'informacion',
        'archivo',
        'FK_id_useradmin',
        'FK_id_capacitacion',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin')->withTrashed();
    }

    public function capacitacion()
    {
        return $this->belongsTo('App\Models\Capacitacion', 'FK_id_capacitacion');
    }
}
