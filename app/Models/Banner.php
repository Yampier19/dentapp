<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    protected $table = 'dentapp_banner';

    protected $primaryKey = 'id_banner';

    protected $fillable = [
        'id_banner',
        'distribuidor',
        'imagen',
        'archivo',
    ];
}
