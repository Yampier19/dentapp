<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NavidadFactura extends Model
{
    use HasFactory;

    protected $table = 'dentapp_navidad_factura';

    protected $primaryKey = 'id_navidad_factura';

    protected $fillable = [
        'id_navidad_factura',
        'tipo',
        'FK_id_factura',
        'FK_id_navidad',
    ];

    public function campaña()
    {
        return $this->belongsTo('App\Models\Navidad', 'FK_id_navidad');
    }

    public function factura()
    {
        return $this->belongsTo('App\Models\Factura', 'FK_id_factura');
    }

    public function productos()
    {
        return $this->hasMany('App\Models\NavidadFacturaProducto', 'FK_id_navidad_factura');
    }
}
