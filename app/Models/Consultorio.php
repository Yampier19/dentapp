<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Consultorio extends Model
{
    use HasFactory;

    protected $table = 'dentapp_consultorio';

    protected $primaryKey = 'id_consultorio';

    protected $fillable = [
        'id_consultorio',
        'nombre',
        'direccion',
        'telefono',
        'numero_pacientes',
        'FK_id_cliente',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'FK_id_cliente',
    ];

    public function premios()
    {
        return $this->hasMany('App\Models\PremioCanjeado', 'FK_id_cliente');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }
}
