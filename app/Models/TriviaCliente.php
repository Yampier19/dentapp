<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TriviaCliente extends Model
{
    use HasFactory;

    protected $table = 'dentapp_trivia_cliente';

    protected $primaryKey = 'id_trivia_cliente';

    protected $fillable = [
        'id_trivia_cliente',
        'nombre',
        'descripcion',
        'numero_estrellas',
        'FK_id_trivia',
        'FK_id_cliente',
    ];

    public function trivia()
    {
        return $this->belongsTo('App\Models\Trivia', 'FK_id_trivia');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }

    public function respuestas()
    {
        return $this->hasMany('App\Models\TriviaClienteRespuesta', 'FK_id_trivia_cliente');
    }
}
