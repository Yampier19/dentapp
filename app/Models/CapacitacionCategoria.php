<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapacitacionCategoria extends Model
{
    use HasFactory;

    protected $table = 'dentapp_capacitacion_categoria';

    protected $primaryKey = 'id_capacitacion_categoria';

    protected $fillable = [
        'id_capacitacion_categoria',
        'nombre',
    ];

    protected $hidden = [
        'id_capacitacion_categoria',
        'created_at',
        'updated_at',
    ];
}
