<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NivelDescripcion extends Model
{
    use HasFactory;

    protected $table = 'dentapp_nivel_descripcion';

    protected $primaryKey = 'id_nivel_descripcion';

    protected $fillable = [
        'id_nivel_descripcion',
        'descripcion',
        'FK_id_nivel',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'FK_id_nivel',
    ];
}
