<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NavidadPremio extends Model
{
    use HasFactory;

    protected $table = 'dentapp_navidad_premios';

    protected $primaryKey = 'id_navidad_premios';

    protected $fillable = [
        'id_navidad_premios',
        'tipo',
        'FK_id_premio',
    ];

    public function premio()
    {
        return $this->belongsTo('App\Models\Premio', 'FK_id_premio')->withTrashed();
    }
}
