<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturaNoLeida extends Model
{
    use HasFactory;

    protected $table = 'dentapp_factura_noleida';

    protected $primaryKey = 'id_factura_noleida';

    protected $fillable = [
        'id_factura_noleida',
        'foto',
        'fecha_registro',
    ];
}
