<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use HasFactory;

    protected $table = 'dentapp_factura';

    protected $primaryKey = 'id_factura';

    protected $fillable = [
        'id_factura',
        'referencia', //
        'distribuidor', //
        'descripcion',
        'telefono', //
        'puntos',
        'monto_total', //
        'foto',
        'estado',
        'revision',
        'fecha_registro', //
        'FK_id_cliente',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }

    public function especiales()
    {
        return $this->hasMany('App\Models\FacturaProductoEspecial', 'FK_id_factura');
    }

    public function halloween()
    {
        return $this->hasOne('App\Models\HalloDentFactura', 'FK_id_factura');
    }

    public function navidad()
    {
        return $this->hasOne('App\Models\NavidadFactura', 'FK_id_factura');
    }
}
