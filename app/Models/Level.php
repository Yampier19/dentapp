<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Level extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, InteractsWithMedia;

    protected $table = 'dentapp_level';

    protected $primaryKey = 'id_level';

    protected $fillable = [
        'id_level',
        'nombre',
        'meta',
        'rango_inicio',
        'rango_inicio_pacientes',
        'rango_final_pacientes',
        'FK_id_nivel',
        'loginrol_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'rango_inicio_pacientes',
        'rango_final_pacientes',
    ];

    public function nivel()
    {
        return $this->belongsTo('App\Models\Nivel', 'FK_id_nivel');
    }

    public function cliente()
    {
        return $this->hasMany('App\Models\Cliente', 'FK_id_level')->withTrashed();
    }

    public function premios()
    {
        return $this->hasMany('App\Models\PremioLevel', 'FK_id_level');
    }

    public function multiplicador()
    {
        return $this->hasMany('App\Models\Multiplicador', 'FK_id_level');
    }

    public function rol()
    {
        return $this->belongsTo(Rol::class, 'loginrol_id', 'id_loginrol');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('level-image')
            ->useFallbackUrl('/img/niveles/img.jpg')
            ->singleFile();
    }
}
