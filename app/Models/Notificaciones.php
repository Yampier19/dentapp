<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
    use HasFactory;

    protected $table = 'dentapp_notificaciones';

    protected $primaryKey = 'id_notificaciones';

    protected $fillable = [
        'id_notificaciones',
        'nombre',
        'tipo',
        'descripcion',
        'FK_id_useradmin',
    ];

    protected $hidden = [
        'FK_id_useradmin',
    ];

    public function creador()
    {
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin');
    }

    public function admins()
    {
        return $this->hasMany('App\Models\AdminNotificacion', 'FK_id_notificaciones');
    }

    public function clientes()
    {
        return $this->hasMany('App\Models\ClienteNotificacion', 'FK_id_notificaciones');
    }
}
