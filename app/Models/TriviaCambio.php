<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TriviaCambio extends Model
{
    use HasFactory;

    protected $table = 'dentapp_trivia_cambios';

    protected $primaryKey = 'id_trivia_cambio';

    protected $fillable = [
        'id_trivia_cambio',
        'accion',
        'nombre',
        'descripcion',
        'tipo', //tipo de trivia
        'puntuacion',
        'estado',
        'fecha_inicio',
        'fecha_fin',
        'FK_id_useradmin',
        'FK_id_trivia',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin')->withTrashed();
    }

    public function capacitacion()
    {
        return $this->belongsTo('App\Models\Trivia', 'FK_id_trivia');
    }
}
