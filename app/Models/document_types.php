<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class document_types extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'is_active',
    ];

    /**
     * Obtiene a los PacUsuarios asociados al Tipo de Documento.
     *
     * @return HasMany
     */
    public function clientes(): HasMany
    {
        return $this->hasMany(clientes::class);
    }
}
