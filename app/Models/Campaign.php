<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use HasFactory;

    protected $table = 'dentapp_campaign';

    protected $primaryKey = 'id_campaign';

    protected $fillable = [
        'id_campaign',
        'titulo',
    ];
}
