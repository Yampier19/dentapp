<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoFamilia extends Model
{
    use HasFactory;

    protected $table = 'dentapp_producto_familia';

    protected $primaryKey = 'id_producto_familia';

    protected $fillable = [
        'id_producto_familia',
        'descripcion_familia',
        'tipo',
    ];

    public function productos()
    {
        return $this->hasMany('App\Models\Producto', 'FK_id_familia');
    }
}
