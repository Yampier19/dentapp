<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'dentapp_clientelogin';

    protected $primaryKey = 'id_clientelogin';

    protected $fillable = [
        'id_clientelogin',
        'email',
        'password',
        'estado',
        'fecha_cambio',
        'FK_id_cliente',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }
}
