<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

//use Illuminate\Database\Eloquent\Model;

class ClienteLogin extends Model
{
    use HasApiTokens, Notifiable;

    protected $table = 'dentapp_clientelogin';

    protected $primaryKey = 'id_clientelogin';

    protected $guarded = 'id_clientelogin';

    protected $fillable = [
        'id_clientelogin',
        'email',
        'password',
        'estado',
        'status',
        'ip',
        'segmento',
        'validacion',
        'cambiar',
        'fecha_cambio',
        'FK_id_cliente',
        'email_verified_at',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'FK_id_cliente')->withTrashed();
    }

    public function accesstokens()
    {
        return $this->hasMany('App\Models\OAuthToken', 'user_id');
    }
}
