<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteHistorial extends Model
{
    use HasFactory;

    protected $table = 'dentapp_historial_cliente';

    protected $primaryKey = 'id_historial_cliente';

    protected $fillable = [
        'id_historial_cliente',
        'accion',
        'fecha_registro',
        'FK_id_cliente',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }
}
