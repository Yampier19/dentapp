<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trivia extends Model
{
    use HasFactory;

    protected $table = 'dentapp_trivia';

    protected $primaryKey = 'id_trivia';

    protected $fillable = [
        'id_trivia',
        'nombre',
        'descripcion',
        'tipo', //tipo de trivia
        'puntuacion',
        'estado',
        'fecha_inicio',
        'fecha_fin',
        'FK_id_capacitacion',
    ];

    public function capacitacion()
    {
        return $this->belongsTo('App\Models\Capacitacion', 'FK_id_capacitacion');
    }

    public function cambios()
    {
        return $this->hasMany('App\Models\TriviaCambio', 'FK_id_trivia');
    }

    public function preguntas()
    {
        return $this->hasMany('App\Models\TriviaPregunta', 'FK_id_trivia');
    }

    public function tcliente()
    {
        return $this->hasMany('App\Models\TriviaCliente', 'FK_id_trivia');
    }
}
