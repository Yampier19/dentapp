<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'dentapp_cliente';

    protected $primaryKey = 'id_cliente';

    protected $fillable = [
        'id_cliente',
        'nombre',
        'apellido',
        'direccion',
        'distribuidor',
        'newdistribuidor',
        'last_change',
        'telefono',
        'foto',
        'correo',
        'total_estrellas',
        'especialidad',
        'loginrol_id',
        'total_reunido',
        'FK_id_level',
        'document_type_id',
        'document_number',
        'city_id',
        'foto',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'FK_id_level',
    ];

    public function login()
    {
        return $this->hasOne(ClienteLogin::class, 'FK_id_cliente');
    }

    public function rol()
    {
        return $this->belongsTo(Rol::class, 'loginrol_id', 'id_loginrol');
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function historialpass()
    {
        return $this->hasMany('App\Models\ClientePassword', 'FK_id_cliente')->orderBy('registro', 'desc');
    }

    public function notificaciones()
    {
        return $this->hasMany('App\Models\ClienteNotificacion', 'FK_id_cliente')->orderBy('created_at', 'desc')->with('notificacion');
    }

    public function unreadnotification()
    {
        return $this->hasMany('App\Models\ClienteNotificacion', 'FK_id_cliente')->whereNull('read_at')->orderBy('created_at', 'desc');
    }

    public function acciones()
    {
        return $this->hasMany('App\Models\ClienteHistorial', 'FK_id_cliente');
    }

    public function consultorio()
    {
        return $this->hasOne('App\Models\Consultorio', 'FK_id_cliente');
    }

    public function login_cambio()
    {
        return $this->hasMany('App\Models\UserHistory', 'FK_id_cliente');
    }

    public function trainings()
    {
        return $this->hasMany('App\Models\CapacitacionCliente', 'FK_id_cliente');
    }

    public function premios()
    {
        return $this->hasMany('App\Models\PremioCanjeado', 'FK_id_cliente');
    }

    public function level()
    {
        return $this->belongsTo('App\Models\Level', 'FK_id_level');
    }

    public function estrellas()
    {
        return $this->hasMany('App\Models\Estrella', 'FK_id_cliente');
    }

    public function trivias()
    {
        return $this->hasMany('App\Models\TriviaCliente', 'FK_id_cliente');
    }

    public function facturas()
    {
        return $this->hasMany('App\Models\Factura', 'FK_id_cliente');
    }

    public function canjeados()
    {
        return $this->hasMany('App\Models\PremioCanjeado', 'FK_id_cliente');
    }

    public function hallodent()
    {
        return $this->hasOne('App\Models\HalloDent', 'FK_id_cliente');
    }

    public function navident()
    {
        return $this->hasOne('App\Models\Navidad', 'FK_id_cliente');
    }

    public function documentType()
    {
        return $this->belongsTo(document_types::class);
    }

    public function userSeasons(){
        return $this->hasMany(UserSeason::class,'cliente_id');
    }
}
