<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoCambio extends Model
{
    use HasFactory;

    protected $table = 'dentapp_producto_gestion_cambio';

    protected $primaryKey = 'id_producto_gestion_cambio';

    protected $fillable = [
        'id_producto_gestion_cambio',
        'accion',
        'categoria',
        'subcategoria',
        'stock',
        'descripcion',
        'precio',
        'estado',
        'FK_id_producto_categoria',
        'FK_id_useradmin',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'FK_id_useradmin')->withTrashed();
    }

    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'FK_id_producto_categoria');
    }
}
