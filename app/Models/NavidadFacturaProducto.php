<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NavidadFacturaProducto extends Model
{
    use HasFactory;

    protected $table = 'dentapp_navidad_factura_producto';

    protected $primaryKey = 'id_navidad_factura_producto';

    protected $fillable = [
        'id_navidad_factura_producto',
        'producto',
        'cantidad',
        'FK_id_navidad_factura',
    ];

    public function factura()
    {
        return $this->belongsTo('App\Models\NavidadFactura', 'FK_id_navidad_factura');
    }
}
