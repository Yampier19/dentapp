<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Premio extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'dentapp_premios';

    protected $primaryKey = 'id_premio';

    protected $fillable = [
        'id_premio',
        'nombre',
        'marca',
        'descripcion',
        'entrega', //
        'tag',
        'stock',
        'limite_usuario',
        'foto',
        'numero_estrellas',
        'distribuidor',
        'brand_id',
        'producto_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function gestionCambio()
    {
        return $this->hasMany('App\Models\PremioCambio', 'FK_id_premios');
    }

    //Cuando me devuelve una colección por el level
    /* public function levels(){
        return $this->hasMany('App\Models\PremioLevel', 'FK_id_premio');
    } */

    //Cuando me devuelve una sola ya que actualmente graba una sola
    public function premiolevel()
    {
        return $this->hasOne('App\Models\PremioLevel', 'FK_id_premio')->orderBy('id_premio_level', 'desc');
    }

    public function levelpremio()
    {
        return $this->hasOne('App\Models\PremioLevel', 'FK_id_premio')
            ->orderBy('id_premio_level', 'desc')->with('levelapi');
    }

    public function canjeados()
    {
        return $this->hasMany('App\Models\PremioCanjeado', 'FK_id_premio');
    }
}
