<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class city extends Model
{
    use HasFactory;

    public function clients(): HasMany
    {
        return $this->hasMany(Cliente::class);
    }
}
