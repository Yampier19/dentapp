<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    use HasFactory;

    protected $table = 'dentapp_loginrol';

    protected $primaryKey = 'id_loginrol';

    protected $fillable = [
        'id_loginrol',
        'rol',
    ];

    public function admin()
    {
        return $this->hasMany(Rol::class);
    }

    public function cliente()
    {
        return $this->hasMany(cliente::class, 'id_loginrol');
    }

    public function level()
    {
        return $this->hasMany(Level::class, 'loginrol_id', 'loginrol_id');
    }
}
