<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nivel extends Model
{
    use HasFactory;

    protected $table = 'dentapp_nivel';

    protected $primaryKey = 'id_nivel';

    protected $fillable = [
        'id_nivel',
        'nombre',
        'foto',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function descripciones()
    {
        return $this->hasMany('App\Models\NivelDescripcion', 'FK_id_nivel');
    }

    //sin segmentación de level (me devuelve solo el objeto referencial)
    public function level()
    {
        return $this->hasOne('App\Models\Level', 'FK_id_nivel');
    }

    //con segmentacion de levels (me devolveria colección de los distintos levels)
    /* public function level(){
        return $this->hasMany('App\Models\Level', 'FK_id_nivel');
    } */
}
