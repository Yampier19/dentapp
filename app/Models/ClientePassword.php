<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientePassword extends Model
{
    use HasFactory;

    protected $table = 'dentapp_history_password';

    protected $primaryKey = 'id_history_password';

    protected $fillable = [
        'id_history_password',
        'password',
        'registro',
        'FK_id_cliente',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }
}
