<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TriviaPregunta extends Model
{
    use HasFactory;

    protected $table = 'dentapp_trivia_pregunta';

    protected $primaryKey = 'id_pregunta';

    protected $fillable = [
        'id_pregunta',
        'informacion',
        'tipo', //radio, text, date, number
        'puntuacion', //0 puntos en default si es trivia normal
        'estado',
        'FK_id_trivia',
    ];

    public function trivia()
    {
        return $this->belongsTo('App\Models\Trivia', 'FK_id_trivia');
    }

    public function respuestas()
    {
        return $this->hasMany('App\Models\TriviaRespuesta', 'FK_id_pregunta');
    }
}
