<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Distribuidor extends Model
{
    use HasFactory;

    protected $table = 'dentapp_distribuidores_3M';

    protected $primaryKey = 'id_distri';

    protected $fillable = [
        'id_distri',
        'distribuidor',
        'correo',
        'num_contacto',
        'identificacion',
    ];
}
