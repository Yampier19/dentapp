<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoPedido extends Model
{
    use HasFactory;

    protected $table = 'dentapp_producto_gestion';

    protected $primaryKey = 'id_producto_gestion';

    protected $fillable = [
        'id_producto_gestion',
        'cantidad',
        'precio_total',
        'FK_id_producto_categoria',
        'FK_id_cliente',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }

    public function producto()
    {
        return $this->belongsTo('App\Models\Producto', 'FK_id_producto_categoria');
    }
}
