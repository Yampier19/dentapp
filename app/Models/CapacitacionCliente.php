<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapacitacionCliente extends Model
{
    use HasFactory;

    protected $table = 'dentapp_capacitacion_cliente';

    protected $primaryKey = 'id_capacitacion_cliente';

    protected $fillable = [
        'id_capacitacion_cliente',
        'FK_id_cliente',
        'FK_id_capacitacion',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'FK_id_cliente')->withTrashed();
    }

    public function capacitacion()
    {
        return $this->belongsTo('App\Models\Capacitacion', 'FK_id_capacitacion');
    }
}
