<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class userPointSeason extends Model
{
    use HasFactory;

    protected $fillable = [
        'temporada_id',
        'cliente_id',
        'points',
    ];
}
