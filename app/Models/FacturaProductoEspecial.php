<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturaProductoEspecial extends Model
{
    use HasFactory;

    protected $table = 'dentapp_factura_producto_especial';

    protected $primaryKey = 'id_factura_producto';

    protected $fillable = [
        'id_factura_producto',
        'producto',
        'cantidad',
        'puntos',
        'FK_id_factura',
    ];

    public function factura()
    {
        return $this->belongsTo('App\Models\Factura', 'FK_id_factura');
    }
}
