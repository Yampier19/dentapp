<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoticiaTipo extends Model
{
    use HasFactory;

    protected $table = 'dentapp_noticias_tipo';

    protected $primaryKey = 'id_noticia_tipo';

    protected $fillable = [
        'id_noticia_tipo',
        'nombre',
    ];

    protected $hidden = [
        'id_noticia_tipo',
        'created_at',
        'updated_at',
    ];
}
