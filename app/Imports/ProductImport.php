<?php

namespace App\Imports;

use App\Models\Producto;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProductImport implements ToModel, WithValidation, WithHeadingRow
{
    use Importable;

    public function __construct($id_useradmin, $validaciones)
    {
        $this->id_useradmin = $id_useradmin;
        $this->validaciones = $validaciones;
    }

    public function model(array $row)
    {
        return Producto::create([
            'categoria' => $row['categoria'],
            'subcategoria' => $row['subcategoria'],
            'stock' => $row['stock'],
            'descripcion' => $row['descripcion'],
            'precio' => $row['precio'],
            'estado' => $row['estado'] == 'activo' ? 1 : 0,
            'numero_estrellas' => $row['numero_estrellas'],
        ]);
    }

    public function rules(): array
    {
        return $this->validaciones;
    }
}
