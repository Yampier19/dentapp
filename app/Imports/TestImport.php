<?php

namespace App\Imports;

use App\Models\Cliente;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
//MODELOS
use Maatwebsite\Excel\Concerns\WithValidation;

//MAILABLE

class TestImport implements ToCollection, WithValidation, WithHeadingRow
{
    use Importable;

    public function __construct($id_useradmin, $validaciones)
    {
        $this->id_useradmin = $id_useradmin;
        $this->validaciones = $validaciones;
    }

    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), $this->validaciones)->validate();

        foreach ($rows as $row) {
            return new Cliente([
                'nombre' => $row['nombre'],
                'apellido' => $row['apellido'],
                'telefono' => $row['telefono'],
                'correo' => $row['correo'],
                'total_estrellas' => 0,
                'especialidad' => $row['especialidad'],
            ]);
        }
    }

    /* public function model(array $row)
    {
        return new Cliente([
            'nombre' => $row['nombre'],
            'apellido' => $row['apellido'],
            'telefono' => $row['telefono'],
            'correo' => $row['correo'],
            'total_estrellas' => 0,
            'especialidad' => $row['especialidad'],
        ]);
    } */

    public function rules(): array
    {
        return $this->validaciones;
    }
}
