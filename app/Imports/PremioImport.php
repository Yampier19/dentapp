<?php

namespace App\Imports;

use App\Models\Premio;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class PremioImport implements ToModel, WithValidation, WithHeadingRow
{
    use Importable;

    public function __construct($id_useradmin, $validaciones)
    {
        $this->id_useradmin = $id_useradmin;
        $this->validaciones = $validaciones;
    }

    public function model(array $row)
    {
        //new Premio Premio::create
        return Premio::create([
            'nombre' => $row['nombre'],
            'marca' => $row['marca'],
            'descripcion' => $row['descripcion'],
            'entrega' => $row['entrega'],
            'tag' => $row['tag'],
            'stock' => $row['stock'],
            'limite_usuario' => $row['limite_usuario'],
            'numero_estrellas' => $row['numero_estrellas'],
        ]);
    }

    public function rules(): array
    {
        return $this->validaciones;
    }
}
