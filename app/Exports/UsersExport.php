<?php

namespace App\Exports;

use App\Models\Cliente;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function headings(): array
    {
        return [
            'Nombre',
            'Apellido',
            'Dirección',
            'Distribuidor',
            'Teléfono',
            'Correo',
            'Total estrellas',
            'Total reunido',
            'Tipo',
            'Especialidad',
            'Numero de documento',
        ];
    }

    public function collection()
    {
        return Cliente::select('nombre', 'apellido', 'direccion', 'distribuidor', 'telefono', 'correo',
            'total_estrellas', 'total_reunido', 'tipo', 'especialidad', 'document_number', 'created_at')->get();
    }
}
