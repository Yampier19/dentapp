<?php

namespace App\Helper;

use App\Models\Admin;
use App\Models\AdminNotificacion;
use App\Models\Cliente;
use App\Models\ClienteNotificacion;
use App\Models\Notificaciones;

//use Illuminate\Support\Facades\Notification;
//use App\Notifications\AdminNotifications;

class Notificacion
{
    public static function instance()
    {
        return new Notificacion();
    }

    ///////////////////////////////////////NOTIFICACIONES DEL ADMIN

    public function store($data, $modulo)
    {
        $admins = Admin::all();

        if (isset(auth()->user()->p_store)) {
            $data['descripcion'] .= ' | Admin: '.auth()->user()->nombre.' '.auth()->user()->apellido;
        }

        $notificacion = Notificaciones::create($data);

        $value['FK_id_notificaciones'] = $notificacion->id_notificaciones;
        foreach ($admins as $admin) {
            $value['FK_id_useradmin'] = $admin->id_useradmin;
            switch ($modulo) {
                case 'usuarios': $admin->FK_id_loginrol == 1 ? AdminNotificacion::create($value) : '';
                    break;
                case 'trivia': $admin->p_trivia ? AdminNotificacion::create($value) : '';
                    break;
                case 'noticia': $admin->p_noticias ? AdminNotificacion::create($value) : '';
                    break;
                case 'premio': $admin->p_premios ? AdminNotificacion::create($value) : '';
                    break;
                case 'producto': $admin->p_productos ? AdminNotificacion::create($value) : '';
                    break;
                case 'capacitacion': $admin->p_capacitaciones ? AdminNotificacion::create($value) : '';
                    break;
            }
            //AdminNotificacion::create($value);
        }
    }

    ///////////////////////////////////////NOTIFICACIONES DEL CLIENTE

    public function storeClient($data)
    {
        $clients = Cliente::all();
        $notificacion = Notificaciones::create($data);

        $value['FK_id_notificaciones'] = $notificacion->id_notificaciones;
        foreach ($clients as $client) {
            $value['FK_id_cliente'] = $client->id_cliente;
            ClienteNotificacion::create($value);
        }
    }

    public function storeUniqueClient($data)
    {
        $notificacion = Notificaciones::create($data);

        $value['FK_id_notificaciones'] = $notificacion->id_notificaciones;
        $value['FK_id_cliente'] = $data['cliente'];
        ClienteNotificacion::create($value);
    }
}
