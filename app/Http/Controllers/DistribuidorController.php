<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
//MODELOS
use App\Models\Distribuidor;
use App\Models\DistribuidorProducto;
use App\Models\Factura;
use App\Models\FacturaProductoEspecial;
use Illuminate\Support\Facades\DB;

class DistribuidorController extends Controller
{
    public function list_distribuidor()
    {
        $distribuidores = Distribuidor::all();

        return view('distribuidor.list_distribuidor', compact('distribuidores'));
    }

    public function list_product($id = null)
    {
        $distribuidor = null;
        if ($id) {
            $productos = DistribuidorProducto::where('id_distri3M', $id)->get();
            $distribuidor = Distribuidor::find($id);
        } else {
            $productos = DistribuidorProducto::all();
        }

        return view('distribuidor.list_product', compact('productos', 'distribuidor'));
    }

    public function list_client_distribuidor(Distribuidor $distribuidor)
    {
        $clientes = Cliente::select('distribuidor', 'nombre', 'correo', 'telefono')->where('distribuidor', $distribuidor->nombre_publico)->orderBy('distribuidor', 'desc')->get();

        return view('distribuidor.list_client', compact('clientes', 'distribuidor'));
    }

    public function list_facturados_distribuidor(Distribuidor $distribuidor)
    {
        $productos = null;
        $nombre = null;

        if ($distribuidor->id_distri) {
            $nombre = $distribuidor->nombre_publico;

            $productos = FacturaProductoEspecial::select(['*', DB::raw('COUNT(producto) as total')])
                ->whereRelation('factura', 'distribuidor', 'LIKE', "%$distribuidor->nombre_publico%")
                ->groupByRaw('DATE(created_at)')
                ->groupBy('producto')
                ->oldest()
                ->get();

            // $productos = Factura::selectRaw('dentapp_factura_producto_especial.producto as producto,
            // COUNT(dentapp_factura_producto_especial.producto) as total, monthname(dentapp_factura_producto_especial.created_at) as mes, year(dentapp_factura_producto_especial.created_at) year')
            //     ->join('dentapp_factura_producto_especial', 'dentapp_factura.id_factura', '=', 'dentapp_factura_producto_especial.FK_id_factura')
            //     ->where('distribuidor', 'like', '%' . $distribuidor->nombre_publico . '%')
            //     ->groupBy('year', 'mes', 'dentapp_factura_producto_especial.producto')
            //     ->orderBy('dentapp_factura_producto_especial.created_at', 'desc')->get();
        }

        $distribuidores = Distribuidor::all();

        return view('distribuidor.list_facturados', compact('distribuidores', 'productos', 'nombre'));
    }
}
