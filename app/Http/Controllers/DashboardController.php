<?php

namespace App\Http\Controllers;

use App\Helper\Notificacion;
use App\Http\Requests\profile\UpdateProfileRequest;
use App\Models\Admin;
use App\Models\Capacitacion;
use App\Models\Cliente;
use App\Models\Factura;
use App\Models\Noticia;
//Modelos
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
//Helper
use Intervention\Image\Facades\Image;

class DashboardController extends Controller
{
    public function index()
    {
        $clientes = Cliente::select('id_cliente', 'nombre', 'direccion', 'telefono')->take(2)->get();
        $facturas = Factura::select('id_factura', 'FK_id_cliente', 'distribuidor')->orderBy('created_at', 'desc')->take(2)->get();
        $trainings = Capacitacion::select('id_capacitacion', 'nombre', 'tipo', 'descripcion')->orderBy('created_at', 'desc')->take(2)->get();
        $noticias = Noticia::select('id_noticia', 'imagen', 'nombre')->orderBy('created_at', 'desc')->take(2)->get();

        return view('modules.dashboard.index', compact('clientes', 'facturas', 'trainings', 'noticias'));
    }

    //Recoger imagen del usuario
    public function getImage($filename = null)
    {
        if ($filename) {
            $exist = Storage::disk('public')->exists('uploads/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('usericon.jpg');
            } else {
                $file = Storage::disk('public')->get('uploads/'.$filename);
            }
        } else {
            $file = Storage::disk('public')->get('usericon.jpg');
        }

        return new Response($file, 200);
    }

    public function update(UpdateProfileRequest $request)
    {
        DB::transaction(function () use ($request) {
            $admin = Admin::findOrFail(auth()->user()->id_useradmin);
            $data = $request->all();
            $data['email'] = $admin->email;

            //Chequiamos si no estamos repitiendo el mismo password
            if (Hash::check($request->input('password'), $admin->password)) {
                return Redirect::back()->with('error', 'El nuevo password es igual al anterior');
            }

            //Chequiamos que no se asemeje al correo
            if ($request->input('password')) {
                if ($this->checkPassword($data)) {
                    return Redirect::back()->with('error', 'El password se asemeja al correo');
                }
            }

            if ($request->hasFile('foto')) {
                Storage::delete(['public/'.$admin->foto]);
                $data['foto'] = $request->file('foto')->store('uploads', 'public');
                $this->imagecompress($data['foto']);
            }

            if ($request->input('password')) {
                $data['password'] = Hash::make($request->input('password'));
            } else {
                $data['password'] = $admin->password;
            }

            $admin->update($data);

            $data = [
                'nombre' => 'Perfil de Admin',
                'tipo' => 'Update',
                'descripcion' => 'Edición de Perfil Admin: '.$admin->nombre.' '.$admin->apellido,
            ];

            Notificacion::instance()->store($data, 'usuarios');
        });

        return Redirect::back()->with('edit', 'El Perfil ha sido editado exitosamente');
    }

    public function perfil()
    {
        $admin = auth()->user();

        return view('modules.dashboard.perfil', compact('admin'));
    }

    //Compresor de Imagen
    public function imagecompress($image)
    {
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Chequiamos password si se asemeja al correo
    public function checkPassword($check)
    {
        //Chequiamos si es el correo y el password son iguales
        $check['email'] = explode(' ', strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['email'])));
        $check['password'] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['password']));

        if (stripos($check['password'], $check['email'][0]) !== false) {
            return true;
        } else {
            return false;
        }
    }
}
