<?php

namespace App\Http\Controllers;

use App\Events\PremioEvent;
use App\Helper\Notificacion;
use App\Imports\PremioImport;
use App\Mail\PremioBono;
use App\Mail\PremioEmail;
use App\Mail\PremioTecno;
//MODELOS
use App\Mail\PremioViaje;
use App\Models\ClienteHistorial;
use App\Models\Level;
use App\Models\Premio;
use App\Models\PremioCambio;
use App\Models\PremioCanjeado;
use App\Models\PremioCategoria;
//Helper o Usables
use App\Models\PremioLevel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
//MAILABLE
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class PremioController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:50',
        'marca' => 'required|string|max:255',
        'descripcion' => 'required|string|max:65535',
        'entrega' => 'required|string|max:255',
        'tag' => 'required|string|max:50',
        'stock' => 'required|numeric|min:0',
        'limite_usuario' => 'numeric|min:0',
        'foto' => 'file|mimes:jpg,jpeg,png|max:2100',
        'numero_estrellas' => 'numeric|min:0',
    ];

    public function index()
    {
        $premios = Premio::orderBy('created_at', 'desc')->take(3)->get();
        $canjeados = PremioCanjeado::orderBy('fecha_redencion', 'desc')->take(3)->get();

        //PARA CREAR PREMIO
        $categorias = PremioCategoria::all();

        return view('premio.index', compact('premios', 'canjeados', 'categorias'));
    }

    public function quantum()
    {
        //PARA CREAR PREMIO
        $categorias = PremioCategoria::all();

        return view('premio.quantum', compact('categorias'));
    }

    //Compresor de Imagen
    public function imagecompress($image)
    {
        $img = Image::make(storage_path('app/public/' . $image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Recoger imagen del premio
    public function getImage($filename = null)
    {
        if ($filename) {
            $exist = Storage::disk('public')->exists('premios/' . $filename);
            if (!$exist) { //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('gift.png');
            } else {
                $file = Storage::disk('public')->get('premios/' . $filename);
            }
        } else {
            $file = Storage::disk('public')->get('gift.png');
        }

        return new Response($file, 200);
    }

    //Listado de Premios
    public function listpremios()
    {
        $premios = Premio::orderBy('created_at', 'desc')->get();

        //PARA CREAR PREMIO
        $categorias = PremioCategoria::all();

        return view('premio.control', compact('premios', 'categorias'));
    }

    //SECCION DE LOS PREMIOS CANJEADOS
    //Listado de Premios Canjeados
    public function listcanjeados($estado = null)
    {
        if ($estado) {
            $premios = PremioCanjeado::where('estado', $estado)->orderBy('fecha_redencion', 'desc')->get();
        } else {
            $premios = PremioCanjeado::orderBy('fecha_redencion', 'desc')->get();
        }

        return view('canjeados.list', [
            'premios' => $premios,
            'estado' => $estado,
        ]);
    }

    //Tomar Datos de Entrega de Premio Canjeado
    public function entrega($id_premio)
    {
        $premio = PremioCanjeado::findOrFail($id_premio);

        return view('canjeados.entrega', compact('premio'));
    }

    //Update de los datos de un premio
    public function updateCanjeado(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'id_premio_canjeado' => 'required',
                'estado' => 'required|string|max:255',
                'referencia' => 'required|string|max:255',
                'cedula' => 'required|numeric|digits_between:4,20',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $canjeado = PremioCanjeado::findOrFail($request->input('id_premio_canjeado'));
            $canjeado->update($request->all());

            switch ($request->input('estado')) {
                case 'pendiente':
                    $mensaje = 'Tu canje ha sido recibido y esta en proceso de entrega';
                    break;
                case 'empacando':
                    $mensaje = 'Tu premio esta siendo empacado para su entrega';
                    break;
                case 'saliendo':
                    $mensaje = 'Tu premio esta en camino para su entrega';
                    break;
                case 'entregado':
                    $mensaje = 'Tu premio ha sido entregado';
                    break;
            }

            //Notificación para los clientes
            $dataClient = [
                'nombre' => '¡NUEVO STATUS ' . strtoupper($canjeado->nombre) . ' CANJEADO A ' . strtoupper($canjeado->estado) . '! 👨🎁',
                'tipo' => 'Premio',
                'descripcion' => $mensaje,
                'cliente' => $canjeado->FK_id_cliente,
            ];

            Notificacion::instance()->storeUniqueClient($dataClient);

            //Correo de Notificación al cliente tambien
            $information = new \stdClass();
            $information->asunto = 'Estatus de Premio Canjeado ' . $canjeado->nombre;
            $information->descripcion = $mensaje;
            $information->entrega = $canjeado->premio->entrega;
            $information->nombre = $canjeado->cliente->nombre . ' ' . $canjeado->cliente->apellido;
            $information->premio = $canjeado->nombre;
            $information->cantidad = $canjeado->cantidad;
            $information->total_estrellas = $canjeado->total_estrellas;
            $information->fecha_redencion = $canjeado->fecha_redencion;
            $information->referencia = $canjeado->referencia;

            try {
                Mail::to($canjeado->cliente->correo)->queue(new PremioEmail($information));
            } catch (\Exception $e) {
            }

            DB::commit();

            return Redirect::back()->with('edit', 'Premio Canjeado Actualizado');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El premio canjeado no pudo ser Editada');
        }
    }
    //FIN SECCION DE LOS PREMIOS CANJEADOS

    //Listado de Premios Cambiados (trazabilidad)
    public function listpremioscambios()
    {
        $cambios = PremioCambio::orderBy('created_at', 'asc')->get();

        //Vista de la lista de premios cambios
        //DEBERIA ENVIAR UNA VISTA DONDE SE VEA LA TRAZABILIDAD
        //return view('premio.index',compact('cambios'));
    }

    //Enviar Vista para el registro de un premio
    public function create()
    {
        $premios = Premio::orderBy('created_at', 'desc')->get();
        //PARA CREAR PREMIO
        $categorias = PremioCategoria::all();

        return view('premio.control', compact('premios', 'categorias'));
    }

    //Guardado de un Premio
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate)->withInput();
            }

            if ($request->has('distribuidor')) {
                $validate = Validator::make($request->all(), [
                    //'imagen_quantum' => 'required|string|max:255',
                    'distribuidor' => 'required|string|max:255',
                    'brand_id' => 'required|numeric',
                    'producto_id' => 'required|numeric',
                ], $this->mensajes_error);

                if ($validate->fails()) {
                    return Redirect::back()->withErrors($validate)->withInput();
                }
            }

            $data = $request->all();

            /* if($request->has('distribuidor')){
                $data["foto"] = $request->imagen_quantum;
            } */

            if ($request->hasFile('foto')) {
                $data['foto'] = $request->file('foto')->store('premios', 'public');
                $this->imagecompress($data['foto']);
            }

            $premio = Premio::create($data);
            PremioCategoria::firstOrCreate(['nombre' => $data['tag']]);

            $data['accion'] = 'Inserción';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_premios'] = $premio->id_premio;

            PremioCambio::create($data);

            $data = [
                'nombre' => 'Store de Premio',
                'tipo' => 'Insert',
                'descripcion' => 'Creación de Premio: ' . $premio->nombre,
            ];

            Notificacion::instance()->store($data, 'premio');

            DB::commit();

            return redirect()->route('premio.index')->with('create', 'Premio Registrado');
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    //Vista para confirmar si de verdad quiere eliminar el premio
    public function confirm($id_premio)
    {
        $premio = Premio::findOrFail($id_premio);

        return view('premio.confirm', compact('premio'));
    }

    //Eliminar un Premio
    public function destroy($id_premio)
    {
        DB::beginTransaction();
        try {
            $premio = Premio::find($id_premio);

            if (!$premio) {
                return redirect()->route('premio.index')->with('edit', 'El Premio no fue encontrado');
            }

            foreach ($premio->gestionCambio as $cambios) {
                $cambios->FK_id_premios = null;
                $cambios->update();
            }

            PremioLevel::where('FK_id_premio', $id_premio)->delete();

            if ($premio->foto) {
                Storage::delete(['public/' . $premio->foto]);
            }

            $premio->delete();

            $data = $premio->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;

            PremioCambio::create($data);

            $data = [
                'nombre' => 'Delete de Premio',
                'tipo' => 'Delete',
                'descripcion' => 'Eliminación de Premio: ' . $premio->nombre,
            ];

            Notificacion::instance()->store($data, 'premio');

            DB::commit();

            // return redirect()->route('premio.control')->with('delete', 'Premio Eliminado');
            return response()->json([
                'message' => 'Premio eliminado con exito',
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    //Enviar la Vista para editar un Premio
    public function edit($id_premio)
    {
        $premio = Premio::findOrFail($id_premio);
        $level = Level::all();

        //PARA EDITAR PREMIO
        $categorias = PremioCategoria::all();

        //Vista para la edición de datos
        return view('premio.edit', compact('premio', 'level', 'categorias'));
    }

    //Update de los datos de un premio
    public function update(Request $request, $id_premio)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();

            if ($request->has('distribuidor')) {
                if ($request->distribuidor != 'N/A') {
                    $validate = Validator::make($request->all(), [
                        //'imagen_quantum' => 'required|string|max:255',
                        'distribuidor' => 'required|string|max:255',
                        'brand_id' => 'required|numeric',
                        'producto_id' => 'required|numeric',
                    ], $this->mensajes_error);

                    if ($validate->fails()) {
                        return Redirect::back()->withErrors($validate)->withInput();
                    }
                } else {
                    $data['distribuidor'] = 'N/A';
                    $data['producto_id'] = null;
                    $data['brand_id'] = null;
                }
            }

            $premio = Premio::findOrFail($id_premio);

            /* if($request->has('distribuidor')){
                if($data["distribuidor"] != "N/A"){
                    Storage::delete(['public/'.$premio->foto]);
                    $data["foto"] = $request->imagen_quantum;
                }
                else{
                    if($request->hasFile('foto')){
                        Storage::delete(['public/'.$premio->foto]);
                        $data["foto"] = $request->file('foto')->store('premios', 'public');
                        $this->imagecompress($data["foto"]);
                    }
                }
            } */

            if ($request->hasFile('foto')) {
                Storage::delete(['public/' . $premio->foto]);
                $data['foto'] = $request->file('foto')->store('premios', 'public');
                $this->imagecompress($data['foto']);
            }

            $premio->update($data);
            PremioCategoria::firstOrCreate(['nombre' => $data['tag']]);

            $data['accion'] = 'Update';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_premios'] = $id_premio;

            PremioCambio::create($data);

            if ($request->input('level')) {
                $premiolevel = new PremioLevel();
                $premiolevel->FK_id_premio = $id_premio;
                $premiolevel->FK_id_level = $request->input('level');
                $premiolevel->save();
            }

            $data = [
                'nombre' => 'Update de Premio',
                'tipo' => 'Update',
                'descripcion' => 'Edición de Premio: ' . $premio->nombre,
            ];

            Notificacion::instance()->store($data, 'premio');

            DB::commit();

            return Redirect::back()->with('edit', 'Premio Actualizado');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El Premio no pudo ser Editado');
        }
    }

    //Vista que explica para hacer el import de CSV
    public function import_csv()
    {
        return view('product.control');
    }

    //Metodo que genera el import de premios
    public function import(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx,csv',
        ], [
            'required' => 'El dato es requerido',
            'file' => 'El dato debe llegar como un archivo',
            'mime' => 'El archivo debe llegar en formato xls, xlsx, csv',
        ]);

        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate);
        }

        try {
            Excel::import(new PremioImport(auth()->user()->id_useradmin, $this->validaciones), $request->file);

            //Vista de que el import fue correcto
            return redirect()->route('premio.index')->with('create', 'Premios Correctamente Importados');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errores = $e->failures();
            $premios = Premio::orderBy('nombre', 'asc')->get();

            return view('premio.control', compact('errores', 'premios'));
        }
    }

    ////////////////////////////////////////////////API CLIENTE//////////////////

    //Recoger imagen del premio
    public function ApigetImage($filename = null)
    {

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('premios/' . $filename);
            if (!$exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/gift.png'));
            } else {
                return response()->file(storage_path('app/public/premios/' . $filename));
            }
        } else {
            return response()->file(storage_path('app/public/gift.png'));
        }
    }

    //Ver los premios canjeados
    public function ApiCanjeados()
    {
        $cliente = auth()->user()->cliente;

        return response()->json([
            'status' => 'success',
            'message' => 'Premios canjeados',
            'total_premios' => count($cliente->canjeados),
            'premios' => $cliente->canjeados,
        ], 200);
    }

    //Ver un premio canjeado detallado
    public function ApiCanje($id_premio)
    {
        $premio = PremioCanjeado::where('id_premio_canjeado', $id_premio)->with('premio')->first();

        if (!$premio) {
            return response()->json([
                'status' => 'error',
                'message' => 'El premio solicitado no se encuentra registrado',
            ], 200);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Premio canjeado',
            'premio' => $premio,
        ], 200);
    }

    //Ver las Categorias disponibles de premios
    public function ApigetCategories()
    {
        //$categorias = PremioCategoria::all();
        $categorias = Premio::selectRaw('COUNT(*) as premios ,tag as nombres')->groupBy('tag')->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Categorias disponibles',
            'categorias' => $categorias,
        ], 200);
    }

    //Ver los nuevos premios con una vigencia de creado hace menos de 2 semanas
    public function ApiNew($registros = 6)
    {
        $premios = Premio::where('created_at', '>=', date('Y-m-d', strtotime('-2 week')))->orderBy('nombre', 'desc')->with('levelpremio')->paginate($registros);

        $paginacion = [
            'total' => $premios->total(),
            'count' => $premios->count(),
            'per_page' => $premios->perPage(),
            'current_page' => $premios->currentPage(),
            'total_pages' => $premios->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Premios disponibles',
            'total_premios' => $premios->total(),
            'premios' => $premios->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Ver los premios disponibles
    public function ApiList($registros = 6)
    {
        $premios = Premio::orderBy('nombre', 'desc')->with('levelpremio')->paginate($registros);

        $paginacion = [
            'total' => $premios->total(),
            'count' => $premios->count(),
            'per_page' => $premios->perPage(),
            'current_page' => $premios->currentPage(),
            'total_pages' => $premios->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Premios disponibles',
            'total_premios' => $premios->total(),
            'premios' => $premios->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Ver los premios por categoria seleccionada
    public function ApiListCategory($categoria, $registros = 6)
    {
        $premios = Premio::where('tag', $categoria)->orderBy('nombre', 'desc')
            ->with('levelpremio')->paginate($registros);

        $paginacion = [
            'total' => $premios->total(),
            'count' => $premios->count(),
            'per_page' => $premios->perPage(),
            'current_page' => $premios->currentPage(),
            'total_pages' => $premios->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Premios disponibles',
            'total_premios' => $premios->total(),
            'premios' => $premios->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Devolver un premio seleccionado
    public function ApiPremio($id_premio)
    {
        DB::beginTransaction();
        try {
            $premio = Premio::where('id_premio', $id_premio)->with('levelpremio')->first();

            if (!$premio) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'El premio solicitado no se encuentra registrado',
                ], 200);
            }

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Visualizo premio (' . $premio->marca . ') ' . $premio->nombre;
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Premio encontrado',
                'premio' => $premio,
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al localizar el premio solicitado en servidor',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Canjear premio
    public function canjear(Request $request, $id_premio)
    {
        //Me deben viajar los siguientes datos por POST
        //'cantidad', //se envia al canjear
        //'receptor', //se envia al canjear
        //'telefono', //se envia al canjear
        //'direccion', //se envia al canjear
        //'adicional', //se envia al canjear

        //Por URL es decir como GET la llave del premio (su ID) FK_id_premio

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'cantidad' => 'required|numeric|min:0',
                'receptor' => 'required|string|max:50',
                'telefono' => 'required|string|max:255',
                'cedula' => 'numeric|digits_between:4,20',
                'direccion' => 'string|max:255',
                'adicional' => 'string|max:255',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $premio = Premio::find($id_premio);

            if (!$premio) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'El premio a canjear no se encuentra registrado',
                ], 200);
            }

            $data = $request->all();

            if ($data['cantidad'] > $premio->stock) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'La cantidad solicitada supera al stock actual del premio',
                ], 200);
            }

            $limituser = PremioCanjeado::where('FK_id_premio', $id_premio)->get();

            if ($limituser->count() + $data['cantidad'] > $premio->limite_usuario) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'El cliente supera el limite de redención del premio',
                ], 200);
            }

            if (auth()->user()->cliente->total_estrellas < $premio->numero_estrellas * $data['cantidad']) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'No posees la cantidad de puntos suficientes para redimir el premio',
                ], 200);
            }

            //YA AQUI TODO PASO CORRECTO Y AHORA SI GRABAMOS LA REDENCIÓN Y ACTUALIZAMOS LOS PUNTOS DEL CLIENTE
            $data['nombre'] = $premio->nombre;
            $data['marca'] = $premio->marca;
            $data['total_estrellas'] = $premio->numero_estrellas * $data['cantidad'];
            $data['fecha_redencion'] = date('Y-m-d');
            $data['FK_id_premio'] = $id_premio;
            $data['FK_id_cliente'] = auth()->user()->FK_id_cliente;
            $canjeado = PremioCanjeado::create($data);

            //Actualizamos el stock
            $premio->stock -= $data['cantidad'];
            $premio->update();

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Registro de Premio Canjeado: ' . $data['nombre'];
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            //Modificamos las estrellas del cliente
            $cliente = auth()->user()->cliente;
            $cliente->total_estrellas -= $data['total_estrellas'] * $data['cantidad'];
            $cliente->update();

            //NOTIFICACIÓN DEL PREMIO POR CORREO
            try {
                $information = new \stdClass();
                $information->asunto = 'Felicitaciones por Canjear tu Premio en DentApp';
                $information->nombre = $canjeado->nombre;
                $information->puntos = $canjeado->total_estrellas;
                $information->cantidad = $canjeado->cantidad;
                $information->direccion = $canjeado->direccion;

                if (strtolower($premio->tag) == 'viajes') {
                    Mail::to($cliente->correo)->queue(new PremioViaje($information));
                } elseif (strtolower($premio->tag) == 'tecnología') {
                    Mail::to($cliente->correo)->queue(new PremioTecno($information));
                } elseif (strtolower($premio->tag) == 'bonos digitales') {
                    Mail::to($cliente->correo)->queue(new PremioBono($information));
                }
            } catch (\Exception $e) {
            }

            //Notificación de canjeo
            $title = '¡NUEVO PREMIO CANJEADO!';
            $text = 'Tu Premio ' . $canjeado->nombre . ' ha sido redimido exitosamente';
            event(new PremioEvent($title, $text, $canjeado->id_premio_canjeado));

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Premio Canjeado Exitosamente',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al almacenar redención de premio',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }
}
