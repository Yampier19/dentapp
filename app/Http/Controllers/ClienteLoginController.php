<?php

namespace App\Http\Controllers;

use App\Mail\Welcome;
use App\Models\Cliente;
use App\Models\ClienteHistorial;
use App\Models\ClienteLogin;
use App\Models\ClientePassword;
use App\Models\Doctor;
use App\Models\Level;
//MODELOS
use App\Models\Navidad;
use App\Models\Nivel;
use App\Models\UserHistory;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
//USABLE
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
//MAILABLE
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ClienteLoginController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'descripcion.max' => 'El dato no debe ser mayor a 255 carácteres',
        'password.min' => 'El dato debe ser mayor a 8 carácteres',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
        'telefono.max' => 'El telefono no puede ser mayor a 15 digitos',
        'regex' => 'La contraseña debe tener mínimo una minuscula, mayuscula, número y caracter especial (@$!%*#?&+-)',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:50',
        'document_type_id' => 'required|exists:document_types,id',
        'document_number' => 'required|alpha_num|unique:dentapp_cliente,document_number',
        'apellido' => 'required|string|max:50',
        'telefono' => 'required|string|max:15|unique:dentapp_cliente,telefono',
        'correo' => 'required|string|email|max:50|unique:dentapp_clientelogin,email',
        'especialidad' => 'required|string|max:50',
        'distribuidor' => 'required|string|max:255',
        'foto' => 'required|file|mimes:jpg,jpeg,png,pdf|max:2100',
    ];


    public function InfoUser(){
        $cliente = auth()->user()->cliente;

        return response()->json([
            'message' => 'Usuario logueado',
            'code' => 200,
            'data' => $cliente,
        ]);
    }

    public function register(Request $request)
    {
        $validaciones = $this->validaciones;
        $mensajes_error = $this->mensajes_error;

        if ($request->has('foto')) {
            if ($request->file('foto')->extension() == 'pdf') {
                $validaciones['foto'] = 'file|mimes:jpg,jpeg,png,pdf|max:'.(1024 * 5);
                $mensajes_error['foto.max'] = 'El pdf debe ser menor a 5 Mb';
            }
        } else {
            $validaciones['foto'] = 'file|mimes:jpg,jpeg,png,pdf';
        }

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $validaciones, $mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            // $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            // $password = substr(str_shuffle($caracteres_permitidos), 0, 8);
            $password = $request->password;

            //SET DE LOS DATOS
            $data = $request->all();
            $data['password'] = $password;
            $data['estado'] = 'activo';
            $data['FK_id_level'] = 1;
            $data['validacion'] = null;
            $data['segmento'] = 'OTRO';
            $data['cambiar'] = 0;
            $data['last_change'] = date('Y-m-d');
            $data['total_estrellas'] = 0;

            if ($request->hasFile('foto')) {
                $data['foto'] = null;
                $data['validacion'] = $request->file('foto')->store('facturas', 'public');

                if ($request->file('foto')->extension() != 'pdf') {
                    $this->imagecompress($data['validacion']);
                }
            }

            if ($this->checkPassword($data)) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'El password se asemeja al correo',
                ], 500);
            }

            $cliente = Cliente::create($data);

            if ($request->nombre_doctor) {
                $client = Cliente::select('id_cliente')->latest()->first();
                $doctor = new Doctor();
                $doctor->name = $data['nombre_doctor'];
                $doctor->distribuidor_id = $data['distribuidor'];
                $doctor->speciality_id = $data['especialidad'];
                $doctor->cliente_id = $client->id_cliente;
                $doctor->save();
            }

            //Grabamos el historial de passwords
            $history = new ClientePassword();
            $history->password = Hash::make($data['password']);
            $history->registro = date('Y-m-d');
            $history->FK_id_cliente = $cliente->id_cliente;
            $history->save();

            //Grabamos el login del cliente
            $login = new ClienteLogin();
            $login->email = $data['correo'];
            $login->password = Hash::make($data['password']);
            $login->estado = $data['estado'];
            $login->status = null;
            $login->segmento = $data['segmento'];
            $login->validacion = $data['validacion'];
            $login->fecha_cambio = date('Y-m-d');
            $login->FK_id_cliente = $cliente->id_cliente;
            $login->save();

            //Grabamos el historial del usuario login store
            $loghistory = new UserHistory();
            $loghistory->accion = 'Store de Usuario Cliente Login';
            $loghistory->email = $data['correo'];
            $loghistory->password = $data['password'];
            $loghistory->FK_id_cliente = $cliente->id_cliente;
            $loghistory->save();

            //Evento Navidad 2021
            $fecha_actual = strtotime(date('Y-m-d H:i:00', time()));
            $inicio = strtotime('2021-11-16');
            $fin = strtotime('2021-12-31');

            if ($fecha_actual >= $inicio && $fecha_actual <= $fin) {
                $navidad = new Navidad();
                $navidad->tipo = 'nuevo';
                $navidad->nombre = $cliente->nombre;
                $navidad->apellido = $cliente->apellido;
                $navidad->correo = $data['correo'];
                $navidad->fecha_participante = date('Y-m-d H:i:s');
                $navidad->tickets = 0;
                $navidad->tickets_usados = 0;
                $navidad->FK_id_cliente = $cliente->id_cliente;
                $navidad->save();
            }
            //Fin evento Navidad

            //Enviamos el correo de bienvenida
            $information = new \stdClass();
            $information->asunto = 'Bienvenido a DentApp';
            $information->nombre = $cliente->nombre.' '.$cliente->apellido;
            $information->correo = $cliente->correo;
            $information->contraseña = $password;
            $information->usuario = $cliente->id_cliente;

            try {
                Mail::to($cliente->correo)->queue(new Welcome($information));
                // Mail::to($cliente->correo)->later(now()->addMinutes(10), new Welcome($information));
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Porfavor digite un correo existente para la creación de su Usuario',
                ], 400);
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Usuario registrado satisfactoriamente, debes esperar 72 horas a que validen tu proceso de registro',
                'login' => $login,
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al Registrar Usuario',
                'errores' => $e->getMessage().' | '.$e->getLine(),
            ], 500);
        }
    }

    public function emailVerified(ClienteLogin $clienteLogin)
    {
        $clienteLogin->update([
            'email_verified_at' => now(),
        ]);

        return view('mails.verify');
    }

    public function login(Request $request)
    {
        $login = $this->validate($request, [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string',
        ], $this->mensajes_error);

        if (! Auth::attempt($login)) {
            return response()->json(['message' => 'Las credenciales del login no coinciden.'], 200);
        }

        $user = $request->user();

        if ($user->estado == 'inactivo' || $user->estado == 'pendiente') {
            return response()->json([
                'status' => 'error',
                'message' => 'El usuario no se encuentra activo',
            ], 200);
        }

        //Diferencia entre la fecha actual y la ultima vez que se edito la contraseña
        $fechainicial = new DateTime($user->fecha_cambio);

        $fechafinal = new DateTime('now');

        $diferencia = $fechainicial->diff($fechafinal);

        $meses = ($diferencia->y * 12) + $diferencia->m;

        //Si pasaron 6 meses de la contraseña debe ser cambiada

        //Si derrepente cambio la contraseña por metodo de olvido contraseña más no anexo distribuidor
        if (! $user->cliente->distribuidor) {
            return response()->json([
                'status' => 'error',
                'distribuidor' => false,
                'message' => 'El usuario debe añadir su distribuidor',
            ], 200);
        }

        //Revisión matutina de solo ver si tiene un distribuidor pendiente por almacenar post update
        if ($user->cliente->newdistribuidor) {
            //fecha del cambio
            $time = strtotime($user->cliente->last_change);
            $fecha = date('Y-m', $time);

            $firstDate = new DateTime($fecha);
            $secondDate = new DateTime(date('Y-m'));
            $diferencia = $firstDate->diff($secondDate);

            //echo $fecha_a." | ".$fecha_o; return;
            $meses = ($diferencia->y * 12) + $diferencia->m;

            //CALCULAR SI YA EMPEZO EL NUEVO MES
            if ($meses > 0) {
                $user->cliente->distribuidor = $user->cliente->newdistribuidor;
                $user->cliente->newdistribuidor = null;
                $user->cliente->update();
            }
        }

        $aToken = $user->createToken('access_token');

        return response()->json([
            'status' => 'success',
            'message' => 'Inicio de Sesión de forma correcta',
            'meses' => 'Días restantes para cambiar contraseña: '.(6 - $meses) * 30,
            'cliente' => $user,
            'accessToken' => $aToken->accessToken,
        ], 200);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'status' => 'success',
            'message' => 'Se cerro la sesión de forma correcta',
        ], 200);
    }

    public function autenthicated(Request $request)
    {
        $cliente = $request->user()->with('cliente')->first();

        $firstDate = new DateTime($cliente['last_change']);
        $secondDate = new DateTime(date('Y-m-d'));
        $diferencia = $firstDate->diff($secondDate);

        $cliente->cambiar_distribuidor = false;
        if ($diferencia->days > 60) {
            $cliente->cambiar_distribuidor = true;
        }

        return response()->json($cliente);
    }

    //Recoger datos del cliente
    public function usuario(Request $request)
    {
        return response()->json($request->user()->cliente);
    }

    //Recoger Perfil del cliente
    public function perfil()
    {
        $cliente = auth()->user()->cliente;
        $cliente->consultorio ? '' : $cliente->consultorio = null;

        $firstDate = new DateTime($cliente['last_change']);
        $secondDate = new DateTime(date('Y-m-d'));
        $diferencia = $firstDate->diff($secondDate);

        $cliente->cambiar_distribuidor = false;
        if ($diferencia->days > 60) {
            $cliente->cambiar_distribuidor = true;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Perfil del Usuario',
            'datos' => $cliente,
            'password' => auth()->user()->password,
            'email_verified_at' => auth()->user()->email_verified_at,
            'cambiar' => auth()->user()->cambiar,
        ], 200);
    }

    //Recoger puntos del cliente
    public function clientpoints(Request $request)
    {
        return response()->json($request->user()->cliente->total_estrellas);
    }

    //Recoger nivel del cliente
    public function clientlevel()
    {
        $niveles = Level::orderBy('id_level', 'desc')->get();
        $actual = null;
        $proximo = null;
        $faltante = null;

        foreach ($niveles as $nivel) {
            if (auth()->user()->cliente->total_reunido >= $nivel->rango_inicio) {
                $actual = $nivel;
                break;
            }
            $proximo = $nivel;
            $faltante = $nivel->rango_inicio - auth()->user()->cliente->total_reunido;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Nivel del Cliente',
            'dinero' => auth()->user()->cliente->total_reunido,
            'faltante' => $faltante,
            'proximo_nivel' => $proximo ? $proximo->nombre : $proximo,
            'nivel' => $actual,
        ], 200);
    }

    //Recoger avatar del cliente
    public function useravatar($filename = null)
    {

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('uploads/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/usericon.jpg'));
            } else {
                return response()->file(storage_path('app/public/uploads/'.$filename));
            }
        } else {
            return response()->file(storage_path('app/public/usericon.jpg'));
        }
    }

    //Chequiamos password si se asemeja al correo
    public function checkPassword($check)
    {
        //Chequiamos si es el correo y el password son iguales
        $check['correo'] = explode(' ', strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['correo'])));
        $check['password'] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['password']));

        if (stripos($check['password'], $check['correo'][0]) !== false) {
            return true;
        } else {
            return false;
        }
    }

    //Cambiar contraseña LOGUEADO
    public function updatePassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'password' => [
                    'string', 'min:8', 'max:50', 'required',
                ],
                'confirmation' => 'string|same:password|required',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $cliente = auth()->user()->cliente;
            $data = $request->all();
            $data['correo'] = $cliente->correo;

            if ($request->input('password')) {
                //Chequiamos si no estamos repitiendo el mismo password
                if (Hash::check($request->input('password'), $cliente->login->password)) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'El nuevo password es igual al anterior',
                    ], 500);
                }

                //Chequiamos que no se asemeje al correo
                if ($this->checkPassword($data)) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'El password se asemeja al correo',
                    ], 500);
                }

                //Revisamos si el password no es el mismo de los ultimos 3 utilizados
                foreach ($cliente->historialpass->take(3) as $pass) {
                    if (Hash::check($request->input('password'), $pass->password)) {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'La contraseña a editar ya fue utilizada en las ultimas 3 contraseñas utilizadas',
                        ], 500);
                    }
                }

                //Grabamos el historial de passwords
                $history = new ClientePassword();
                $history->password = Hash::make($request->input('password'));
                $history->registro = date('Y-m-d');
                $history->FK_id_cliente = $cliente->id_cliente;
                $history->save();
            }

            //Update de los datos de login
            $login = $cliente->login;
            $login->password = Hash::make($request->input('password'));
            $login->cambiar = 0;
            $login->fecha_cambio = date('Y-m-d');
            $login->update();

            //Grabamos el historial del usuario login update
            if ($request->input('password')) {
                $loghistory = new UserHistory();
                $loghistory->accion = 'Update de Usuario Cliente Login';
                $loghistory->email = $data['correo'];
                $loghistory->password = $request->input('password');
                $loghistory->FK_id_cliente = $cliente->id_cliente;
                $loghistory->save();
            }

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Cambio de Contraseña';
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Contraseña Actualizada',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al editar Contraseña',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Cambiar contraseña DESLOGUEADO CUANDO TE DIGA QUE DEBES CAMBIAR CONTRA
    public function changePassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email|max:255',
                'password' => [
                    'string',
                    'min:8',
                    'max:50',
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    //'regex:/[@$!%*#?&+-]/',
                    'required',
                ],
                'confirmation' => 'string|same:password|required',
                'distribuidor' => 'nullable|string|max:255',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $login = ClienteLogin::where('email', $request->email)->first();

            if (! $login) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Las credenciales de correo no se encuentra registrado en el sistema.',
                ], 200);
            }

            $data = $request->all();
            $data['correo'] = $login->email;

            if ($request->input('password')) {
                //Chequiamos si no estamos repitiendo el mismo password
                if (Hash::check($request->input('password'), $login->password)) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'El nuevo password es igual al anterior',
                    ], 500);
                }

                //Chequiamos que no se asemeje al correo
                if ($this->checkPassword($data)) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'El password se asemeja al correo',
                    ], 500);
                }

                //Revisamos si el password no es el mismo de los ultimos 3 utilizados
                foreach ($login->cliente->historialpass->take(3) as $pass) {
                    if (Hash::check($request->input('password'), $pass->password)) {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'La contraseña a editar ya fue utilizada en las ultimas 3 contraseñas utilizadas',
                        ], 500);
                    }
                }

                //Grabamos el historial de passwords
                $history = new ClientePassword();
                $history->password = Hash::make($request->input('password'));
                $history->registro = date('Y-m-d');
                $history->FK_id_cliente = $login->cliente->id_cliente;
                $history->save();
            }

            //Update de distribuidor si hace falta
            if (! $login->cliente->distribuidor) {
                if ($request->input('distribuidor')) {
                    $cliente = $login->cliente;
                    $cliente->distribuidor = $request->distribuidor;
                    $cliente->last_change = date('Y-m-d');
                    $cliente->update();
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'El dato de distribuidor debe ser enviado',
                    ], 500);
                }
            }

            //Update de los datos de login
            $login->password = Hash::make($request->input('password'));
            $login->cambiar = 0;
            $login->fecha_cambio = date('Y-m-d');
            $login->update();

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Cambio de Contraseña';
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = $login->FK_id_cliente;
            $historial->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Cambios realizados exitosamente',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al hacer los cambios',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Registrar el distribuidor si hizo el cambio de password por correo en su primera vez
    public function updateDist(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'distribuidor' => 'required|string|max:255',
                'email' => 'required',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $login = ClienteLogin::where('email', $request->email)->first();

            if (! $login) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Las credenciales de correo no se encuentra registrado en el sistema.',
                ], 200);
            }

            $cliente = $login->cliente;
            $cliente->distribuidor = $request->distribuidor;
            $cliente->last_change = date('Y-m-d');
            $cliente->update();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Cambios realizados exitosamente',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al hacer los cambios',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Cambiar avatar del usuario
    public function changeAvatar(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'foto' => 'required|file|mimes:jpg,jpeg,png|max:2100',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $cliente = auth()->user()->cliente;
            $data = $request->all();

            if ($request->hasFile('foto')) {
                Storage::delete(['public/'.$cliente->foto]);
                $data['foto'] = $request->file('foto')->store('uploads', 'public');

                $this->imagecompress($data['foto']);
            }

            $cliente->update($data);

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Edición de Imagen de Perfil';
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Imagen de Perfil Actualizada',
                'foto' => $cliente->foto,
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al editar Avatar',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Update user IP cuando entre a home
    public function storeIp(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'ip' => 'required|string',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $cliente = auth()->user()->cliente->login;
            $cliente->update($request->all());

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Ip almacenada de forma exitosa',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al almacenar Ip',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Compresor de Imagen
    public function imagecompress($image)
    {
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }
}
