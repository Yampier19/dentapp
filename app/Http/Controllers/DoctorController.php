<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::all();

        return response()->json([
            'code' => '200',
            'message' => 'lista de doctores',
            'data' => $doctors,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $doctors = Doctor::create($request->all());

                return response()->json([
                    'message' => 'Doctor creado correctamente',
                    'code' => '200',
                    'data' => $doctors,
                ]);
            });
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'no se pudo crear el doctor',
                'code' => '304',
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor)
    {
        try {
            $doctor->fill($request->all())->save();

            return response()->json([
                'code' => '200',
                'message' => 'doctor editado',
                'data' => $doctor,
            ]);
        } catch (\Throwable $e) {
            return response()->json([
                'code' => '304',
                'message' => 'Error al editar el doctor',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        try {
            $doctor->delete();

            return response()->json([
                'message' => 'doctor eliminado',
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
