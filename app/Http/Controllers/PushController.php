<?php

namespace App\Http\Controllers;

use App\Events\BNotificacion;
use App\Events\PuntosEvent;
use App\Models\Cliente;
use Illuminate\Http\Request;
//Modelos
use Illuminate\Support\Facades\DB;
//Helper o Usables
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class PushController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'string' => 'El dato debe ser enviado en formato texto',
    ];

    //vista inicial de enviar PUSH
    public function index()
    {
        $usuarios = Cliente::all();

        return view('push.index', compact('usuarios'));
    }

    //envio de notificación PUSH
    public function sendNotificacion(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'usuarios' => 'required',
                'titulo' => 'required|string',
                'texto' => 'required|string',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $title = $request->titulo;
            $text = $request->texto;

            if (in_array('all', $request->usuarios)) {
                event(new BNotificacion($text, $title));
            } else {
                foreach ($request->usuarios as $id) {
                    if ($id != 'all') {
                        event(new PuntosEvent($title, $text, $id));
                    }
                }
            }

            return Redirect::back()->with('create', 'Notificación enviada de forma exitosa');
        } catch (\Exception $e) {
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La notificación no pudo ser enviada: '.$e->getMessage());
        }
    }
}
