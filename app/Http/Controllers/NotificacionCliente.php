<?php

namespace App\Http\Controllers;

use App\Models\ClienteNotificacion;
//MODELOS
use App\Models\Notificaciones;
use Illuminate\Support\Facades\DB;

//Helper o Usables

class NotificacionCliente extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'descripcion.max' => 'El dato no debe ser mayor a 100 carácteres',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:50',
        'descripcion' => 'required|string|max:100',
        'tag' => 'required|string|max:50',
        'stock' => 'required|numeric|min:0',
        'limite_usuario' => 'numeric|min:0',
        'foto' => 'file|mimes:jpg,jpeg,png',
        'numero_estrellas' => 'numeric|min:0',
    ];

    //Cantidad de notificaciones sin leer
    public function index()
    {
        $notifications = auth()->user()->cliente->unreadnotification;

        return response()->json([
            'status' => 'success',
            'message' => 'Número de Notificaciones sin leer',
            'notificaciones' => $notifications->count(),
        ], 200);
    }

    //Devolver todas las notificaciones
    public function ApiListado()
    {
        $notifications = auth()->user()->cliente->notificaciones;

        return response()->json([
            'status' => 'success',
            'message' => 'Notificaciones disponibles',
            'notificaciones' => $notifications,
        ], 200);
    }

    //Marcar como leido una notificación
    public function marcarLeido($id_notificacion)
    {
        DB::beginTransaction();
        try {
            ClienteNotificacion::where('FK_id_notificaciones', $id_notificacion)->
                                where('FK_id_cliente', auth()->user()->FK_id_cliente)->
                                update(['read_at' => date('Y-m-d H:i:s')]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Notificación Editada',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al marcar leida la notificación',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Marcar como leido todas las notificaciones
    public function marcartodo()
    {
        DB::beginTransaction();
        try {
            ClienteNotificacion::where('FK_id_cliente', auth()->user()->FK_id_cliente)->
                                update(['read_at' => date('Y-m-d H:i:s')]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Todas las notificaciones fueron marcadas como leidas',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al marcar todas las notificaciones como leida',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Eliminar el global o la data general de una notificación
    public function deleteGeneral()
    {
        $notificaciones = Notificaciones::all();

        foreach ($notificaciones as $notificacion) {
            if ($notificacion->admins->isEmpty() && $notificacion->clientes->isEmpty()) {
                $notificacion->delete();
            }
        }
    }

    //Eliminar una sola notificación
    public function deleteone($id_notificacion)
    {
        DB::beginTransaction();
        try {
            ClienteNotificacion::where('FK_id_notificaciones', $id_notificacion)->
                                where('FK_id_cliente', auth()->user()->FK_id_cliente)->delete();

            $notificacion = Notificaciones::where('id_notificaciones', $id_notificacion)->first();
            if ($notificacion->admins->isEmpty() && $notificacion->clientes->isEmpty()) {
                $notificacion->delete();
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Notificación fue eliminada correctamente',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'La Notificación no pudo ser eliminada',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Eliminar todas las notificaciones
    public function deletetodo()
    {
        DB::beginTransaction();
        try {
            ClienteNotificacion::where('FK_id_cliente', auth()->user()->FK_id_cliente)->delete();

            $this->deleteGeneral();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Todas las notificaciones fueron eliminadas',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Las Notificaciones no pudieron ser eliminadas',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }
}
