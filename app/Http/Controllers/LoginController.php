<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\LoginController as DefaultLoginController;
use Illuminate\Support\Facades\Auth;

class LoginController extends DefaultLoginController
{
    /* protected $redirectTo = '/escritorio'; */

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function form_login()
    {
        return view('modules.auth.form_login');
    }

    public function username()
    {
        return 'email';
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }
}
