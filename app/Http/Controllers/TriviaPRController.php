<?php

namespace App\Http\Controllers;

use App\Events\BNotificacion;
use App\Events\PuntosEvent;
use App\Helper\Notificacion;
use App\Mail\PuntosTrivia;
//MODELOS
use App\Models\ClienteHistorial;
use App\Models\Estrella;
use App\Models\Trivia;
use App\Models\TriviaCliente;
use App\Models\TriviaClienteRespuesta;
use App\Models\TriviaPregunta;
use App\Models\TriviaRespuesta;
//Helper o Usables
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
//MAILABLE
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class TriviaPRController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg, mp4, wm, wmv o pdf',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'boolean' => 'Debes enviar el dato como true o false',
        'informacion.max' => 'El dato no debe ser mayor a 255 carácteres',
    ];

    //PREGUNTAS Y RESPUESTAS DE LAS TRIVIAS CONTROLADOR

    //Listado de Preguntas + Respuesta
    public function preguntas($id_trivia)
    {
        $trivia = Trivia::findOrFail($id_trivia);
        $preguntas = TriviaPregunta::where('FK_id_trivia', $id_trivia)->get();

        if ($trivia->tipo == 'trivia') {
            return view('trivia.detail', [
                'trivia' => $trivia,
                'preguntas' => $preguntas,
            ]);
        } else {
            return view('encuesta.detail', [
                'trivia' => $trivia,
                'preguntas' => $preguntas,
            ]);
        }
    }

    //Enviar Vista para el registro de una pregunta
    public function createPregunta($id_trivia)
    {
        $trivia = Trivia::find($id_trivia);

        if (! $trivia) {
            return redirect()->route('encuesta.index')->with('delete', 'La trivia indicada no se encuentra registrada');
        }

        //Si no existe trivia de la capacitación seleccionada pues se envia a la vista para generarla
        //return view('premio.control',compact('premios'));
    }

    //Guardado de una pregunta
    public function storePregunta(Request $request)
    {
        DB::beginTransaction();
        try {
            $trivia = Trivia::find($request->input('FK_id_trivia'));

            if (! $trivia) {
                return redirect()->route('trivia.index')->with('delete', 'La trivia indicada no se encuentra registrada');
            }

            $validate = Validator::make($request->all(), [
                'informacion' => 'required|string|max:255',
                'puntuacion' => 'numeric|min:1',
                'FK_id_trivia' => 'required',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();
            if ($request->has('puntuacion')) {
                $data['estado'] = true;
            }

            TriviaPregunta::create($data);

            if ($trivia->preguntas->count() < 2) {
                if ($trivia->tipo == 'home') {
                    $title = '!NUEVA ENCUESTA DISPONIBLE!';
                    $text = 'Encuesta de Home disponible: '.$trivia->nombre;
                    event(new BNotificacion($text, $title));
                }
            }

            DB::commit();
            //DEVUELVO LA VISTA DONDE SE VEN LAS PREGUNTAS DE DICHA TRIVIA
            return Redirect::back()->with('create', 'Pregunta Registrada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Pregunta no pudo ser Registrada');
        }
    }

    //Guardado de una respuesta
    public function storeRepuesta(Request $request)
    {
        DB::beginTransaction();
        try {
            $pregunta = TriviaPregunta::find($request->input('FK_id_pregunta'));

            if (! $pregunta) {
                return Redirect::back()->with('delete', 'La pregunta a la cual se le registra la respuesta no se encuentra registrada');
            }

            $validate = Validator::make($request->all(), [
                'correcta' => 'required|boolean',
                'informacion' => 'required|string|max:255',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            TriviaRespuesta::create($request->all());

            if ($request->input('correcta')) {
                $pregunta->estado ? '' : $pregunta->update(['estado' => 1]);
            }

            DB::commit();
            //DEVUELVO LA VISTA DONDE SE VEN LAS PREGUNTAS DE DICHA TRIVIA
            return Redirect::back()->with('create', 'Respuesta Registrada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Respuesta no pudo ser Registrada');
        }
    }

    //Vista para confirmar si de verdad quiere eliminar algo
    public function confirm($id_trivia)
    {
        $trivia = Trivia::findOrFail($id_trivia);
        /* return view('premio.confirm', compact('premio')); */
    }

    //Eliminar una pregunta
    public function destroyPregunta(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $pregunta = TriviaPregunta::find($id);

            if (! $pregunta) {
                return Redirect::back()->with('edit', 'La pregunta no fue encontrada');
            }

            //Elmino Respuestas de la trivia y de los clientes + las pregunta
            TriviaRespuesta::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
            //TriviaClienteRespuesta::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
            $pregunta->delete();

            $trivia = $pregunta->trivia;

            if ($trivia->preguntas->isEmpty()) {
                $trivia->update(['estado' => 0]);
            }

            DB::commit();

            // return Redirect::back()->with('delete', 'Pregunta Eliminada');
            return response()->json([
                'message' => 'pregunta eliminada correctamente'
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return Redirect::back()->with('error', 'La Pregunta no pudo ser eliminada');
        }
    }

    //Eliminar una respuesta
    public function destroyRespuesta(Request $request)
    {
        DB::beginTransaction();
        try {
            $respuesta = TriviaRespuesta::find($request->input('id_respuesta'));

            if (! $respuesta) {
                return Redirect::back()->with('edit', 'La respuesta no fue encontrada');
            }

            //Elmino la respuesta
            $respuesta->delete();

            $pregunta = $respuesta->pregunta;
            if ($pregunta->respuestas->isEmpty()) {
                $pregunta->update(['estado' => 0]);
            }

            DB::commit();

            return Redirect::back()->with('delete', 'Respuesta Eliminada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return Redirect::back()->with('error', 'La Respuesta no pudo ser eliminada');
        }
    }

    //Enviar la Vista para editar una Pregunta o  Respuestas
    public function edit($id_pregunta)
    {
        $pregunta = TriviaPregunta::findOrFail($id_pregunta);
        //Vista para la edición de datos
        if ($pregunta->trivia->tipo == 'trivia') {
            return view('trivia.editPregunta', compact('pregunta'));
        } else {
            return view('encuesta.editPregunta', compact('pregunta'));
        }
    }

    //Update de los datos de una Pregunta
    public function updatePregunta(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'informacion' => 'required|string|max:255',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $pregunta = TriviaPregunta::findOrFail($request->input('id_pregunta'));

            $data = $request->all();
            if ($request->has('tipo')) {
                if ($request->input('tipo') != 'radio') {
                    TriviaRespuesta::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                    $data['estado'] = true;
                }
            }

            $pregunta->update($data);

            DB::commit();

            return redirect()->route('pregunta.edit', $pregunta->id_pregunta)->with('edit', 'Pregunta Actualizada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Pregunta no pudo ser Editada');
        }
    }

    //Update de los datos de una Respuesta
    public function updateRespuesta(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'correcta' => 'required|boolean',
                'informacion' => 'required|string|max:255',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $respuesta = TriviaRespuesta::findOrFail($request->input('id_respuesta'));
            $respuesta->update($request->all());

            $pregunta = $respuesta->pregunta;
            $change = true;
            foreach ($pregunta->respuestas as $respuesta) {
                $respuesta->correcta ? $change = false : '';
            }
            $change ? $pregunta->update(['estado' => 0]) : $pregunta->update(['estado' => 1]);

            DB::commit();

            return Redirect::back()->with('edit', 'Respuesta Actualizada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Respuesta no pudo ser Editada');
        }
    }

    ///////////////////////////////////////API CLIENTE//////////////////////////////////////////

    //Ver las preguntas de la trivia seleccionada
    public function ApiPreguntas($id_trivia)
    {
        $triviacliente = TriviaCliente::where('FK_id_trivia', $id_trivia)->where('FK_id_cliente', auth()->user()->FK_id_cliente)->first();

        if ($triviacliente) {
            return response()->json([
                'status' => 'error',
                'message' => 'El usuario ya respondio la trivia',
            ], 200);
        }

        $preguntas = TriviaPregunta::where('FK_id_trivia', $id_trivia)->where('estado', 1)->with('respuestas')->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Preguntas disponibles',
            'preguntas' => $preguntas,
        ], 200);
    }

    //Devolver una trivia respondida y sus valores correspondiente
    public function ApiTriviaRespuesta(Request $request, $id_trivia)
    {

        //Yo recibo ID de la trivia en la URL
        //Recibo las respuestas con los siguientes datos siendo un array
        //trivia: [{ "pregunta": "pregunta que envio", "respuesta": "respuesta que envio", "correcta": false  }],
        //total_correctas: 1

        $trivia = Trivia::find($id_trivia);
        $respuestas = $request->all();

        if (! $trivia) {
            return response()->json([
                'status' => 'error',
                'message' => 'La trivia respondida no fue encontrada',
            ], 200);
        }

        $check = TriviaCliente::where('FK_id_trivia', $id_trivia)->where('FK_id_cliente', auth()->user()->FK_id_cliente)->first();

        if ($check) {
            return response()->json([
                'status' => 'error',
                'message' => 'La trivia ya fue respondida por el cliente',
            ], 200);
        }

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'total_correctas' => 'required|numeric|min:0',
                'trivia.*.pregunta' => 'required|string|max:255',
                'trivia.*.respuesta' => 'required|string|max:255',
                'trivia.*.correcta' => 'required|boolean',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            if ($respuestas['total_correctas'] > count($respuestas['trivia'])) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'El número de respuestas correctas no corresponde al número de preguntas contestadas',
                    'respuestas' => count($respuestas['trivia']),
                    'correctas' => $respuestas['total_correctas'],
                ], 500);
            }

            $clienteTrivia = new TriviaCliente();
            $clienteTrivia->nombre = $trivia->nombre;
            $clienteTrivia->descripcion = $trivia->descripcion;
            $clienteTrivia->numero_estrellas = round(($respuestas['total_correctas'] * $trivia->puntuacion) / count($respuestas['trivia']));
            $clienteTrivia->FK_id_trivia = $trivia->id_trivia;
            $clienteTrivia->FK_id_cliente = auth()->user()->FK_id_cliente;
            $clienteTrivia->save();

            foreach ($respuestas['trivia'] as $answer) {
                $clienteR = new TriviaClienteRespuesta();
                $clienteR->informacion = $answer['respuesta'];
                $clienteR->correcta = $answer['correcta'];
                $clienteR->pregunta = $answer['pregunta'];
                $clienteR->FK_id_trivia_cliente = $clienteTrivia->id_trivia_cliente;
                $clienteR->save();
            }

            //Actualizamos al cliente
            $cliente = auth()->user()->cliente;
            $cliente->total_estrellas += $clienteTrivia->numero_estrellas;
            $cliente->update();

            //Grabamos las estrellas o puntos en su tabla correspondiente
            $estrella = new Estrella();
            $estrella->descripcion = 'Puntos por Trivia '.$trivia->nombre;
            $estrella->cantidad = $clienteTrivia->numero_estrellas;
            $estrella->fecha_registro = date('Y-m-d H:i:s');
            $estrella->estado = 'activo';
            $estrella->FK_id_cliente = auth()->user()->FK_id_cliente;
            $estrella->save();

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Registro de Trivia Respondida Código: '.$trivia->id_trivia;
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            //Notificaciones de puntos ganados
            $dataClient = [
                'nombre' => '¡FELICIDADES, GANASTE!',
                'tipo' => 'Puntos',
                'descripcion' => 'Acabas de sumar '.$clienteTrivia->numero_estrellas.' puntos por contestar encuesta, aprovecha y descubre que puedes canjear.',
                'cliente' => $cliente->id_cliente,
            ];

            Notificacion::instance()->storeUniqueClient($dataClient);

            $title = '¡FELICIDADES, GANASTE!';
            $text = 'Acabas de sumar '.$clienteTrivia->numero_estrellas.' puntos por contestar encuesta, aprovecha y descubre que puedes canjear.';
            event(new PuntosEvent($title, $text, $cliente->id_cliente));

            //ENVIO DEL CORREO DE GANO X PUNTOS DE TRIVIA
            $information = new \stdClass();
            $information->asunto = 'Felicitaciones Puntos por Capacitación';
            $information->nombre = $cliente->nombre.' '.$cliente->apellido;
            $information->puntos = $estrella->cantidad;
            $information->total = $cliente->total_estrellas;

            try {
                Mail::to($cliente->correo)->queue(new PuntosTrivia($information));
            } catch (\Exception $e) {
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'La trivia y resultado fue almacenado de forma correcta',
                'puntos' => $clienteTrivia->numero_estrellas,
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Error interno del servidor, intentelo mas tarde',
                'error message' => $e->getMessage(),
                'codigo' => $e->getCode(),
            ], 500);
        }
    }

    //Devolver una trivia respondida y sus valores correspondiente
    public function ApiEncuestaRespuesta(Request $request, $id_trivia)
    {

        //Yo recibo ID de la trivia en la URL
        //Recibo las respuestas con los siguientes datos siendo un array
        //trivia: [{ "pregunta": "pregunta que envio", "respuesta": "respuesta que envio", "puntuacion": 20  }],
        //total_puntos: 50,

        $trivia = Trivia::find($id_trivia);
        $respuestas = $request->all();

        if (! $trivia) {
            return response()->json([
                'status' => 'error',
                'message' => 'La trivia respondida no fue encontrada',
            ], 200);
        }

        $check = TriviaCliente::where('FK_id_trivia', $id_trivia)->where('FK_id_cliente', auth()->user()->FK_id_cliente)->first();

        if ($check) {
            return response()->json([
                'status' => 'error',
                'message' => 'La trivia ya fue respondida o saltada por el cliente',
            ], 200);
        }

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'trivia.*.pregunta' => 'required|string|max:255',
                'trivia.*.respuesta' => 'required|string|max:255',
                'trivia.*.puntuacion' => 'required|numeric|min:0',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $clienteTrivia = new TriviaCliente();
            $clienteTrivia->nombre = $trivia->nombre;
            $clienteTrivia->descripcion = $trivia->descripcion;
            $clienteTrivia->numero_estrellas = round($respuestas['total_puntos']);
            $clienteTrivia->FK_id_trivia = $trivia->id_trivia;
            $clienteTrivia->FK_id_cliente = auth()->user()->FK_id_cliente;
            $clienteTrivia->save();

            foreach ($respuestas['trivia'] as $answer) {
                $clienteR = new TriviaClienteRespuesta();
                $clienteR->informacion = $answer['respuesta'];
                $clienteR->correcta = true;
                $clienteR->pregunta = $answer['pregunta'];
                $clienteR->FK_id_trivia_cliente = $clienteTrivia->id_trivia_cliente;
                $clienteR->save();
            }

            //Actualizamos al cliente
            $cliente = auth()->user()->cliente;
            $cliente->total_estrellas += $clienteTrivia->numero_estrellas;
            $cliente->update();

            //Grabamos las estrellas o puntos en su tabla correspondiente
            $estrella = new Estrella();
            $estrella->descripcion = 'Puntos por Encuesta '.$trivia->nombre;
            $estrella->cantidad = $clienteTrivia->numero_estrellas;
            $estrella->fecha_registro = date('Y-m-d H:i:s');
            $estrella->estado = 'activo';
            $estrella->FK_id_cliente = auth()->user()->FK_id_cliente;
            $estrella->save();

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Registro de Encuesta Respondida Código: '.$trivia->id_trivia;
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            //Notificaciones de puntos ganados
            $dataClient = [
                'nombre' => '¡FELICIDADES, GANASTE!',
                'tipo' => 'Puntos',
                'descripcion' => 'Acabas de sumar '.$clienteTrivia->numero_estrellas.' puntos por contestar trivia, aprovecha y descubre que puedes canjear.',
                'cliente' => $cliente->id_cliente,
            ];

            Notificacion::instance()->storeUniqueClient($dataClient);

            $title = '¡FELICIDADES, GANASTE!';
            $text = 'Acabas de sumar '.$clienteTrivia->numero_estrellas.' puntos por contestar trivia, aprovecha y descubre que puedes canjear.';
            event(new PuntosEvent($title, $text, $cliente->id_cliente));

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'La encuesta y puntos conseguidos fueron almacenados de forma correcta',
                'puntos' => $clienteTrivia->numero_estrellas,
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Error interno del servidor, intentelo mas tarde',
                'error message' => $e->getMessage(),
                'codigo' => $e->getCode(),
            ], 500);
        }
    }

    //El proceso si le da skip a una encuesta del home, se le cancela o elimina por decir así
    public function ApiEncuestaSkip($id_trivia)
    {
        //Yo recibo ID de la trivia en la URL

        $trivia = Trivia::where('id_trivia', $id_trivia)->where('tipo', 'home')->first();

        if (! $trivia) {
            return response()->json([
                'status' => 'error',
                'message' => 'La encuesta no fue encontrada o no es de home',
            ], 200);
        }

        $check = TriviaCliente::where('FK_id_trivia', $id_trivia)->where('FK_id_cliente', auth()->user()->FK_id_cliente)->first();

        if ($check) {
            return response()->json([
                'status' => 'error',
                'message' => 'La encuesta ya fue respondida o saltada por el cliente',
            ], 200);
        }

        DB::beginTransaction();
        try {
            $clienteTrivia = new TriviaCliente();
            $clienteTrivia->nombre = $trivia->nombre;
            $clienteTrivia->descripcion = 'skip';
            $clienteTrivia->numero_estrellas = 0;
            $clienteTrivia->FK_id_trivia = $trivia->id_trivia;
            $clienteTrivia->FK_id_cliente = auth()->user()->FK_id_cliente;
            $clienteTrivia->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'La encuesta fue saltada exitosamente no aparecerá más',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Error interno del servidor, intentelo mas tarde',
                'error message' => $e->getMessage(),
                'codigo' => $e->getCode(),
            ], 500);
        }
    }
}
