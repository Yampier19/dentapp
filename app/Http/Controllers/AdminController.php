<?php

namespace App\Http\Controllers;

use App\Helper\Notificacion;
use App\Http\Requests\Admin\StoreAdminRequest;
use App\Http\Requests\Admin\UpdateAdminRequest;
use App\Mail\AdminEmail;
use App\Models\Admin;
use App\Models\AdminNotificacion;
use App\Models\Rol;
use App\Models\UserHistory;
use Illuminate\Support\Facades\Auth;
//Modelos
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
//Helper
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
//MAILABLE
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    //Listado de Admins
    public function listadmin()
    {
        $admin = Admin::orderBy('id_useradmin', 'desc')
            ->get();
        $rol = Rol::all();

        return view('cliente.listadmin', compact('admin', 'rol'));
    }

    public function adminlist()
    {
        $admin = Admin::with('roles')
            ->where('id_useradmin', '!=', Auth::user()->id_useradmin)
            ->orderBy('id_useradmin', 'desc')
            ->get();

        return response()->json([
            'message' => 'Listado de usuarios',
            'code' => 200,
            'data' => $admin,
        ]);
    }

    //Enviar Vista para el registro de un admin nuevo
    public function create()
    {
        $rol = Rol::all();

        return view('cliente.create', compact('rol'));
    }

    //Guardado de un Admin
    public function store(StoreAdminRequest $request)
    {
        DB::transaction(function () use ($request) {
            $data = $request->all();

            if ($this->checkPassword($data)) {
                return redirect()->route('cliente.create')->with('error', 'El password se asemeja al correo');
            }

            if ($request->input('password')) {
                $password = $request->input('password');
            } else {
                $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $password = substr(str_shuffle($caracteres_permitidos), 0, 8);
            }

            $data['password'] = Hash::make($password);

            //GRABAMOS LOS PERMISOS
            if ($data['FK_id_loginrol'] == 1) {
                $data['p_store'] = 1;
                $data['p_update'] = 1;
                $data['p_destroy'] = 1;
                $data['p_productos'] = 1;
                $data['p_premios'] = 1;
                $data['p_facturas'] = 1;
                $data['p_noticias'] = 1;
                $data['p_capacitaciones'] = 1;
                $data['p_trivia'] = 1;
            } elseif ($data['FK_id_loginrol'] != 2) {
                $data['p_store'] = 0;
                $data['p_update'] = 0;
                $data['p_destroy'] = 0;
            }

            //FIN GRABAMOS LOS PERMISOS

            $admin = Admin::create($data);

            //Grabamos el historial del usuario login store
            if ($request->input('password')) {
                $loghistory = new UserHistory();
                $loghistory->accion = 'Store de Usuario Admin Login';
                $loghistory->email = $data['email'];
                $loghistory->password = $request->input('password');
                $loghistory->FK_id_useradmin = $admin->id_useradmin;
                $loghistory->save();
            }

            $data = [
                'nombre' => 'Creación de Usuario',
                'tipo' => 'Insert',
                'descripcion' => 'Creación nuevo Usuario Admin: '.$request->nombre.' '.$request->apellido,
            ];

            Notificacion::instance()->store($data, 'usuarios');

            //NOTIFICACION DE NUEVO ADMIN CREADO
            $rolnombre = '';
            $admin->rol == 1 ? $rolnombre = 'Super Admin' : '';
            $admin->rol == 2 ? $rolnombre = 'Call Center' : '';
            $admin->rol == 3 ? $rolnombre = 'Básico' : '';

            $admin->rol == 4 ? $rolnombre = 'Asistente' : '';
            $admin->rol == 5 ? $rolnombre = 'Clinica' : '';

            $information = new \stdClass();
            $information->asunto = 'Bienvenido Admin a DentApp';
            $information->nombre = $request->nombre.' '.$request->apellido;
            $information->rol = $rolnombre;
            $information->correo = $request->email;
            $information->contraseña = $password;

            Mail::to($request->email)->queue(new AdminEmail($information));
        });

        return response()->json([
            'message' => 'Usuario creado correctamente',
            'code' => 200,
        ]);
    }

    //Eliminar un Admin
    public function destroy(Admin $admin)
    {
        DB::transaction(function () use ($admin) {
            if (! $admin) {
                return redirect()->route('cliente.index')->with('edit', 'El Usuario no se encuentra registrado');
            }

            if ($admin->foto) {
                Storage::delete(['public/'.$admin->foto]);
            }

            AdminNotificacion::where('FK_id_useradmin', $admin->id_useradmin)->delete();
            $admin->email = null;
            $admin->update();
            $admin->delete();

            $data = [
                'nombre' => 'Delete de Usuario',
                'tipo' => 'Delete',
                'descripcion' => 'Eliminado Usuario Admin: '.$admin->nombre.' '.$admin->apellido,
            ];

            Notificacion::instance()->store($data, 'usuarios');
        });

        return redirect()->route('cliente.listadmin')->with('eliminar', 'Usuario Eliminado');
        // return response()->json([
        //     'message' => 'Usuario eliminaodo correctamente',
        //     'code' => 200,
        // ]);
    }

    //Enviar la Vista para editar un Admin
    public function edit(Admin $admin)
    {
        $rol = Rol::select('id_loginrol', 'rol')->get();
        $admin->load('roles');

        return view('admin.edit', compact('admin', 'rol'));

        // return response()->json([
        //     'message' => 'usuario encontrado',
        //     'code' => 200,
        //     'data' => $admin,
        // ]);
    }

    //Update de los datos de un admin
    public function update(UpdateAdminRequest $request, Admin $admin)
    {
        DB::transaction(function () use ($admin, $request) {
            $lastemail = $admin->email;
            $data = $request->all();

            //Chequiamos si no estamos repitiendo el mismo password
            if (Hash::check($request->input('password'), $admin->password)) {
                return Redirect::back()->with('error', 'El nuevo password es igual al anterior');
            }

            //Chequiamos que no se asemeje al correo
            if ($request->input('password')) {
                if ($this->checkPassword($data)) {
                    return Redirect::back()->with('error', 'El password se asemeja al correo');
                }
            }

            if ($request->hasFile('foto')) {
                Storage::delete(['public/'.$admin->foto]);
                $data['foto'] = $request->file('foto')->store('uploads', 'public');
                $this->imagecompress($data['foto']);
            }

            if ($request->input('password')) {
                $data['password'] = Hash::make($request->input('password'));
            } else {
                $data['password'] = $admin->password;
            }

            //GRABAMOS LOS PERMISOS
            if ($data['FK_id_loginrol'] == 1) {
                $data['p_store'] = 1;
                $data['p_update'] = 1;
                $data['p_destroy'] = 1;
                $data['p_productos'] = 1;
                $data['p_premios'] = 1;
                $data['p_facturas'] = 1;
                $data['p_noticias'] = 1;
                $data['p_capacitaciones'] = 1;
                $data['p_trivia'] = 1;
            } elseif ($data['FK_id_loginrol'] != 2) {
                $data['p_store'] = 0;
                $data['p_update'] = 0;
                $data['p_destroy'] = 0;
            }
            //FIN GRABAMOS LOS PERMISOS

            //Grabamos el historial del usuario login update
            if ($request->input('password')) {
                $loghistory = new UserHistory();
                $loghistory->accion = 'Update de Usuario Admin Login';
                $loghistory->email = $data['email'];
                $loghistory->password = $request->input('password');
                $loghistory->FK_id_useradmin = $admin->id_useradmin;
                $loghistory->save();
            }

            $admin->update($data);

            $data = [
                'nombre' => 'Update de Usuario',
                'tipo' => 'Update',
                'descripcion' => 'Edición de Usuario Admin: '.$request->nombre.' '.$request->apellido,
            ];

            Notificacion::instance()->store($data, 'usuarios');

            //NOTIFICACION DE NUEVO ADMIN CREADO
            if ($lastemail != $request->input('email')) {
                $rolnombre = '';
                $admin->rol == 1 ? $rolnombre = 'Super Admin' : '';
                $admin->rol == 2 ? $rolnombre = 'Call Center' : '';
                $admin->rol == 3 ? $rolnombre = 'Básico' : '';

                $admin->rol == 4 ? $rolnombre = 'Asistente' : '';
                $admin->rol == 5 ? $rolnombre = 'Clinica' : '';

                $information = new \stdClass();
                $information->asunto = 'Update Admin en DentApp';
                $information->nombre = $request->nombre.' '.$request->apellido;
                $information->rol = $rolnombre;
                $information->correo = $request->email;
                $information->contraseña = 'La misma de antes';

                Mail::to($request->email)->queue(new AdminEmail($information));
            }
        });

        return Redirect::back()->with('edit', 'El Usuario ha sido editado exitosamente');
        // return response()->json([
        //     'message' => 'El usuario fue editado correctamente',
        //     'code' => 200,
        // ]);
    }

    //Compresor de Imagen
    public function imagecompress($image)
    {
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Chequiamos password si se asemeja al correo
    public function checkPassword($check)
    {
        //Chequiamos si es el correo y el password son iguales
        $check['email'] = explode(' ', strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['email'])));
        $check['password'] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['password']));

        if (stripos($check['password'], $check['email'][0]) !== false) {
            return true;
        } else {
            return false;
        }
    }
}
