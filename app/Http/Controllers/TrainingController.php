<?php

namespace App\Http\Controllers;

use App\Events\BNotificacion;
use App\Helper\Notificacion;
use App\Helper\VideoStream;
use App\Mail\CapacitacionEmail;
use App\Models\Capacitacion;
use App\Models\CapacitacionCambio;
//MODELOS
use App\Models\CapacitacionCategoria;
use App\Models\CapacitacionCliente;
use App\Models\Cliente;
use App\Models\ClienteHistorial;
use App\Models\ClienteLogin;
use App\Models\Trivia;
use App\Models\TriviaCambio;
use App\Models\TriviaCliente;
use App\Models\TriviaPregunta;
use App\Models\TriviaRespuesta;
//Helper o Usables
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
//MAILABLE
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class TrainingController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'imagen.mimes' => 'El archivo debe llegar en formato imagen png, jpg, jpeg',
        'mimes' => 'El archivo debe llegar en formato png, jpg, jpeg, o mp4',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'tipo.max' => 'El dato no debe ser mayor a 255 carácteres',
        'informacion.max' => 'El dato no debe ser mayor a 255 carácteres',
        'imagen.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:50',
        'descripcion' => 'required|string|max:255',
        'tipo' => 'required|string|max:255',
        'informacion' => 'string|max:255',
        'archivo' => 'file|mimes:jpg,jpeg,png,pdf,mp4',
        'imagen' => 'file|mimes:jpg,jpeg,png|max:2100',
    ];

    public function index()
    {
        $trainings = Capacitacion::orderBy('created_at', 'desc')->paginate(5);

        //PARA COMPLETAR LA CAPACITACION
        $categorias = CapacitacionCategoria::all();

        return view('capacitacion.index', compact('trainings', 'categorias'));
    }

    public function clienteDetail($id_cliente = null)
    {
        try {
            $trivias = TriviaCliente::where('FK_id_cliente', $id_cliente)->with('respuestas')->get();

            if ($trivias->count() <= 0) {
                return response()->json([
                    'message' => 'El Cliente no posee trivias realizadas',
                ]);
            }

            $cuestionarios = collect();

            foreach ($trivias as $trivia) {
                if ($trivia->trivia) {
                    if ($trivia->trivia->tipo == 'home') {
                        continue;
                    }
                }
                $cuestionarios->add($trivia);
            }

            return response()->json([
                'trivias' => $cuestionarios,
                'message' => 'Trivias del Cliente',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error al consultar trivias: '.$e->getMessage(),
            ]);
        }
    }

    public function list_user()
    {
        $clientes = Cliente::all();

        return view('capacitacion.list_user', compact('clientes'));
    }

    public function list_capacitacion()
    {
        $trainings = Capacitacion::orderBy('created_at', 'desc')->get();

        return view('capacitacion.list', compact('trainings'));
    }

    //Compresor de Imagen o PDF
    public function imagecompress($image, $extension)
    {
        if ($extension == 'pdf') {
            return 'nada';
        } elseif ($extension == 'mp4') {
            return 'nada';
        } else {
            $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save();
            $img->destroy();
        }
    }

    //Recoger archivo de la capacitacion
    public function getArchive($filename = null)
    {
        $extension = null;

        if ($filename) {
            $extension = pathinfo($filename)['extension'];

            $exist = Storage::disk('public')->exists('training/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $extension = null;
                $file = storage_path('app/public/nodisponible.png');
            } else {
                $file = storage_path('app/public/training/'.$filename);
            }
        } else {
            $file = storage_path('app/public/nodisponible.png');
        }

        switch ($extension) {
            case 'pdf':
                $extension = 'application/pdf';
                break;
            case 'mp4':
                $extension = 'mp4';
                break;
            default:
                $extension = 'image';
                break;
        }

        if ($extension == 'mp4') {
            return view('capacitacion.stream', compact('filename'));
        } else {
            return Response(file_get_contents($file), 200, [
                'Content-Type' => $extension,
                'Content-Disposition' => 'inline; filename="'.$file.'"',
            ]);
        }
    }

    //Recoger preview de la capacitacion
    public function getImage($filename = null)
    {
        if ($filename) {
            $exist = Storage::disk('public')->exists('training/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('usericon.jpg');
            } else {
                $file = Storage::disk('public')->get('training/'.$filename);
            }
        } else {
            $file = Storage::disk('public')->get('usericon.jpg');
        }

        return new Response($file, 200);
    }

    //Listado de Capacitacion
    public function list()
    {
        $training = Capacitacion::orderBy('created_at', 'desc')->get();

        return view('premio.control', compact('training'));
    }

    //Listado de Capacitaciones Cambiadas (trazabilidad)
    public function listcambios()
    {
        $cambios = CapacitacionCambio::orderBy('created_at', 'asc')->get();

        //Vista de la lista de premios cambios
        //return view('premio.index',compact('cambios'));
    }

    //Envio de correos masivos a usuarios activos
    public function sendEmails($information)
    {
        try {
            $clientes = ClienteLogin::where('estado', 'activo')->get();

            foreach ($clientes as $cliente) {
                try {
                    Mail::to($cliente->email)->queue(new CapacitacionEmail($information));
                    sleep(5);
                } catch (\Exception $e) {
                }
            }
        } catch (\Exception $e) {
            return;
        }
    }

    //Guardado de una capacitacion
    public function store(Request $request)
    {
        $validaciones = $this->validaciones;
        $mensajes_error = $this->mensajes_error;

        if ($request->hasFile('archivo')) {
            if ($request->file('archivo')->extension() == 'mp4') {
                $validaciones['archivo'] = 'file|mimes:jpg,jpeg,png,pdf,mp4|max:'.(1024 * 150);
                $mensajes_error['archivo.max'] = 'El archivo debe ser menor a 150 Mb';
            } else {
                $validaciones['archivo'] = 'file|mimes:jpg,jpeg,png,pdf,mp4|max:'.(2100);
                $mensajes_error['archivo.max'] = 'El archivo debe ser menor a 2 Mb';
            }
        }

        /* VALIDACIONES NEWS AGREGADAS */
        $validaciones['multiple'] = 'required';
        $validaciones['descripcion_trivia'] = 'required|string|max:255';
        $validaciones['puntuacion'] = 'required|numeric|min:0';

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $validaciones);

            if ($validate->fails()) {
                return response()->json(['errors' => $validate->errors()->all()]);
                //return Redirect::back()->withErrors($validate);
            }

            $data = $request->all();

            if ($request->hasFile('imagen')) {
                $data['imagen'] = $request->file('imagen')->store('training', 'public');
                $this->imagecompress($data['imagen'], $request->file('imagen')->extension());
            }

            if ($request->hasFile('archivo')) {
                $data['archivo'] = $request->file('archivo')->store('training', 'public');
                $this->imagecompress($data['archivo'], $request->file('archivo')->extension());
            }

            $capacitacion = Capacitacion::create($data);
            CapacitacionCategoria::firstOrCreate(['nombre' => $data['tipo']]);

            $data['accion'] = 'Inserción';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_capacitacion'] = $capacitacion->id_capacitacion;

            CapacitacionCambio::create($data);

            /* CREAMOS LA TRIVIA */
            $data['descripcion'] = $request->descripcion_trivia;
            $data['tipo'] = 'trivia';
            $data['estado'] = 1;
            $data['FK_id_capacitacion'] = $capacitacion->id_capacitacion;
            $trivia = Trivia::create($data);

            /* GUARDAMOS LAS PREGUNTAS CON SUS RESPUESTAS */
            if ($request->has('multiple')) {
                foreach ($request->multiple as $preguntas) {
                    //GRABAMOS PREGUNTA
                    $pregunta = new TriviaPregunta();
                    $pregunta->informacion = $preguntas['pregunta'];
                    $pregunta->estado = 1;
                    $pregunta->FK_id_trivia = $trivia->id_trivia;
                    $pregunta->save();

                    unset($preguntas['pregunta']);
                    //GRABAMOS SUS RESPUESTAS
                    for ($i = 0; $i < count($preguntas); $i++) {
                        $respuesta = new TriviaRespuesta();
                        $respuesta->informacion = $preguntas[$i]['texto'];
                        if (array_key_exists('correcta', $preguntas[$i])) {
                            $respuesta->correcta = 1;
                        } else {
                            $respuesta->correcta = 0;
                        }
                        $respuesta->FK_id_pregunta = $pregunta->id_pregunta;
                        $respuesta->save();
                    }
                }
            }
            /* FIN GUARDAR PREGUNTA Y RESPUESTA */

            $data = [
                'nombre' => 'Store de Capacitación',
                'tipo' => 'Insert',
                'descripcion' => 'Creación de Training: '.$capacitacion->nombre,
            ];

            Notificacion::instance()->store($data, 'capacitacion');

            //Notificación para los clientes
            $dataClient = [
                'nombre' => '¡NUEVO RETO DE CONOCIMIENTO! 👨‍⚕️🎁',
                'tipo' => 'Capacitación',
                'descripcion' => $capacitacion->nombre,
            ];

            Notificacion::instance()->storeClient($dataClient);

            $title = '¡NUEVO RETO DE CONOCIMIENTO! 👨‍⚕️🎁';
            $text = 'Aprovecha para sumar hasta '.$trivia->puntuacion.' puntos extra y demuestra que tanto sabes.¡CLIC AQUÍ PARA GANAR!';
            //$text = 'Nueva Capacitación disponible: '.$capacitacion->nombre;
            event(new BNotificacion($text, $title));

            //ENVIO DEL CORREO DE QUE HAY UNA CAPACITACION A TODOS LOS USUARIOS ACTIVOS
            $information = new \stdClass();
            $information->asunto = 'Nueva Capacitación Disponible';
            $information->nombre = $capacitacion->nombre;
            $information->puntuacion = $trivia->puntuacion;
            $this->sendEmails($information);

            DB::commit();

            return response()->json(['success' => 'Capacitación Registrada de forma exitosa']);
            //return Redirect::back()->with('create', 'Capacitación Registrada');
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json(['fallo' => 'La Capacitación no pudo ser Registrada, intentelo más tarde, Error: ' + $e->getMessage()]);
            //return Redirect::back()->with('error', 'La Capicitación no pudo ser Registrada');
        }
    }

    //Vista para confirmar si de verdad quiere eliminar la capacitacion
    public function confirm($id_capacitacion)
    {
        $training = Capacitacion::findOrFail($id_capacitacion);

        return view('capacitacion.confirm', compact('training'));
    }

    //Eliminar una Capacitación
    public function destroy($id_capacitacion)
    {
        DB::beginTransaction();
        try {
            $capacitacion = Capacitacion::find($id_capacitacion);

            if (! $capacitacion) {
                return redirect()->route('training.index')->with('edit', 'La capacitación no fue encontrada');
            }

            CapacitacionCambio::where('FK_id_capacitacion', $capacitacion->id_capacitacion)->update(['FK_id_capacitacion' => null]);

            CapacitacionCliente::where('FK_id_capacitacion', $capacitacion->id_capacitacion)->delete();

            /* ELIMINO LA TRIVIA - PREGUNTAS Y RESPUESTAS */
            $trivia = $capacitacion->trivia;

            if ($trivia) {
                TriviaCambio::where('FK_id_trivia', $trivia->id_trivia)->update(['FK_id_trivia' => null]);

                //Elmino Respuestas de la trivia y de los clientes + las pregunta
                foreach ($trivia->preguntas as $pregunta) {
                    TriviaRespuesta::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                    //TriviaClienteRespuesta::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                    $pregunta->delete();
                }

                //Dejo el cliente trivia con la información y pongo nula la relación
                TriviaCliente::where('FK_id_trivia', $trivia->id_trivia)->update(['FK_id_trivia' => null]);

                $trivia->delete();
            }

            if ($capacitacion->archivo) {
                Storage::delete(['public/'.$capacitacion->archivo]);
            }

            $capacitacion->delete();

            $data = $capacitacion->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;

            CapacitacionCambio::create($data);

            $data = [
                'nombre' => 'Delete de Capacitación',
                'tipo' => 'Delete',
                'descripcion' => 'Eliminación de Capacitación: '.$capacitacion->nombre,
            ];

            Notificacion::instance()->store($data, 'capacitacion');

            DB::commit();

            // return redirect()->route('training.index')->with('delete', 'Capacitación Eliminada');
            return response()->json([
                'message' => 'Capacitación eliminada con exito',
                'code' => 200,
            ]);
        } catch (\Exception $e) {
            DB::rollback();

            return Redirect::back()->with('error', 'La Capacitación no pudo ser eliminada, error: '.$e->getMessage());
        }
    }

    //Enviar la Vista para editar una Capacitación
    public function edit($id_capacitacion)
    {
        $training = Capacitacion::findOrFail($id_capacitacion);
        //PARA COMPLETAR LA CAPACITACION
        $categorias = CapacitacionCategoria::all();

        return view('capacitacion.edit', compact('training', 'categorias'));
    }

    //Update de los datos de una capacitación
    public function update(Request $request, $id_capacitacion)
    {
        $validaciones = $this->validaciones;
        $mensajes_error = $this->mensajes_error;

        if ($request->hasFile('archivo')) {
            if ($request->file('archivo')->extension() == 'mp4') {
                $validaciones['archivo'] = 'file|mimes:jpg,jpeg,png,pdf,mp4|max:'.(1024 * 150);
                $mensajes_error['archivo.max'] = 'El archivo debe ser menor a 150 Mb';
            } else {
                $validaciones['archivo'] = 'file|mimes:jpg,jpeg,png,pdf,mp4|max:'.(2100);
                $mensajes_error['archivo.max'] = 'El archivo debe ser menor a 2 Mb';
            }
        }

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $validaciones);

            if ($validate->fails()) {
                return response()->json(['errors' => $validate->errors()->all()]);
                //return Redirect::back()->withErrors($validate);
            }

            $training = Capacitacion::findOrFail($id_capacitacion);
            $data = $request->all();

            if ($request->hasFile('imagen')) {
                Storage::delete(['public/'.$training->imagen]);
                $data['imagen'] = $request->file('imagen')->store('training', 'public');
                $this->imagecompress($data['imagen'], $request->file('imagen')->extension(), 'noticia');
            }

            if ($request->hasFile('archivo')) {
                Storage::delete(['public/'.$training->archivo]);
                $data['archivo'] = $request->file('archivo')->store('training', 'public');
                $this->imagecompress($data['archivo'], $request->file('archivo')->extension());
            }

            $training->update($data);
            CapacitacionCategoria::firstOrCreate(['nombre' => $data['tipo']]);

            $data['accion'] = 'Update';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_capacitacion'] = $id_capacitacion;

            CapacitacionCambio::create($data);

            $data = [
                'nombre' => 'Update de Capacitación',
                'tipo' => 'Update',
                'descripcion' => 'Edición de Capacitación: '.$training->nombre,
            ];

            Notificacion::instance()->store($data, 'capacitacion');

            DB::commit();

            return response()->json(['success' => 'Capacitación Actualizada']);
            //return Redirect::back()->with('edit', 'Capacitación Actualizada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json(['fallo' => 'La Capacitación no pudo ser Editada']);
            //return Redirect::back()->with('error', 'La Capacitación no pudo ser Editada');
        }
    }

    ///////////////////////////////////////API CLIENTE//////////////////////////////////////////

    //Recoger archivo de la capacitacion API
    public function ApigetArchive($filename = null)
    {

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('training/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/nodisponible.png'));
            } else {
                return response()->file(storage_path('app/public/training/'.$filename));
            }
        } else {
            return response()->file(storage_path('app/public/nodisponible.png'));
        }
    }

    //Recoger imagen preview de la noticia API
    public function ApigetImage($filename = null)
    {

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('training/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/usericon.jpg'));
            } else {
                return response()->file(storage_path('app/public/training/'.$filename));
            }
        } else {
            return response()->file(storage_path('app/public/usericon.jpg'));
        }
    }

    //Recoger video PRUEBA #TODO
    public function ApiTestVideo($filename = null)
    {
        try {
            $filePath = storage_path('app/public/training/'.$filename);
            $stream = new VideoStream($filePath);

            return response()->stream(function () use ($stream) {
                $stream->start();
            });
            //$stream->start();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al abrir stream: No se encontro el archivo',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Ver las categorias que existen de Capacitaciones
    public function ApigetCategories()
    {
        $categorias = CapacitacionCategoria::all();

        return response()->json([
            'status' => 'success',
            'message' => 'Categorias disponibles',
            'categorias' => $categorias,
        ], 200);
    }

    //Ver las capacitaciones que son nuevas, de hace una semana o de hace mas de un mes
    public function ApiNew($registros = 3)
    {
        $training = Capacitacion::orderBy('created_at', 'desc')->paginate($registros);
        $trainingnew = collect();
        $trainingweek = collect();
        $trainingmonth = collect();

        $paginacion = [
            'total' => $training->total(),
            'count' => $training->count(),
            'per_page' => $training->perPage(),
            'current_page' => $training->currentPage(),
            'total_pages' => $training->lastPage(),
        ];

        foreach ($training as $cap) {
            if ($cap->created_at > date('Y-m-d', strtotime('-2 days'))) {
                $trainingnew->push($cap);
            } elseif ($cap->created_at > date('Y-m-d', strtotime('-1 week'))) {
                $trainingweek->push($cap);
            } elseif ($cap->created_at > date('Y-m-d', strtotime('-4 week'))) {
                $trainingmonth->push($cap);
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Capacitaciones disponibles',
            'nuevos' => $trainingnew,
            'semana' => $trainingweek,
            'mes' => $trainingmonth,
            'paginacion' => $paginacion,
        ], 200);
    }

    //Ver las capacitaciones por categoria seleccionada
    public function ApiListCategory($categoria, $registros = 3)
    {
        $training = Capacitacion::where('tipo', $categoria)->orderBy('nombre', 'desc')->paginate($registros);

        $paginacion = [
            'total' => $training->total(),
            'count' => $training->count(),
            'per_page' => $training->perPage(),
            'current_page' => $training->currentPage(),
            'total_pages' => $training->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Capacitaciones disponibles '.$categoria,
            'trainings' => $training->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Ver las capacitaciones disponibles el cliente
    public function ApiListado($registros = 3)
    {
        $training = Capacitacion::orderBy('created_at', 'desc')->paginate($registros);

        $paginacion = [
            'total' => $training->total(),
            'count' => $training->count(),
            'per_page' => $training->perPage(),
            'current_page' => $training->currentPage(),
            'total_pages' => $training->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Capacitaciones disponibles',
            'trainings' => $training->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Ver las capacitaciones disponible con el buscador
    public function ApiBuscador($parametro = null, $registros = 3)
    {
        $training = Capacitacion::orderBy('created_at', 'desc')->where('nombre', 'like', '%'.$parametro.'%')->paginate($registros);

        $paginacion = [
            'total' => $training->total(),
            'count' => $training->count(),
            'per_page' => $training->perPage(),
            'current_page' => $training->currentPage(),
            'total_pages' => $training->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Capacitaciones disponibles',
            'trainings' => $training->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Devolver una capacitación seleccionada y actualizar la tabla de cliente capacitación como vista
    public function ApiTraining($id_capacitacion)
    {
        $training = Capacitacion::where('id_capacitacion', $id_capacitacion)->with('trivia')->first();

        if (! $training) {
            return response()->json([
                'status' => 'error',
                'message' => 'La capacitación solicitada no se encuentra registrada',
            ], 404);
        }

        DB::beginTransaction();
        try {
            $data['FK_id_cliente'] = auth()->user()->FK_id_cliente;
            $data['FK_id_capacitacion'] = $id_capacitacion;

            CapacitacionCliente::firstOrCreate($data);

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Visualizo la Capactiación: '.$training->nombre;
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Capacitación encontrada',
                'training' => $training,
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Error interno del servidor, intentelo mas tarde',
                'error message' => $e->getMessage(),
                'codigo' => $e->getCode(),
            ], 500);
        }
    }
}
