<?php

namespace App\Http\Controllers;

use App\Helper\Notificacion;
use App\Imports\ProductImport;
use App\Models\Multiplicador;
use App\Models\Producto;
use App\Models\ProductoCambio;
use App\Models\ProductoFamilia;
//Modelos
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
//Helper y Usables
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'descripcion.max' => 'El dato no debe ser mayor a 255 carácteres',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'categoria' => 'required|string|max:50',
        'subcategoria' => 'required|string|max:50',
        'stock' => 'required|numeric|min:0',
        'descripcion' => 'required|string|max:255',
        'precio' => 'required|numeric|min:0',
        'estado' => 'required',
        'foto' => 'file|mimes:jpg,jpeg,png|max:2100',
        'numero_estrellas' => 'required|numeric|min:0',
    ];

    //Compresor de Imagen
    public function imagecompress($image)
    {
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Home inicial de menu de productos
    public function index()
    {
        return view('product.index');
    }

    //Recoger imagen del producto
    public function getImage($filename = null)
    {
        if ($filename) {
            $exist = Storage::disk('public')->exists('products/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('product.png');
            } else {
                $file = Storage::disk('public')->get('products/'.$filename);
            }
        } else {
            $file = Storage::disk('public')->get('product.png');
        }

        return new Response($file, 200);
    }

    //Listado de Productos
    public function list()
    {
        $products = Producto::all();

        return view('product.list', compact('products'));
    }

    //Listado de Productos Cambiados (trazabilidad)
    public function listpremioscambios()
    {
        $cambios = ProductoCambio::orderBy('created_at', 'asc')->get();

        //Vista de la lista de productos cambios
        /* return view('cliente.listadmin',compact('admin')); */
    }

    //Enviar Vista para el registro de un premio
    public function create()
    {
        $familia = ProductoFamilia::all();

        return view('product.create', compact('familia'));
    }

    //Guardado de un Producto
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $data = $request->all();

            if ($request->hasFile('foto')) {
                $data['foto'] = $request->file('foto')->store('products', 'public');
                $this->imagecompress($data['foto']);
            }

            $producto = Producto::create($data);

            $data['accion'] = 'Inserción';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_producto_categoria'] = $producto->id_producto_categoria;

            ProductoCambio::create($data);

            $data = [
                'nombre' => 'Store Producto',
                'tipo' => 'Insert',
                'descripcion' => 'Producto Creado: '.$request->subcategoria,
            ];

            Notificacion::instance()->store($data, 'producto');

            DB::commit();

            return redirect()->route('product.index')->with('create', 'Producto Registrado');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return redirect()->route('product.create')->with('error', 'El Producto no pudo ser creado');
        }
    }

    //Vista para confirmar si de verdad quiere eliminar el producto
    public function confirm($id)
    {
        $product = Producto::findOrFail($id);

        return view('product.confirm', compact('product'));
    }

    //Eliminar un Producto
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $producto = Producto::find($id);

            if (! $producto) {
                //VISTA QUE ENVIE MENSAJE DE NO ENCONTRADO
                return redirect()->route('product.index')->with('edit', 'El Producto no fue encontrado');
            }

            foreach ($producto->cambios as $cambios) {
                $cambios->FK_id_producto_categoria = null;
                $cambios->update();
            }

            if ($producto->foto) {
                Storage::delete(['public/'.$producto->foto]);
            }

            Multiplicador::where('FK_id_producto_categoria', $id)->delete();

            $producto->delete();

            $data = $producto->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;

            ProductoCambio::create($data);

            $data = [
                'nombre' => 'Delete Producto',
                'tipo' => 'Delete',
                'descripcion' => 'Producto Eliminado: '.$producto->subcategoria,
            ];

            Notificacion::instance()->store($data, 'producto');

            DB::commit();

            return redirect()->route('product.index')->with('delete', 'Producto Eliminado');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El Producto no pudo ser Eliminado');
        }
    }

    //Enviar la Vista para editar un Producto
    public function edit($id)
    {
        $product = Producto::findOrFail($id);
        $familia = ProductoFamilia::all();

        return view('product.edit', compact('product', 'familia'));
    }

    //Update de los datos de un Producto
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $producto = Producto::findOrFail($id);
            $data = $request->all();

            if ($request->hasFile('foto')) {
                Storage::delete(['public/'.$producto->foto]);
                $data['foto'] = $request->file('foto')->store('uploads', 'public');
                $this->imagecompress($data['foto']);
            }

            $producto->update($data);

            $data['accion'] = 'Update';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_producto_categoria'] = $id;

            ProductoCambio::create($data);

            $data = [
                'nombre' => 'Update Producto',
                'tipo' => 'Update',
                'descripcion' => 'Producto Editado: '.$producto->subcategoria,
            ];

            Notificacion::instance()->store($data, 'producto');

            DB::commit();

            return redirect()->route('product.index')->with('edit', 'Producto Actualizado');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El Producto no pudo ser editado');
        }
    }

    //Vista que explica para hacer el import de CSV
    public function import_csv()
    {
        return view('product.import_csv');
    }

    //Metodo que genera el import de productos
    public function import(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx,csv',
        ], [
            'required' => 'El dato es requerido',
            'file' => 'El dato debe llegar como un archivo',
            'mime' => 'El archivo debe llegar en formato xls, xlsx, csv',
        ]);

        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate);
        }

        try {
            Excel::import(new ProductImport(auth()->user()->id_useradmin, $this->validaciones), $request->file);

            //Vista de que el import fue correcto
            return redirect()->route('product.index')->with('create', 'Productos Correctamente Importados');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errores = $e->failures();

            return view('product.import_csv', compact('errores'));
        }
    }
}
