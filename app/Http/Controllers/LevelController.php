<?php

namespace App\Http\Controllers;

use App\Http\Requests\LevelStoreRequest;
use App\Http\Requests\LevelUpdateRequest;
use App\Models\Cliente;
use App\Models\Level;
use App\Models\Rol;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Rol::where('id_loginrol', 5)
            ->orwhere('id_loginrol', 6)
            ->orwhere('id_loginrol', 4)
            ->get();
        $levels = Level::with('rol')->orderby('id_level', 'desc')
            ->get();

        return view('Levels.index', compact('levels', 'roles'));
    }

    public function actualizar()
    {
        $clientes = Cliente::all();

        foreach ($clientes as $cliente) {
            $puntos = $cliente->estrellas()->sum('cantidad');
            $cliente->total_puntos_reunidos = $puntos;
            $cliente->update();

            $level = Level::whereRaw('? BETWEEN rango_inicio and meta', [$cliente->total_puntos_reunidos])->first();
            $cliente->FK_id_level = $level->id_level;
            $cliente->update();
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LevelStoreRequest $request)
    {
        $level = Level::orderby('id_level', 'desc')->first();
        $range = range($level->rango_inicio, $level->meta);

        if (array_search($request['rango_inicio'], $range) || array_search($request['meta'], $range)) {
            return redirect()->back()->with('fail', 'Ya hay un nivel con un rango similar');
        } else {
            $level = Level::create($request->all());
            if ($request->hasFile('level-image') && $request->file('level-image')->isValid()) {
                $level->addMediaFromRequest('level-image')->toMediaCollection('level-image');
            }

            return redirect()->back()->with('crear', 'Nivel creado con exito');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Level $level)
    {
        return response()->json([
            'message' => 'nivel disponible',
            'code' => 200,
            'data' => $level,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LevelUpdateRequest $request, Level $level)
    {
        $level->fill($request->all())->save();
        if ($request->hasFile('level-image') && $request->file('level-image')->isValid()) {
            $level->addMediaFromRequest('level-image')->toMediaCollection('level-image');
        }

        $clientes = Cliente::all();

        foreach ($clientes as $cliente) {
            $level = Level::whereRaw('? BETWEEN rango_inicio and meta', [$cliente->total_estrellas])->first();
            // $cliente->FK_id_level = $level->id_level;
            $cliente->update([
                'FK_id_level' => $level->id_level,
            ]);
        }

        return redirect()->back()->with('edit', 'e');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level)
    {
        $level->clearMediaCollection('level-image');
        $level->delete();

        return redirect()->back()->with('destroy', 'producto eliminado correctamente');
    }
}
