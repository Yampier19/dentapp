<?php

namespace App\Http\Controllers;

use App\Http\Requests\temporada\storeRequest;
use App\Http\Requests\temporada\UpdateRequest;
use App\Models\Temporada;

class TemporadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $temporadas = Temporada::orderby('id', 'desc')->get();

        return view('temporada.index', compact('temporadas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(storeRequest $request)
    {
        $temporada = Temporada::create($request->validated());

        return redirect()->back()->with('store', 'temporada creada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function show(Temporada $temporada)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function edit(Temporada $temporada)
    {
        return response()->json([
            'message' => 'Usuario encontrado',
            'code' => 200,
            'data' => $temporada,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Temporada $temporada)
    {
        $temporada->fill($request->all())->save();

        return redirect()->back()->with('update', 'temporada editada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function destroy(Temporada $temporada)
    {
        $temporada->delete();

        return redirect()->back()->with('delete', 'temporada eliminada con exito');
    }
}
