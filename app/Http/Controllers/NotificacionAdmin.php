<?php

namespace App\Http\Controllers;

use App\Models\Admin;
//MODELOS
use App\Models\AdminNotificacion;
use App\Models\Notificaciones;
use App\Models\Rol;
use Illuminate\Support\Facades\DB;

//Helper o Usables

class NotificacionAdmin extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'descripcion.max' => 'El dato no debe ser mayor a 100 carácteres',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:50',
        'descripcion' => 'required|string|max:100',
        'tag' => 'required|string|max:50',
        'stock' => 'required|numeric|min:0',
        'limite_usuario' => 'numeric|min:0',
        'foto' => 'file|mimes:jpg,jpeg,png',
        'numero_estrellas' => 'numeric|min:0',
    ];

    public function all()
    {
        $admins = Admin::orderBy('created_at', 'desc')->take(3)->get();
        $rol = Rol::all();

        return view('notificacion.allnotificacion', compact('admins', 'rol'));
    }

    public function list()
    {
        $admins = Admin::orderBy('created_at', 'desc')->take(3)->get();
        $rol = Rol::all();

        return view('notificacion.list', compact('admins', 'rol'));
    }

    public function index()
    {
        $notifications = auth()->user()->unreadnotification;
        $readnotificaciones = auth()->user()->notificaciones;

        return view('notificacion.index', compact('notifications', 'readnotificaciones'));
    }

    //Marcar como leido una notificación
    public function marcarLeido($id_notificacion)
    {
        DB::beginTransaction();
        try {
            AdminNotificacion::where('FK_id_notificaciones', $id_notificacion)->
                                where('FK_id_useradmin', auth()->user()->id_useradmin)->
                                update(['read_at' => date('Y-m-d H:i:s')]);

            DB::commit();

            return redirect()->route('notificacion.index')->with('edit', 'Notificación fue marcada como leida');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return redirect()->route('notificacion.index')->with('delete', 'La Notificación no pudo ser editada');
        }
    }

    //Marcar como leido todas las notificaciones
    public function marcartodo()
    {
        DB::beginTransaction();
        try {
            AdminNotificacion::where('FK_id_useradmin', auth()->user()->id_useradmin)->
                                update(['read_at' => date('Y-m-d H:i:s')]);

            DB::commit();

            return redirect()->route('notificacion.index')->with('edit', 'Todas las notificaciones fueron marcadas como leidas');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return redirect()->route('notificacion.index')->with('delete', 'La Notificación no pudo ser editada');
        }
    }

    //Marcar como leido todas las notificaciones
    public function marcartres()
    {
        DB::beginTransaction();
        try {
            $notificaciones = AdminNotificacion::where('FK_id_useradmin', auth()->user()->id_useradmin)->whereNull('read_at')->take(3)->get();

            foreach ($notificaciones as $notificacion) {
                $notificacion->read_at = date('Y-m-d H:i:s');
                $notificacion->update();
            }

            DB::commit();

            return redirect()->route('notificacion.index')->with('edit', 'Las ultimas tres notificaciones fueron marcadas como leidas');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return redirect()->route('notificacion.index')->with('delete', 'La Notificación no pudo ser editada');
        }
    }

    //Eliminar el global o la data general de una notificación
    public function deleteGeneral()
    {
        $notificaciones = Notificaciones::all();

        foreach ($notificaciones as $notificacion) {
            if ($notificacion->admins->isEmpty() && $notificacion->clientes->isEmpty()) {
                $notificacion->delete();
            }
        }
    }

    //Eliminar una sola notificación
    public function deleteone($id_notificacion)
    {
        DB::beginTransaction();
        try {
            AdminNotificacion::where('FK_id_notificaciones', $id_notificacion)->
                                where('FK_id_useradmin', auth()->user()->id_useradmin)->delete();

            $notificacion = Notificaciones::where('id_notificaciones', $id_notificacion)->first();
            if ($notificacion->admins->isEmpty() && $notificacion->clientes->isEmpty()) {
                $notificacion->delete();
            }

            DB::commit();

            return redirect()->route('notificacion.index')->with('delete', 'Notificación fue eliminada correctamente');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return redirect()->route('notificacion.index')->with('delete', 'La Notificación no pudo ser eliminada');
        }
    }

    //Eliminar todas las notificaciones
    public function deletetodo()
    {
        DB::beginTransaction();
        try {
            AdminNotificacion::where('FK_id_useradmin', auth()->user()->id_useradmin)->delete();

            $this->deleteGeneral();

            DB::commit();

            return redirect()->route('notificacion.index')->with('delete', 'Todas las notificaciones fueron eliminadas');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return redirect()->route('notificacion.index')->with('delete', 'La Notificación no pudo ser eliminada');
        }
    }
}
