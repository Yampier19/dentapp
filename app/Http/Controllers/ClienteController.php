<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\Helper\Notificacion;
use App\Http\Requests\Cliente\StoreRequest;
use App\Http\Requests\Cliente\UpdateRequest;
use App\Imports\TestImport;
use App\Mail\Rechazado;
use App\Mail\Welcome;
use App\Models\city;
use App\Models\Cliente;
//Helper
use App\Models\ClienteHistorial;
use App\Models\ClienteLogin;
use App\Models\ClientePassword;
use App\Models\Consultorio;
use App\Models\departamentos;
use App\Models\Distribuidor;
//Modelos
use App\Models\document_types;
use App\Models\Level;
use App\Models\Rol;
use App\Services\ClienteService;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
//MAILABLE
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ClienteController extends Controller
{
    /**
     * The service instante
     *
     * @var ClienteService
     */
    private $clienteService;

    /**
     * Create a controller instance
     *
     * @param  ClienteService  $clienteService
     * @return void
     */
    public function __construct(ClienteService $clienteService)
    {
        $this->clienteService = $clienteService;
    }

    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'descripcion.max' => 'El dato no debe ser mayor a 100 carácteres',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:50',
        'descripcion' => 'required|string|max:100',
        'tag' => 'required|string|max:50',
        'stock' => 'required|numeric|min:0',
        'limite_usuario' => 'numeric|min:0',
        'foto' => 'file|mimes:jpg,jpeg,png',
        'numero_estrellas' => 'numeric|min:0',
    ];

    // Listado de Clientes
    public function index()
    {
        $cliente = $this->clienteService->index();
        $document_types = document_types::select('id', 'name')->get();
        $cities = city::select('id', 'name')->get();
        $departaments = departamentos::all();
        $distribuidoras = Distribuidor::select('id_distri', 'distribuidor', 'nombre_publico')->get();
        $roles = Rol::where('id_loginrol', '!=', 1)
            ->where('id_loginrol', '!=', 2)
            ->where('id_loginrol', '!=', 3)
            ->get();

        return view('cliente.index', compact('document_types', 'roles', 'cliente', 'distribuidoras', 'cities', 'departaments'));
    }

    public function ClientList(Request $request)
    {
        $search = $request->search;
        if ($search) {
            $clientes = Cliente::with(['login', 'consultorio', 'level', 'rol'])
                ->where('nombre', 'like', "%$search%")
                ->orwhere('loginrol_id', 6)
                ->orwhere('loginrol_id', 5)
                ->where('loginrol_id', 4)
                ->whereRelation('login', 'estado', 'activo')
                ->orderBy('id_cliente', 'desc')
                ->get();
        } else {
            $clientes = Cliente::with(['login', 'consultorio', 'level', 'rol'])
                ->where('loginrol_id', 6)
                ->orwhere('loginrol_id', 5)
                ->orwhere('loginrol_id', 4)
                ->whereRelation('login', 'estado', 'activo')
                ->orderBy('id_cliente', 'desc')
                ->get();
        }

        return response()->json([
            'message' => 'lista de doctores',
            'code' => 200,
            'data' => $clientes,
        ]);
    }

    public function requestUser()
    {
        $clientes = Cliente::whereRelation('login', 'estado', 'pendiente')
            ->get();

        $inactiveClients = Cliente::whereRelation('login', 'estado', 'inactivo')
            ->get();

        return view('cliente.request-user.index', compact('clientes', 'inactiveClients'));
    }

    public function desabilitarCliente(Cliente $cliente)
    {
        $cliente->load(['login' => function ($query) {
            $query->update([
                'estado' => 'inactivo',
            ]);
        }]);

        return redirect()->back();
    }

    public function desabilitarClienteApi()
    {
        $cliente = auth()->user()->cliente;
        $cliente->load(['login' => function ($query) {
            $query->update([
                'estado' => 'pendiente',
            ]);
        }]);

        try {
            return response()->json([
                'message' => 'Estado actualizado con exito',
                'code' => 200,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'No se actualizo el estado del cliente',
                'code' => 500,
            ]);
        }
    }

    public function restoreUser(Cliente $cliente)
    {
        $cliente->load(['login' => function ($query) {
            $query->update([
                'estado' => 'activo',
            ]);
        }]);

        return redirect()->back();
    }

    //Enviar Vista para el registro de un cliente nuevo
    public function create()
    {
        return view('cliente.create');
    }

    //Guardado de un Cliente
    public function store(StoreRequest $request)
    {
        $this->clienteService->store($request->validated());

        // return redirect()->route('cliente.index')->with('create', 'Cliente Registrado');
        return response()->json([
            'message' => 'Usuario registrado correctamente',
            'code' => 200,
        ]);
    }

    //llevar a la vista para editar el cliente
    public function edit(Cliente $cliente)
    {
        $cliente->load('documentType', 'login');
        $distribuidoras = Distribuidor::select('id_distri', 'distribuidor', 'nombre_publico')->get();
        $document_types = document_types::select('id', 'name')->get();
        $cities = city::select('id', 'name')->get();
        $departaments = departamentos::all();
        $roles = Rol::where('id_loginrol', '!=', 1)
            ->where('id_loginrol', '!=', 2)
            ->where('id_loginrol', '!=', 3)
            ->get();

        return view('cliente.edit', compact('cliente', 'roles', 'document_types', 'distribuidoras', 'cities', 'departaments'))->with('edit', 'usuario editado con exito');
    }

    //editar cliente
    public function update(UpdateRequest $request, Cliente $cliente)
    {
        $this->clienteService->update($cliente, $request->validated());
        $level = Level::whereRaw('? BETWEEN rango_inicio and meta', [$cliente->total_estrellas])->first();
        // $cliente->FK_id_level = $level->id_level;
        $cliente->update([
            'FK_id_level' => $level->id_level,
        ]);

        return Redirect::back()->with('edit', 'El Cliente fue actualizado de forma exitosa');
        // return response()->json([
        //     'message' => 'Cliente actualizado con exito',
        //     'code' => 200,
        // ]);
    }

    //eliminar cliente
    public function destroy(Cliente $cliente)
    {
        $this->clienteService->destroy($cliente);

        return redirect()->route('cliente.index')->with('destroy', 'Cliente Eliminado');
        // return response()->json([
        //     'message' => 'Usuario eliminado',
        //     'code' => 200,
        // ]);
    }

    //Recoger avatar del cliente
    public function getImage($filename = null)
    {
        if ($filename) {
            $exist = Storage::disk('public')->exists('uploads/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('usericon.jpg');
            } else {
                $file = Storage::disk('public')->get('uploads/'.$filename);
            }
        } else {
            $file = Storage::disk('public')->get('usericon.jpg');
        }

        return new Response($file, 200);
    }

    //Listado de interacciones en el sistema
    public function interacciones()
    {
        $interacciones = ClienteHistorial::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('dentapp_cliente.loginrol_id', 6)->get();

        return view('cliente.interacciones', compact('interacciones'));
    }

    //APARTADO DE USUARIO REGISTRADOS
    //Recoger archivo de la factura de un usuario registrado por la APP
    public function getArchive($filename = null)
    {
        $extension = null;

        if ($filename) {
            $extension = pathinfo($filename)['extension'];

            $exist = Storage::disk('public')->exists('facturas/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $extension = null;
                $file = storage_path('app/public/nodisponible.png');
            } else {
                $file = storage_path('app/public/facturas/'.$filename);
            }
        } else {
            $file = storage_path('app/public/nodisponible.png');
        }

        switch ($extension) {
            case 'pdf':
                $extension = 'application/pdf';
                break;
            default:
                $extension = 'image';
                break;
        }

        return Response(file_get_contents($file), 200, [
            'Content-Type' => $extension,
            'Content-Disposition' => 'inline; filename="'.$file.'"',
        ]);
    }

    //Apartado de la tabla de usuarios registrados
    public function listapp()
    {
        $logins = ClienteLogin::whereNotNull('validacion')->orderBy('created_at', 'desc')->get();

        return view('cliente.register.c_register', compact('logins'));
    }

    //Update datos del usuario registrado
    public function updateapp(Request $request)
    {
        $validaciones = [
            'segmento' => 'required|string|max:255',
            'distribuidor' => 'nullable|string|max:255',
            'especialidad' => 'required|string|max:255',
            'telefono' => 'required|string|max:15',
            'idperson' => 'required',
        ];

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $validaciones, $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $data = $request->all();
            $id = $data['idperson'];
            $login = ClienteLogin::findOrFail($id);
            $cliente = $login->cliente;

            //Datos tabla cliente
            if ($cliente->distribuidor != $data['distribuidor']) {
                $cliente->distribuidor = $data['distribuidor'];
                $cliente->last_change = date('Y-m-d');
            }
            $cliente->especialidad = $data['especialidad'];
            $cliente->telefono = $data['telefono'];
            $cliente->save();

            //Datos tabla del login
            $login->segmento = $data['segmento'];
            $login->save();

            DB::commit();

            return Redirect::back()->with('edit', 'El Usuario fue actualizado de forma exitosa');
        } catch (\Exception $e) {
            DB::rollback();

            return Redirect::back()->with('delete', 'El Usuario no pudo ser editado: '.$e->getMessage());
        }
    }

    //Update del estado aceptado o rechazado del usuario registrado por App
    public function statusapp($id, $status)
    {
        $login = ClienteLogin::findOrFail($id);

        DB::beginTransaction();
        try {
            $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $password = substr(str_shuffle($caracteres_permitidos), 0, 8);
            $data['status'] = $status;

            $cliente = $login->cliente;
            $login->status = $data['status'];

            if ($data['status'] == 'aceptado') {
                $data['password'] = $password;
                $login->password = Hash::make($data['password']);
                $login->estado = 'activo';

                //Grabamos el historial de passwords
                $history = new ClientePassword();
                $history->password = Hash::make($data['password']);
                $history->registro = date('Y-m-d');
                $history->FK_id_cliente = $cliente->id_cliente;
                $history->save();

                //Enviamos el correo
                $information = new \stdClass();
                $information->asunto = 'Bienvenido a DentApp';
                $information->nombre = $cliente->nombre.' '.$cliente->apellido;
                $information->correo = $cliente->correo;
                $information->contraseña = $password;
                $information->correo = $cliente->correo;

                try {
                    // Mail::to($cliente->correo)->queue(new Welcome($information));
                } catch (\Exception $e) {
                }
            } elseif ($data['status'] == 'rechazado') {
                $login->estado = 'inactivo';

                //Enviamos el correo
                $information = new \stdClass();
                $information->asunto = 'Rechazado tu Ingreso a Dentapp';
                $information->nombre = $cliente->nombre.' '.$cliente->apellido;
                $information->correo = $cliente->correo;
                $information->contraseña = $password;

                try {
                    Mail::to($cliente->correo)->queue(new Rechazado($information));
                } catch (\Exception $e) {
                }
            }

            $login->update();

            DB::commit();

            return Redirect::back()->with('edit', 'El Usuario fue actualizado de forma exitosa');
        } catch (\Exception $e) {
            DB::rollback();

            return Redirect::back()->with('error', 'El Usuario no pudo ser actualizado');
        }
    }

    //FIN APARTADO DE USUARIOS REGISTRADOS

    //Tomar consultorio
    public function getConsultorio($id_consultorio)
    {
        $consultorio = Consultorio::findOrFail($id_consultorio);

        return view('cliente.consultorio', compact('consultorio'));
    }

    //Tomar detallado del cliente
    public function getDetail(Cliente $cliente)
    {
        return view('cliente.detail', compact('cliente'));
    }

    // pagina de confirmacion de eliminar usuario
    public function confirm(Cliente $cliente)
    {
        return view('cliente.confirm', compact('cliente'));
    }

    //Vista que explica para hacer el import de CSV
    public function import_csv()
    {
        return view('cliente.import');
    }

    //Tabla de importados
    public function tabla_import()
    {
        return view('cliente.tabla_import');
    }

    //Metodo que lee el form enviado y manda la tabla de imports
    public function import_form(Request $request)
    {
        $import = [];
        $header = [];

        foreach ($request->nombre as $key => $nombre) {
            $usuario = [];
            $usuario['nombre'] = $nombre;
            $usuario['apellido'] = $request->apellido[$key];
            $usuario['segmento'] = $request->segmento[$key];
            $usuario['telefono'] = $request->telefono[$key];
            $usuario['correo'] = $request->correo[$key];
            $usuario['especialidad'] = $request->especialidad[$key];
            array_push($import, $usuario);
        }

        return view('cliente.tabla_import', compact('import', 'header'));
    }

    //Metodo que lee el archivo excel y manda la tabla de imports
    public function import(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx,csv',
        ], [
            'required' => 'El dato es requerido',
            'file' => 'El dato debe llegar como un archivo',
            'mime' => 'El archivo debe llegar en formato xls, xlsx, csv',
        ]);

        if ($validate->fails()) {
            return redirect()->route('cliente.import')->withErrors($validate);
        }

        $validaciones['telefono'] = 'required|max:15';
        unset($validaciones['total_estrellas']);
        unset($validaciones['estado']);

        try {
            $collection = Excel::toCollection(new TestImport(auth()->user()->id_useradmin, $validaciones), $request->file);
            $import = [];
            $header = [];

            ! array_key_exists('nombre', $collection[0][0]->toArray()) ? array_push($header, 'nombre') : '';
            ! array_key_exists('apellido', $collection[0][0]->toArray()) ? array_push($header, 'apellido') : '';
            ! array_key_exists('segmento', $collection[0][0]->toArray()) ? array_push($header, 'segmento') : '';
            ! array_key_exists('telefono', $collection[0][0]->toArray()) ? array_push($header, 'telefono') : '';
            ! array_key_exists('correo', $collection[0][0]->toArray()) ? array_push($header, 'correo') : '';
            ! array_key_exists('especialidad', $collection[0][0]->toArray()) ? array_push($header, 'especialidad') : '';

            foreach ($collection[0] as $usuario) {
                array_push($import, $usuario);
            }

            return view('cliente.tabla_import', compact('import', 'header'));
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errores = $e->failures();

            return view('cliente.import', compact('errores'));
        }
    }

    public function import_store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nombre.*' => 'required|string|max:50',
            'apellido.*' => 'required|string|max:50',
            'segmento.*' => 'required|string|max:255',
            'telefono.*' => 'required|string|max:15',
            'correo.*' => 'required|string|distinct|email|max:50|unique:dentapp_clientelogin,email',
            'especialidad.*' => 'required|string|max:50',
        ], $this->validaciones);

        $header = [];
        $import = [];

        foreach ($request->nombre as $key => $nombre) {
            $usuario = [];
            $usuario['nombre'] = $nombre;
            $usuario['apellido'] = $request->apellido[$key];
            $usuario['segmento'] = $request->segmento[$key];
            $usuario['telefono'] = $request->telefono[$key];
            $usuario['correo'] = $request->correo[$key];
            $usuario['especialidad'] = $request->especialidad[$key];
            array_push($import, $usuario);
        }

        if ($validate->fails()) {
            $errors = $validate->errors();

            return view('cliente.tabla_import', compact('import', 'header', 'errors'));
        }

        //Proceso completo ahora si de Importar
        DB::beginTransaction();
        try {
            foreach ($import as $usuario) {
                $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $password = substr(str_shuffle($caracteres_permitidos), 0, 8);

                $cliente = Cliente::create([
                    'nombre' => $usuario['nombre'],
                    'apellido' => $usuario['apellido'],
                    'telefono' => $usuario['telefono'],
                    'correo' => $usuario['correo'],
                    'total_estrellas' => 0,
                    'especialidad' => $usuario['especialidad'],
                ]);

                ClienteLogin::create([
                    'email' => $usuario['correo'],
                    'password' => Hash::make($password),
                    'estado' => 'activo',
                    'segmento' => $usuario['segmento'],
                    'fecha_cambio' => date('Y-m-d'),
                    'FK_id_cliente' => $cliente->id_cliente,
                ]);

                ClientePassword::create([
                    'password' => Hash::make($password),
                    'registro' => date('Y-m-d'),
                    'FK_id_cliente' => $cliente->id_cliente,
                ]);

                //ENVIO DEL CORREO
                $information = new \stdClass();
                $information->asunto = 'Bienvenido a DentApp';
                $information->nombre = $usuario['nombre'].' '.$usuario['apellido'];
                $information->correo = $usuario['correo'];
                $information->contraseña = $password;

                try {
                    Mail::to($usuario['correo'])->queue(new Welcome($information));
                } catch (\Exception $e) {
                }
            }
            DB::commit();

            return redirect()->route('cliente.import')->with('create', 'Los Usuarios fueron creados satisfactoriamente');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->route('cliente.import')->with('delete', 'Los Usuarios no pudieron ser creados: '.$e->getMessage());
        }
    }

    ////////////////////////////////////////////API CLIENTE//////////////////////////

    //Update del Perfil del cliente
    public function ApiUpdatePerfil(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'nombre' => 'required|string|max:50',
                'apellido' => 'required|string|max:50',
                'direccion' => 'string|max:50',
                'distribuidor' => 'string|max:255',
                'telefono' => 'required|string|max:15',
                'especialidad' => 'required|string|max:50',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $data = $request->all();
            if ($data['consultorio']) {
                $validate = Validator::make($request->all(), [
                    'nombre_c' => 'required|string|max:100',
                    'direccion_c' => 'required|string|max:100',
                    'telefono_c' => 'required|string|max:10',
                    'numero_pacientes' => 'required|numeric|min:0',
                ], $this->mensajes_error);

                if ($validate->fails()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Error formato de datos recibidos',
                        'errores' => $validate->errors(),
                    ], 500);
                }
            }

            $cliente = auth()->user()->cliente;

            if ($request->has('distribuidor')) {
                $firstDate = new DateTime($cliente->last_change);
                $secondDate = new DateTime(date('Y-m-d'));
                $diferencia = $firstDate->diff($secondDate);

                if ($diferencia->days > 60) {
                    $data['newdistribuidor'] = $data['distribuidor'];
                    $data['distribuidor'] = $cliente->distribuidor;
                    $data['last_change'] = date('Y-m-d');
                }

                $data['distribuidor'] = $cliente->distribuidor;
            }

            $cliente->update($data);

            //Grabado o Update de consultorio
            if ($data['consultorio']) {
                if ($cliente->consultorio) {
                    $consultorio = $cliente->consultorio;
                    $consultorio->nombre = $data['nombre_c'];
                    $consultorio->direccion = $data['direccion_c'];
                    $consultorio->telefono = $data['telefono_c'];
                    $consultorio->numero_pacientes = $data['numero_pacientes'];
                    $consultorio->FK_id_cliente = $cliente->id_cliente;
                    $consultorio->update();
                } elseif ($data['consultorio']) {
                    $consultorio = new Consultorio();
                    $consultorio->nombre = $data['nombre_c'];
                    $consultorio->direccion = $data['direccion_c'];
                    $consultorio->telefono = $data['telefono_c'];
                    $consultorio->numero_pacientes = $data['numero_pacientes'];
                    $consultorio->FK_id_cliente = $cliente->id_cliente;
                    $consultorio->save();
                }
            }

            $data = [
                'nombre' => 'Update Cliente',
                'tipo' => 'Update',
                'descripcion' => 'Cliente Editado: '.$cliente->nombre.' '.$cliente->apellido,
            ];

            Notificacion::instance()->store($data, 'usuarios');

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Edición de datos del Perfil';
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Perfil Actualizado',
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al editar Perfil',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
