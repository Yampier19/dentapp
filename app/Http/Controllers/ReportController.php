<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\ClienteHistorial;
//MODELOS
use App\Models\ClienteLogin;
use App\Models\ClientePassword;
use App\Models\Estrella;
use App\Models\Factura;
use App\Models\FacturaProductoEspecial;
use App\Models\PremioCanjeado;
use App\Models\TriviaCliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function tablas()
    {
        return view('report.tablas');
    }

    public function nice_number($n)
    {
        // first strip any formatting;
        $n = (0 + str_replace(',', '', $n));

        // is this a number?
        if (! is_numeric($n)) {
            return false;
        }

        // now filter it;
        if ($n > 1000000000000) {
            return round(($n / 1000000000000), 2).' T';
        } elseif ($n > 1000000000) {
            return round(($n / 1000000000), 2).' B';
        } elseif ($n > 1000000) {
            return round(($n / 1000000), 2).' M';
        } elseif ($n > 1000) {
            return round(($n / 1000), 2).' K';
        }

        return number_format($n);
    }

    public function graficas(Request $request)
    {

        //VALORES QUE LLEGAN DEL FILTRADO
        $filter = false;
        $segmento = $request->has('segmento') ? $request->segmento : null;
        $nivel = $request->has('nivel') ? $request->nivel : null;
        $mes = $request->has('mes') ? $request->mes : null;
        if ($segmento || $nivel || $mes) {
            $filter = true;
        }

        //BLOQUES DE LOS CONTADORES DEL INICIO
        $users = Cliente::where('loginrol_id', 6)->get();
        $registros = Cliente::where('loginrol_id', 7)->get();
        /* $registros = ClientePassword::selectRaw('COUNT(id_history_password) as total')
                                    ->groupBy('FK_id_cliente')
                                    ->having('total', '>=', 1)->get(); */

        $interacciones = ClienteHistorial::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('loginrol_id', 6)->get(); //ClienteHistorial::all();

        $factura = Factura::selectRaw('dentapp_factura.estado as estado, monto_total')
            ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('loginrol_id', 6)->get(); //Factura::all();

        $trivias = TriviaCliente::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('loginrol_id', 6)->get();
        //TriviaCliente::all();

        $canjeados = PremioCanjeado::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('loginrol_id', 6)->get();

        //GRAFICA DE COLUMNAS DE PRODUCTOS ESPECIALES
        $productos = FacturaProductoEspecial::selectRaw('SUM(dentapp_factura_producto_especial.cantidad) as total, producto')
            ->join('dentapp_factura', 'dentapp_factura.id_factura', '=', 'dentapp_factura_producto_especial.FK_id_factura')
            ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_factura.FK_id_cliente')
            ->where('estado', '=', 'aceptado')->where('loginrol_id', 6)
            ->groupBy('producto')->get();

        //GRAFICA DE PIE (ESPECIALIDADES)
        $pie = Cliente::selectRaw('COUNT(id_cliente) as total, especialidad')
            ->where('loginrol_id', 6)->groupBy('especialidad')->get();

        //GRAFICA DE PERSONAS EN CADA NIVEL
        $bar = Cliente::select(DB::raw('COUNT(*) as total, CASE
                                        WHEN total_reunido BETWEEN 0 AND 1499999 THEN "Bronce"
                                        WHEN total_reunido BETWEEN 1500000 AND 1949999 THEN "Plata"
                                        WHEN total_reunido BETWEEN 1950000 AND 2849999 THEN "Oro"
                                        WHEN total_reunido BETWEEN 2850000 AND 6599999 THEN "Diamante"
                                        WHEN total_reunido >= 6600000 THEN "Zafiro"
                                        ELSE "Ninguno"
                                    END as nivel'))->where('loginrol_id', 6)
            ->groupBy(DB::raw('CASE
                                    WHEN total_reunido BETWEEN 0 AND 1499999 THEN "Bronce"
                                    WHEN total_reunido BETWEEN 1500000 AND 1949999 THEN "Plata"
                                    WHEN total_reunido BETWEEN 1950000 AND 2849999 THEN "Oro"
                                    WHEN total_reunido BETWEEN 2850000 AND 6599999 THEN "Diamante"
                                    WHEN total_reunido >= 6600000 THEN "Zafiro"
                                    ELSE "Ninguno"
                                    END'))->get();

        //GRAFICA DE PASTEL SEGMENTO DE USUARIOS
        $pastel = ClienteLogin::selectRaw('COUNT(*) as total, segmento')
            ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('loginrol_id', 6)->groupBy('segmento')->get();

        //GRAFICA DE LINEA | USUARIOS FACTURAS VS FACTURAS TOTAL (MES)
        $lineal = Factura::selectRaw('SUM(puntos) as total_puntos, COUNT(DISTINCT FK_id_cliente) as total_user, COUNT(id_factura) as total_fact, MONTH(fecha_registro) as mes')
            ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('loginrol_id', 6)->groupBy('mes')->get();

        //GRAFICA DE AREA | PUNTOS DE FACTRAS VS PUNTOS DE PRODUCTOS ESPECIALES
        $especiales = FacturaProductoEspecial::selectRaw('SUM(dentapp_factura_producto_especial.puntos) as total_puntos, MONTH(fecha_registro) as mes')
            ->join('dentapp_factura', 'dentapp_factura.id_factura', '=', 'dentapp_factura_producto_especial.FK_id_factura')
            ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_factura.FK_id_cliente')
            ->where('estado', '=', 'aceptado')->where('dentapp_cliente.loginrol_id', 6)
            ->groupBy('mes')->get();

        //GRAFICA TREEMAP | VALOR DE FACTURAS CARGADAS POR ESPECIALIDADES
        $treemap = Cliente::selectRaw('SUM(total_reunido) as total, especialidad')
            ->where('loginrol_id', 6)
            ->groupBy('especialidad')->get();
        /* selectRaw('SUM(monto_total) as total, especialidad')
        ->join('dentapp_factura', 'dentapp_factura.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')->
        where('dentapp_factura.estado','=',"aceptado")->
        groupBy('especialidad')->get(); */

        //GRAFICA DE STACKED BAR HORIZONTAL | PUNTOS GANADOS POR MES DE CADA FORMA
        $stacked = Estrella::select(DB::raw("SUM(cantidad) as total, MONTH(fecha_registro) as mes,
                        CASE
                            WHEN descripcion LIKE 'Puntos por Facturación%' THEN 'Factura'
                            WHEN descripcion LIKE 'Puntos por Encuesta%' THEN 'Encuesta'
                            WHEN descripcion LIKE 'Puntos por Trivia%' THEN 'Trivia'
                            ELSE 'Ninguno'
                        END as type"))
            ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('dentapp_cliente.loginrol_id', 6)->groupBy('type', 'mes')->get();

        //GRAFICA COLUMNAS | RELACIÓN DE LOS PUNTOS OTORGADOS GLOBALIZADO DE LOS DISTINTOS CASOS
        $column = Estrella::select(DB::raw("SUM(cantidad) as total,
                        CASE
                            WHEN descripcion LIKE 'Puntos por Facturación%' THEN 'Factura'
                            WHEN descripcion LIKE 'Puntos por Encuesta%' THEN 'Encuesta'
                            WHEN descripcion LIKE 'Puntos por Trivia%' THEN 'Trivia'
                            ELSE 'Ninguno'
                        END as type"))
            ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
            ->where('dentapp_cliente.loginrol_id', 6)->groupBy('type')->get();

        return view('report.graficas', [
            /* TOTALES */
            'users' => $users->count(),
            'registros' => $registros->count(),
            'interacciones' => $interacciones->count(),
            'retos' => $trivias->count(),
            'factura' => $factura->count(),
            //'monto' => $this->nice_number($factura->where('estado', 'aceptado')->sum('monto_total')),
            'monto' => number_format($factura->where('estado', 'aceptado')->sum('monto_total'), 2, ',', '.'),
            'canjeados' => $canjeados->count(),
            /* GRAFICAS */
            'productos' => $productos,
            'pie' => $pie,
            'bar' => $bar,
            'pastel' => $pastel,
            'lineal' => $lineal,
            'especiales' => $especiales,
            'treemap' => $treemap,
            'stacked' => $stacked,
            'column' => $column,
            /* VARIABLES DE FILTRO */
            'filter' => $filter,
            'segmento' => $segmento,
            'nivel' => $nivel,
            'mes' => $mes,
        ]);
    }

    public function graficasFilter(Request $request)
    {
        //VALORES QUE LLEGAN DEL FILTRADO
        $filter = false;
        $segmento = $request->has('segmento') ? $request->segmento : null;
        $mes = $request->has('mes') ? $request->mes : null;
        $nivel = $request->has('nivel') ? $request->nivel : null;
        $max = 10 * 100000000;
        $min = 0;

        if ($request->has('nivel')) {
            switch ($request->nivel) {
                case 'bronce': $max = 1500000;
                    break;
                case 'plata': $min = 1500000;
                    $max = 1950000;
                    break;
                case 'oro': $min = 1950000;
                    $max = 2850000;
                    break;
                case 'diamante': $min = 2850000;
                    $max = 6600000;
                    break;
                case 'zafiro': $min = 6600000;
                    break;
            }
        }

        if ($segmento || $nivel || $mes) {
            $filter = true;
        } else {
            return redirect()->route('report.graficas');
        }

        //BLOQUES DE LOS CONTADORES DEL INICIO
        //BUSQUEDA SIN MES
        if (! $mes) {
            $users = Cliente::join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])->get();

            $registros = ClientePassword::selectRaw('COUNT(id_history_password) as total')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_history_password.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 7)
                ->whereBetween('total_reunido', [$min, $max])
                ->groupBy('dentapp_history_password.FK_id_cliente')
                ->having('total', '>=', 1)->get();  //ESTO DEBERIA SER 2

            $interacciones = ClienteHistorial::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])->get();

            $factura = Factura::selectRaw('dentapp_factura.estado as estado, monto_total')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])->get();

            $trivias = TriviaCliente::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])->get();

            $canjeados = PremioCanjeado::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])->get();
        }
        //BUSQUEDA CON MES
        else {
            $users = Cliente::selectRaw('MONTH(dentapp_cliente.created_at) as mes')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'id_cliente')
                ->where('loginrol_id', 6)
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_cliente.created_at', $mes)->get();

            $registros = ClientePassword::selectRaw('COUNT(id_history_password) as total')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_history_password.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 7)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_clientelogin.created_at', $mes)
                ->groupBy('dentapp_history_password.FK_id_cliente')
                ->having('total', '>=', 1)->get(); //ESTO DEBERIA SER 2

            $interacciones = ClienteHistorial::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_clientelogin.created_at', $mes)->get();

            $factura = Factura::selectRaw('dentapp_factura.estado as estado, monto_total')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_factura.created_at', $mes)->get();

            $trivias = TriviaCliente::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_trivia_cliente.created_at', $mes)->get();

            $canjeados = PremioCanjeado::join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_premios_canjeado.created_at', $mes)->get();
        }

        //FILTRADO DE LAS GRAFICAS
        //FILTRO SIN MES
        if (! $mes) {
            //GRAFICA DE COLUMNAS DE PRODUCTOS ESPECIALES
            $productos = FacturaProductoEspecial::selectRaw('SUM(dentapp_factura_producto_especial.cantidad) as total, producto')
                ->join('dentapp_factura', 'dentapp_factura.id_factura', '=', 'dentapp_factura_producto_especial.FK_id_factura')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_factura.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->where('dentapp_factura.estado', '=', 'aceptado')
                ->groupBy('producto')->get();

            //GRAFICA DE PIE (ESPECIALIDADES)
            $pie = Cliente::selectRaw('COUNT(id_cliente) as total, especialidad')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->groupBy('especialidad')->get();

            //GRAFICA DE PERSONAS EN CADA NIVEL
            $bar = Cliente::select(DB::raw('COUNT(*) as total, CASE
                                WHEN total_reunido BETWEEN 0 AND 1499999 THEN "Bronce"
                                WHEN total_reunido BETWEEN 1500000 AND 1949999 THEN "Plata"
                                WHEN total_reunido BETWEEN 1950000 AND 2849999 THEN "Oro"
                                WHEN total_reunido BETWEEN 2850000 AND 6599999 THEN "Diamante"
                                WHEN total_reunido >= 6600000 THEN "Zafiro"
                                ELSE "Ninguno"
                            END as nivel'))
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('loginrol_id', 6)
                    //->whereBetween('total_reunido', [$min, $max])
                ->groupBy('nivel')->get();

            //GRAFICA DE PASTEL SEGMENTO DE USUARIOS
            $pastel = ClienteLogin::selectRaw('COUNT(*) as total, segmento')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                    //->where('segmento', 'like', $segmento ? $segmento : "%%")
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->groupBy('segmento')->get();

            //GRAFICA DE LINEA | USUARIOS FACTURAS VS FACTURAS TOTAL (MES)
            $lineal = Factura::selectRaw('SUM(puntos) as total_puntos, COUNT(DISTINCT dentapp_factura.FK_id_cliente) as total_user, COUNT(id_factura) as total_fact, MONTH(fecha_registro) as mes')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->groupBy('mes')->get();

            //GRAFICA DE AREA | PUNTOS DE FACTURAS VS PUNTOS DE PRODUCTOS ESPECIALES
            $especiales = FacturaProductoEspecial::selectRaw('SUM(dentapp_factura_producto_especial.puntos) as total_puntos, MONTH(fecha_registro) as mes')
                ->join('dentapp_factura', 'dentapp_factura.id_factura', '=', 'dentapp_factura_producto_especial.FK_id_factura')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_factura.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->where('dentapp_factura.estado', '=', 'aceptado')
                ->groupBy('mes')->get();

            //GRAFICA TREEMAP | VALOR DE FACTURAS CARGADAS POR ESPECIALIDADES
            $treemap = Cliente::selectRaw('SUM(total_reunido) as total, especialidad')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->groupBy('especialidad')->get();

            //GRAFICA DE STACKED BAR HORIZONTAL | PUNTOS GANADOS POR MES DE CADA FORMA
            $stacked = Estrella::select(DB::raw("SUM(cantidad) as total, MONTH(fecha_registro) as mes,
                        CASE
                            WHEN descripcion LIKE 'Puntos por Facturación%' THEN 'Factura'
                            WHEN descripcion LIKE 'Puntos por Encuesta%' THEN 'Encuesta'
                            WHEN descripcion LIKE 'Puntos por Trivia%' THEN 'Trivia'
                            ELSE 'Ninguno'
                        END as type"))
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_estrellas.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->groupBy('type', 'mes')->get();

            //GRAFICA COLUMNAS | RELACIÓN DE LOS PUNTOS OTORGADOS GLOBALIZADO DE LOS DISTINTOS CASOS
            $column = Estrella::select(DB::raw("SUM(cantidad) as total,
                        CASE
                            WHEN descripcion LIKE 'Puntos por Facturación%' THEN 'Factura'
                            WHEN descripcion LIKE 'Puntos por Encuesta%' THEN 'Encuesta'
                            WHEN descripcion LIKE 'Puntos por Trivia%' THEN 'Trivia'
                            ELSE 'Ninguno'
                        END as type"))
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_estrellas.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->groupBy('type')->get();
        }
        //FILTRO POR MES
        else {
            //GRAFICA DE COLUMNAS DE PRODUCTOS ESPECIALES
            $productos = FacturaProductoEspecial::selectRaw('SUM(dentapp_factura_producto_especial.cantidad) as total, producto')
                ->join('dentapp_factura', 'dentapp_factura.id_factura', '=', 'dentapp_factura_producto_especial.FK_id_factura')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_factura.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->where('dentapp_factura.estado', '=', 'aceptado')
                ->whereMonth('dentapp_factura.created_at', $mes)
                ->groupBy('producto')->get();

            //GRAFICA DE PIE (ESPECIALIDADES)
            $pie = Cliente::selectRaw('COUNT(id_cliente) as total, especialidad')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_cliente.created_at', $mes)
                ->groupBy('especialidad')->get();

            //GRAFICA DE PERSONAS EN CADA NIVEL
            $bar = Cliente::select(DB::raw('COUNT(*) as total, CASE
                                WHEN total_reunido BETWEEN 0 AND 1499999 THEN "Bronce"
                                WHEN total_reunido BETWEEN 1500000 AND 1949999 THEN "Plata"
                                WHEN total_reunido BETWEEN 1950000 AND 2849999 THEN "Oro"
                                WHEN total_reunido BETWEEN 2850000 AND 6599999 THEN "Diamante"
                                WHEN total_reunido >= 6600000 THEN "Zafiro"
                                ELSE "Ninguno"
                            END as nivel'))
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('loginrol_id', 6)
                    //->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_cliente.created_at', $mes)
                ->groupBy('nivel')->get();

            //GRAFICA DE PASTEL SEGMENTO DE USUARIOS
            $pastel = ClienteLogin::selectRaw('COUNT(*) as total, segmento')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                    //->where('segmento', 'like', $segmento ? $segmento : "%%")
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_cliente.created_at', $mes)
                ->groupBy('segmento')->get();

            //GRAFICA DE LINEA | USUARIOS FACTURAS VS FACTURAS TOTAL (MES)
            $lineal = Factura::selectRaw('SUM(puntos) as total_puntos, COUNT(DISTINCT dentapp_factura.FK_id_cliente) as total_user, COUNT(id_factura) as total_fact, MONTH(fecha_registro) as mes')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_factura.created_at', $mes)
                ->groupBy('mes')->get();

            //GRAFICA DE AREA | PUNTOS DE FACTURAS VS PUNTOS DE PRODUCTOS ESPECIALES
            $especiales = FacturaProductoEspecial::selectRaw('SUM(dentapp_factura_producto_especial.puntos) as total_puntos, MONTH(fecha_registro) as mes')
                ->join('dentapp_factura', 'dentapp_factura.id_factura', '=', 'dentapp_factura_producto_especial.FK_id_factura')
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_factura.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_factura.created_at', $mes)
                    //->where('dentapp_factura.estado','=',"aceptado")
                ->groupBy('mes')->get();

            //GRAFICA TREEMAP | VALOR DE FACTURAS CARGADAS POR ESPECIALIDADES
            $treemap = Cliente::selectRaw('SUM(total_reunido) as total, especialidad')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('dentapp_cliente.created_at', $mes)
                ->groupBy('especialidad')->get();

            //GRAFICA DE STACKED BAR HORIZONTAL | PUNTOS GANADOS POR MES DE CADA FORMA
            $stacked = Estrella::select(DB::raw("SUM(cantidad) as total, MONTH(fecha_registro) as mes,
                        CASE
                            WHEN descripcion LIKE 'Puntos por Facturación%' THEN 'Factura'
                            WHEN descripcion LIKE 'Puntos por Encuesta%' THEN 'Encuesta'
                            WHEN descripcion LIKE 'Puntos por Trivia%' THEN 'Trivia'
                            ELSE 'Ninguno'
                        END as type"))
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_estrellas.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->whereMonth('fecha_registro', $mes)
                ->groupBy('type', 'mes')->get();

            //GRAFICA COLUMNAS | RELACIÓN DE LOS PUNTOS OTORGADOS GLOBALIZADO DE LOS DISTINTOS CASOS
            $column = Estrella::select(DB::raw("SUM(cantidad) as total,
                        CASE
                            WHEN descripcion LIKE 'Puntos por Facturación%' THEN 'Factura'
                            WHEN descripcion LIKE 'Puntos por Encuesta%' THEN 'Encuesta'
                            WHEN descripcion LIKE 'Puntos por Trivia%' THEN 'Trivia'
                            ELSE 'Ninguno'
                        END as type"))
                ->join('dentapp_cliente', 'dentapp_cliente.id_cliente', '=', 'dentapp_estrellas.FK_id_cliente')
                ->join('dentapp_clientelogin', 'dentapp_clientelogin.FK_id_cliente', '=', 'dentapp_cliente.id_cliente')
                ->where('segmento', 'like', $segmento ? $segmento : '%%')
                ->where('dentapp_cliente.loginrol_id', 6)
                ->whereBetween('total_reunido', [$min, $max])
                ->groupBy('type')->get();
        }

        return view('report.graficas', [
            /* TOTALES */
            'users' => $users->count(),
            'registros' => $registros->count(),
            'interacciones' => $interacciones->count(),
            'retos' => $trivias->count(),
            'factura' => $factura->count(),
            //'monto' => $this->nice_number($factura->where('estado', 'aceptado')->sum('monto_total')),
            'monto' => number_format($factura->where('estado', 'aceptado')->sum('monto_total'), 2, ',', '.'),
            'canjeados' => $canjeados->count(),
            /* GRAFICAS */
            'productos' => $productos,
            'pie' => $pie,
            'bar' => $bar,
            'pastel' => $pastel,
            'lineal' => $lineal,
            'especiales' => $especiales,
            'treemap' => $treemap,
            'stacked' => $stacked,
            'column' => $column,
            /* VARIABLES DE FILTRO */
            'filter' => $filter,
            'segmento' => $segmento,
            'nivel' => $nivel,
            'mes' => $mes,
        ]);
    }
}
