<?php

namespace App\Http\Controllers;

use App\Events\BNotificacion;
use App\Helper\Notificacion;
use App\Mail\encuesta;
use App\Models\Capacitacion;
use App\Models\CapacitacionCliente;
//MODELOS
use App\Models\Cliente;
use App\Models\ClienteHistorial;
use App\Models\ClienteLogin;
use App\Models\Trivia;
use App\Models\TriviaCambio;
use App\Models\TriviaCliente;
use App\Models\TriviaClienteRespuesta;
use App\Models\TriviaPregunta;
use App\Models\TriviaRespuesta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
//Helper o Usables
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class EncuestaController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'boolean' => 'Debes enviar el dato como true (1) o false (0)',
        'fecha_inicio.after' => 'Selecciona una fecha mayor de hoy',
        'fecha_fin.after' => 'Selecciona una fecha superior a la de inicio',
    ];

    //////////////////////////////////////////////////////TRIVIAS Y ENCUESTAS GENERAL///////////////////////////////////

    //Listado de Trivias
    public function index()
    {
        $trivias = Trivia::where('tipo', 'trivia')->orderBy('created_at', 'desc')->get();

        return view('trivia.index', compact('trivias'));
    }

    public function clienteDetail($id_cliente = null)
    {
        try {
            $trivias = TriviaCliente::where('FK_id_cliente', $id_cliente)->with('respuestas')->get();

            if ($trivias->count() <= 0) {
                return response()->json([
                    'message' => 'El Cliente no posee encuestas realizadas',
                ]);
            }

            $cuestionarios = collect();

            foreach ($trivias as $trivia) {
                if ($trivia->trivia) {
                    if ($trivia->trivia->tipo == 'trivia') {
                        continue;
                    }
                } else {
                    continue;
                }

                $cuestionarios->add($trivia);
            }

            return response()->json([
                'trivias' => $cuestionarios,
                'message' => 'Encuestas del Cliente',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error al consultar encuestas: '.$e->getMessage(),
            ]);
        }
    }

    public function list_user()
    {
        $clientes = Cliente::all();

        return view('encuesta.list_user', compact('clientes'));
    }

    //Listado de Encuestas
    public function listencuestas()
    {
        $trivias = Trivia::where('tipo', 'home')->orderBy('created_at', 'desc')->get();

        return view('encuesta.index', compact('trivias'));
    }

    //Listado de Trivias Cambiadas (trazabilidad)
    public function listcambios()
    {
        $cambios = TriviaCambio::orderBy('created_at', 'asc')->get();
    }

    //Enviar Vista para el registro de una trivia
    public function create($id_capacitacion)
    {
        $trivia = Trivia::where('FK_id_capacitacion', $id_capacitacion)->first();

        if ($trivia) {
            return redirect()->route('training.index')->with('delete', 'La capacitación seleccionada ya tiene trivia');
        }

        return view('trivia.create', ['id_capacitacion' => $id_capacitacion]);
    }

    //Enviar Vista para el registro de una encuesta
    public function createEncuesta()
    {
        return view('encuesta.create');
    }

    public function sendEmails($information)
    {
        try {
            $clientes = ClienteLogin::where('estado', 'activo')->get();

            foreach ($clientes as $cliente) {
                try {
                    $information->nombre = $cliente->nombre;
                    Mail::to($cliente->email)->queue(new encuesta($information));
                    sleep(5);
                } catch (\Exception $e) {
                }
            }
        } catch (\Exception $e) {
            return;
        }
    }

    //Guardado de una trivia | encuesta
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'nombre' => 'required|string|max:50',
                'descripcion' => 'required|string|max:50',
                'puntuacion' => 'required|numeric|min:0',
                'estado' => 'boolean',
                'fecha_inicio' => 'required|date|after:yesterday',
                'fecha_fin' => 'required|date|after:fecha_inicio',
                'FK_id_capacitacion' => 'required',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $capacitacion = Capacitacion::find($request->input('FK_id_capacitacion'));

            if (! $capacitacion && ! $request->has('tipo')) {
                return redirect()->route('training.index')->with('delete', 'La trivia no puede registrarse debido a que la capacitación no existe');
            }

            $data = $request->all();

            if ($request->has('tipo')) {
                $data['FK_id_capacitacion'] = null;
            }
            $trivia = Trivia::create($data);

            $data['accion'] = 'Inserción';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_trivia'] = $trivia->id_trivia;

            TriviaCambio::create($data);

            if ($request->has('tipo')) {
                $data = [
                    'nombre' => 'Store de Encuesta',
                    'tipo' => 'Insert',
                    'descripcion' => 'Se genero Encuesta: '.$trivia->nombre,
                ];
            } else {
                $data = [
                    'nombre' => 'Store de Trivia',
                    'tipo' => 'Insert',
                    'descripcion' => 'Se genero Trivia en la capacitación: '.$trivia->capacitacion->nombre,
                ];
            }

            Notificacion::instance()->store($data, 'trivia');

            if ($request->has('tipo')) {
                $title = '!NUEVA ENCUESTA DISPONIBLE!';
                $text = 'Encuesta de Home disponible: '.$trivia->nombre;
            } else {
                $title = '!NUEVA TRIVIA DISPONIBLE!';
                $text = 'Nueva trivia disponible para capacitación: '.$trivia->capacitacion->nombre;
                event(new BNotificacion($text, $title));
            }
            //event(new BNotificacion($text, $title));

            DB::commit();
            if ($request->has('tipo')) {
                $information = new \stdClass();
                $information->asunto = '!NUEVA ENCUESTA DISPONIBLE!';
                // $this->sendEmails($information);

                return redirect()->route('encuesta.detail', $trivia->id_trivia)->with('create', 'Encuesta Registrada: Ahora añade preguntas a la misma');
            } else {
                $information = new \stdClass();
                $information->asunto = '!NUEVA TRIVIA DISPONIBLE!';
                // $this->sendEmails($information);

                return redirect()->route('trivia.detail', $trivia->id_trivia)->with('create', 'Trivia Registrada: Ahora añade preguntas y respuestas a la misma');
            }
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Trivia no pudo ser Registrada');
        }
    }

    //Vista para confirmar si de verdad quiere eliminar la trivia
    public function confirm($id_trivia)
    {
        $trivia = Trivia::findOrFail($id_trivia);

        if ($trivia->tipo == 'trivia') {
            return view('trivia.confirmTrivia', compact('trivia'));
        } else {
            return view('encuesta.confirmEncuesta', compact('trivia'));
        }
    }

    //Eliminar una trivia
    public function destroy($id_trivia)
    {
        DB::beginTransaction();
        try {
            $trivia = Trivia::find($id_trivia);

            if (! $trivia) {
                return redirect()->route('trivia.index')->with('edit', 'La trivia no fue encontrada');
            }

            TriviaCambio::where('FK_id_trivia', $trivia->id_trivia)->update(['FK_id_trivia' => null]);

            //Elmino Respuestas de la trivia y de los clientes + las pregunta
            foreach ($trivia->preguntas as $pregunta) {
                TriviaRespuesta::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                //TriviaClienteRespuesta::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                $pregunta->delete();
            }

            //Dejo el cliente trivia con la información y pongo nula la relación
            TriviaCliente::where('FK_id_trivia', $trivia->id_trivia)->update(['FK_id_trivia' => null]);

            $trivia->delete();

            $data = $trivia->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;

            TriviaCambio::create($data);

            if ($trivia->tipo == 'home') {
                $data = [
                    'nombre' => 'Delete de Encuesta',
                    'tipo' => 'Delete',
                    'descripcion' => 'Eliminación de Encuesta: '.$trivia->nombre,
                ];
            } else {
                $data = [
                    'nombre' => 'Delete de Trivia',
                    'tipo' => 'Delete',
                    'descripcion' => 'Eliminación de Trivia de Capacitacion: '.$trivia->capacitacion->nombre,
                ];
            }

            Notificacion::instance()->store($data, 'trivia');

            DB::commit();
            if ($trivia->tipo == 'home') {
                return redirect()->route('encuesta.index')->with('delete', 'Encuesta Eliminada');
            // return response()->json([
                //     'message' => 'Trivia eliminada con exito',
                //     'code' => 200,
            // ]);
            } else {
                return redirect()->route('trivia.index')->with('delete', 'Trivia Eliminada');
                // return response()->json([
                //     'message' => 'Trivia eliminada con exito',
                //     'code' => 200,
                // ]);
            }
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return Redirect::back()->with('error', 'La Trivia no pudo ser eliminada');
        }
    }

    //Enviar la Vista para editar una Trivia
    public function edit($id_trivia)
    {
        $trivia = Trivia::findOrFail($id_trivia);

        $trainings = null;
        if (! $trivia->capacitacion && $trivia->tipo == 'trivia') {
            $trainings = Capacitacion::whereNotIn(
                'id_capacitacion',
                function ($query) {
                    $query->select('FK_id_capacitacion')
                        ->from(with(new Trivia)->getTable())
                        ->whereNotNull('FK_id_capacitacion');
                }
            )->get();
        }

        if ($trivia->tipo == 'trivia') {
            return view('trivia.editTrivia', compact('trivia', 'trainings'));
        } else {
            return view('encuesta.editEncuesta', compact('trivia'));
        }
    }

    //Update de los datos de una Trivia
    public function update(Request $request, $id_trivia)
    {
        $validaciones = [
            'nombre' => 'required|string|max:50',
            'descripcion' => 'required|string|max:255',
            'puntuacion' => 'required|numeric|min:0',
            'estado' => 'boolean',
            'fecha_inicio' => 'date',
            'fecha_fin' => 'date|after:fecha_inicio',
        ];

        if ($request->has('selectPregunta')) {
            if ($request->selectPregunta == 1) {
                $validaciones['multiple'] = 'required';
            }
        }

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $validaciones, $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate)->withInput();
            }

            $trivia = Trivia::findOrFail($id_trivia);
            $data = $request->all();

            if ($trivia->preguntas->isEmpty()) {
                $data['estado'] = 0;
            }

            $trivia->update($data);

            $data['accion'] = 'Update';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_trivia'] = $id_trivia;

            TriviaCambio::create($data);

            /* GUARDAMOS LAS PREGUNTAS CON SUS RESPUESTAS */
            if ($request->has('multiple') && $request->has('selectPregunta')) {
                if ($request->selectPregunta == 1) {
                    //Elmino Preguntas y Respuestas de la trivia
                    foreach ($trivia->preguntas as $pregunta) {
                        TriviaRespuesta::where('FK_id_pregunta', $pregunta->id_pregunta)->delete();
                        $pregunta->delete();
                    }

                    foreach ($request->multiple as $preguntas) {
                        //GRABAMOS PREGUNTA
                        $pregunta = new TriviaPregunta();
                        $pregunta->informacion = $preguntas['pregunta'];
                        $pregunta->estado = 1;
                        $pregunta->FK_id_trivia = $trivia->id_trivia;
                        $pregunta->save();

                        unset($preguntas['pregunta']);
                        //GRABAMOS SUS RESPUESTAS
                        for ($i = 0; $i < count($preguntas); $i++) {
                            $respuesta = new TriviaRespuesta();
                            $respuesta->informacion = $preguntas[$i]['texto'];
                            if (array_key_exists('correcta', $preguntas[$i])) {
                                $respuesta->correcta = 1;
                            } else {
                                $respuesta->correcta = 0;
                            }
                            $respuesta->FK_id_pregunta = $pregunta->id_pregunta;
                            $respuesta->save();
                        }
                    }
                }
            }
            /* FIN GUARDAR PREGUNTA Y RESPUESTA */

            if ($request->has('tipo')) {
                $data = [
                    'nombre' => 'Update de Encuesta',
                    'tipo' => 'Update',
                    'descripcion' => 'Edición de Encuesta: '.$trivia->nombre,
                ];
            } else {
                $data = [
                    'nombre' => 'Update de Trivia',
                    'tipo' => 'Update',
                    'descripcion' => 'Edición de Trivia para Capacitación: '.$trivia->capacitacion->nombre,
                ];
            }

            Notificacion::instance()->store($data, 'trivia');

            DB::commit();
            if ($request->has('tipo')) {
                return redirect()->route('encuesta.index')->with('edit', 'Encuesta Actualizada');
            } else {
                return redirect()->route('trivia.index')->with('edit', 'Trivia Actualizada');
            }
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Trivia no pudo ser Editada');
        }
    }

    ///////////////////////////////////////API CLIENTE//////////////////////////////////////////

    //Ver las trivias disponibles del cliente
    public function ApiListado($registros = 3)
    {
        try {
            //Debo probar esta consulta
            $trivias = Trivia::whereIn(
                'FK_id_capacitacion',
                function ($query) {
                    $query->select('FK_id_capacitacion')
                        ->from(with(new CapacitacionCliente)->getTable())
                        ->whereIn('FK_id_cliente', [auth()->user()->FK_id_cliente])
                        ->where('estado', 1);
                }
            )->where('tipo', 'trivia')->paginate($registros);

            $paginacion = [
                'total' => $trivias->total(),
                'count' => $trivias->count(),
                'per_page' => $trivias->perPage(),
                'current_page' => $trivias->currentPage(),
                'total_pages' => $trivias->lastPage(),
            ];

            return response()->json([
                'status' => 'success',
                'message' => 'Trivias disponibles',
                'trivias' => $trivias->getCollection(),
                'paginacion' => $paginacion,
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'La consulta de trivias fallo',
                'error' => $e->getMessage(),
                'codigo' => $e->getCode(),
            ], 500);
        }
    }

    //Ver las encuestas disponibles
    public function ApiListadoEncuestas($registros = 3)
    {
        try {
            //Debo probar esta consulta
            $encuestas = Trivia::whereNotIn(
                'id_trivia',
                function ($query) {
                    $query->select('FK_id_trivia')
                        ->from(with(new TriviaCliente)->getTable())
                        ->whereNotNull('FK_id_trivia')
                        ->whereIn('FK_id_cliente', [auth()->user()->FK_id_cliente]);
                }
            )->where('estado', 1)->where('tipo', 'home')->where('fecha_inicio', '<=', date('Y-m-d'))->Where('fecha_fin', '>=', date('Y-m-d'))->paginate($registros);

            $paginacion = [
                'total' => $encuestas->total(),
                'count' => $encuestas->count(),
                'per_page' => $encuestas->perPage(),
                'current_page' => $encuestas->currentPage(),
                'total_pages' => $encuestas->lastPage(),
            ];

            return response()->json([
                'status' => 'success',
                'message' => 'Encuestas disponibles',
                'encuestas' => $encuestas->getCollection(),
                'paginacion' => $paginacion,
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'La consulta de encuestas fallo',
                'error' => $e->getMessage(),
                'codigo' => $e->getCode(),
            ], 500);
        }
    }

    //Devolver una trivia seleccionada
    public function ApiTrivia($id_trivia)
    {
        DB::beginTransaction();
        try {
            $trivia = Trivia::find($id_trivia);

            if (! $trivia) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'La trivia solicitada no se encuentra registrada',
                ], 404);
            }

            $preguntas = TriviaPregunta::where('FK_id_trivia', $id_trivia)->where('estado', 1)->get();

            if ($preguntas->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'La trivia no esta disponible (aun se esta ensamblando)',
                ], 404);
            }

            if ($trivia->tipo == 'trivia') {
                //Grabamos el historial del cliente
                $historial = new ClienteHistorial();
                $historial->accion = 'Visualización de Trivia: '.$trivia->nombre;
                $historial->fecha_registro = date('Y-m-d');
                $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
                $historial->save();

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Trivia encontrada',
                    'trivia' => $trivia,
                    'preguntas' => $preguntas,
                    'n_preguntas' => $preguntas->count(),
                ], 200);
            } elseif (strtotime('now') >= strtotime($trivia->fecha_inicio) && strtotime('now') <= strtotime($trivia->fecha_fin)) {

                //Grabamos el historial del cliente
                $historial = new ClienteHistorial();
                $historial->accion = 'Visualización de Trivia: '.$trivia->nombre;
                $historial->fecha_registro = date('Y-m-d');
                $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
                $historial->save();

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Trivia encontrada',
                    'trivia' => $trivia,
                    'preguntas' => $preguntas,
                    'n_preguntas' => $preguntas->count(),
                ], 200);
            }

            DB::commit();

            return response()->json([
                'status' => 'error',
                'message' => 'La trivia solicitada no se encuentra disponible',
            ], 404);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al solicitar la trivia, intente más tarde',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }
}
