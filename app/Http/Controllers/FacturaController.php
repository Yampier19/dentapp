<?php

namespace App\Http\Controllers;

use App\Events\FacturaEvent;
use App\Events\PuntosEvent;
use App\Helper\FacturaIA;
use App\Helper\Notificacion;
use App\Mail\FacturaEmail;
use App\Mail\HallodentRanking;
//MODELOS
use App\Mail\SubirOro;
use App\Mail\SubirPlata;
use App\Mail\SubirZafiro;
use App\Models\Cliente;
use App\Models\ClienteHistorial;
use App\Models\Distribuidor;
use App\Models\DistribuidorProducto;
use App\Models\Estrella;
use App\Models\Factura;
use App\Models\FacturaNoLeida;
//Helper o Usables
use App\Models\FacturaProductoEspecial;
use App\Models\HalloDent;
use App\Models\Level;
use App\Models\NavidadCanjeado;
use App\Models\NavidadFacturaProducto;
use App\Models\Nivel;
use App\Models\Premio;
use DateTime;
//MAILABLE
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class FacturaController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mimes' => 'El archivo debe llegar en formato png, jpg, jpeg, o pdf',
        'max' => 'El dato no debe ser mayor a 255 carácteres',
        'fecha_registro.after' => 'Selecciona una fecha mayor de hoy',
        'date_format' => 'Debe viajar en formato fecha (año-mes-dia)',
        'foto.max' => 'El archivo no puede ser mayor a 2Mb',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'puntos' => 'numeric|min:0',
        'monto_total' => 'required|numeric|min:0',
        'distribuidor' => 'required|string|max:255',
        'descripcion' => 'string|max:255',
        'telefono' => 'required|string|max:255',
        'foto' => 'required|file|mimes:jpg,jpeg,png,pdf|max:2100',
        'fecha_registro' => 'required|date_format:Y-m-d',
        'referencia' => 'required|string|max:255',
    ];

    public function montoRango($monto, $cliente)
    {
        $niveles = Nivel::orderBy('id_nivel', 'desc')->get();
        $actual = null;

        foreach ($niveles as $nivel) {
            if ($cliente->total_reunido >= $nivel->level->rango_inicio) {
                $actual = $nivel;
                break;
            }
        }

        if ($actual->nombre == 'Bronce') {
            return round($monto * 0.08 / 10);
        } elseif ($actual->nombre == 'Plata') {
            return round($monto * 0.10 / 10);
        } elseif ($actual->nombre == 'Oro') {
            return round($monto * 0.12 / 10);
        } elseif ($actual->nombre == 'Zafiro') {
            return round($monto * 0.15 / 10);
        } else {
            return round($monto * 0.17 / 10);
        }
    }

    public function NewLevel($final, $inicio, $id)
    {
        $nivel1 = null;
        $nivel2 = null;

        $niveles = Nivel::orderBy('id_nivel', 'desc')->get();

        foreach ($niveles as $nivel) {
            if ($final >= $nivel->level->rango_inicio) {
                $nivel1 = $nivel;
                break;
            }
        }

        foreach ($niveles as $nivel) {
            if ($inicio >= $nivel->level->rango_inicio) {
                $nivel2 = $nivel;
                break;
            }
        }

        if (strcmp($nivel1->level->nombre, $nivel2->level->nombre) !== 0) {
            $title = 'A CELEBRAR, ¡SUBISTE DE NIVEL EN DENTAPP!';
            $text = 'Ahora estás un nivel más alto, ingresa a tu app y conoce tus nuevos premios y beneficios.';
            event(new PuntosEvent($title, $text, $id));

            //Puntos por subir de nivel Oro, Zafiro o DIamante
            try {
                //(final, inicio, id) -> (sumando la factura sumada, sin sumar la factura, codigo)
                //nivel 1 es el nuevo nivel ya que es el que se evalua con el monto final
                //oro 500 puntos
                //zafiro 800 puntos
                //diamante 1000 puntos

                if (strtolower($nivel1->nombre) == 'plata') {
                    $cliente = Cliente::find($id);
                    //ENVIO DEL CORREO POR SUBIR DE NIVEL
                    $information = new \stdClass();
                    $information->asunto = 'A CELEBRAR, ¡SUBISTE A NIVEL PLATA EN DENTAPP!';
                    $information->usuario = $cliente->nombre . ' ' . $cliente->apellido;

                    try {
                        Mail::to([$cliente->correo])
                            ->bcc('ngodoy@peoplecontact.cc')
                            ->queue(new SubirPlata($information));
                    } catch (\Exception $e) {
                    }
                } elseif (strtolower($nivel1->nombre) == 'oro') {
                    $cliente = Cliente::find($id);
                    $cliente->total_estrellas += 500;
                    $cliente->save();

                    //ENVIO DEL CORREO POR SUBIR DE NIVEL
                    $information = new \stdClass();
                    $information->asunto = 'A CELEBRAR, ¡SUBISTE A NIVEL ORO EN DENTAPP!';
                    $information->usuario = $cliente->nombre . ' ' . $cliente->apellido;

                    try {
                        Mail::to([$cliente->correo])
                            ->bcc('ngodoy@peoplecontact.cc')
                            ->queue(new SubirOro($information));
                    } catch (\Exception $e) {
                    }
                } elseif (strtolower($nivel1->nombre) == 'zafiro') {
                    $cliente = Cliente::find($id);
                    $cliente->total_estrellas += 800;
                    $cliente->save();

                    //ENVIO DEL CORREO POR SUBIR DE NIVEL
                    $information = new \stdClass();
                    $information->asunto = 'A CELEBRAR, ¡SUBISTE A NIVEL ZAFIRO EN DENTAPP!';
                    $information->usuario = $cliente->nombre . ' ' . $cliente->apellido;

                    try {
                        Mail::to([$cliente->correo])
                            ->bcc('ngodoy@peoplecontact.cc')
                            ->queue(new SubirZafiro($information));
                    } catch (\Exception $e) {
                    }
                } elseif (strtolower($nivel1->nombre) == 'diamante') {
                    $cliente = Cliente::find($id);
                    $cliente->total_estrellas += 1000;
                    $cliente->save();
                }
            } catch (\Exception $e) {
                return;
            }
        }
    }

    public function index()
    {
        $clientes = Cliente::orderBy('created_at', 'desc')->take(3)->get();
        $facturas = Factura::orderBy('created_at', 'desc')->take(3)->get();

        return view('facturas.index', compact('clientes', 'facturas'));
    }

    //Recoger archivo de la factura
    public function getArchive($filename = null)
    {
        $extension = null;

        if ($filename) {
            $extension = pathinfo($filename)['extension'];

            $exist = Storage::disk('public')->exists('facturas/' . $filename);
            if (!$exist) { //si no existe el file devuelveme el estandar
                $extension = null;
                $file = storage_path('app/public/nodisponible.png');
            } else {
                $file = storage_path('app/public/facturas/' . $filename);
            }
        } else {
            $file = storage_path('app/public/nodisponible.png');
        }

        switch ($extension) {
            case 'pdf':
                $extension = 'application/pdf';
                break;
            default:
                $extension = 'image';
                break;
        }

        return Response(file_get_contents($file), 200, [
            'Content-Type' => $extension,
            'Content-Disposition' => 'inline; filename="' . $file . '"',
        ]);
    }

    //Listado de Clientes
    public function listcliente()
    {
        $clientes = Cliente::orderBy('created_at', 'desc')->get();

        return view('facturas.clienteslist', compact('clientes'));
    }

    //Listado de Facturas
    public function listfactura($id_cliente = null, $nombre = null)
    {
        if ($id_cliente) {
            $facturas = Factura::where('FK_id_cliente', $id_cliente)->orderBy('created_at', 'desc')->get();
        } else {
            $facturas = Factura::orderBy('created_at', 'desc')->get();
        }

        $productos = DistribuidorProducto::all();
        $distribuidores = Distribuidor::all();

        return view('facturas.facturaslist', [
            'facturas' => $facturas,
            'cliente' => $nombre,
            'productos' => $productos,
            'distribuidores' => $distribuidores,
        ]);
    }

    //Listado de Facturas no reconocidas por el bot
    public function listbot()
    {
        $facturas = FacturaNoLeida::orderBy('created_at', 'desc')->get();

        return view('facturas.noleidas', compact('facturas'));
    }

    //Tomar detallado de los productos especiales de la factura
    public function getDetail($id_factura)
    {
        $factura = Factura::findOrFail($id_factura);

        return view('facturas.detail', compact('factura'));
    }

    //Editar los datos de la factura
    public function updateDatos(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'id_factura' => 'required',
                'monto_total' => 'required|numeric|min:0',
                'distribuidor' => 'required|string|max:255',
                'referencia' => 'required|string|max:255',
                'telefono' => 'required|string|max:255',
                'fecha_registro' => 'required|date_format:Y-m-d',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $factura = Factura::findOrFail($request->input('id_factura'));

            $data = $request->all();

            //Evaluamos los productos extra si cargo o no
            if ($request->has('productId')) {
                $factura->descripcion = null;

                //Elmino los productos especiales de la factura
                foreach ($factura->especiales as $especial) {
                    $especial->delete();
                }

                //Elimino todos los productos de la factura de navidad si lo es
                if ($factura->navidad) {
                    NavidadFacturaProducto::where('FK_id_navidad_factura', $factura->navidad->id_navidad_factura)->delete();
                }

                //Productos de la factura
                for ($i = 0; $i < count($request->productId); $i++) {
                    $factura->descripcion .= $request->productName[$i] . "\n";

                    //Grabo los productos (si son especiales)
                    $p_distribuidor = DistribuidorProducto::where('id_pro', $request->productId[$i])->first();
                    if ($p_distribuidor) {
                        if ($p_distribuidor->puntos_extra > 0) {
                            $p_especial = new FacturaProductoEspecial();
                            $p_especial->producto = $request->productName[$i];
                            $p_especial->cantidad = $request->productCantidad[$i];
                            $p_especial->puntos = $request->productCantidad[$i] * $p_distribuidor->puntos_extra;
                            $p_especial->FK_id_factura = $factura->id_factura;
                            $p_especial->save();
                        }
                    } else {
                        //No existe por lo que entonces lo grabo en el distribuidor seleccionado
                        $nombreDist = str_replace('.', '', $request->distribuidor);
                        $nombreDist = substr($nombreDist, 0, 11);
                        $distri = Distribuidor::where('nombre', 'like', '%' . $nombreDist . '%')->first();

                        $newProduct = new DistribuidorProducto();

                        if ($distri) {
                            $newProduct->id_distri3M = $distri->id_distri == 10 ? 16 : $distri->id_distri;
                        } else {
                            $newProduct->id_distri3M = 19;
                        }

                        $newProduct->referencia = 'sistema-automatico';
                        $newProduct->stock_3m = 'sistema-automatico';
                        $newProduct->familia = 'NA';
                        $newProduct->distribuidor = $request->productName[$i];
                        $newProduct->tipo = 'ODONTOLOGÍA';
                        $newProduct->puntos_extra = 0;
                        $newProduct->save();
                    }

                    //Revisamos si la factura es de navidad 2021 y grabo en la sección productos de la factura
                    if ($factura->navidad) {
                        $navifact = $factura->navidad;
                        $navifact_producto = new NavidadFacturaProducto();
                        $navifact_producto->producto = $request->productName[$i];
                        $navifact_producto->cantidad = $request->productCantidad[$i];
                        $navifact_producto->FK_id_navidad_factura = $navifact->id_navidad_factura;
                        $navifact_producto->save();
                    }
                    //echo $productId[$i]."\n";
                    //echo $productName[$i]."\n";
                    //echo $productCantidad[$i]."\n";
                    //echo "\n\n\n";
                }
            }

            $data['puntos'] = $this->montoRango($data['monto_total'], $factura->cliente);
            $factura->update($data);

            DB::commit();

            return Redirect::back()->with('edit', 'Factura Actualizada');
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Factura no pudo ser Editada: ' . $e->getMessage());
        }
    }

    //Editar el estado de la factura
    public function update($id_factura, $estado)
    {

        DB::beginTransaction();
        try {
            $factura = Factura::findOrFail($id_factura);
            $factura->estado = $estado;
            $factura->revision = true;
            $factura->update();
            //$factura->update(['estado' => $estado]);

            if ($estado == 'aceptado') {

                //Ver si la factura es de registro de la campaña navidad
                if ($factura->navidad) {
                    if ($factura->navidad->tipo != 'registro') {
                        //Tomamos los puntos ganados a traves de productos especiales
                        $extra = 0;
                        foreach ($factura->especiales as $productos) {
                            $extra += $productos->puntos;
                        }

                        //Actualizamos al cliente
                        $cliente = $factura->cliente;
                        $cliente->total_estrellas += $factura->puntos + $extra;
                        $cliente->total_reunido += $factura->monto_total;
                        $cliente->update();

                        //Grabamos las estrellas o puntos en su tabla correspondiente
                        $estrella = new Estrella();
                        $estrella->descripcion = 'Puntos por Facturación';
                        $estrella->cantidad = $factura->puntos + $extra;
                        $estrella->fecha_registro = date('Y-m-d H:i:s');
                        $estrella->estado = 'activo';
                        $estrella->FK_id_cliente = $factura->FK_id_cliente;
                        $estrella->save();
                    }
                } else {
                    //Tomamos los puntos ganados a traves de productos especiales
                    $extra = 0;
                    foreach ($factura->especiales as $productos) {
                        $extra += $productos->puntos;
                    }

                    //Actualizamos al cliente
                    $cliente = $factura->cliente;
                    $cliente->total_estrellas += $factura->puntos + $extra;
                    $cliente->total_reunido += $factura->monto_total;
                    $cliente->update();

                    //Grabamos las estrellas o puntos en su tabla correspondiente
                    $estrella = new Estrella();
                    $estrella->descripcion = 'Puntos por Facturación';
                    $estrella->cantidad = $factura->puntos + $extra;
                    $estrella->fecha_registro = date('Y-m-d H:i:s');
                    $estrella->estado = 'activo';
                    $estrella->FK_id_cliente = $factura->FK_id_cliente;
                    $estrella->save();

                    $clientes = Cliente::all();

                    foreach ($clientes as $cliente) {
                        $puntos = $cliente->estrellas()->sum('cantidad');
                        $cliente->total_puntos_reunidos = $puntos;
                        $cliente->update();

                        $level = Level::whereRaw('? BETWEEN rango_inicio and meta', [$cliente->total_puntos_reunidos])->first();
                        $cliente->FK_id_level = $level->id_level;
                        $cliente->update();
                    }
                }

                //Revisamos si la factura es de hallodent
                if ($factura->halloween) {
                    $hallodent = $factura->halloween->campaña;

                    $hallomail = false;
                    if ($hallodent->facturado < 280000) {
                        if ($hallodent->facturado + $factura->monto_total >= 280000) {
                            $hallodent->fecha_participante = date('Y-m-d H:i:s');
                            $rankings = HalloDent::selectRaw('COUNT(ranking) as total')->whereNotNull('ranking')->first();
                            $hallodent->ranking = $rankings->total + 1;
                            $hallomail = true;
                        }
                    }
                    $hallodent->facturado += $factura->monto_total;
                    $hallodent->save();

                    //ENVIO DEL CORREO DE HALLODENT SI SUPERO LOS 250MIL FACTURADO Y ESTA PARTICIPANDO PRIMERA VEZ
                    /* if($hallomail){
                        $information = new \stdClass();
                        $information->asunto = 'Felicidades ya estas participando en HalloDent';
                        $information->nombre = $hallodent->nombre." ".$hallodent->apellido;
                        $information->puntos = $hallodent->ranking; //Tu ranking en la campaña
                        $information->total = $rankings->total; //Numero de participantes activos

                        try{
                            Mail::to($hallodent->correo)->queue(new HallodentRanking($information));
                        }
                        catch(\Exception $e){
                            "";
                        }
                    } */
                }

                //Revisamos si la factura es de navidad 2021
                if ($factura->navidad) {
                    $navidad = $factura->navidad->campaña;
                    $navimail = false;

                    if ($factura->navidad->tipo == 'registro') {
                        $marca1 = "McDonald's";
                        $marca2 = 'Mimos';

                        $premios = Premio::where(function ($query) use ($marca1, $marca2) {
                            $query->where('marca', '=', $marca1)
                                ->orWhere('marca', '=', $marca2);
                        })
                            ->where('distribuidor', 'Quantum')
                            ->where('numero_estrellas', '<=', 3000)->get();

                        if ($premios->count() > 0) {
                            $premio = $premios->random();
                        } else {
                            $premio = null;
                        }

                        if ($premio) {
                            $canjeo = new NavidadCanjeado();
                            $canjeo->forma = 'registro';
                            $canjeo->tipo = 'bono';
                            $canjeo->nombre = $premio->nombre;
                            $canjeo->marca = $premio->marca;
                            $canjeo->descripcion = $premio->descripcion;
                            $canjeo->referencia = 'No canjeado';
                            $canjeo->FK_id_navidad = $navidad->id_navidad;
                            $canjeo->FK_id_premio = $premio->id_premio;
                            $canjeo->save();
                        }

                        $titulo = 'TU FACTURA DE REGISTRO POR LA CAMPAÑA NAVIDEÑA FUE APROBADA!!';
                        $descripcion = '¡Reclama tu premio de bienvenida!';
                    } else {
                        if ($factura->monto_total >= 400000) {
                            //Premio Bono Ruleta Ticket normal 400.000 a 999.999 pesos
                            if ($factura->monto_total >= 400000 && $factura->monto_total <= 999999) {
                                $navidad->tickets++;
                                $texto = strtoupper($factura->descripcion);
                                $productos = ['RESINA FILTEK Z350XT', 'RESINA FILTEK ONE', 'RESINA FILTEK UNIVERSAL', 'RELYX U200', 'RELYX ULTIMATE'];
                                $cantidad = 0;
                                while (str_contains($texto, "\n")) {
                                    $pro = substr($texto, 0, stripos($texto, "\n"));
                                    $texto = substr($texto, stripos($texto, "\n") + 1);

                                    if (in_array($pro, $productos)) {
                                        $cantidad++;
                                    }
                                }

                                //PREGUNTAR SI ES 1 POR PRODUCTO O 1 Y YA ASI TENGO LOS 5
                                //Apartado de sumar tickets por productos de la campaña
                                if ($cantidad > 0) {
                                    $navidad->tickets++;
                                }

                                $descripcion = '¡Has obtenido nuevos tickets por tu factura, ingresa a la app y gira la ruleta!';
                                $navidad->save();
                            }
                            //PREMIO DE PRODUCTO ENTRE 1M A 1.9M
                            //else if($factura->monto_total >= 1000000 && $factura->monto_total <= 1999999){
                            else {
                                $navidad->tickets_especial++;
                                $texto = strtoupper($factura->descripcion);
                                $productos = ['RESINA FILTEK Z350XT', 'RESINA FILTEK ONE', 'RESINA FILTEK UNIVERSAL', 'RELYX U200', 'RELYX ULTIMATE'];
                                $cantidad = 0;
                                while (str_contains($texto, "\n")) {
                                    $pro = substr($texto, 0, stripos($texto, "\n"));
                                    $texto = substr($texto, stripos($texto, "\n") + 1);

                                    if (in_array($pro, $productos)) {
                                        $cantidad++;
                                    }
                                }

                                //PREGUNTAR SI ES 1 POR PRODUCTO O 1 Y YA ASI TENGO LOS 5
                                //Apartado de sumar tickets por productos de la campaña
                                if ($cantidad > 0) {
                                    $navidad->tickets++;
                                }

                                $descripcion = '¡Has obtenido nuevos tickets por tu factura, ingresa a la app y gira la ruleta!';
                                $navidad->save();
                            }
                            //PREMIO CENA NAVIDEÑA FACTURA POR ENCIMA DE 2M
                            /* else{
                                $canjeados = NavidadCanjeado::whereNull('FK_id_premio')->get();
                                if($canjeados->count() < 2){
                                    //Creamos el premio canjeado pero no lo canjeamos eso queda para otro endpoint
                                    $canjeo = new NavidadCanjeado();
                                    $canjeo->tipo = "Cena";
                                    $canjeo->nombre = "Cena Navideña";
                                    $canjeo->marca = "Ninguna";
                                    $canjeo->descripcion = "Premio redimible para unicamente dos usuarios";
                                    $canjeo->referencia = "No canjeado";
                                    $canjeo->FK_id_navidad = $navidad->id_navidad;
                                    $canjeo->FK_id_premio = null;
                                    $canjeo->save();

                                    $descripcion = "¡Has ganado una cena navideña, ingresa a la app en la campaña navideña para canjearlo!";
                                }
                                else{
                                    $navidad->tickets_especial++;
                                    $texto = strtoupper($factura->descripcion);
                                    $productos = array("RESINA FILTEK Z350XT", "RESINA FILTEK ONE", "RESINA FILTEK UNIVERSAL", "RELYX U200", "RELYX ULTIMATE");
                                    $cantidad = 0;
                                    while(str_contains($texto, "\n")){
                                        $pro = substr($texto, 0, stripos($texto,"\n"));
                                        $texto = substr($texto, stripos($texto,"\n") + 1);

                                        if (in_array($pro, $productos))
                                            $cantidad++;
                                    }

                                    //PREGUNTAR SI ES 1 POR PRODUCTO O 1 Y YA ASI TENGO LOS 5
                                    //Apartado de sumar tickets por productos de la campaña
                                    if($cantidad > 0){
                                        $navidad->tickets++;
                                    }

                                    $descripcion = "¡Has obtenido nuevos tickets por tu factura, ingresa a la app y gira la ruleta!";
                                    $navidad->save();
                                }
                            } */
                        }

                        $titulo = 'TU FACTURA CARGADA POR LA CAMPAÑA NAVIDEÑA FUE APROBADA!!';
                    }
                    $navidad->save();

                    //ENVIO DEL CORREO DE NAVIDAD SI SUPERA LOS 400MIL Y ESTA PARTICIPANDO
                    /* if($navimail){
                        $information = new \stdClass();
                        $information->asunto = 'Felicidades has conseguido tickets por tu factura en Navidad';
                        $information->nombre = $navidad->nombre." ".$navidad->apellido;
                        $information->puntos = 100; //Tu ranking en la campaña
                        $information->total = 200; //Tu ranking en la campaña

                        try{
                            Mail::to($navidad->correo)->queue(new HallodentRanking($information));
                        }
                        catch(\Exception $e){
                            "";
                        }
                    } */

                    //ENVIO DE NOTIFICACIÓN PUSH CAMPAÑA NAVIDEÑA
                    event(new FacturaEvent($titulo, $descripcion, $id_factura));
                }

                $mensaje = 'Su factura ha sido aceptada y procesada exitosamente';
            } elseif ($estado == 'rechazado') {
                $mensaje = 'Su factura ha sido rechazada';
            }

            //Notificación para los clientes
            if ($estado == 'aceptado') {
                $dataClient = [
                    'nombre' => '¡BUENAS NOTICIAS! FACTURA ' . $factura->referencia . ' APROBADA',
                    'tipo' => 'Factura',
                    'descripcion' => 'Hemos revisado y aprobado tu factura con éxito. Ingresa aquí y conoce cuántos puntos ganaste.',
                    'cliente' => $factura->FK_id_cliente,
                ];
            } elseif ($estado == 'rechazado') {
                $dataClient = [
                    'nombre' => 'TU FACTURA ' . $factura->referencia . ' FUE RECHAZADA',
                    'tipo' => 'Factura',
                    'descripcion' => 'Parece que tu factura no pudo ser verificada, entra aquí y conoce las posibles razones.',
                    'cliente' => $factura->FK_id_cliente,
                ];
            }

            Notificacion::instance()->storeUniqueClient($dataClient);

            //Si esta aceptada la factura mando dos notificaciones una push y una normal de puntos
            if ($estado == 'aceptado') {
                $dataClient = [
                    'nombre' => '¡FELICIDADES, GANASTE!',
                    'tipo' => 'Puntos',
                    'descripcion' => 'Acabas de sumar ' . $estrella->cantidad . ' puntos por cargar una factura, aprovecha y descubre que puedes canjear',
                    'cliente' => $factura->FK_id_cliente,
                ];

                Notificacion::instance()->storeUniqueClient($dataClient);

                //NOTIFICACIÓN DE PUNTOS
                $title = '¡FELICIDADES, GANASTE!';
                $text = 'Acabas de sumar ' . $estrella->cantidad . ' puntos por cargar una factura, aprovecha y descubre que puedes canjear';
                event(new PuntosEvent($title, $text, $factura->FK_id_cliente));

                $this->NewLevel($cliente->total_reunido, $cliente->total_reunido - $factura->monto_total, $factura->FK_id_cliente);

                //ENVIO DEL CORREO DE GANO X PUNTOS FACTURA
                $information = new \stdClass();
                $information->asunto = 'Felicitaciones Carga de Factura';
                $information->nombre = $factura->cliente->nombre . ' ' . $factura->cliente->apellido;
                $information->puntos = $estrella->cantidad;
                $information->total = $cliente->total_estrellas;

                try {
                    Mail::to($factura->cliente->correo)->queue(new FacturaEmail($information));
                } catch (\Exception $e) {
                }
            }

            if ($estado == 'aceptado') {
                $title = '¡BUENAS NOTICIAS! FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' APROBADA';
                $text = 'Hemos revisado y aprobado tu factura con éxito. Ingresa aquí y conoce cuántos puntos ganaste.';
            } elseif ($estado == 'rechazado') {
                $title = 'TU FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' FUE RECHAZADA';
                $text = 'Parece que tu factura no pudo ser verificada, entra aquí y conoce las posibles razones.';
            }
            event(new FacturaEvent($title, $text, $id_factura));

            DB::commit();

            return Redirect::back()->with('edit', 'Factura Actualizada');
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Factura no pudo ser Editada: ' . $e->getMessage() . ' | ' . $e->getLine());
        }
    }

    //Eliminar el registro de una factura no leida por el bot
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $factura = FacturaNoLeida::find($id);

            if (!$factura) {
                return Redirect::back()->with('edit', 'El registro no fue encontrado');
            }

            $factura->delete();

            DB::commit();

            return Redirect::back()->with('edit', 'El registro fue eliminado correctamente');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('edit', 'El registro no pudo ser Eliminado');
        }
    }

    ///////////////////////////////////////API CLIENTE//////////////////////////////////////////

    //Compresor de Imagen o PDF
    public function imagecompress($image, $extension)
    {
        if ($extension != 'pdf') {
            $img = Image::make(storage_path('app/public/' . $image))->resize(1200, 1200, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save();
            $img->destroy();
        }
    }

    //Envio primario de factura
    public function store(Request $request)
    {
        $mensajes_error = $this->mensajes_error;
        $validaciones = $this->validaciones;

        if ($request->has('foto')) {
            if ($request->file('foto')->extension() == 'pdf') {
                $validaciones = ['foto' => 'required|file|mimes:jpg,jpeg,png,pdf|max:' . (1024 * 5)];
                $mensajes_error['foto.max'] = 'El pdf debe ser menor a 5 Mb';
            } else {
                $validaciones = ['foto' => 'required|file|mimes:jpg,jpeg,png,pdf'];
            }
        }

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $validaciones, $mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $data = $request->all();

            if ($request->hasFile('foto')) {
                $data['foto'] = $request->file('foto')->store('facturas', 'public');
                $this->imagecompress($data['foto'], $request->file('foto')->extension());
            }

            $data['FK_id_cliente'] = auth()->user()->FK_id_cliente;

            $values = FacturaIA::instance()->read($request->file('foto'), strtoupper(auth()->user()->cliente->distribuidor));

            if (isset($values->error)) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error en la lectura del archivo',
                    'errores' => $values->error,
                ], 500);
            }

            if (!$values) { //SUPONGAMOS QUE LA FACTURA NO LA PUDO LEER EL BOT
                $data['estado'] = 'rechazado';
                $data['distribuidor'] = 'N/A';
                $data['descripcion'] = 'N/A';
                $data['telefono'] = 'N/A';
                $data['monto_total'] = 0;
                $data['referencia'] = 'N/A';
                $data['fecha_registro'] = date('Y-m-d');
                FacturaNoLeida::create($data);
            } else {  //SUPONGAMOS QUE LA FACTURA SI LA LEYO EL BOT Y LOS PRODUCTOS ESPECIALES SI TIENE
                $values->estado = 'pendiente';
                $data['estado'] = $values->estado;
                $data['distribuidor'] = $values->distribuidor;
                $data['descripcion'] = $values->descripcion;
                $data['telefono'] = $values->telefono;
                $data['monto_total'] = $values->monto_total;
                $data['referencia'] = $values->referencia;
                $data['fecha_registro'] = $values->fecha_registro;
            }

            //CHEQUIAMOS SI LA FACTURA ESTA DUPLICADA O NO CUMPLE LA FECHA
            if ($data['estado'] == 'aceptado') {
                //Por referencia
                $check = Factura::where('referencia', $data['referencia'])->where('FK_id_cliente', auth()->user()->FK_id_cliente)->first();

                if ($check) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'La Factura enviada ya se encuentra registrada',
                        'referencia' => $data['referencia'],
                    ], 500);
                }

                //Por la fecha 30 días máximo a partir del dia de hoy
                $fecha_actual = strtotime(date($data['fecha_registro']));
                $fecha_entrada = strtotime(date('Y-m-d', strtotime(' -30 days')));

                if ($fecha_actual < $fecha_entrada) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'La Factura enviada debe tener máximo 30 días de vigencia para ser aceptada',
                        'fecha' => $data['fecha_registro'],
                    ], 500);
                }
            }

            $data['puntos'] = $this->montoRango($data['monto_total'], auth()->user()->cliente);

            $factura = Factura::create($data);

            $extrapoints = 0;
            if ($values && $values->estado != 'rechazado') {
                //Grabamos los productos especiales si en tal caso tiene
                foreach ($values->productos as $producto) {
                    $extra = new FacturaProductoEspecial();
                    $extra->producto = $producto['nombre'];
                    $extra->cantidad = $producto['cantidad'];
                    $extra->puntos = $producto['puntos'];
                    $extra->FK_id_factura = $factura->id_factura;
                    $extra->save();

                    $extrapoints += $extra->puntos;
                }
            }

            if ($values && $values->estado == 'aceptado') {
                //Actualizamos al cliente
                $cliente = $factura->cliente;
                $cliente->total_estrellas += $factura->puntos + $extrapoints;
                $cliente->total_reunido += $factura->monto_total;
                $cliente->update();

                //Grabamos las estrellas o puntos en su tabla correspondiente
                $estrella = new Estrella();
                $estrella->descripcion = 'Puntos por Facturación';
                $estrella->cantidad = $factura->puntos + $extrapoints;
                $estrella->fecha_registro = date('Y-m-d H:i:s');
                $estrella->estado = 'activo';
                $estrella->FK_id_cliente = $factura->FK_id_cliente;
                $estrella->save();

                //ENVIO DEL CORREO DE GANO X PUNTOS FACTURA
                $information = new \stdClass();
                $information->asunto = 'Felicitaciones Carga de Factura';
                $information->nombre = $factura->cliente->nombre . ' ' . $factura->cliente->apellido;
                $information->puntos = $estrella->cantidad;
                $information->total = $cliente->total_estrellas;

                try {
                    Mail::to($factura->cliente->correo)->queue(new FacturaEmail($information));
                } catch (\Exception $e) {
                }

                //Notificación si paso de nivel
                $this->NewLevel($cliente->total_reunido, $cliente->total_reunido - $factura->monto_total, $factura->FK_id_cliente);
            }

            if ($data['estado'] == 'aceptado') {
                $dataClient = [
                    'nombre' => '¡BUENAS NOTICIAS! FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' APROBADA',
                    'tipo' => 'Factura',
                    'descripcion' => 'Hemos revisado y aprobado tu factura con éxito. Ingresa aquí y conoce cuántos puntos ganaste.',
                    'cliente' => $factura->FK_id_cliente,
                ];

                $title = '¡BUENAS NOTICIAS! FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' APROBADA';
                $text = 'Hemos revisado y aprobado tu factura con éxito. Ingresa aquí y conoce cuántos puntos ganaste.';
            } elseif ($data['estado'] == 'pendiente') {
                $dataClient = [
                    'nombre' => '¡UPS! TU FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' NO PUDO SER VERIFICADA',
                    'tipo' => 'Factura',
                    'descripcion' => 'Hemos trasladado tu factura a revisión. Regresa en 48h para conocer si fue aprobada.',
                    'cliente' => $factura->FK_id_cliente,
                ];

                $title = '¡UPS! TU FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' NO PUDO SER VERIFICADA';
                $text = 'Hemos trasladado tu factura a revisión. Regresa en 72h para conocer si fue aprobada.';
            } else {
                $dataClient = [
                    'nombre' => 'TU FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' FUE RECHAZADA',
                    'tipo' => 'Factura',
                    'descripcion' => 'Parece que tu factura no pudo ser verificada, entra aquí y conoce las posibles razones.',
                    'cliente' => $factura->FK_id_cliente,
                ];

                $title = 'TU FACTURA DEL ' . date('Y-m-d', strtotime($factura->created_at)) . ' FUE RECHAZADA';
                $text = 'Parece que tu factura no pudo ser verificada, entra aquí y conoce las posibles razones.';
            }

            Notificacion::instance()->storeUniqueClient($dataClient);
            event(new FacturaEvent($title, $text, $factura->id_factura));

            if ($data['estado'] == 'aceptado') {
                $dataClient = [
                    'nombre' => '¡FELICIDADES, GANASTE!',
                    'tipo' => 'Puntos',
                    'descripcion' => 'Acabas de sumar ' . ($factura->puntos + $extrapoints) . ' puntos por cargar una factura, aprovecha y descubre que puedes canjear',
                    'cliente' => $factura->FK_id_cliente,
                ];

                Notificacion::instance()->storeUniqueClient($dataClient);

                //NOTIFICACIÓN DE PUNTOS
                $title = '¡FELICIDADES, GANASTE!';
                $text = 'Acabas de sumar ' . ($factura->puntos + $extrapoints) . ' puntos por cargar una factura, aprovecha y descubre que puedes canjear';
                event(new PuntosEvent($title, $text, $factura->FK_id_cliente));
            }

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Registro de Factura: ' . $data['referencia'];
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Factura Almacenada',
                'datos' => $factura,
                'estado' => $factura->estado,
                'puntos' => $extrapoints + $data['puntos'],
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al almacenar factura',
                'errores' => $e->getMessage() . ' | ' . $e->getLine(),
            ], 500);
        }
    }

    //Confirmación y guardado final de factura
    public function confirm(Request $request)
    {
        //Aqui me pasas si aceptas o no y el id de la factura
        //id_factura (id de la factura)
        //estado (representa si acepto la factura o no) aceptado o pendiente
        //descripción (si la envia o no puede estar vacia)

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'id_factura' => 'required',
                'estado' => 'required',
                'descripcion' => 'string|max:255',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $id_factura = $request->input('id_factura');
            $estado = $request->input('estado');
            $descripcion = $request->input('descripcion');

            $factura = Factura::find($id_factura);

            if (!$factura) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'El id de la factura enviada no se encuentra registrada',
                ], 500);
            }

            $factura->estado = $estado;
            $factura->descripcion = $descripcion;
            $factura->update();

            if ($estado == 'aceptado') {
                //Tomamos los puntos ganados a traves de productos especiales
                $extra = 0;
                foreach ($factura->especiales as $productos) {
                    $extra += $productos->puntos;
                }

                //Actualizamos al cliente
                $cliente = $factura->cliente;
                $cliente->total_estrellas += $factura->puntos + $extra;
                $cliente->total_reunido += $factura->monto_total;
                $cliente->update();

                //Grabamos las estrellas o puntos en su tabla correspondiente
                $estrella = new Estrella();
                $estrella->descripcion = 'Puntos por Facturación';
                $estrella->cantidad = $factura->puntos;
                $estrella->fecha_registro = date('Y-m-d H:i:s');
                $estrella->estado = 'activo';
                $estrella->FK_id_cliente = $factura->FK_id_cliente;
                $estrella->save();

                //Notificación si paso de nivel
                $this->NewLevel($cliente->total_reunido, $cliente->total_reunido - $factura->monto_total, $factura->FK_id_cliente);

                $mensaje = 'Su factura ha sido aceptada y procesada exitosamente';
            } elseif ($estado == 'pendiente') {
                $mensaje = 'Su factura ha sido enviada a revisión exitosamente';
            } elseif ($estado == 'rechazado') {
                $mensaje = 'Su factura ha sido rechazada';
            }

            if ($estado == 'aceptado') {
                $dataClient = [
                    'nombre' => '¡BUENAS NOTICIAS! FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' APROBADA',
                    'tipo' => 'Factura',
                    'descripcion' => 'Hemos revisado y aprobado tu factura con éxito. Ingresa aquí y conoce cuántos puntos ganaste.',
                    'cliente' => $factura->FK_id_cliente,
                ];

                $title = '¡BUENAS NOTICIAS! FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' APROBADA';
                $text = 'Hemos revisado y aprobado tu factura con éxito. Ingresa aquí y conoce cuántos puntos ganaste.';
            } elseif ($estado == 'pendiente') {
                $dataClient = [
                    'nombre' => '¡UPS! TU FACTURA DEL ' . date('d-m-Y', strtotime($factura->created_at)) . ' NO PUDO SER VERIFICADA',
                    'tipo' => 'Factura',
                    'descripcion' => 'Hemos trasladado tu factura a revisión. Regresa en 72h para conocer si fue aprobada.',
                    'cliente' => $factura->FK_id_cliente,
                ];

                $title = '¡UPS! TU FACTURA DEL ' . date('Y-m-d', strtotime($factura->created_at)) . ' NO PUDO SER VERIFICADA';
                $text = 'Hemos trasladado tu factura a revisión. Regresa en 48h para conocer si fue aprobada.';
            }

            Notificacion::instance()->storeUniqueClient($dataClient);
            event(new FacturaEvent($title, $text, $id_factura));

            if ($estado == 'aceptado') {
                $dataClient = [
                    'nombre' => '¡HAS ACUMULADO PUNTOS POR FACTURACIÓN!',
                    'tipo' => 'Puntos',
                    'descripcion' => 'Sumas ' . $cliente->total_estrellas . ' puntos, utilizalos para canjear nuevos premios',
                    'cliente' => $factura->FK_id_cliente,
                ];

                Notificacion::instance()->storeUniqueClient($dataClient);
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Su factura ha sido actualizada de forma exitosa',
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al almacenar factura',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Enviar una factura rechazada a revisión
    public function sendRevision(Request $request)
    {
        //Aqui me pasas si aceptas o no y el id de la factura
        //id_factura (id de la factura)

        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'id_factura' => 'required',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Error formato de datos recibidos',
                    'errores' => $validate->errors(),
                ], 500);
            }

            $id_factura = $request->input('id_factura');
            $factura = Factura::find($id_factura);

            if (!$factura) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'El id de la factura enviada no se encuentra registrada',
                ], 500);
            }

            if ($factura->estado != 'rechazado') {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Tu factura solo puede ser enviada a revisión si esta rechazada',
                ], 500);
            }

            if ($factura->revision) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Tu factura ya fue enviada a revisión anteriormente y no puede ser enviada de nuevo',
                ], 500);
            }

            $factura->estado = 'pendiente';
            $factura->revision = true;
            $factura->update();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Su factura ha sido enviada a revisión de forma exitosa',
            ], 500);
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al enviar a revisión factura',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Recoger archivo de la factura API
    public function ApigetArchive($filename = null)
    {

        /* $file = storage_path('app/public/facturas/'.$filename);

        return Response(file_get_contents($file), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$file.'"'
        ]); */

        $extension = null;

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('facturas/' . $filename);
            if (!$exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/nodisponible.png'));
            } else {
                return response()->file(storage_path('app/public/facturas/' . $filename));
            }
        } else {
            return response()->file(storage_path('app/public/nodisponible.png'));
        }

        /*
        if($filename){
            $extension = pathinfo($filename)['extension'];

            $exist = Storage::disk('public')->exists('facturas/'.$filename);
            if(!$exist) //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/nodisponible.png'));
            else{
                if($extension == 'pdf')
                    return Response(file_get_contents(storage_path('app/public/facturas/'.$filename)), 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'inline; filename="'.storage_path('app/public/facturas/'.$filename).'"'
                    ]);
                else
                    return response()->file(storage_path('app/public/facturas/'.$filename));
            }
        }
        else
            return response()->file(storage_path('app/public/nodisponible.png'));
        */
    }

    //Ver todas las facturas del cliente login
    public function ApiList($registros = 10)
    {
        $facturas = Factura::where('FK_id_cliente', auth()->user()->FK_id_cliente)->orderBy('updated_at', 'desc')->paginate($registros);

        $paginacion = [
            'total' => $facturas->total(),
            'count' => $facturas->count(),
            'per_page' => $facturas->perPage(),
            'current_page' => $facturas->currentPage(),
            'total_pages' => $facturas->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Facturas enviadas',
            'facturas' => $facturas->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Devolver una factura seleccionada
    public function ApiFactura($id_factura)
    {
        $factura = Factura::where('id_factura', $id_factura)->where('FK_id_cliente', auth()->user()->FK_id_cliente)->first();

        if (!$factura) {
            return response()->json([
                'status' => 'error',
                'message' => 'La factura solicitada no se encuentra registrada',
            ], 404);
        }

        $fecha_s = new DateTime($factura->created_at);
        $fecha_a = new DateTime();
        $horas = $fecha_s->diff($fecha_a);

        return response()->json([
            'status' => 'success',
            'message' => 'Factura encontrada',
            'factura' => $factura,
            'horas' => $horas->h,
        ], 200);
    }
}
