<?php

namespace App\Http\Controllers;

use App\Mail\PasswordResetMail;
use App\Models\Admin;
use App\Models\ClienteLogin;
use App\Models\ClientePassword;
//MODELOS
use App\Models\PasswordReset;
use App\Models\UserHistory;
use App\Notifications\PasswordResetRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
//USABLES
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
//MAILABLE
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    public $message_error = [
        'email' => 'El correo debe ser una dirección de correo valida.',
        'same' => 'La confirmación y la contraseña deben coincidir',
        'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
    ];

    public function create(Request $request, $tipo = null)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email',
            ], $this->message_error);

            if ($validate->fails()) {
                if ($tipo == 'admin') {
                    return Redirect::back()->withErrors($validate);
                } elseif ($tipo == 'cliente') {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Error formato de datos recibidos',
                        'errores' => $validate->errors(),
                    ], 500);
                }
            }

            if ($tipo == 'admin') {
                $user = Admin::where('email', $request->email)->first();
                $nombre = $user ? $user->nombre.' '.$user->apellido : '';
            } elseif ($tipo == 'cliente') {
                $user = ClienteLogin::where('email', $request->email)->first();
                $nombre = $user ? $user->cliente->nombre.' '.$user->cliente->apellido : '';
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'El tipo debe viajar con el valor (cliente)',
                ], 500);
            }

            if (! $user) {
                if ($tipo == 'admin') {
                    return Redirect::back()->with('error', 'El correo no se encuentra registrado dentro de dentapp');
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'No se encontro ningun usuario bajo dicho correo.',
                    ], 404);
                }
            }

            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'tipo' => $tipo,
                    'email' => $user->email,
                    'token' => Str::random(60),
                ]
            );

            if ($user && $passwordReset) {
                Mail::to($user->email)->queue(new PasswordResetMail($passwordReset->token, $passwordReset->tipo, $nombre));
            }
            //$user->notify(new PasswordResetRequest($passwordReset->token, $passwordReset->tipo));

            DB::commit();
            if ($tipo == 'admin') {
                return Redirect::back()->with('edit', 'Se envio un correo con el link para resetear su contraseña. Tienes 30 min para cambiar la misma');
            } else {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Se envio un correo con el link para resetear su contraseña. Tienes 30 min para cambiar la misma',
                ], 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            if ($tipo == 'admin') {
                return Redirect::back()->with('error', $e->getMessage());
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Fallo al enviar correo',
                    'errores' => $e->getMessage(),
                ], 500);
            }
        }
    }

    public function find($token, $tipo = null)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (! $passwordReset) {
            if ($tipo == 'admin') {
                return redirect()->route('login')->with('message', 'El token de recuperación de clave es invalido');
            } else {
                /* return response()->json([
                    'status' => 'error',
                    'message' => 'El token es invalido.'
                ], 404); */
                $mensaje = 'El token de recuperación de clave no existe';

                return view('auth.passwords.reset', compact('passwordReset', 'mensaje'));
            }
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(30)->isPast()) {
            $passwordReset->delete();
            if ($tipo == 'admin') {
                return redirect()->route('login')->with('message', 'El token de recuperación de clave ya se encuentra expirado');
            } else {
                /* return response()->json([
                    'status' => 'error',
                    'message' => 'El token ya se encuentra expirado.'
                ], 404); */
                $mensaje = 'El token de recuperación de clave ya se encuentra expirado';

                return view('auth.passwords.reset', compact('passwordReset', 'mensaje'));
            }
        }

        return view('auth.passwords.reset', compact('passwordReset'));
        /* return response()->json([
            'status' => 'success',
            'message' => 'Token valido por favor culmina el proceso de reestablecer contraseña.',
            'reset' => $passwordReset
        ], 200); */
    }

    //Chequiamos password si se asemeja al correo
    public function checkPassword($check)
    {
        //Chequiamos si es el correo y el password son iguales
        $check['correo'] = explode(' ', strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['correo'])));
        $check['password'] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['password']));

        if (stripos($check['password'], $check['correo'][0]) !== false) {
            return true;
        } else {
            return false;
        }
    }

    public function reset(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'email' => 'required|string|email',
                'password' => [
                    'string',
                    'min:8',
                    'max:50',
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&+-]/',
                    'required',
                ],
                'password_confirmation' => 'required|string|same:password',
                'token' => 'required',
                'tipo' => 'required',
            ], $this->message_error);

            $tipo = $request->tipo;

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $passwordReset = PasswordReset::where('token', $request->token)->first();

            if (! $passwordReset) {
                if ($tipo == 'admin') {
                    return redirect()->route('login')->with('message', 'El token de recuperación de clave es invalido');
                } else {
                    $mensaje = 'El token de recuperación de clave no existe';

                    return view('auth.passwords.reset', compact('passwordReset', 'mensaje'));
                }
            }

            if ($tipo == 'admin') {
                $user = Admin::where('email', $request->email)->first();
            } else {
                $user = ClienteLogin::where('email', $request->email)->first();
            }

            if (! $user) {
                if ($tipo == 'admin') {
                    return redirect()->route('login')->with('message', 'No se encontro ningun usuario bajo este correo electrónico.');
                } else {
                    return Redirect::back()->with('edit', 'No se encontro ningun usuario bajo este correo electrónico.');
                }
            }

            //Chequiamos si no estamos repitiendo el mismo password
            if (Hash::check($request->input('password'), $user->password)) {
                return Redirect::back()->with('edit', 'El nuevo password es igual al anterior');
            }

            //Chequiamos que no se asemeje al correo
            $data['correo'] = $request->email;
            $data['password'] = $request->password;
            if ($this->checkPassword($data)) {
                return Redirect::back()->with('edit', 'El password se asemeja al correo, por favor utiliza otro');
            }

            //Revisamos si el password no es el mismo de los ultimos 3 utilizados
            if ($tipo != 'admin') {
                foreach ($user->cliente->historialpass->take(3) as $pass) {
                    if (Hash::check($request->input('password'), $pass->password)) {
                        return Redirect::back()->with('edit', 'La contraseña a editar ya fue utilizada en las ultimas 3 contraseñas utilizadas');
                    }
                }
            }

            //Grabamos el historial de passwords
            if ($tipo != 'admin') {
                $history = new ClientePassword();
                $history->password = Hash::make($request->input('password'));
                $history->registro = date('Y-m-d');
                $history->FK_id_cliente = $user->FK_id_cliente;
                $history->save();
            }

            //Update de los datos de login
            $user->password = Hash::make($request->input('password'));
            $tipo != 'admin' ? $user->cambiar = 0 : '';
            $tipo != 'admin' ? $user->fecha_cambio = date('Y-m-d') : '';
            $user->update();

            //Grabamos el historial del usuario login update
            $loghistory = new UserHistory();
            $loghistory->accion = 'Recuperación de Contraseña: '.$tipo.' | '.$request->email;
            $loghistory->email = $request->email;
            $loghistory->password = $request->input('password');
            $tipo == 'admin' ?
                $loghistory->FK_id_useradmin = $user->id_useradmin :
                $loghistory->FK_id_cliente = $user->FK_id_cliente;
            $loghistory->save();

            $passwordReset->delete();

            DB::commit();
            if ($tipo == 'admin') {
                return redirect()->route('login')->with('edit', 'Se modifico el password satisfactoriamente.');
            } else {
                $mensaje = 'Se modifico el password satisfactoriamente.';
                $passwordReset = null;

                return view('auth.passwords.reset', compact('passwordReset', 'mensaje'));
            }
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', $e->getMessage());
        }
    }
}
