<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Banner;
use App\Models\Distribuidor;
use App\Models\Noticia;
use App\Models\NoticiaTipo;

class InformativoController extends Controller
{
    public function index()
    {
        $noticias = Noticia::orderBy('created_at', 'desc')->take(3)->get();
        $banners = Banner::orderBy('created_at', 'desc')->take(3)->get();

        //PARA CREAR NOTICIA Y BANNERS
        $tipos = NoticiaTipo::all();
        $distribuidores = Distribuidor::all();

        return view('informativo.index', compact('noticias', 'banners', 'tipos', 'distribuidores'));
    }

    public function listnoticias()
    {
        $noticias = Noticia::orderBy('id_noticia', 'desc')->get();
        $tipos = NoticiaTipo::all();

        return view('informativo.listnoticias', compact('noticias', 'tipos'));
    }

    public function listnew()
    {
        $noticias = Noticia::orderBy('id_noticia', 'desc')->get();

        return response()->json([
            'message' => 'Listado de noticias',
            'code' => 200,
            'data' => $noticias,
        ]);
    }

    public function listbanners()
    {
        $banners = Banner::orderBy('id_banner', 'desc')->get();

        return view('informativo.listbanners', compact('banners'));
    }
}
