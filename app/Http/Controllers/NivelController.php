<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Level;
use App\Models\Nivel;
use App\Models\NivelDescripcion;
use Illuminate\Http\Request;
//MODELOS
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
//Helper o Usables
use Intervention\Image\Facades\Image;

class NivelController extends Controller
{
    //Mensajes de Error en las validaciones
    public $mensajes_error = [
        'required' => 'El dato es requerido',
        'min' => 'El dato debe ser mayor a 0',
        'numeric' => 'El dato debe ir en formato numerico',
        'file' => 'El dato debe llegar como un archivo',
        'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
        'max' => 'El dato no debe ser mayor a 50 carácteres',
        'meta.max' => 'El dato no debe ser mayor a 100 carácteres',
        'foto.max' => 'La imagen no puede ser mayor a 2Mb',
    ];

    //Validaciones - Datos que viajaran como tal
    public $validaciones = [
        'nombre' => 'required|string|max:50',
        'meta' => 'required|string|max:100',
        'rango_inicio' => 'required|numeric|min:0',
        'foto' => 'file|mimes:jpg,jpeg,png|max:2100',
    ];

    //Validación del rango al editar o crear level
    public function validarRango($datos)
    {
        //$datos["rango_inicio"],, $datos["FK_id_nivel"]
        $levels = Level::orderBy('FK_id_nivel', 'asc')->get();

        $inicio = 0;
        $fin = 400000000;

        if (! $levels->isEmpty()) {
            foreach ($levels as $level) {
                if ($level->FK_id_nivel < $datos['FK_id_nivel']) {
                    $inicio = $level->rango_inicio;
                } elseif ($level->FK_id_nivel > $datos['FK_id_nivel'] && $fin == 400000000) {
                    $fin = $level->rango_inicio;
                }
            }

            if ($datos['rango_inicio'] > $inicio && $datos['rango_inicio'] < $fin) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    //Compresor de Imagen
    public function imagecompress($image)
    {
        $img = Image::make(storage_path('app/public/'.$image))->resize(1200, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }

    //Modificar el level de los usuarios de acuerdo al dinero recolectado
    public function changeLevelClients($level)
    {
        //total_reunido simboliza el dinero recolectado del cliente
        //EN HABLAR
    }

    //Enviar a las vistas correspondientes
    public function nivel()
    {
        $niveles = Level::with('cliente')->get();

        return view('premio.nivel', compact('niveles'));
    }

    //Vista para eliminar nivel
    public function nivel_confirm()
    {
        return view('premio.nivel_confirm');
    }

    //Vista para eliminar level
    public function level_confirm()
    {
        return view('premio.level_confirm');
    }
    //Fin enviar a las vistas correspondientes

    //Ver la descripcion del nivel
    public function descripcion($id_nivel)
    {
        $nivel = Nivel::findOrFail($id_nivel);

        return view('premio.niveldescripcion', compact('nivel'));
    }

    //Update del nivel
    public function updateNivel(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'foto' => 'required|file|mimes:jpg,jpeg,png|max:2100',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $nivel = Nivel::findOrFail($request->input('nivel'));

            if ($request->hasFile('foto')) {
                Storage::delete(['public/'.$nivel->foto]);
                $nivel->foto = $request->file('foto')->store('niveles', 'public');
                $this->imagecompress($nivel->foto);
            }

            $nivel->update();

            DB::commit();

            return redirect()->route('premio.nivel')->with('edit', 'Foto de Nivel Actualizado');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'El Nivel no pudo ser Editado');
        }
    }

    //Guardado de un Level
    public function levelStore(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $level = Level::where('FK_id_nivel', $request->input('FK_id_nivel'))->first();

            if ($level) {
                return Redirect::back()->with('error', 'El Nivel seleccionado ya posee una segmentación');
            }

            if ($this->validarRango($request->all())) {
                return Redirect::back()->with('error', 'El Rango a crear no va conforme a los rangos de sus niveles superiores o inferiores');
            }

            Level::create($request->all());

            DB::commit();

            return redirect()->route('premio.nivel')->with('create', 'El Rango fue creado exitosamente');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return $e->getMessage();

            return Redirect::back()->with('error', 'El Rango no pudo ser Registrado');
        }
    }

    //Update de un Level
    public function updateLevel(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), $this->validaciones, $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $level = Level::where('FK_id_nivel', $request->input('FK_id_nivel'))->first();

            if (! $level) {
                return Redirect::back()->with('error', 'El Nivel seleccionado no posee una segmentación');
            }

            if ($this->validarRango($request->all())) {
                return Redirect::back()->with('error', 'El Rango a editar no va conforme a los rangos de sus niveles superiores o inferiores');
            }

            Level::where('FK_id_nivel', $request->input('FK_id_nivel'))->update($request->except(['_token']));

            //$this->changeLevelClients($level->id_level);

            DB::commit();

            return redirect()->route('premio.nivel')->with('edit', 'El Rango fue editado exitosamente');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return $e->getMessage();

            return Redirect::back()->with('error', 'El Rango no pudo ser Editado');
        }
    }

    //Store Descripción
    public function storeDesc(Request $request)
    {
        DB::beginTransaction();
        try {
            $nivel = Nivel::find($request->input('FK_id_nivel'));

            if (! $nivel) {
                return Redirect::back()->with('delete', 'El nivel no fue encontrado al momento de adjuntar descripción');
            }

            $validate = Validator::make($request->all(), [
                'descripcion' => 'required|string|max:255',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            NivelDescripcion::create($request->all());

            DB::commit();
            //DEVUELVO LA VISTA DONDE SE VEN LAS PREGUNTAS DE DICHA TRIVIA
            return Redirect::back()->with('create', 'Descripción Registrada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Descripción no pudo ser Registrada');
        }
    }

    //Update de los datos de una Descripcion
    public function updateDesc(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'descripcion' => 'required|string|max:255',
            ], $this->mensajes_error);

            if ($validate->fails()) {
                return Redirect::back()->withErrors($validate);
            }

            $descripcion = NivelDescripcion::findOrFail($request->input('id_nivel_descripcion'));
            $descripcion->update($request->all());

            DB::commit();

            return Redirect::back()->with('edit', 'Descripción Actualizada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            return Redirect::back()->with('error', 'La Descripción no pudo ser Editada');
        }
    }

    //Eliminar una descripcion
    public function destroyDesc(Request $request)
    {
        DB::beginTransaction();
        try {
            $descripcion = NivelDescripcion::find($request->input('id_nivel_descripcion'));

            if (! $descripcion) {
                return Redirect::back()->with('edit', 'La descripción no fue encontrada');
            }

            //Elmino la descripcion
            $descripcion->delete();

            DB::commit();

            return Redirect::back()->with('delete', 'Descripción Eliminada');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return Redirect::back()->with('error', 'La Descripción no pudo ser eliminada');
        }
    }

    ///////////////////////////////////////////API CLIENTE//////////////////

    //Recoger imagen del nivel
    public function ApigetImage($filename = null)
    {

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('niveles/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/nodisponible.png'));
            } else {
                return response()->file(storage_path('app/public/niveles/'.$filename));
            }
        } else {
            return response()->file(storage_path('app/public/nodisponible.png'));
        }
    }

    //Ver los niveles disponibles
    public function ApiList()
    {
        // $niveles = Nivel::orderBy('id_nivel', 'desc')->with('level')->get();
        $niveles = level::orderBy('id_level', 'desc')->with('nivel')->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Niveles disponibles',
            'niveles' => $niveles,
        ], 200);
    }

    //Devolver un nivel seleccionado
    // public function ApiNivel($id_nivel)
    public function ApiNivel($id_level)
    {
        $nivel = level::where('id_level', $id_level)->first();
        // $nivel = Nivel::where('id_nivel', $id_nivel)->with('descripciones')->first();

        if (! $nivel) {
            return response()->json([
                'status' => 'error',
                'message' => 'El nivel solicitado no se encuentra registrado',
            ], 200);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Nivel encontrado',
            'nivel' => $nivel,
        ], 200);
    }
}
