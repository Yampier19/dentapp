<?php

namespace App\Http\Controllers;

use App\Events\BNotificacion;
use App\Helper\Notificacion;
use App\Helper\VideoStream;
use App\Http\Requests\NoticiaStoreRequest;
use App\Http\Requests\NoticiaUpdateRequest;
use App\Mail\Noticia as MailNoticia;
use App\Models\Banner;
use App\Models\ClienteHistorial;
use App\Models\ClienteLogin;
use App\Models\Noticia;
//MODELOS
use App\Models\NoticiaCambio;
use App\Models\NoticiaTipo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
//Helper o Usables
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class NoticiaController extends Controller
{
    //Compresor de Imagen, PDF o Video
    public function imagecompress($image, $extension, $tipo = 'noticia')
    {
        if ($tipo == 'noticia') {
            $width = 400;
            $height = 500;
        } else {
            $width = 600;
            $height = 280;
        }

        if ($extension != 'pdf') {
            $img = Image::make(storage_path('app/public/'.$image))->resize($width, $height)->save();
            $img->destroy();
        }
    }

    public function index()
    {
        $noticias = Noticia::orderBy('id_noticia', 'desc')->paginate(3);
        $tipos = NoticiaTipo::all();

        return view('noticia.index', compact('noticias', 'tipos'));
    }

    //Recoger archivo de la noticia
    public function getDocument($filename = null)
    {
        $extension = null;

        if ($filename) {
            $extension = pathinfo($filename)['extension'];

            $exist = Storage::disk('public')->exists('noticias/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $extension = null;
                $file = storage_path('app/public/nodisponible.png');
            } else {
                $file = storage_path('app/public/noticias/'.$filename);
            }
        } else {
            $file = storage_path('app/public/nodisponible.png');
        }

        switch ($extension) {
            case 'pdf':
                $extension = 'application/pdf';
                break;
            default:
                $extension = 'image';
                break;
        }

        return Response(file_get_contents($file), 200, [
            'Content-Type' => $extension,
            'Content-Disposition' => 'inline; filename="'.$file.'"',
        ]);
    }

    //Recoger preview de la noticia
    public function getImage($filename = null)
    {
        if ($filename) {
            $exist = Storage::disk('public')->exists('noticias/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('nodisponible.png');
            } else {
                $file = Storage::disk('public')->get('noticias/'.$filename);
            }
        } else {
            $file = Storage::disk('public')->get('nodisponible.png');
        }

        return new Response($file, 200);
    }

    //Recoger preview del banner
    public function getBannerImage($filename = null)
    {
        if ($filename) {
            $exist = Storage::disk('public')->exists('banner/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $file = Storage::disk('public')->get('nodisponible.png');
            } else {
                $file = Storage::disk('public')->get('banner/'.$filename);
            }
        } else {
            $file = Storage::disk('public')->get('nodisponible.png');
        }

        return new Response($file, 200);
    }

    //Recoger video del Banner
    public function getBannerArchive($filename = null)
    {
        $extension = null;

        if ($filename) {
            $extension = pathinfo($filename)['extension'];

            $exist = Storage::disk('public')->exists('banner/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                $extension = null;
                $file = storage_path('app/public/nodisponible.png');
            } else {
                $file = storage_path('app/public/banner/'.$filename);
            }
        } else {
            $file = storage_path('app/public/nodisponible.png');
        }

        switch ($extension) {
            case 'pdf':
                $extension = 'application/pdf';
                break;
            case 'mp4':
                $extension = 'mp4';
                break;
            default:
                $extension = 'image';
                break;
        }

        if ($extension == 'mp4') {
            return view('informativo.stream', compact('filename'));
        } else {
            return Response(file_get_contents($file), 200, [
                'Content-Type' => $extension,
                'Content-Disposition' => 'inline; filename="'.$file.'"',
            ]);
        }
    }

    //Listado de Noticias
    public function list()
    {
        $noticias = Noticia::orderBy('id_noticia', 'desc')->get();
        //PARA CREAR NOTICIA
        $tipos = NoticiaTipo::all();

        return view('noticia.listnoticias', compact('noticias', 'tipos'));
    }

    //Listado de Noticias Cambiadas (trazabilidad)
    public function listcambios()
    {
        $noticias = NoticiaCambio::orderBy('created_at', 'desc')->get();

        //Vista de la lista de premios cambios
        //return view('premio.index',compact('cambios'));
    }

    //Envio de correos masivos a usuarios activos
    public function sendEmails($information)
    {
        try {
            $clientes = ClienteLogin::where('estado', 'activo')->get();

            foreach ($clientes as $cliente) {
                try {
                    $information->nombre = $cliente->nombre;
                    Mail::to($cliente->email)->queue(new MailNoticia($information));
                    sleep(5);
                } catch (\Exception $e) {
                }
            }
        } catch (\Exception $e) {
            return;
        }
    }

    public function store(NoticiaStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $data = $request->all();

            if ($request->hasFile('documento')) {
                $data['documento'] = $request->file('documento')->store('noticias', 'public');
            }

            if ($request->hasFile('imagen')) {
                $data['imagen'] = $request->file('imagen')->store('noticias', 'public');
                $this->imagecompress($data['imagen'], $request->file('imagen')->extension(), 'noticia');
            }

            $noticia = Noticia::create($data);
            NoticiaTipo::firstOrCreate(['nombre' => $data['tipo']]);

            $data['accion'] = 'Inserción';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_noticia'] = $noticia->id_noticia;

            NoticiaCambio::create($data);

            //Notificación para los admin
            $data = [
                'nombre' => 'Store de Noticia',
                'tipo' => 'Insert',
                'descripcion' => 'Creación de Noticia: '.$noticia->nombre,
            ];

            Notificacion::instance()->store($data, 'noticia');

            //Notificación para los clientes
            $dataClient = [
                'nombre' => '¡LO ÚLTIMO EN TENDENCIAS ODONTOLÓGICAS!',
                'tipo' => 'Noticia',
                'descripcion' => 'Actualízate y mantente informado con la nueva noticia que publicamos para ti: '.$noticia->nombre.'.',
            ];

            Notificacion::instance()->storeClient($dataClient);

            $title = '¡LO ÚLTIMO EN TENDENCIAS ODONTOLÓGICAS!';
            $text = 'Actualízate y mantente informado con la nueva noticia que publicamos para ti.';
            event(new BNotificacion($text, $title));
            //ENVIO DEL CORREO DE QUE HAY UNA CAPACITACION A TODOS LOS USUARIOS ACTIVOS
            $information = new \stdClass();
            $information->asunto = 'Nueva Noticia Disponible';
            $this->sendEmails($information);
        });

        return Redirect::back()->with('create', 'Noticia Registrada');
        // return response()->json([
        //     'message' => 'noticia creada correctamente',
        //     'code' => 200,
        // ]);
    }

    //Guardado de banner
    public function storeBanner(Request $request)
    {
        DB::beginTransaction();
        try {
            $validate = Validator::make($request->all(), [
                'distribuidor' => 'required|string|max:255',
                'imagen' => 'required|file|mimes:jpg,jpeg,png|max:2100',
                'archivo' => 'file|mimes:mp4|max:'.(1024 * 150),
            ]);

            if ($validate->fails()) {
                //return Redirect::back()->withErrors($validate);
                return response()->json(['errors' => $validate->errors()->all()]);
            }

            $data = $request->all();
            $data['imagen'] = $request->file('imagen')->store('banner', 'public');
            $this->imagecompress($data['imagen'], $request->file('imagen')->extension(), 'banner');

            if ($request->hasFile('archivo')) {
                $data['archivo'] = $request->file('archivo')->store('banner', 'public');
            }

            Banner::create($data);

            DB::commit();
            //return Redirect::back()->with('create', 'Banner Registrado');
            return response()->json(['success' => 'Banner Registrado de forma exitosa']);
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            //return Redirect::back()->with('error', 'El Banner no pudo ser Registrada: '.$e->getMessage());
            return response()->json(['fallo' => 'El Banner no pudo ser Registrado, intentelo más tarde. Error: ' + $e->getMessage()]);
        }
    }

    //Vista para confirmar si de verdad quiere eliminar la noticia
    public function confirm($id_noticia)
    {
        $noticia = Noticia::findOrFail($id_noticia);

        return view('noticia.confirm', compact('noticia'));
    }

    //Eliminar una Noticia
    //Eliminar una Noticia
    public function destroy($id_noticia)
    {
        DB::beginTransaction();
        try {
            $noticia = Noticia::find($id_noticia);

            if (! $noticia) {
                return redirect()->route('noticias.index')->with('edit', 'La capacitación no fue encontrada');
            }

            NoticiaCambio::where('FK_id_noticia', $noticia->id_noticia)->update(['FK_id_noticia' => null]);

            if ($noticia->documento) {
                Storage::delete(['public/'.$noticia->documento]);
            }

            if ($noticia->imagen) {
                Storage::delete(['public/'.$noticia->imagen]);
            }

            $noticia->delete();

            $data = $noticia->toArray();
            $data['accion'] = 'Delete';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;

            NoticiaCambio::create($data);

            $data = [
                'nombre' => 'Delete de Noticia',
                'tipo' => 'Delete',
                'descripcion' => 'Eliminación de Noticia: '.$noticia->nombre,
            ];

            Notificacion::instance()->store($data, 'noticia');

            DB::commit();

            return redirect()->route('noticias.listnoticias')->with('delete', 'Noticia Eliminada Exitosamente');
            // return response()->json([
            //     'message' => 'Noticia eliminada con exito',
            //     'code' => 200,
            // ]);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return Redirect::back()->with('error', 'La Noticia no pudo ser eliminada');
        }
    }

    //Eliminar un Banner
    public function destroyBanner($id_banner)
    {
        DB::beginTransaction();
        try {
            $banner = Banner::find($id_banner);

            if (! $banner) {
                return redirect()->route('noticias.index')->with('edit', 'El banner no fue encontrado');
            }

            if ($banner->imagen) {
                Storage::delete(['public/'.$banner->imagen]);
            }

            if ($banner->archivo) {
                Storage::delete(['public/'.$banner->archivo]);
            }

            $banner->delete();

            DB::commit();

            return Redirect::back()->with('delete', 'Banner Eliminado');
            // return response()->json([
            //     'message' => 'Banner eliminado con exito',
            // ]);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return Redirect::back()->with('error', 'El Banner no pudo ser eliminado');
        }
    }

    //Enviar la Vista para editar una Noticia
    public function edit($id_noticia)
    {
        $noticia = Noticia::findOrFail($id_noticia);
        //PARA CREAR NOTICIA
        $tipos = NoticiaTipo::all();

        return view('noticia.edit', compact('noticia', 'tipos'));
    }

    //Update de los datos de una noticia
    public function update(NoticiaUpdateRequest $request, $id_noticia)
    {
        DB::transaction(function () use ($request, $id_noticia) {
            $noticia = Noticia::findOrFail($id_noticia);
            $data = $request->all();

            if ($request->hasFile('documento')) {
                Storage::delete(['public/'.$noticia->documento]);
                $data['documento'] = $request->file('documento')->store('noticias', 'public');
            }

            if ($request->hasFile('imagen')) {
                Storage::delete(['public/'.$noticia->imagen]);
                $data['imagen'] = $request->file('imagen')->store('noticias', 'public');
                $this->imagecompress($data['imagen'], $request->file('imagen')->extension(), 'noticia');
            }

            $noticia->update($data);
            NoticiaTipo::firstOrCreate(['nombre' => $data['tipo']]);

            $data['accion'] = 'Update';
            $data['FK_id_useradmin'] = auth()->user()->id_useradmin;
            $data['FK_id_noticia'] = $id_noticia;

            NoticiaCambio::create($data);

            $data = [
                'nombre' => 'Update de Noticia',
                'tipo' => 'Update',
                'descripcion' => 'Edición de Noticia: '.$noticia->nombre,
            ];

            Notificacion::instance()->store($data, 'noticia');
        });

        // return response()->json([
        //     'message' => 'Noticia Actualizada correctamente',
        //     'code' => 200,
        // ]);

        return redirect()->back()->with('edit','Noticia editada con exito');
    }

    //Update de los datos de un Banner
    public function updateBanner(Request $request)
    {
        DB::beginTransaction();
        try {
            // $validate = Validator::make($request->all(), [
            //     'id_banner' => 'required',
            //     'imagen' => 'file|mimes:jpg,jpeg,png|max:2100',
            //     'archivo' => 'file|mimes:mp4|max:' . (1024 * 150),
            // ]

            // if ($validate->fails()) {
            //     //return Redirect::back()->withErrors($validate);
            //     return response()->json(['errors' => $validate->errors()->all()]);
            // }

            $banner = Banner::findOrFail($request->input('id_banner'));

            $data = $request->all();

            if ($request->hasFile('imagen')) {
                Storage::delete(['public/'.$banner->imagen]);
                $data['imagen'] = $request->file('imagen')->store('banner', 'public');
                $this->imagecompress($data['imagen'], $request->file('imagen')->extension(), 'banner');
            }

            if ($request->hasFile('archivo')) {
                Storage::delete(['public/'.$banner->archivo]);
                $data['archivo'] = $request->file('archivo')->store('banner', 'public');
            }

            $banner->update($data);

            DB::commit();

            return response()->json(['success' => 'Banner Actualizado']);
            //return Redirect::back()->with('edit', 'Banner Actualizado');
        } catch (\Exception $e) {
            DB::rollback();
            //Enviamos a una vista con un mensaje de error
            //return Redirect::back()->with('error', 'El Banner no pudo ser Editada');
            return response()->json(['fallo' => 'El Banner no pudo ser Editada, Error:'.$e->getMessage()]);
        }
    }

    ///////////////////////////////////////API CLIENTE//////////////////////////////////////////

    //Recoger archivo de la noticia API
    public function ApigetDocument($filename = null)
    {

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('noticias/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/nodisponible.png'));
            } else {
                return response()->file(storage_path('app/public/noticias/'.$filename));
            }
        } else {
            return response()->file(storage_path('app/public/nodisponible.png'));
        }
    }

    //Recoger imagen preview de la noticia API
    public function ApigetImage($filename = null)
    {

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('noticias/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/nodisponible.png'));
            } else {
                return response()->file(storage_path('app/public/noticias/'.$filename));
            }
        } else {
            return response()->file(storage_path('app/public/nodisponible.png'));
        }
    }

    //Recoger imagen preview del Banner API
    public function ApigetImageBanner($filename = null)
    {

        //EJEMPLO RUTA GRABADA 3W9XwS4IJIWHch66dPbFSfNQwqNAmXv6JmNJpk1T.png
        if ($filename) {
            $exist = Storage::disk('public')->exists('banner/'.$filename);
            if (! $exist) { //si no existe el file devuelveme el estandar
                return response()->file(storage_path('app/public/nodisponible.png'));
            } else {
                return response()->file(storage_path('app/public/banner/'.$filename));
            }
        } else {
            return response()->file(storage_path('app/public/nodisponible.png'));
        }
    }

    //Recoger video de Banner
    public function ApiTestVideo($filename = null)
    {
        try {
            $filePath = storage_path('app/public/banner/'.$filename);
            $stream = new VideoStream($filePath);

            return response()->stream(function () use ($stream) {
                $stream->start();
            });
            //$stream->start();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al abrir stream: No se encontro el archivo',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Ver las categorias que existen de noticias
    public function ApigetCategories()
    {
        $categorias = NoticiaTipo::all();

        return response()->json([
            'status' => 'success',
            'message' => 'Categorias disponibles',
            'categorias' => $categorias,
        ], 200);
    }

    //Ver las noticias disponibles
    public function ApiListado($registros = 3)
    {

        /* $noticias = Noticia::where('estado',1)->
                            where('fecha_inicio', '<=', date("Y-m-d"))->where('fecha_fin', '>=', date("Y-m-d"))->
                            orderBy('created_at', 'desc')->paginate($registros); */

        $noticias = Noticia::where('estado', 1)->where('fecha_inicio', '<=', date('Y-m-d'))->where(function ($query) {
            $query->whereNull('fecha_fin')
                ->orWhere('fecha_fin', '>=', date('Y-m-d'));
        })->orderBy('created_at', 'desc')->paginate($registros);

        $paginacion = [
            'total' => $noticias->total(),
            'count' => $noticias->count(),
            'per_page' => $noticias->perPage(),
            'current_page' => $noticias->currentPage(),
            'total_pages' => $noticias->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Noticias disponibles',
            'noticias' => $noticias->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Ver las noticias por categoria seleccionada
    public function ApiListCategory($categoria, $registros = 3)
    {

        /* $noticias = Noticia::where('estado',1)->where('tipo',$categoria)->
                                where('fecha_inicio', '<=', date("Y-m-d"))->where('fecha_fin', '>=', date("Y-m-d"))->
                                orderBy('created_at', 'desc')->paginate($registros); */

        $noticias = Noticia::where('estado', 1)->where('tipo', $categoria)->where('fecha_inicio', '<=', date('Y-m-d'))->where(function ($query) {
            $query->whereNull('fecha_fin')
                ->orWhere('fecha_fin', '>=', date('Y-m-d'));
        })->orderBy('created_at', 'desc')->paginate($registros);

        $paginacion = [
            'total' => $noticias->total(),
            'count' => $noticias->count(),
            'per_page' => $noticias->perPage(),
            'current_page' => $noticias->currentPage(),
            'total_pages' => $noticias->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Noticias disponibles '.$categoria,
            'noticias' => $noticias->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Ver las ultimas noticias por fechas menores de 2 dias
    public function ApiNew($registros = 3)
    {

        /* $noticias = Noticia::where('estado',1)->where('updated_at', '>=' , date('Y-m-d', strtotime("-2 days")))->
                                where('fecha_inicio', '<=', date("Y-m-d"))->where('fecha_fin', '>=', date("Y-m-d"))->
                                orderBy('created_at', 'desc')->paginate($registros); */

        $noticias = Noticia::where('estado', 1)->where('updated_at', '>=', date('Y-m-d', strtotime('-2 days')))->where('fecha_inicio', '<=', date('Y-m-d'))->where(function ($query) {
            $query->whereNull('fecha_fin')
                ->orWhere('fecha_fin', '>=', date('Y-m-d'));
        })->orderBy('created_at', 'desc')->paginate($registros);

        $paginacion = [
            'total' => $noticias->total(),
            'count' => $noticias->count(),
            'per_page' => $noticias->perPage(),
            'current_page' => $noticias->currentPage(),
            'total_pages' => $noticias->lastPage(),
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Noticias disponibles',
            'noticias' => $noticias->getCollection(),
            'paginacion' => $paginacion,
        ], 200);
    }

    //Devolver una noticia seleccionada
    public function ApiNoticia($id_noticia)
    {
        DB::beginTransaction();
        try {
            $noticia = Noticia::where('id_noticia', $id_noticia)->first();

            if (! $noticia) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'La noticia solicitada no se encuentra registrada',
                ], 200);
            }

            //Grabamos el historial del cliente
            $historial = new ClienteHistorial();
            $historial->accion = 'Visualizo la noticia: '.$noticia->nombre;
            $historial->fecha_registro = date('Y-m-d');
            $historial->FK_id_cliente = auth()->user()->FK_id_cliente;
            $historial->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Noticia encontrada',
                'noticia' => $noticia,
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Fallo al obtener los datos de la noticia solicitada',
                'errores' => $e->getMessage(),
            ], 500);
        }
    }

    //Devolver todos los banner
    public function ApiBanners()
    {
        $user = auth()->user()->cliente;

        $banners = Banner::where('distribuidor', 'All')
            ->orWhere('distribuidor', 'like', '%'.$user->distribuidor.'%')->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Banners disponibles',
            'banners' => $banners,
        ], 200);
    }
}
