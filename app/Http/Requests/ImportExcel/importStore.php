<?php

namespace App\Http\Requests\ImportExcel;

use Illuminate\Foundation\Http\FormRequest;

class importStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre.*' => 'required|string|max:50',
            'apellido.*' => 'required|string|max:50',
            'segmento.*' => 'required|string|max:255',
            'telefono.*' => 'required|string|max:15',
            'correo.*' => 'required|string|distinct|email|max:50|unique:dentapp_clientelogin,email',
            'especialidad.*' => 'required|string|max:50',
        ];
    }
}
