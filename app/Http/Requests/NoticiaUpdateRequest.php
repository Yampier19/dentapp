<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoticiaUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:255',
            'informacion' => 'required|string|max:65535',
            'tipo' => 'required|string|max:255',
            'estado' => 'boolean',
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'date',
            'imagen' => 'nullable|file|mimes:jpg,jpeg,png|max:2100',
            'documento' => 'nullable|file|mimes:jpg,jpeg,png,pdf|max:2100',
        ];
    }
}
