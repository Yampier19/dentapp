<?php

namespace App\Http\Requests\Cliente;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'foto' => 'nullable|mimes:png,jpg',
            'nombre' => 'required|string|max:50',
            'apellido' => 'required|string|max:50',
            'loginrol_id' => 'required|exists:dentapp_loginrol,id_loginrol',
            'segmento' => 'required|string|max:255',
            'document_type_id' => 'required|exists:document_types,id',
            'city_id' => 'required|exists:cities,id',
            'document_number' => 'required|alpha_num|unique:dentapp_cliente,document_number',
            'direccion' => 'nullable|string|max:50',
            'distribuidor' => 'required|string|max:255',
            'telefono' => 'required|string|max:15',
            'correo' => 'required|string|email|max:50|unique:dentapp_clientelogin,email',
            'total_estrellas' => 'required|numeric|min:0',
            'especialidad' => 'required|string|max:50',
            'password' => [
                'nullable',
                'string',
                'min:8',
                'max:50',
            ],
            'estado' => 'required',
            'consultorio' => 'required',
        ];
        if ($this->consultorio == 1) {
            $validations['nombre_c'] = 'required|string|max:100';
            $validations['direccion_c'] = 'required|string|max:100';
            $validations['telefono_c'] = 'required|string|max:15';
            $validations['numero_pacientes'] = 'required|numeric|min:0';
        } else {
            $validations['nombre_c'] = 'exclude';
            $validations['direccion_c'] = 'exclude';
            $validations['telefono_c'] = 'exclude';
            $validations['numero_pacientes'] = 'exclude';
        }

        return $validations;
    }
}
