<?php

namespace App\Http\Requests\Cliente;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'nombre' => 'required|string|max:50',
            'apellido' => 'required|string|max:50',
            'segmento' => 'required|string|max:255',
            'direccion' => 'nullable|string|max:50',
            'city_id' => 'nullable|exists:cities,id',
            'loginrol_id' => 'required|exists:dentapp_loginrol,id_loginrol',
            'distribuidor' => 'required|string|max:255',
            'telefono' => 'required|string|max:15',
            'foto' => 'nullable|file|mimes:png,jpg,jpeg',
            'correo' => [
                'required',
                'email:rfc,dns',
                Rule::unique('dentapp_clientelogin', 'email')
                    ->ignore($this->cliente->login->id_clientelogin, 'id_clientelogin'),
            ],
            'total_estrellas' => 'required|numeric|min:0',
            'especialidad' => 'required|string|max:50',
            'password' => [
                'nullable',
                'string',
                'min:8',
                'max:50',
            ],
            'estado' => 'required',
            'consultorio' => 'required',
            'document_number' => 'required',
            'document_type_id' => 'required',
        ];
        if ($this->input('consultorio') == 1) {
            $validations['nombre_c'] = 'required|string|max:100';
            $validations['direccion_c'] = 'required|string|max:100';
            $validations['telefono_c'] = 'required|string|max:15';
            $validations['numero_pacientes'] = 'required|numeric|min:0';
        } else {
            $validations['nombre_c'] = 'exclude';
            $validations['direccion_c'] = 'exclude';
            $validations['telefono_c'] = 'exclude';
            $validations['numero_pacientes'] = 'exclude';
        }

        return $validations;
    }

    public function messages()
    {
        return [
            'telefono.max' => 'El telefono no puede ser mayor a 15 digitos',
            'telefono_c.max' => 'El telefono no puede ser mayor a 15 digitos',
            'nombre_c.max' => 'El nombre no puede ser mayor a 100 caracteres',
            'direccion_c.max' => 'El dirección no puede ser mayor a 100 caracteres',
            'foto.max' => 'La imagen no puede ser mayor a 2Mb',
            'password.min' => 'La contraseña debe ser mayor a 8 caracteres',
            'correo.email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
            'password.regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
        ];
    }
}
