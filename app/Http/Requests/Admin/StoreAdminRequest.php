<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'nombre' => 'required|string|max:50',
            'apellido' => 'required|string|max:50',
            'email' => 'required|string|email|max:50|unique:dentapp_useradmin,email',
            'estado' => 'required|string|max:50',
            'password' => 'nullable', 'string', 'min:8', 'regex:/[a-z]/', 'regex:/[A-Z]/', 'regex:/[0-9]/',
            'FK_id_loginrol' => 'required',
        ];

        if ($this->FK_id_loginrol == 1) {
            $validations['permisos'] = 'exclude';
        } else {
            $validations['permisos'] = 'required|array|min:1';
        }

        return $validations;
    }

    public function messages()
    {
        return [
            'foto.max' => 'La imagen no puede ser mayor a 2Mb',
            'permisos.required' => 'El usuario debe tener minimo un permiso',
            'FK_id_loginrol.required' => 'El Rol es un campo obligatorio',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'permisos' => array_filter([
                $this->p_store,
                $this->p_update,
                $this->p_destroy,
                $this->p_productos,
                $this->p_premios,
                $this->p_facturas,
                $this->p_noticias,
                $this->p_capacitaciones,
                $this->p_trivia,
            ]),
        ]);
    }
}
