<?php

namespace App\Http\Requests\temporada;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'start_date' => 'date|required',
            'finish_date' => 'date|required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'start_date' => 'Fecha de inicio',
            'finish_date' => 'Fecha de finalización',
        ];
    }
}
