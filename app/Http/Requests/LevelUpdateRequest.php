<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LevelUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rangoinicio = $this->rango_inicio;

        return [
            'nombre' => 'required|min:3|required',
            'rango_inicio' => [
                'required', 'numeric', 'min:0',
                Rule::unique('dentapp_level', 'rango_inicio')
                    ->ignore($this->level->id_level, 'id_level'),
            ],
            'meta' => [
                'required', 'numeric', 'min:'.$rangoinicio,
                Rule::unique('dentapp_level', 'meta')
                    ->ignore($this->level->id_level, 'id_level'),
            ],
        ];
    }
}
