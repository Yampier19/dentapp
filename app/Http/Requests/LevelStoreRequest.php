<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LevelStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rangoinicio = $this->rango_inicio;

        return [
            'nombre' => 'required|min:3|required',
            'rango_inicio' => 'required|unique:dentapp_level,rango_inicio|min:0|unique:dentapp_level,meta',
            'meta' => 'required|unique:dentapp_level,rango_inicio|unique:dentapp_level,meta|numeric|min:'.$rangoinicio,
            'level-image' => 'required|image|mimes:png,jpg',
        ];
    }

    public function attributes()
    {
        return [
            'rango_inicio' => 'Puntaje inicial',
            'meta' => 'Puntaje maximo',
        ];
    }
}
