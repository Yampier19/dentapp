<?php

namespace App\Http\Requests\profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'foto' => 'file|mimes:jpg,jpeg,png|max:2100',
            'password' => 'string',
            'min:8',
            'max:50',
            'regex:/[a-z]/',
            'regex:/[A-Z]/',
            'regex:/[0-9]/',
            'nullable',
            'confirmacion' => 'string|same:password|nullable',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'El dato es requerido',
            'numeric' => 'El dato debe ir en formato numerico',
            'file' => 'El dato debe llegar como un archivo',
            'mime' => 'El archivo debe llegar en formato png, jpg, jpeg o pdf',
            'min' => 'El dato debe ser mayor a 8 carácteres',
            'max' => 'El dato no debe ser mayor a 50 carácteres',
            'email' => 'El dato debe ir en formato de correo (ejemplo@correo.com)',
            'unique' => 'El dato enviado ya se encuentra registrado',
            'same' => 'La contraseña no coincide con la de confirmación',
            'regex' => 'La contraseña debe contener mayuscula, números y caracteres especiales',
            'foto.max' => 'La imagen no puede ser mayor a 2Mb',
        ];
    }
}
