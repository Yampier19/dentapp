<?php

namespace App\Listeners;

use App\Events\eventnew;
use App\Mail\Noticia as MailNoticia;
use Illuminate\Support\Facades\Mail;

class EventNewListener
{
    /**
     * Handle the event.
     *
     * @param  \App\Events\eventnew  $event
     * @return void
     */
    public function handle(eventnew $event)
    {
        foreach ($event->clientes as $cliente) {
            Mail::to($cliente)->queue(new MailNoticia($event->noticia));
        }
    }
}
