<?php

namespace App\Mail;

use App\Models\Cliente;
use App\Models\Level;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class leveldefault extends Mailable
{
    use Queueable, SerializesModels;

    public $level;

    public $cliente;

    public $asunto;

    public function __construct(Level $level, Cliente $cliente)
    {
        $this->level = $level;
        $this->cliente = $cliente;
        $this->asunto = 'A CELEBRAR, ¡SUBISTE A '.Str::upper($level->nombre).' EN DENTAPP!';
    }

    public function build()
    {
        return $this->view('mails.leveldefault')
            ->subject($this->asunto);
    }
}
