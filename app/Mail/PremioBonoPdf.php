<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PremioBonoPdf extends Mailable
{
    use Queueable, SerializesModels;

    public $information;

    public function __construct($information)
    {
        $this->information = $information;
        $this->information->url = 'https://api.activarpromo.com/productos/viewpdf/'.$this->information->referencia;
    }

    public function build()
    {
        if ($this->information->archivo) {
            return $this->view('mails.PremioBonoPdf')
                ->subject($this->information->asunto)
                ->attach($this->information->archivo, [
                    'as' => 'premio.pdf',
                    'mime' => 'application/pdf',
                ]);
        } else {
            return $this->view('mails.PremioBonoPdf')
                ->subject($this->information->asunto);
        }
    }
}
