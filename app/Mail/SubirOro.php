<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubirOro extends Mailable
{
    use Queueable, SerializesModels;

    public $information;

    public function __construct($information)
    {
        $this->information = $information;
    }

    public function build()
    {
        return $this->view('mails.OroEmail')
            ->subject($this->information->asunto);
    }
}
