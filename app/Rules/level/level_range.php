<?php

namespace App\Rules\level;

use Illuminate\Contracts\Validation\InvokableRule;

class level_range implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if ($value !== $value) {
            $fail('The :attribute must be uppercase.');
        }
    }
}
