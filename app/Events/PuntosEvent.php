<?php

namespace App\Events;

use App\Models\Cliente;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
//MODELOS
use Illuminate\Queue\SerializesModels;

class PuntosEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $afterCommit = true;

    public $cliente;

    public $title;

    public $message;

    public function __construct($title, $message, $id_cliente)
    {
        $cliente = Cliente::find($id_cliente);
        $this->cliente = $cliente->correo;
        $this->message = $message;
        $this->title = $title;
    }

    public function broadcastOn()
    {
        return new Channel('puntos.'.$this->cliente);
    }

    public function broadcastAs()
    {
        return 'puntosState';
    }
}
