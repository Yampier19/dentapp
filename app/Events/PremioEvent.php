<?php

namespace App\Events;

use App\Models\PremioCanjeado;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
//MODELOS
use Illuminate\Queue\SerializesModels;

class PremioEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $afterCommit = true;

    //public $canjeado;
    public $cliente;

    public $title;

    public $message;

    public function __construct($title, $message, $id_premio)
    {
        $canjeado = PremioCanjeado::find($id_premio);
        $this->cliente = $canjeado->cliente->correo;
        $this->message = $message;
        $this->title = $title;
    }

    public function broadcastOn()
    {
        return new Channel('premio.'.$this->cliente);
    }

    public function broadcastAs()
    {
        return 'premioState';
    }
}
