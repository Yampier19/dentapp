<?php

namespace App\Events;

use App\Models\ClienteLogin;
use App\Models\Noticia;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class eventnew
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $mailNoticia;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Noticia $noticia)
    {
        $this->noticia = $noticia;
        $this->clientes = ClienteLogin::where('estado', 'activo')->get();
    }
    //  public function sentemail(){
//     try {
//         $clientes = ClienteLogin::where('estado', 'activo')->get();

//         foreach ($clientes as $cliente) {
//             try {
//                 $information->nombre = $cliente->nombre;
//                 Mail::to($cliente->email)->queue(new MailNoticia($information));
//             } catch (\Exception $e) {
//             }
//         }
//     } catch (\Exception $e) {
//         return;
//     }
//  }
}
