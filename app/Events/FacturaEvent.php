<?php

namespace App\Events;

use App\Models\Factura;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
//MODELOS
use Illuminate\Queue\SerializesModels;

class FacturaEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $afterCommit = true;

    //public $factura;
    public $cliente;

    public $title;

    public $message;

    public function __construct($title, $message, $id_factura)
    {
        $factura = Factura::find($id_factura);
        $this->cliente = $factura->cliente->correo;
        $this->message = $message;
        $this->title = $title;
    }

    public function broadcastOn()
    {
        return new Channel('factura.'.$this->cliente);
    }

    public function broadcastAs()
    {
        return 'facturaState';
    }
}
