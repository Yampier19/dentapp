<?php

namespace App\Console\Commands;

use App\Models\Cliente;
use App\Models\Level;
use App\Models\UserSeason;
use Illuminate\Console\Command;

class ExpirationDateTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inabilitar puntos vencidos';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /*Cambiamos el estado a inactivo cuando el año de expiracion sea equivalente a el dia actual*/
        UserSeason::where('expired_at', now()->toDateString())
            ->update(['status' => 'Inactivo']);


        /*Traemos los registros de puntos que se vencieron ese dia*/
        $overduePoints = UserSeason::where('status', 'Inactivo')
            ->where('expired_at', now()->toDateString())
            ->get();

        /**Recorremos los puntos vencidos y se los descontamos a el cliente */
        foreach ($overduePoints as $overduePoint) {
            $client = $overduePoint->client;
            $client->total_estrellas -= $overduePoint->points;
            $overduePoint->cliente;
            $overduePoint->points = 0;
            $overduePoint->save();
            $client->save();
        }

        /*Recorremos los clientes sumamos las estrellas y asignamos el nivel*/
        $clientes = Cliente::all();
        foreach ($clientes as $cliente) {
            $level = Level::whereRaw('? BETWEEN rango_inicio and meta', [$cliente->total_puntos_reunidos])->first();
            $cliente->FK_id_level = $level->id_level;
            $cliente->update();
        }
    }
}
