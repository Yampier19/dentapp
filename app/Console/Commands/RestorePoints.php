<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
//MODELOS
use Illuminate\Support\Facades\DB;

class RestorePoints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'points:halfyear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que se ejecuta cada 6 meses para reiniciar los puntos y niveles de los clientes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('dentapp_cliente')
            ->update([
                'total_estrellas' => 0,
                'total_reunido' => 0.000,
            ]);
        $this->info('Todos los usuarios clientes reiniciaron sus puntos y facturación a 0');
    }
}
