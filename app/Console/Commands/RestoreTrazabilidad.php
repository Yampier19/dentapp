<?php

namespace App\Console\Commands;

use App\Models\CapacitacionCambio;
//MODELOS
use App\Models\ClienteHistorial;
use App\Models\NoticiaCambio;
use App\Models\PremioCambio;
use App\Models\ProductoCambio;
use App\Models\TriviaCambio;
use App\Models\UserHistory;
use Illuminate\Console\Command;

class RestoreTrazabilidad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trazabilidad:year';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cada inicia del año fiscal se elimina los datos de trazabilidad almacenados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Eliminamos todos los de trazabilidad
        CapacitacionCambio::query()->each(function ($oldRecord) {
            $respaldo = $oldRecord->replicate();
            $respaldo->setTable('dentapp_capacitacion_respaldo');
            $respaldo->save();
            $oldRecord->delete();
        });

        ClienteHistorial::query()->each(function ($oldRecord) {
            $respaldo = $oldRecord->replicate();
            $respaldo->setTable('dentapp_historial_cliente_respaldo');
            $respaldo->save();
            $oldRecord->delete();
        });

        NoticiaCambio::query()->each(function ($oldRecord) {
            $respaldo = $oldRecord->replicate();
            $respaldo->setTable('dentapp_noticias_respaldo');
            $respaldo->save();
            $oldRecord->delete();
        });

        PremioCambio::query()->each(function ($oldRecord) {
            $respaldo = $oldRecord->replicate();
            $respaldo->setTable('dentapp_premios_respaldo');
            $respaldo->save();
            $oldRecord->delete();
        });

        ProductoCambio::query()->each(function ($oldRecord) {
            $respaldo = $oldRecord->replicate();
            $respaldo->setTable('dentapp_producto_respaldo');
            $respaldo->save();
            $oldRecord->delete();
        });

        TriviaCambio::query()->each(function ($oldRecord) {
            $respaldo = $oldRecord->replicate();
            $respaldo->setTable('dentapp_trivia_respaldo');
            $respaldo->save();
            $oldRecord->delete();
        });

        UserHistory::query()->each(function ($oldRecord) {
            $respaldo = $oldRecord->replicate();
            $respaldo->setTable('dentapp_history_user_respaldo');
            $respaldo->save();
            $oldRecord->delete();
        });

        $this->info('Todos los datos de trazabilidad han sido eliminado');
    }
}
