<?php

namespace App\Services;

use App\Helper\Notificacion;
use App\Mail\Welcome;
use App\Models\Cliente;
use App\Models\ClienteLogin;
use App\Models\ClienteNotificacion;
//Helper
use App\Models\ClientePassword;
use App\Models\Consultorio;
use App\Models\OAuthToken;
//Modelos
use App\Models\UserHistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

//MAILABLE

class ClienteService
{
    public function index()
    {
        return Cliente::where('loginrol_id', 6)
            ->orwhere('loginrol_id', 5)
            ->orwhere('loginrol_id', 4)
            ->orderBy('id_cliente', 'desc')->get();
    }

    public function store(array $input)
    {
        DB::transaction(function () use ($input) {
            if (! empty($input['foto'])) {
                $input['foto'] = $input['foto']->store('uploads', 'public');
                $this->imagecompress($input['foto']);
            }

            if (! empty($input['distribuidor'])) {
                $input['last_change'] = date('Y-m-d');
            }

            $input['FK_id_level'] = 1;
            $cliente = Cliente::create($input);

            $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $password = substr(str_shuffle($caracteres_permitidos), 0, 8);

            //Grabamos el historial de passwords
            $history = new ClientePassword();
            $history->password = Hash::make($password);
            $history->registro = date('Y-m-d');
            $history->FK_id_cliente = $cliente->id_cliente;
            $history->save();

            //Grabamos el login del cliente
            $login = new ClienteLogin();
            $login->email = $input['correo'];
            $login->password = Hash::make($password);
            $login->estado = $input['estado'];
            $login->segmento = $input['segmento'];
            $login->fecha_cambio = date('Y-m-d');
            $login->FK_id_cliente = $cliente->id_cliente;
            $login->save();

            //Grabamos el historial del usuario login store
            $loghistory = new UserHistory();
            $loghistory->accion = 'Store de Usuario Cliente Login';
            $loghistory->email = $input['correo'];
            $loghistory->password = $password;
            $loghistory->FK_id_cliente = $cliente->id_cliente;
            $loghistory->save();

            //Grabamos el consultorio
            if ($input['consultorio'] == 1) {
                $consultorio = new Consultorio();
                $consultorio->nombre = $input['nombre_c'];
                $consultorio->direccion = $input['direccion_c'];
                $consultorio->telefono = $input['telefono_c'];
                $consultorio->numero_pacientes = $input['numero_pacientes'];
                $consultorio->FK_id_cliente = $cliente->id_cliente;
                $consultorio->save();
            }

            $datos = [
                'nombre' => 'Store Cliente',
                'tipo' => 'Insert',
                'descripcion' => 'Cliente Creado: '.$cliente->nombre.' '.$cliente->apellido,
            ];

            Notificacion::instance()->store($datos, 'usuarios');

            //ENVIO DEL CORREO DE BIENVENIDA
            $information = new \stdClass();
            $information->asunto = 'Bienvenido a DentApp';
            $information->nombre = $cliente->nombre.' '.$cliente->apellido;
            $information->correo = $cliente->correo;
            $information->contraseña = $password;
            $information->usuario = $cliente->id_cliente;

            Mail::to($cliente->correo)->queue(new Welcome($information));
        });
    }

    public function update(Cliente $cliente, array $input)
    {
        DB::transaction(function () use ($input, $cliente) {
            $lastemail = $cliente->correo;

            if (! empty($input['foto'])) {
                Storage::delete(['public/'.$cliente->foto]);
                $input['foto'] = $input['foto']->store('uploads', 'public');
                $this->imagecompress($input['foto']);
            }

            if (! empty($input['distribuidor'])) {
                $input['last_change'] = date('Y-m-d');
            }

            $cliente->update($input);

            //Cerramos los token de session del usuario si su estado pasa a incativo
            if ($input['estado'] == 'inactivo') {
                OAuthToken::where('user_id', $cliente->login->id_clientelogin)->update(['revoked' => 1]);
            }

            //Update de los datos de login
            $login = $cliente->login;
            $login->email = $input['correo'];
            if ($input['password']) {
                $login->password = Hash::make($input['password']);
                $login->cambiar = 1;
                $login->fecha_cambio = date('Y-m-d');
            }
            $login->estado = $input['estado'];
            $login->segmento = $input['segmento'];
            $login->update();

            //Grabamos el historial del usuario login update
            if ($input['password']) {
                $loghistory = new UserHistory();
                $loghistory->accion = 'Update de Usuario Cliente Login';
                $loghistory->email = $input['correo'];
                $loghistory->password = $input['password'];
                $loghistory->FK_id_cliente = $cliente->id_cliente;
                $loghistory->save();
            }

            //Grabado o Update de consultorio
            if ($cliente->consultorio) {
                $consultorio = $cliente->consultorio;
                $consultorio->nombre = $input['nombre_c'];
                $consultorio->direccion = $input['direccion_c'];
                $consultorio->telefono = $input['telefono_c'];
                $consultorio->numero_pacientes = $input['numero_pacientes'];
                $consultorio->FK_id_cliente = $cliente->id_cliente;
                $consultorio->update();
            } elseif ($input['consultorio'] == 1) {
                $consultorio = new Consultorio();
                $consultorio->nombre = $input['nombre_c'];
                $consultorio->direccion = $input['direccion_c'];
                $consultorio->telefono = $input['telefono_c'];
                $consultorio->numero_pacientes = $input['numero_pacientes'];
                $consultorio->FK_id_cliente = $cliente->id_cliente;
                $consultorio->save();
            }

            $inputNotification = [
                'nombre' => 'Update Cliente',
                'tipo' => 'Update',
                'descripcion' => 'Cliente Editado: '.$cliente->nombre.' '.$cliente->apellido,
            ];

            Notificacion::instance()->store($inputNotification, 'usuarios');

            if ($input['password'] || $lastemail != $input['correo']) {
                $information = new \stdClass();
                $information->asunto = 'Cambio de Datos de Usuario - DentApp';
                $information->nombre = $cliente->nombre.' '.$cliente->apellido;
                $information->correo = $input['correo'];
                $information->contraseña = $input['password'] ? $input['password'] : 'La misma de antes';
                $information->usuario = '';
                Mail::to($input['correo'])->queue(new Welcome($information));
            }
        });
    }

    public function destroy(Cliente $cliente)
    {
        DB::transaction(function () use ($cliente) {
            if (! $cliente) {
                return redirect()->route('cliente.index')->with('edit', 'El Cliente no se encuentra registrado');
            }

            if ($cliente->foto) {
                Storage::delete(['public/'.$cliente->foto]);
            }

            OAuthToken::where('user_id', $cliente->login->id_clientelogin)->delete();
            $cliente->login->delete();
            $cliente->consultorio ? $cliente->consultorio->delete() : '';
            ClienteNotificacion::where('FK_id_cliente', $cliente)->delete();
            $cliente->delete();

            $data = [
                'nombre' => 'Delete Cliente',
                'tipo' => 'Delete',
                'descripcion' => 'Cliente Eliminado: '.$cliente->nombre.' '.$cliente->apellido,
            ];

            Notificacion::instance()->store($data, 'usuarios');
        });
    }

    //Chequiamos password si se asemeja al correo
    public function checkPassword($check)
    {
        //Chequiamos si es el correo y el password son iguales
        $check['correo'] = explode(' ', strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['correo'])));
        $check['password'] = strtolower(preg_replace('/[^A-Za-z]+/', ' ', $check['password']));

        if (stripos($check['password'], $check['correo'][0]) !== false) {
            return true;
        } else {
            return false;
        }
    }

    //Compresor de Imagen
    public function imagecompress($image)
    {
        $img = Image::make(storage_path('app/public/'.$image))->resize(500, 500, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save();
        $img->destroy();
    }
}
