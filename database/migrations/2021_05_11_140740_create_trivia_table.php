<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriviaTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_trivia', function (Blueprint $table) {
            Schema::create('dentapp_trivia', function (Blueprint $table) {
                $table->id('id_trivia');
                $table->string('nombre', 50);
                $table->string('descripcion', 255);
                $table->string('tipo', 50)->default('trivia'); //home o trivia normal
                $table->integer('puntuacion');
                $table->boolean('estado')->default(false);
                $table->date('fecha_inicio')->nullable();
                $table->date('fecha_fin')->nullable();
                $table->bigInteger('FK_id_capacitacion')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_capacitacion')->references('id_capacitacion')->on('dentapp_capacitacion');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_trivia');
    }
}
