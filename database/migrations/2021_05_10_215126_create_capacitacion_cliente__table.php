<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionClienteTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_capacitacion_cliente', function (Blueprint $table) {
            Schema::create('dentapp_capacitacion_cliente', function (Blueprint $table) {
                $table->id('id_capacitacion_cliente');
                $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
                $table->bigInteger('FK_id_capacitacion')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
                $table->foreign('FK_id_capacitacion')->references('id_capacitacion')->on('dentapp_capacitacion');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_capacitacion_cliente');
    }
}
