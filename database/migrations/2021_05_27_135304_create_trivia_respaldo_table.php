<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriviaRespaldoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_trivia_respaldo', function (Blueprint $table) {
            Schema::create('dentapp_trivia_respaldo', function (Blueprint $table) {
                $table->id('id_trivia_respaldo');
                $table->string('accion', 50);
                $table->string('nombre', 50);
                $table->string('descripcion', 255);
                $table->integer('puntuacion');
                $table->boolean('estado')->default(true);
                $table->date('fecha_inicio')->nullable();
                $table->date('fecha_fin')->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable();
                $table->bigInteger('FK_id_trivia')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_trivia_respaldo');
    }
}
