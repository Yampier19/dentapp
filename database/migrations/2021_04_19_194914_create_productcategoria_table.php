<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductcategoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_producto_categoria', function (Blueprint $table) {
            $table->id('id_producto_categoria');
            $table->string('categoria', 50);
            $table->string('subcategoria', 50);
            $table->bigInteger('stock');
            $table->string('descripcion', 255);
            $table->float('precio');
            $table->bigInteger('estado');
            $table->string('foto', 255)->nullable();
            $table->bigInteger('numero_estrellas');
            $table->bigInteger('FK_id_familia')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_familia')->references('id_producto_familia')->on('dentapp_producto_familia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_producto_categoria');
    }
}
