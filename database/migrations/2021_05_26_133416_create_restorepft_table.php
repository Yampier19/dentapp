<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestorepftTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_restorepft', function (Blueprint $table) {
            Schema::create('dentapp_restorepft', function (Blueprint $table) {
                $table->id('id_restore');
                $table->string('nombre');
                $table->date('fecha_cambio');
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_restorepft');
    }
}
