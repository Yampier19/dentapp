<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasCambiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_noticias_cambios', function (Blueprint $table) {
            Schema::create('dentapp_noticias_cambios', function (Blueprint $table) {
                $table->id('id_noticia_cambio');
                $table->string('accion', 255);
                $table->string('nombre', 255);
                $table->text('informacion');
                $table->string('tipo', 255);
                $table->boolean('estado');
                $table->date('fecha_inicio')->nullable();
                $table->date('fecha_fin')->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
                $table->bigInteger('FK_id_noticia')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('dentapp_useradmin');
                $table->foreign('FK_id_noticia')->references('id_noticia')->on('dentapp_noticias');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_noticias_cambios');
    }
}
