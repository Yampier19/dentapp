<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificacionesTable extends Migration
{
    public function up()
    {
        Schema::create('dentapp_notificaciones', function (Blueprint $table) {
            $table->id('id_notificaciones');
            $table->string('nombre', 100);
            $table->string('tipo', 100);
            $table->string('descripcion', 1000);
            $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('dentapp_useradmin');
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_notificaciones');
    }
}
