<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_factura', function (Blueprint $table) {
            $table->id('id_factura');
            $table->string('referencia', 255);
            $table->string('distribuidor', 255);
            $table->text('descripcion')->nullable();
            $table->string('telefono', 255);
            $table->bigInteger('puntos');
            $table->decimal('monto_total', 13, 3);
            $table->string('foto', 255)->nullable();
            $table->date('fecha_registro');
            $table->string('estado')->default('pendiente');
            $table->boolean('revision')->default(false);
            $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_factura');
    }
}
