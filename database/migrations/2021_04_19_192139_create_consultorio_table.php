<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultorioTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_consultorio', function (Blueprint $table) {
            Schema::create('dentapp_consultorio', function (Blueprint $table) {
                $table->id('id_consultorio');
                $table->string('nombre', 100);
                $table->string('direccion', 100);
                $table->string('telefono');
                $table->integer('numero_pacientes');
                $table->bigInteger('FK_id_cliente')->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_consultorio');
    }
}
