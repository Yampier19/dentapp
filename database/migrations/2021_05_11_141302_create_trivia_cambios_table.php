<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriviaCambiosTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_trivia_cambios', function (Blueprint $table) {
            Schema::create('dentapp_trivia_cambios', function (Blueprint $table) {
                $table->id('id_trivia_cambio');
                $table->string('accion', 50);
                $table->string('nombre', 50);
                $table->string('descripcion', 255);
                $table->string('tipo', 50)->default('trivia'); //nuevo
                $table->integer('puntuacion');
                $table->boolean('estado')->default(true);
                $table->date('fecha_inicio')->nullable();
                $table->date('fecha_fin')->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
                $table->bigInteger('FK_id_trivia')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('dentapp_useradmin');
                $table->foreign('FK_id_trivia')->references('id_trivia')->on('dentapp_trivia');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_trivia_cambios');
    }
}
