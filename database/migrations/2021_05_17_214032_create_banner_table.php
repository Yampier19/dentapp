<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_banner', function (Blueprint $table) {
            Schema::create('dentapp_banner', function (Blueprint $table) {
                $table->id('id_banner');
                $table->string('distribuidor', 255)->nullable();
                $table->string('imagen', 255)->nullable();
                $table->string('archivo', 255)->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_banner');
    }
}
