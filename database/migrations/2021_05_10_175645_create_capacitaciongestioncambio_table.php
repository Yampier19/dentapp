<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitaciongestioncambioTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_capacitacion_gestion_cambio', function (Blueprint $table) {
            Schema::create('dentapp_capacitacion_gestion_cambio', function (Blueprint $table) {
                $table->id('id_capacitacion_cambio');
                $table->string('accion', 50);
                $table->string('nombre', 50);
                $table->string('descripcion', 255);
                $table->string('tipo');
                $table->string('informacion')->nullable();
                $table->string('archivo', 255)->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
                $table->bigInteger('FK_id_capacitacion')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('dentapp_useradmin');
                $table->foreign('FK_id_capacitacion')->references('id_capacitacion')->on('dentapp_capacitacion');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_capacitacion_gestion_cambio');
    }
}
