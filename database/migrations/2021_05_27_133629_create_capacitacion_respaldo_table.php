<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionRespaldoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_capacitacion_respaldo', function (Blueprint $table) {
            Schema::create('dentapp_capacitacion_respaldo', function (Blueprint $table) {
                $table->id('id_capacitacion_respaldo');
                $table->string('accion', 50);
                $table->string('nombre', 50);
                $table->string('descripcion', 255);
                $table->string('tipo');
                $table->string('informacion')->nullable();
                $table->string('archivo', 255)->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable();
                $table->bigInteger('FK_id_capacitacion')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_capacitacion_respaldo');
    }
}
