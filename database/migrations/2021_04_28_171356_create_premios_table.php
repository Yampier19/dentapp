<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_premios', function (Blueprint $table) {
            Schema::create('dentapp_premios', function (Blueprint $table) {
                $table->id('id_premio');
                $table->string('nombre', 50);
                $table->string('marca', 255); //
                $table->text('descripcion'); //
                $table->string('entrega', 255);
                $table->string('tag', 50);
                $table->bigInteger('stock');
                $table->bigInteger('limite_usuario')->nullable();
                $table->string('foto', 500)->nullable();
                $table->bigInteger('numero_estrellas')->nullable();

                //Datos de Distribuidor si tiene
                $table->string('distribuidor', 255)->default('N/A');
                $table->bigInteger('brand_id')->nullable();
                $table->bigInteger('producto_id')->nullable();

                $table->timestamps();
                $table->softDeletes();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_premios');
    }
}
