<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavidadPremiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_navidad_premios', function (Blueprint $table) {
            Schema::create('dentapp_navidad_premios', function (Blueprint $table) {
                $table->id('id_navidad_premios');
                $table->string('tipo', 255)->default('bono');
                $table->bigInteger('FK_id_premio')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_premio')->references('id_premio')->on('dentapp_premios');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_navidad_premios');
    }
}
