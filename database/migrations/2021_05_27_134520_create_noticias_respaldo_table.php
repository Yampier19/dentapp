<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasRespaldoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_noticias_respaldo', function (Blueprint $table) {
            Schema::create('dentapp_noticias_respaldo', function (Blueprint $table) {
                $table->id('id_noticia_respaldo');
                $table->string('accion', 255);
                $table->string('nombre', 255);
                $table->text('informacion');
                $table->string('tipo', 255);
                $table->boolean('estado');
                $table->date('fecha_inicio')->nullable();
                $table->date('fecha_fin')->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable();
                $table->bigInteger('FK_id_noticia')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_noticias_respaldo');
    }
}
