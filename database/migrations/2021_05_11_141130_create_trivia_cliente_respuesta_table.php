<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriviaClienteRespuestaTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_trivia_cliente_respuesta', function (Blueprint $table) {
            Schema::create('dentapp_trivia_cliente_respuesta', function (Blueprint $table) {
                $table->id('id_trivia_cliente_respuesta');
                $table->string('informacion', 255);
                $table->boolean('correcta');
                $table->string('pregunta', 255);
                //$table->bigInteger('FK_id_pregunta')->nullable()->unsigned();
                $table->bigInteger('FK_id_trivia_cliente')->nullable()->unsigned();
                $table->timestamps();

                //$table->foreign('FK_id_pregunta')->references('id_pregunta')->on('dentapp_trivia_pregunta');
                $table->foreign('FK_id_trivia_cliente')->references('id_trivia_cliente')->on('dentapp_trivia_cliente');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_trivia_cliente_respuesta');
    }
}
