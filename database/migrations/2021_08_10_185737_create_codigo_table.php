<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodigoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_codigo', function (Blueprint $table) {
            Schema::create('dentapp_codigo', function (Blueprint $table) {
                $table->id('id_codigo');
                $table->string('referencia', 255);
                $table->string('nombre_proveedor', 255);
                $table->string('correo', 255);
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_codigo');
    }
}
