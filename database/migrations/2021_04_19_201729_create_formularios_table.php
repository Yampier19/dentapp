<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_formularios', function (Blueprint $table) {
            $table->id('id_formulario');
            $table->string('nombre_form', 50);
            $table->bigInteger('estado');
            $table->string('descripcion', 255);
            $table->dateTime('fecha_inicio');
            $table->dateTime('fecha_fin');
            $table->integer('ubicacion_movil')->nullable();
            $table->bigInteger('FK_id_useradmin')->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('dentapp_useradmin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_formularios');
    }
}
