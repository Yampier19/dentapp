<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiosCanjeadoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_premios_canjeado', function (Blueprint $table) {
            Schema::create('dentapp_premios_canjeado', function (Blueprint $table) {
                $table->id('id_premio_canjeado');
                $table->string('cedula', 255)->nullable();
                $table->string('nombre', 50);
                $table->string('marca', 255); //
                $table->bigInteger('cantidad');
                $table->bigInteger('total_estrellas');
                $table->string('receptor', 255);
                $table->string('telefono', 255);
                $table->string('direccion', 255)->nullable();
                $table->string('adicional', 255)->nullable();
                $table->string('estado', 255)->default('pendiente');
                $table->date('fecha_redencion');
                $table->string('referencia')->nullable();
                $table->bigInteger('FK_id_premio')->nullable()->unsigned();
                $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_premio')->references('id_premio')->on('dentapp_premios');
                $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_premios_canjeado');
    }
}
