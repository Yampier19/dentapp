<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaingTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_campaign', function (Blueprint $table) {
            Schema::create('dentapp_campaign', function (Blueprint $table) {
                $table->id('id_campaign');
                $table->string('titulo', 255);
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_campaign');
    }
}
