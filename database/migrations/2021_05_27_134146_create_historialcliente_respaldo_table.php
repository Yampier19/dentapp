<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialclienteRespaldoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_historial_cliente_respaldo', function (Blueprint $table) {
            Schema::create('dentapp_historial_cliente_respaldo', function (Blueprint $table) {
                $table->id('id_historial_cliente_respaldo');
                $table->string('accion', 255);
                $table->date('fecha_registro');
                $table->bigInteger('FK_id_cliente')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_historial_cliente_respaldo');
    }
}
