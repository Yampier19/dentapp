<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNivelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_nivel', function (Blueprint $table) {
            Schema::create('dentapp_nivel', function (Blueprint $table) {
                $table->id('id_nivel');
                $table->string('nombre', 50);
                $table->string('foto', 500)->nullable();
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_nivel');
    }
}
