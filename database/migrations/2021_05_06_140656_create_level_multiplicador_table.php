<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevelMultiplicadorTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_multiplicador', function (Blueprint $table) {
            Schema::create('dentapp_multiplicador', function (Blueprint $table) {
                $table->id('id_multiplicador');
                $table->integer('multiplo');
                $table->bigInteger('FK_id_level')->nullable()->unsigned();
                $table->bigInteger('FK_id_producto_categoria')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_level')->references('id_level')->on('dentapp_level');
                $table->foreign('FK_id_producto_categoria')->references('id_producto_categoria')->on('dentapp_producto_categoria');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_multiplicador');
    }
}
