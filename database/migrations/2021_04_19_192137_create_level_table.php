<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevelTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_level', function (Blueprint $table) {
            Schema::create('dentapp_level', function (Blueprint $table) {
                $table->id('id_level');
                $table->string('nombre', 50);
                $table->string('meta', 100);
                $table->unsignedBigInteger('loginrol_id')->nullable();
                $table->integer('rango_inicio');
                $table->integer('rango_inicio_pacientes')->nullable();
                $table->integer('rango_final_pacientes')->nullable();
                $table->bigInteger('FK_id_nivel')->unsigned()->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('FK_id_nivel')->references('id_nivel')->on('dentapp_nivel');
                $table->foreign('loginrol_id')->references('id_loginrol')->on('dentapp_loginrol');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_level');
    }
}
