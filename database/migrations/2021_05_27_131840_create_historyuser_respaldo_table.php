<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryuserRespaldoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_history_user_respaldo', function (Blueprint $table) {
            Schema::create('dentapp_history_user_respaldo', function (Blueprint $table) {
                $table->id('id_history_user_respaldo');
                $table->string('accion', 255);
                $table->string('email', 255);
                $table->string('password', 255);
                $table->bigInteger('FK_id_cliente')->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_history_user_respaldo');
    }
}
