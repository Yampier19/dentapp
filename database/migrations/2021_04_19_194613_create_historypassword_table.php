<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorypasswordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_history_password', function (Blueprint $table) {
            $table->id('id_history_password');
            $table->string('password');
            $table->date('registro');
            $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_history_password');
    }
}
