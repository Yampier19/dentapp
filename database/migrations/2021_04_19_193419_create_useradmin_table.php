<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUseradminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_useradmin', function (Blueprint $table) {
            $table->id('id_useradmin');
            $table->string('nombre', 50);
            $table->string('apellido', 50);
            $table->string('email', 50)->nullable();
            $table->string('foto', 500)->nullable();
            $table->string('estado', 20);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();

            //PERMISOS
            $table->boolean('p_store')->default(1);
            $table->boolean('p_update')->default(1);
            $table->boolean('p_destroy')->default(1);

            $table->boolean('p_productos')->default(1);
            $table->boolean('p_premios')->default(1);
            $table->boolean('p_facturas')->default(1);
            $table->boolean('p_noticias')->default(1);
            $table->boolean('p_capacitaciones')->default(1);
            $table->boolean('p_trivia')->default(1);
            //FIN PERMISOS
            $table->rememberToken();
            $table->bigInteger('FK_id_loginrol')->unsigned();

            $table->foreign('FK_id_loginrol')->references('id_loginrol')->on('dentapp_loginrol');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_useradmin');
    }
}
