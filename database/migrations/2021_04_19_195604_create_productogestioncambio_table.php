<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductogestioncambioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_producto_gestion_cambio', function (Blueprint $table) {
            $table->id('id_producto_gestion_cambio');
            $table->string('accion', 50);
            $table->string('categoria', 50);
            $table->string('subcategoria', 50);
            $table->bigInteger('stock');
            $table->string('descripcion', 255);
            $table->float('precio');
            $table->bigInteger('estado');
            $table->bigInteger('FK_id_producto_categoria')->nullable()->unsigned();
            $table->bigInteger('FK_id_useradmin')->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_producto_categoria')->references('id_producto_categoria')->on('dentapp_producto_categoria');
            $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('dentapp_useradmin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_producto_gestion_cambio');
    }
}
