<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_noticias', function (Blueprint $table) {
            Schema::create('dentapp_noticias', function (Blueprint $table) {
                $table->id('id_noticia');
                $table->string('nombre', 255);
                $table->text('informacion');
                $table->string('tipo', 255);
                $table->boolean('estado');
                $table->string('imagen', 255)->nullable();
                $table->string('documento', 255)->nullable();
                $table->date('fecha_inicio')->nullable();
                $table->date('fecha_fin')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_noticias');
    }
}
