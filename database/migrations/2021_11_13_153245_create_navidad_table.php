<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavidadTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_navidad', function (Blueprint $table) {
            Schema::create('dentapp_navidad', function (Blueprint $table) {
                $table->id('id_navidad');
                $table->string('tipo', 255)->default('habitual');
                $table->string('nombre', 255);
                $table->string('apellido', 255);
                $table->string('correo', 255);
                $table->timestamp('fecha_participante')->nullable();
                $table->bigInteger('tickets')->nullable();
                $table->bigInteger('tickets_especial')->nullable();
                $table->bigInteger('tickets_usados')->nullable();
                $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_navidad');
    }
}
