<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremioCategoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_premio_categoria', function (Blueprint $table) {
            Schema::create('dentapp_premio_categoria', function (Blueprint $table) {
                $table->id('id_premio_subcategoria');
                $table->string('nombre', 255);
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_premio_categoria');
    }
}
