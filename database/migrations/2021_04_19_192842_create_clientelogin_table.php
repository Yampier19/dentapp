<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClienteloginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_clientelogin', function (Blueprint $table) {
            $table->id('id_clientelogin');
            $table->string('email', 50);
            $table->string('password')->nullable();
            $table->string('estado');
            $table->string('status')->nullable();
            $table->string('ip')->nullable();
            $table->string('segmento');
            $table->string('validacion')->nullable();
            $table->boolean('cambiar')->default(0);
            $table->date('fecha_cambio')->nullable();
            $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
            $table->timestamps();
            $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_clientelogin');
    }
}
