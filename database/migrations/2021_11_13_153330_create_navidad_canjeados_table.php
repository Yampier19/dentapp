<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavidadCanjeadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_navidad_canjeados', function (Blueprint $table) {
            Schema::create('dentapp_navidad_canjeados', function (Blueprint $table) {
                $table->id('id_navidad_canjeado');
                $table->string('forma', 255)->default('normal');
                $table->string('tipo', 255)->default('bono');
                $table->string('nombre', 255);
                $table->string('marca', 255);
                $table->text('descripcion');
                $table->string('referencia', 255);
                $table->boolean('completado')->default(0);
                $table->bigInteger('FK_id_navidad')->nullable()->unsigned();
                $table->bigInteger('FK_id_premio')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_navidad')->references('id_navidad')->on('dentapp_navidad');
                $table->foreign('FK_id_premio')->references('id_premio')->on('dentapp_premios');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_navidad_canjeados');
    }
}
