<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClienteNotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_cliente_notificaciones', function (Blueprint $table) {
            $table->id('id_cliente_notificaciones');
            $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
            $table->bigInteger('FK_id_notificaciones')->nullable()->unsigned();
            $table->timestamp('read_at')->nullable();
            $table->timestamps();

            $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
            $table->foreign('FK_id_notificaciones')->references('id_notificaciones')->on('dentapp_notificaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_cliente_notificaciones');
    }
}
