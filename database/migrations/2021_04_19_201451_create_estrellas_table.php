<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstrellasTable extends Migration
{
    public function up()
    {
        Schema::create('dentapp_estrellas', function (Blueprint $table) {
            $table->id('id_estrellas');
            $table->string('descripcion');
            $table->bigInteger('cantidad');
            $table->date('fecha_registro');
            $table->string('estado', 50);
            $table->bigInteger('FK_id_cliente')->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
        });
    }

    public function down()
    {
        Schema::dropIfExists('estrellas');
    }
}
