<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_capacitacion', function (Blueprint $table) {
            Schema::create('dentapp_capacitacion', function (Blueprint $table) {
                $table->id('id_capacitacion');
                $table->string('nombre', 50);
                $table->string('descripcion', 255);
                $table->string('tipo');
                $table->string('informacion')->nullable();
                $table->string('imagen', 255)->nullable();
                $table->string('archivo', 255)->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_capacitacion');
    }
}
