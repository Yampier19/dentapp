<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHallodentFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_hallodent_facturas', function (Blueprint $table) {
            Schema::create('dentapp_hallodent_facturas', function (Blueprint $table) {
                $table->id('id_hallodent_facturas');
                $table->bigInteger('FK_id_factura')->nullable()->unsigned();
                $table->bigInteger('FK_id_hallodent')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_factura')->references('id_factura')->on('dentapp_factura');
                $table->foreign('FK_id_hallodent')->references('id_hallodent')->on('dentapp_hallodent');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_hallodent_facturas');
    }
}
