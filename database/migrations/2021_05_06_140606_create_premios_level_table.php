<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiosLevelTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_premios_level', function (Blueprint $table) {
            Schema::create('dentapp_premios_level', function (Blueprint $table) {
                $table->id('id_premio_level');
                $table->bigInteger('FK_id_premio')->nullable()->unsigned();
                $table->bigInteger('FK_id_level')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_premio')->references('id_premio')->on('dentapp_premios');
                $table->foreign('FK_id_level')->references('id_level')->on('dentapp_level');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_premios_level');
    }
}
