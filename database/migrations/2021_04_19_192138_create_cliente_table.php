<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClienteTable extends Migration
{
    public function up()
    {
        Schema::create('dentapp_cliente', function (Blueprint $table) {
            $table->id('id_cliente');
            $table->string('nombre', 50);
            $table->string('apellido', 50);
            $table->string('direccion', 50)->nullable();
            $table->string('distribuidor')->nullable();
            $table->string('newdistribuidor')->nullable();
            $table->date('last_change')->nullable();
            $table->string('telefono');
            $table->string('foto', 255)->nullable();
            $table->string('correo', 50);
            $table->bigInteger('total_estrellas');
            $table->decimal('total_reunido', 16, 3)->default(0);
            $table->string('especialidad', 50);
            $table->string('tipo')->default(6);
            $table->bigInteger('FK_id_level')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('FK_id_level')->references('id_level')->on('dentapp_level');
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_cliente');
    }
}
