<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialclienteTable extends Migration
{
    public function up()
    {
        Schema::create('dentapp_historial_cliente', function (Blueprint $table) {
            $table->id('id_historial_cliente');
            $table->string('accion', 255);
            $table->date('fecha_registro');
            $table->bigInteger('FK_id_cliente')->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_historial_cliente');
    }
}
