<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductogestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_producto_gestion', function (Blueprint $table) {
            $table->id('id_producto_gestion');
            $table->bigInteger('cantidad');
            $table->float('precio_total');
            $table->bigInteger('FK_id_producto_categoria')->unsigned();
            $table->bigInteger('FK_id_cliente')->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_producto_categoria')->references('id_producto_categoria')->on('dentapp_producto_categoria');
            $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_producto_gestion');
    }
}
