<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiasTipoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_noticias_tipo', function (Blueprint $table) {
            Schema::create('dentapp_noticias_tipo', function (Blueprint $table) {
                $table->id('id_noticia_tipo');
                $table->string('nombre', 255);
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_noticias_tipo');
    }
}
