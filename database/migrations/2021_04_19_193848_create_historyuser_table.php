<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryuserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentapp_history_user', function (Blueprint $table) {
            $table->id('id_history_user');
            $table->string('accion', 255);
            $table->string('email', 255);
            $table->string('password', 255);
            $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
            $table->bigInteger('FK_id_useradmin')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
            $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('dentapp_useradmin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_history_user');
    }
}
