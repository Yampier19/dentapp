<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriviaClienteTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_trivia_cliente', function (Blueprint $table) {
            Schema::create('dentapp_trivia_cliente', function (Blueprint $table) {
                $table->id('id_trivia_cliente');
                $table->string('nombre', 50);
                $table->string('descripcion', 50);
                $table->integer('numero_estrellas');
                $table->bigInteger('FK_id_trivia')->nullable()->unsigned();
                $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_trivia')->references('id_trivia')->on('dentapp_trivia');
                $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_trivia_cliente');
    }
}
