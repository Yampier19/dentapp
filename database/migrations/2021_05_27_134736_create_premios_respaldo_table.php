<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiosRespaldoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_premios_respaldo', function (Blueprint $table) {
            Schema::create('dentapp_premios_respaldo', function (Blueprint $table) {
                $table->id('id_premio_respaldo');
                $table->string('accion', 50);
                $table->string('nombre', 50);
                $table->string('marca', 255); //
                $table->text('descripcion'); //
                $table->string('entrega', 255);
                $table->string('tag', 50);
                $table->bigInteger('stock');
                $table->bigInteger('limite_usuario')->nullable();
                $table->bigInteger('numero_estrellas')->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable();
                $table->bigInteger('FK_id_premios')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_premios_respaldo');
    }
}
