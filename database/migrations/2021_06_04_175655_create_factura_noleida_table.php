<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaNoleidaTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_factura_noleida', function (Blueprint $table) {
            Schema::create('dentapp_factura_noleida', function (Blueprint $table) {
                $table->id('id_factura_noleida');
                $table->string('foto', 255)->nullable();
                $table->date('fecha_registro');
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_factura_noleida');
    }
}
