<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaProductoEspecialTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_factura_producto_especial', function (Blueprint $table) {
            Schema::create('dentapp_factura_producto_especial', function (Blueprint $table) {
                $table->id('id_factura_producto');
                $table->string('producto', 255);
                $table->bigInteger('cantidad');
                $table->bigInteger('puntos');
                $table->bigInteger('FK_id_factura')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_factura')->references('id_factura')->on('dentapp_factura');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_factura_producto_especial');
    }
}
