<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavidadFacturaProductoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_navidad_factura_producto', function (Blueprint $table) {
            Schema::create('dentapp_navidad_factura_producto', function (Blueprint $table) {
                $table->id('id_navidad_factura_producto');
                $table->string('producto');
                $table->bigInteger('cantidad');
                $table->bigInteger('FK_id_navidad_factura')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_navidad_factura')->references('id_navidad_factura')->on('dentapp_navidad_factura');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_navidad_factura_producto');
    }
}
