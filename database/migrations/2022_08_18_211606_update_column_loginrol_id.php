<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_cliente', function (Blueprint $table) {
            $table->unsignedBigInteger('loginrol_id')->change();
            $table->foreign('loginrol_id')->references('id_loginrol')->on('dentapp_loginrol')->default(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dentapp_cliente', function (Blueprint $table) {
            //
        });
    }
};
