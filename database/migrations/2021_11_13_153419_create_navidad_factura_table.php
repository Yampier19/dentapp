<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavidadFacturaTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_navidad_factura', function (Blueprint $table) {
            Schema::create('dentapp_navidad_factura', function (Blueprint $table) {
                $table->id('id_navidad_factura');
                $table->string('tipo')->default('normal');
                $table->bigInteger('FK_id_factura')->nullable()->unsigned();
                $table->bigInteger('FK_id_navidad')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_factura')->references('id_factura')->on('dentapp_factura');
                $table->foreign('FK_id_navidad')->references('id_navidad')->on('dentapp_navidad');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_navidad_factura');
    }
}
