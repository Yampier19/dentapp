<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiosgestioncambioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_premios_gestion_cambio', function (Blueprint $table) {
            Schema::create('dentapp_premios_gestion_cambio', function (Blueprint $table) {
                $table->id('id_premio_cambio');
                $table->string('accion', 50);
                $table->string('nombre', 50);
                $table->string('marca', 255); //
                $table->text('descripcion'); //
                $table->string('entrega', 255);
                $table->string('tag', 50);
                $table->bigInteger('stock');
                $table->bigInteger('limite_usuario')->nullable();
                $table->bigInteger('numero_estrellas')->nullable();
                $table->bigInteger('FK_id_useradmin')->unsigned();
                $table->bigInteger('FK_id_premios')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_useradmin')->references('id_useradmin')->on('dentapp_useradmin');
                $table->foreign('FK_id_premios')->references('id_premio')->on('dentapp_premios');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_premios_gestion_cambio');
    }
}
