<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoRespaldoTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_producto_respaldo', function (Blueprint $table) {
            Schema::create('dentapp_producto_respaldo', function (Blueprint $table) {
                $table->id('id_producto_respaldo');
                $table->string('accion', 50);
                $table->string('categoria', 50);
                $table->string('subcategoria', 50);
                $table->bigInteger('stock');
                $table->string('descripcion', 255);
                $table->float('precio');
                $table->bigInteger('estado');
                $table->bigInteger('FK_id_producto_categoria')->nullable();
                $table->bigInteger('FK_id_useradmin')->nullable();
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_producto_respaldo');
    }
}
