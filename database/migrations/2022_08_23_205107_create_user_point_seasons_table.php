<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_point_seasons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('temporada_id');
            $table->unsignedBigInteger('cliente_id');
            $table->integer('points')->default(0);
            $table->date('expired_at');
            $table->enum('status', ['Activo', 'Inactivo'])->default('Activo');

            $table->foreign('temporada_id')->references('id')->on('temporadas');
            $table->foreign('cliente_id')->references('id_cliente')->on('dentapp_cliente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_point_seasons');
    }
};
