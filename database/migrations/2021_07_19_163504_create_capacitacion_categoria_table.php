<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionCategoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_capacitacion_categoria', function (Blueprint $table) {
            Schema::create('dentapp_capacitacion_categoria', function (Blueprint $table) {
                $table->id('id_capacitacion_categoria');
                $table->string('nombre', 255);
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentapp_capacitacion_categoria');
    }
}
