<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHallodentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentapp_hallodent', function (Blueprint $table) {
            Schema::create('dentapp_hallodent', function (Blueprint $table) {
                $table->id('id_hallodent');
                $table->string('nombre', 255);
                $table->string('apellido', 255);
                $table->string('correo', 255);
                $table->timestamp('fecha_participante')->nullable();
                $table->bigInteger('ranking')->nullable();
                $table->decimal('facturado', 13, 3);
                $table->bigInteger('FK_id_cliente')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_cliente')->references('id_cliente')->on('dentapp_cliente');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_hallodent');
    }
}
