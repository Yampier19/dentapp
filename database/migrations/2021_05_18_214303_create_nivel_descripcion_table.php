<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNivelDescripcionTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_nivel_descripcion', function (Blueprint $table) {
            Schema::create('dentapp_nivel_descripcion', function (Blueprint $table) {
                $table->id('id_nivel_descripcion');
                $table->string('descripcion');
                $table->bigInteger('FK_id_nivel')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_nivel')->references('id_nivel')->on('dentapp_nivel');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_nivel_descripcion');
    }
}
