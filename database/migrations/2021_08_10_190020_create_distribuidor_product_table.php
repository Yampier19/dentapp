<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistribuidorProductTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_distribuidores_product_3M', function (Blueprint $table) {
            Schema::create('dentapp_distribuidores_product_3M', function (Blueprint $table) {
                $table->id('id_pro');
                $table->bigInteger('id_distri3M');
                $table->bigInteger('referencia');
                $table->string('stock_3m', 255);
                $table->string('familia', 255);
                $table->string('distribuidor', 255);
                $table->bigInteger('puntos_extra');
                $table->timestamps();
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_distribuidores_product_3M');
    }
}
