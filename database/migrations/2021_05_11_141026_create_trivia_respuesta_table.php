<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriviaRespuestaTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_trivia_respuesta', function (Blueprint $table) {
            Schema::create('dentapp_trivia_respuesta', function (Blueprint $table) {
                $table->id('id_respuesta');
                $table->string('informacion', 255);
                $table->boolean('correcta'); //Grabo 1 o 0 si es correcta true o 0 false
                $table->bigInteger('FK_id_pregunta')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_pregunta')->references('id_pregunta')->on('dentapp_trivia_pregunta');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_trivia_respuesta');
    }
}
