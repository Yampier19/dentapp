<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistribuidorTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_distribuidores_3M', function (Blueprint $table) {
            Schema::create('dentapp_distribuidores_3M', function (Blueprint $table) {
                $table->id('id_distri');
                $table->string('distribuidor', 255);
                $table->string('correo', 500);
                $table->string('num_contacto', 255);
                $table->string('identificacion', 255);
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_distribuidores_3M');
    }
}
