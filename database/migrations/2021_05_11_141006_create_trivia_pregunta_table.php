<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriviaPreguntaTable extends Migration
{
    public function up()
    {
        Schema::table('dentapp_trivia_pregunta', function (Blueprint $table) {
            Schema::create('dentapp_trivia_pregunta', function (Blueprint $table) {
                $table->id('id_pregunta');
                $table->string('informacion', 255);
                $table->string('tipo', 50)->default('radio'); //radio, text, date, number
                $table->integer('puntuacion')->default(0); //0 puntos en default si es trivia normal
                $table->boolean('estado')->default(false);
                $table->bigInteger('FK_id_trivia')->nullable()->unsigned();
                $table->timestamps();

                $table->foreign('FK_id_trivia')->references('id_trivia')->on('dentapp_trivia');
            });
        });
    }

    public function down()
    {
        Schema::dropIfExists('dentapp_trivia_pregunta');
    }
}
