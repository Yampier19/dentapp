<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Cliente;
use App\Models\ClienteLogin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //creacion del usuario administrador
        Admin::insert([
            [
                'nombre' => 'Admin',
                'apellido' => '3M',
                'email' => 'admin@admin.com',
                'estado' => 'activo',
                'password' => Hash::make('admin'), //la contraseña por defecto es admin
                'FK_id_loginrol' => 1,
                'p_store' => 1,
                'p_update' => 1,
                'p_destroy' => 1,
                'p_productos' => 1,
                'p_premios' => 1,
                'p_facturas' => 1,
                'p_noticias' => 1,
                'p_capacitaciones' => 1,
                'p_trivia' => 1,
            ],
        ]);

        Cliente::insert([
            [
                'nombre' => 'doctor',
                'apellido' => '3M',
                'direccion' => 'Colombia',
                'city_id' => 5001,
                'telefono' => '4127983465',
                'correo' => 'cliente@cliente.com',
                'total_estrellas' => 0,
                'especialidad' => 'Especialista - Ortodoncia',
                'loginrol_id' => 6,
            ],
            [
                'nombre' => 'clinica',
                'apellido' => '3M',
                'direccion' => 'Colombia',
                'city_id' => 5001,
                'telefono' => '123456',
                'correo' => 'clinica@cliente.com',
                'total_estrellas' => 0,
                'especialidad' => 'Especialista - Ortodoncia',
                'loginrol_id' => 5,
            ],
            [
                'nombre' => 'asistente',
                'apellido' => '3M',
                'direccion' => 'Colombia',
                'city_id' => 5001,
                'telefono' => '123456',
                'correo' => 'clinica@cliente.com',
                'total_estrellas' => 0,
                'especialidad' => 'Especialista - Ortodoncia',
                'loginrol_id' => 4,
            ],

        ]);

        //creacion del usuario cliente
        ClienteLogin::insert([
            [
                'email' => 'cliente@cliente.com',
                'estado' => 'activo',
                'segmento' => 'Invest',
                'password' => Hash::make('cliente'), //la contraseña por defecto es cliente
                'FK_id_cliente' => 1,
                'cambiar' => 1,
            ],
            [
                'email' => 'clinica@cliente.com',
                'estado' => 'activo',
                'segmento' => 'Invest',
                'password' => Hash::make('cliente'), //la contraseña por defecto es cliente
                'FK_id_cliente' => 2,
                'cambiar' => 1,
            ],
            [
                'email' => 'clinica@cliente.com',
                'estado' => 'activo',
                'segmento' => 'Invest',
                'password' => Hash::make('cliente'), //la contraseña por defecto es cliente
                'FK_id_cliente' => 3,
                'cambiar' => 1,
            ],
        ]);
    }
}
