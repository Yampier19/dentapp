<?php

namespace Database\Seeders;

use App\Models\NoticiaTipo;
use Illuminate\Database\Seeder;

class TipoCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NoticiaTipo::insert([
            [
                'nombre' => 'Dental',
            ],
            [
                'nombre' => 'Ortodoncia',
            ],
            [
                'nombre' => 'Salud',
            ],
            [
                'nombre' => 'Nacionales',
            ],
            [
                'nombre' => 'Internacionales',
            ],
            [
                'nombre' => 'Deportes',
            ],
            [
                'nombre' => 'Tecnológicas',
            ],
        ]);
    }
}
