<?php

namespace Database\Seeders;

use App\Models\RestorePFT;
use Illuminate\Database\Seeder;

class RestorePftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RestorePFT::insert([
            [
                'nombre' => 'puntos',
                'fecha_cambio' => date('Y-m-d'),
            ],
            [
                'nombre' => 'trazabilidad',
                'fecha_cambio' => date('Y-m-d'),
            ],
        ]);
    }
}
