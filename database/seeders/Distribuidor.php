<?php

namespace Database\Seeders;

use App\Models\Distribuidor as ModelsDistribuidor;
use Illuminate\Database\Seeder;

class Distribuidor extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ModelsDistribuidor::insert(
            [
                [
                    'id_distri' => 1, 'distribuidor' => 'DENTAL 83 S.A.S.',
                    'nombre_publico' => 'DENTAL 83', 'num_contacto' => '7432082',
                    'correo' => 'CONTACTO@DENTAL83.COM', 'identificacion' => '830,015,820 - 1',
                ],

                [
                    'id_distri' => 2, 'distribuidor' => 'ADENTAL S.A.S',
                    'nombre_publico' => 'ADENTAL', 'num_contacto' => '3107289781',
                    'correo' => 'lab@adental.com.co', 'identificacion' => '802008300-0',
                ],
                [
                    'id_distri' => 3, 'distribuidor' => 'ALDENTAL S.A.',
                    'nombre_publico' => 'ALDENTAL', 'num_contacto' => '0',
                    'correo' => 'NA', 'identificacion' => '0',
                ],
                [
                    'id_distri' => 4, 'distribuidor' => 'AUDIFARMA S.A.',
                    'nombre_publico' => 'AUDIFARMA S.A.', 'num_contacto' => '0',
                    'correo' => 'NA', 'identificacion' => '0',
                ],
                [
                    'id_distri' => 5, 'distribuidor' => 'CASA DENTAL GABRIEL VELASQUEZ & CIA S.A.S',
                    'nombre_publico' => 'CASA DENTAL GABRIEL VELASQUEZ', 'num_contacto' => '3175177121',
                    'correo' => 'casadentalgv@casadentalsas.com', 'identificacion' => '890300417-4',
                ],
                [
                    'id_distri' => 6, 'distribuidor' => 'CASA ODONTOLOGICA IMPORTADORA',
                    'nombre_publico' => 'CASA ODONTOLOGICA', 'num_contacto' => '8308700-8372985',
                    'correo' => 'Comercializadora', 'identificacion' => '900552695-1',
                ],
                [
                    'id_distri' => 7, 'distribuidor' => 'DENTAL NADER SAS',
                    'nombre_publico' => 'DENTAL NADER', 'num_contacto' => '6199095',
                    'correo' => 'ventasOdentalnader.com', 'identificacion' => '900.486.577-8',
                ],
                [
                    'id_distri' => 8, 'distribuidor' => 'DENTALES ANTIOQUIA S.A.S.',
                    'nombre_publico' => 'DENTALES ANTIOQUIA', 'num_contacto' => '3229998',
                    'correo' => 'dentalesantioquiaQune.net.co', 'identificacion' => '890.913.400-0',
                ],
                [
                    'id_distri' => 9, 'distribuidor' => 'DENTALES MARKET S.A.S',
                    'nombre_publico' => 'DENTALES MARKET', 'num_contacto' => '43221219',
                    'correo' => 'gerencia(dentales market.com', 'identificacion' => '901046793-9',
                ],
                [
                    'id_distri' => 10, 'distribuidor' => 'DENTALES PADILLA LTDA',
                    'nombre_publico' => 'DENTALES PADILLA', 'num_contacto' => '3124499755',
                    'correo' => 'atencioncliente@dentalespadilla.com', 'identificacion' => '860.069.435-8',
                ],
                [
                    'id_distri' => 11, 'distribuidor' => 'ERASO RIVAS JOSE ANTONIO',
                    'nombre_publico' => 'JANER DISTRIBUCIONES', 'num_contacto' => '7299031',
                    'correo' => 'distrijaner@hotmail.com', 'identificacion' => '5.199.626-3',
                ],
                [
                    'id_distri' => 12, 'distribuidor' => 'HERNANDEZ HERRERA JOSE FRANCISCO',
                    'nombre_publico' => 'DENTAL PALERMO', 'num_contacto' => '3406778',
                    'correo' => 'dentalespalermo@outlook.com', 'identificacion' => '2914274-1',
                ],
                [
                    'id_distri' => 13, 'distribuidor' => 'ORBIDENTAL S A S',
                    'nombre_publico' => 'ORBIDENTAL', 'num_contacto' => '2353110',
                    'correo' => 'info@orbidental.com', 'identificacion' => '800.005.972-9',
                ],
                [
                    'id_distri' => 14, 'distribuidor' => 'POSADA HENAO LUIS FERNANDO',
                    'nombre_publico' => 'LUIS FERNANDO POSADA HENAO', 'num_contacto' => '0',
                    'correo' => 'dentalesyacrilicos@gmail.com', 'identificacion' => '10.251.565-2',
                ],
                [
                    'id_distri' => 15, 'distribuidor' => 'SUMINISTROS Y DOTACIONES COLOM',
                    'nombre_publico' => 'SUMINISTROS Y DOTACIONES COLOM', 'num_contacto' => '0',
                    'correo' => 'NA', 'identificacion' => '0',
                ],
                [
                    'id_distri' => 16, 'distribuidor' => 'Dentales Padilla LTDA',
                    'nombre_publico' => 'Dentales Padilla', 'num_contacto' => '3124499755',
                    'correo' => 'atencioncliente@dentalespadilla.com', 'identificacion' => '03-3711-12',
                ],
                [
                    'id_distri' => 17, 'distribuidor' => 'BRAKET S.A.S',
                    'nombre_publico' => 'Bracket', 'num_contacto' => '0',
                    'correo' => 'NA', 'identificacion' => '0',
                ],
                [
                    'id_distri' => 18, 'distribuidor' => 'Vertice Aliados',
                    'nombre_publico' => 'Vertice Aliados', 'num_contacto' => '3115154116',
                    'correo' => 'vertice.contabilidad@gmail.com', 'identificacion' => '900708757-1',
                ],
                [
                    'id_distri' => 19, 'distribuidor' => 'Dentales y Acrilicos',
                    'nombre_publico' => 'Dentales y Acrilicos', 'num_contacto' => '0',
                    'correo' => 'NA', 'identificacion' => '0',
                ],

            ]
        );
    }
}
