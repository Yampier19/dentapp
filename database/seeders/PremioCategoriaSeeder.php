<?php

namespace Database\Seeders;

use App\Models\PremioCategoria;
use Illuminate\Database\Seeder;

class PremioCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PremioCategoria::insert([
            [
                'nombre' => 'Consumibles',
            ],
            [
                'nombre' => 'Educación',
            ],
            [
                'nombre' => 'Especiales',
            ],
            [
                'nombre' => 'Ofertas',
            ],
            [
                'nombre' => 'Viajes',
            ],
            [
                'nombre' => 'Tecnología',
            ],
        ]);
    }
}
