<?php

namespace Database\Seeders;

use App\Models\document_types;
use Illuminate\Database\Seeder;

class documentTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        document_types::insert(
            [
                ['id' => 1, 'name' => 'DNI', 'updated_at' => now(), 'created_at' => now()],
                ['id' => 2, 'name' => 'Cédula', 'updated_at' => now(), 'created_at' => now()],
                ['id' => 3, 'name' => 'Pasaporte', 'updated_at' => now(), 'created_at' => now()],
                ['id' => 4, 'name' => 'Nit', 'updated_at' => now(), 'created_at' => now()],
                ['id' => 5, 'name' => 'Cédula de extranjeria', 'updated_at' => now(), 'created_at' => now()],
            ]
        );
    }
}
