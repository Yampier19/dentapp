<?php

namespace Database\Seeders;

use App\Models\CapacitacionCategoria;
use Illuminate\Database\Seeder;

class CapacitacionCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CapacitacionCategoria::insert([
            [
                'nombre' => 'Salud',
            ],
            [
                'nombre' => 'Investigación',
            ],
            [
                'nombre' => 'Crecimiento Personal',
            ],
            [
                'nombre' => 'Crecimiento Profesional',
            ],
            [
                'nombre' => 'Economía',
            ],
        ]);
    }
}
