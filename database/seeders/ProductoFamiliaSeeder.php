<?php

namespace Database\Seeders;

use App\Models\ProductoFamilia;
use Illuminate\Database\Seeder;

class ProductoFamiliaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductoFamilia::insert([
            [
                'descripcion_familia' => 'Carillas Rojas',
                'tipo' => 1,
            ],
        ]);
    }
}
