<?php

namespace Database\Seeders;

use App\Models\Level;
use App\Models\Nivel;
use Illuminate\Database\Seeder;

class NivelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Nivel::insert([
            ['nombre' => 'Bronce'],
            ['nombre' => 'Plata'],
            ['nombre' => 'Oro'],
            ['nombre' => 'Zafiro'],
            ['nombre' => 'Diamante'],
        ]);

        Level::insert([
            [
                'nombre' => 'Rango Bronce', 'loginrol_id' => 6, 'meta' => '1499999', 'rango_inicio' => 0, 'FK_id_nivel' => 1,
            ],

            [
                'nombre' => 'Rango Plata', 'loginrol_id' => 6, 'meta' => '1949999', 'rango_inicio' => 1500000, 'FK_id_nivel' => 2,
            ],

            [
                'nombre' => 'Rango Oro', 'loginrol_id' => 6, 'meta' => '2849999', 'rango_inicio' => 1950000, 'FK_id_nivel' => 3,
            ],

            [
                'nombre' => 'Rango Zafiro', 'loginrol_id' => 6, 'meta' => '6599999', 'rango_inicio' => 2850000, 'FK_id_nivel' => 4,
            ],

            [
                'nombre' => 'Rango Diamante', 'loginrol_id' => 6, 'meta' => '6600000', 'rango_inicio' => 6600000, 'FK_id_nivel' => 5,
            ],
        ]);
    }
}
