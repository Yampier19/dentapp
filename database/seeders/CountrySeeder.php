<?php

namespace Database\Seeders;

use App\Models\country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        country::insert([
            'name' => 'Colombia', 'updated_at' => now(), 'created_at' => now(),
        ]);
    }
}
