<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            LoginRolSeeder::class,
            documentTypes::class,
            Distribuidor::class,
            CountrySeeder::class,
            CitySeeder::class,
            NivelSeeder::class,
            UsersSeeder::class,
            ProductoFamiliaSeeder::class,
            CampaignSeeder::class,
            PremioCategoriaSeeder::class,
            CapacitacionCategoriaSeeder::class,
            TipoCategoriaSeeder::class,
            RestorePftSeeder::class,
        ]);
    }
}
