<?php

namespace Database\Seeders;

use App\Models\Rol;
use Illuminate\Database\Seeder;

class LoginRolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::insert([
            ['id_loginrol' => 1, 'rol' => 'Super Admin', 'updated_at' => now(), 'created_at' => now()],
            ['id_loginrol' => 2, 'rol' => 'Call Center', 'updated_at' => now(), 'created_at' => now()],
            ['id_loginrol' => 3, 'rol' => 'Básico', 'updated_at' => now(), 'created_at' => now()],
            ['id_loginrol' => 4, 'rol' => 'Asistente', 'updated_at' => now(), 'created_at' => now()],
            ['id_loginrol' => 5, 'rol' => 'Clinica', 'updated_at' => now(), 'created_at' => now()],
            ['id_loginrol' => 6, 'rol' => 'Doctor', 'updated_at' => now(), 'created_at' => now()],
            ['id_loginrol' => 7, 'rol' => 'Demo', 'updated_at' => now(), 'created_at' => now()],
        ]);
    }
}
