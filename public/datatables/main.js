$(document).ready(function() {
    $('#example').DataTable({
        //  order: [[1, 'desc']],
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
                 },
                 "sProcessing":"Procesando...",
            },
        //para usar los botones
        dom: 'Bfrtilp',
        buttons:[
            {
                extend:    'excelHtml5',
                text:      '<span class="iconify" data-icon="vscode-icons:file-type-excel" data-width="25"></span> ',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success btn-sm mr-2 rounded'
            },
            {
                extend:    'pdfHtml5',
                text:      '<span class="iconify" data-icon="vscode-icons:file-type-pdf2" data-width="25"></span>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger btn-sm mr-2 rounded'
            },
            {
                extend:    'print',
                text:      '<span class="iconify" data-icon="flat-color-icons:print" data-width="25"></span>',
                titleAttr: 'Imprimir',
                className: 'btn btn-info btn-sm mr-2 rounded'
            },
        ]
    });

    $('#doctor').DataTable({
         order: [[0, 'desc']],
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
                 },
                 "sProcessing":"Procesando...",
            },
        //para usar los botones
        dom: 'Bfrtilp',
        buttons:[
            {
                extend:    'excelHtml5',
                text:      '<span class="iconify" data-icon="vscode-icons:file-type-excel" data-width="25"></span> ',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success btn-sm mr-2 rounded'
            },
            {
                extend:    'pdfHtml5',
                text:      '<span class="iconify" data-icon="vscode-icons:file-type-pdf2" data-width="25"></span>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger btn-sm mr-2 rounded'
            },
            {
                extend:    'print',
                text:      '<span class="iconify" data-icon="flat-color-icons:print" data-width="25"></span>',
                titleAttr: 'Imprimir',
                className: 'btn btn-info btn-sm mr-2 rounded'
            },
        ]
    });
    $('#premiosCanjeados').DataTable({
        order: [[6, 'desc']],
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
                 },
                 "sProcessing":"Procesando...",
            },
        //para usar los botones
        dom: 'Bfrtilp',
        buttons:[
            {
                extend:    'excelHtml5',
                text:      '<span class="iconify" data-icon="vscode-icons:file-type-excel" data-width="25"></span> ',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success btn-sm mr-2 rounded'
            },
            {
                extend:    'pdfHtml5',
                text:      '<span class="iconify" data-icon="vscode-icons:file-type-pdf2" data-width="25"></span>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger btn-sm mr-2 rounded'
            },
            {
                extend:    'print',
                text:      '<span class="iconify" data-icon="flat-color-icons:print" data-width="25"></span>',
                titleAttr: 'Imprimir',
                className: 'btn btn-info btn-sm mr-2 rounded'
            },
        ]
    });
    $('#facturas').DataTable({
        order: [[1, 'desc']],
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
                 },
                 "sProcessing":"Procesando...",
            },
        //para usar los botones
        dom: 'Bfrtilp',
        buttons:[
            {
                extend:    'excelHtml5',
                text:      '<span class="iconify" data-icon="vscode-icons:file-type-excel" data-width="25"></span> ',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success btn-sm mr-2 rounded'
            },
            {
                extend:    'pdfHtml5',
                text:      '<span class="iconify" data-icon="vscode-icons:file-type-pdf2" data-width="25"></span>',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger btn-sm mr-2 rounded'
            },
            {
                extend:    'print',
                text:      '<span class="iconify" data-icon="flat-color-icons:print" data-width="25"></span>',
                titleAttr: 'Imprimir',
                className: 'btn btn-info btn-sm mr-2 rounded'
            },
        ]
    });
});
