/*API para obtener los datos*/
function getAllData() {
    axios.get("/admin/list-user")
        .then(function (res) {
            table_data_row(res.data.data)
        })
};
getAllData();

$('#crear_usuario').click(function () {
    openmodal();
})


/*Creación de la estructura de la tabla*/
function table_data_row(data) {

    var tb = document.createElement("tbody");
    tb.id = "tbody";

    $.each(data, function (key, value) {

        var t = document.querySelector('#administradores');
        var clone = document.importNode(t.content, true);
        clone.querySelector("td:first-child h6").textContent = `${value.nombre} ${value.apellido}`;
        clone.querySelector("td:first-child p").textContent = value.email;

        clone.querySelector("td:nth-child(2) span").textContent = value.estado;
        clone.querySelector("td:nth-child(3) span").textContent = value.roles.rol;

        clone.querySelector("td:nth-child(4) button.editRow").dataset.id = value.id_useradmin;

        // clone.querySelector("td:nth-child(4) button.deleteRow").dataset.id = value.id_useradmin;

        const deleteButton = clone.querySelector("td:nth-child(4) button.deleteRow");
        deleteButton.dataset.id = value.id_useradmin;
        deleteButton.addEventListener('click', deleteuser);

        tb.appendChild(clone);
    });
    document.getElementById("tbody").replaceWith(tb);
}


/*API para la creacion de un nuevo usuario*/
$('body').on('click', '#addNewDataForm', function (e) {
    IsValid();
    e.preventDefault();
    axios.post("/admin/store", {
        nombre: $('#nombre').val(),
        apellido: $('#apellido').val(),
        email: $('#email').val(),
        password: $('#password').val(),
        estado: $('#estado').val(),
        FK_id_loginrol: $('#user_rol').val(),
        p_store: document.getElementById("p_store").checked ? 1 : 0,
        p_update: document.getElementById("p_update").checked ? 1 : 0,
        p_destroy: document.getElementById("p_destroy").checked ? 1 : 0,
        p_productos: document.getElementById("p_productos").checked ? 1 : 0,
        p_premios: document.getElementById("p_premios").checked ? 1 : 0,
        p_facturas: document.getElementById("p_facturas").checked ? 1 : 0,
        p_noticias: document.getElementById("p_noticias").checked ? 1 : 0,
        p_capacitaciones: document.getElementById("p_capacitaciones").checked ? 1 : 0,
        p_trivia: document.getElementById("p_trivia").checked ? 1 : 0,
    })
        .then(function (response) {
            Swal.fire({
                icon: 'success',
                title: '¡Buen trabajo!',
                text: '¡Usuario registrado con exito!',
            });
            location.reload();
            inputDefault();
            $('.modal').modal('hide')
        })
        .catch(function (error) {
            if (error.response.data.errors) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'No se pudo crear el usuario!',
                })
                $('#invalid-nombre').text(error.response.data.errors.nombre);
                $('#invalid-apellido').text(error.response.data.errors.apellido);
                $('#invalid-email').text(error.response.data.errors.email);
                $('#invalid-password').text(error.response.data.errors.password);
                $('#invalid-estado').text(error.response.data.errors.estado);
                $('#invalid-rol').text(error.response.data.errors.FK_id_loginrol);

            }
        });
});


/*Llenamos los inputs con la informacion que llege desde el api*/
$('body').on('click', '.editRow', function () {
    IsValid();
    openmodal(true);
    let id = $(this).data('id');
    axios.get('/admin/' + id + '/edit')
        .then(function (res) {
            formatSelect();
            $('#nombre').val(res.data.data.nombre),
                $('#email').val(res.data.data.email),
                $('#apellido').val(res.data.data.apellido),
                $('#estado').val(res.data.data.estado),
                $('#user_rol').val(res.data.data.FK_id_loginrol),
                $('#id').val(res.data.data.id_useradmin),
                $("#p_store").prop("checked", res.data.data.p_store),
                $("#p_update").prop("checked", res.data.data.p_update),
                $("#p_destroy").prop("checked", res.data.data.p_destroy),
                $("#p_productos").prop("checked", res.data.data.p_productos),
                $("#p_premios").prop("checked", res.data.data.p_premios),
                $("#p_facturas").prop("checked", res.data.data.p_facturas),
                $("#p_noticias").prop("checked", res.data.data.p_noticias),
                $("#p_capacitaciones").prop("checked", res.data.data.p_capacitaciones),
                $("#p_trivia").prop("checked", res.data.data.p_trivia);
        })
})


/*Edicion de los usuarios*/
$('body').on('click', '#editDataForm', function (e) {
    e.preventDefault();
    let id = $('#id').val()
    let data = {
        id: id,
        nombre: $('#nombre').val(),
        apellido: $('#apellido').val(),
        email: $('#email').val(),
        estado: $('#estado').val(),
        FK_id_loginrol: $('#user_rol').val(),
        p_store: document.getElementById("p_store").checked ? 1 : 0,
        p_update: document.getElementById("p_update").checked ? 1 : 0,
        p_destroy: document.getElementById("p_destroy").checked ? 1 : 0,
        p_productos: document.getElementById("p_productos").checked ? 1 : 0,
        p_premios: document.getElementById("p_premios").checked ? 1 : 0,
        p_facturas: document.getElementById("p_facturas").checked ? 1 : 0,
        p_noticias: document.getElementById("p_noticias").checked ? 1 : 0,
        p_capacitaciones: document.getElementById("p_capacitaciones").checked ? 1 : 0,
        p_trivia: document.getElementById("p_trivia").checked ? 1 : 0,
    }
    axios.put('/admin/update/' + id, data)
        .then(function (response) {
            Swal.fire({
                icon: 'success',
                title: '¡Buen trabajo!',
                text: '¡Usuario modificado con exito!',
            }),
            location.reload();
            inputDefault();
            $('.modal').modal('hide')
        })
        .catch(function (error) {
            if (error.response.data.errors) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'No se pudo crear el usuario!',
                })
                $('#invalid-nombre').text(error.response.data.errors.nombre)
                $('#invalid-apellido').text(error.response.data.errors.apellido)
                $('#invalid-email').text(error.response.data.errors.email)
                $('#invalid-password').text(error.response.data.errors.password)
                $('#invalid-estado').text(error.response.data.errors.estado)
                $('#invalid-rol').text(error.response.data.errors.FK_id_loginrol)
            }
        });
});


/*Eliminacion de los usuario*/
// $('body').on('click', '.deleteRow', function (e) {
//     e.preventDefault();
//     let id = $(this).data('id')
//     const swalWithBootstrapButtons = Swal.mixin({
//         customClass: {
//             confirmButton: 'btn btn-info mx-2',
//             cancelButton: 'btn btn-danger'
//         },
//         buttonsStyling: false
//     })
//     swalWithBootstrapButtons.fire({
//         title: '¿Estas seguro?',
//         text: "¡No podras revertir estó!",
//         icon: 'warning',
//         showCancelButton: true,
//         confirmButtonText: '¡Si,eliminalo!',
//         cancelButtonText: 'No, cancelar!',
//         reverseButtons: true
//     }).then((result) => {
//         if (result.isConfirmed) {
//             axios.delete(`/admin/${id}`).then(function (r) {
//                 getAllData();
//                 swalWithBootstrapButtons.fire(
//                     'Eliminado!',
//                     'El registro fue eliminado.',
//                     'success'
//                 )
//             }).catch(function (error) {
//                 Swal.fire({
//                     icon: 'error',
//                     title: 'Oops...',
//                     text: '¡No cuentas con los permisos suficientes!',
//                 })
//             });
//         } else if (
//             result.dismiss === Swal.DismissReason.cancel
//         ) {
//             swalWithBootstrapButtons.fire(
//                 'Cancelled',
//                 'Tu archivo está seguro :)',
//                 'error'
//             )
//         }
//     })
// });


function deleteuser(e) {
    e.preventDefault();
    let id = $(this).data('id')
    if (confirm('¿Desea eliminar esté usuario?')) {
        axios.delete(`/admin/${id}`)
            .then(function () {
                getAllData();
                swal.fire(
                    'Eliminado!',
                    'El registro fue eliminado.',
                    'success'
                )
            }).catch(function (error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '¡No se puedo eliminar el registro!',
                })
            });
    }
}

/*limpia los texto de error*/
function IsValid() {
    $('#invalid-nombre').text('');
    $('#invalid-apellido').text('');
    $('#invalid-email').text('');
    $('#invalid-password').text('');
    $('#invalid-estado').text('');
    $('#invalid-rol').text('');
}

/*Formateamos todos los valores del input*/
function inputDefault() {
    $('#nombre').val('');
    $('#apellido').val('');
    $('#email').val('');
    $('#password').val('');
    $('#estado').val('');
    $('#user_rol').val('');
};

/*Formateamos los permisos cuando se cambia de rol*/
function formatSelect() {
    $('#user_rol').change(function () {
        $("#listado input[type=checkbox]").prop('checked', false);
    });
}

/*Formateamos los permisos cuando se cambia de rol*/
function openmodal(edit = false) {
    formatSelect();
    if (!edit) {
        inputDefault()
        IsValid();
        $("#base_permisos").addClass("d-none");
        $('#modulos_permisos').addClass('d-none');
        $('#editDataForm').addClass('d-none');
        $('#addNewDataForm').removeClass('d-none');
        $('.password').show();
        document.getElementById('title').innerHTML = 'Crear Nuevo Usuario'
    } else {
        $("#base_permisos").removeClass("d-none");
        $('#modulos_permisos').removeClass('d-none');
        $('#editDataForm').removeClass('d-none');
        $('#addNewDataForm').addClass('d-none');
        $('.password').hide();
        document.getElementById('title').innerHTML = 'Editar Usuario'
    }
    $('.modal').modal('show')
}

