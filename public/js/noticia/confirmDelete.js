document.addEventListener('DOMContentLoaded', () => {

    function deleteAdmin(e) {
        Swal.fire({
            title: '¿Estás seguro?',
            text: "¡No podras revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, bórralo!'
        }).then((result) => {
            if (result.isConfirmed) {
                const adminId = e.target.closest('.delete-new').dataset.id;
                axios.delete(`/noticia/${adminId}`)
                    .then(Response => {
                        Swal.fire(
                            'Eliminado!',
                            'El registro fue eliminado correctamente.',
                            'success'
                        )
                            .then(() => {
                                e.target.closest('tr').remove()
                            })
                    })
                    .catch(Error => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'No se pudo eliminar el registro',
                        })
                        location.reload()
                    })
            }
        })
    }

    Array.from(document.querySelectorAll('.delete-new')).forEach((button) => {
        button.addEventListener('click', deleteAdmin);
    })
}
)
