axios.defaults.headers.common['Accepts'] = "Application/json";
// axios.defaults.headers.common['Content-Type'] = "multipart/form-data";




function getData() {
    axios.get("/informativo/listnews")
        .then(function (res) {
            // table_data_rows(res.data.data)
        })
};
getData();


/*Formateamos los permisos cuando se cambia de rol*/
function openmodalnew(edit = false) {
    if (!edit) {
    } else {
    }
    $('.modal').modal('show')
}

$('#open-modal-new').click(function () {
    openmodalnew();
})


/*API para la creacion de un nuevo usuario*/
$('#agregarnoticia').click(function (e) {
    e.preventDefault();
    let data = new FormData();

    data.append('nombre', $('#nombre').val());
    data.append('tipo', $('#tipo').val());
    data.append('informacion', $('#informacion').val());
    data.append('fecha_inicio', $('#fecha_inicio').val());
    data.append('fecha_fin', $('#fecha_fin').val());
    data.append('estado', document.getElementById("estado").checked ? 1 : 0,);

    const input = document.getElementById('documento');
    data.append('documento', input.files[0]);
    const input2 = document.getElementById('imagen');
    data.append('imagen', input2.files[0]);

    axios.post("/noticia/store", data)
        .then(function (response) {
            window.location.reload()
            Swal.fire({
                icon: 'success',
                title: '¡Buen trabajo!',
                text: 'Noticia registrada con exito!',
            });
            // getData();
            $('.modal').modal('hide')
        })
        .catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: '¡opssss!',
                text: '¡No se pudo crear la noticia!',
            });
        });
});


/*Edicion de los usuarios*/
$('body').on('click', '#updatenew', function (e) {
    e.preventDefault();
    let id = $('#id').val()
    let imagen = document.getElementById('imagen');
    let documento = document.getElementById('documento');

    let data = new FormData();
    data.append('nombre', $('#nombre').val());
    data.append('tipo', $('#tipo').val());
    data.append('informacion', $('#informacion').val());
    data.append('fecha_inicio', $('#fecha_inicio').val());
    data.append('fecha_fin', $('#fecha_fin').val());
    data.append('estado', document.getElementById("estado").checked ? 1 : 0,);

    data.append('documento', documento.files[0]||"");
    data.append('imagen', imagen.files[0]||"");

    axios.post('/noticia/update/' + id, data)
        .then(function (response) {
            window.location.reload()
            Swal.fire({
                icon: 'success',
                title: '¡Buen trabajo!',
                text: '¡Usuario modificado con exito!',
            }),
                $('.modal').modal('hide')
        })
        .catch(function (error) {
            if (error.response.data.errors) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '¡No se pudo editar el usuario!',
                })
                $('.modal').modal('hide')
                $('#invalid-nombre').text(error.response.data.errors.nombre)
                $('#invalid-apellido').text(error.response.data.errors.apellido)
                $('#invalid-email').text(error.response.data.errors.email)
                $('#invalid-password').text(error.response.data.errors.password)
                $('#invalid-estado').text(error.response.data.errors.estado)
                $('#invalid-rol').text(error.response.data.errors.FK_id_loginrol)
            }
        });
});

