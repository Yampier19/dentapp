document.addEventListener('DOMContentLoaded', () => {

    function deleteBanner(e) {
        Swal.fire({
            title: '¿Estás seguro?',
            text: "¡No podras revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, bórralo!'
        }).then((result) => {
            if (result.isConfirmed) {
                const bannerId = e.target.closest('.delete-banner').dataset.id;
                axios.delete(`/noticia/destroyBanner/${bannerId}`)
                    .then(Response => {
                        Swal.fire(
                            'Eliminado!',
                            'El registro fue eliminado correctamente.',
                            'success'
                        )
                            .then(() => {
                                e.target.closest('tr').remove()
                            })
                    })
                    .catch(Error => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'No se pudo eliminar el registro',
                        })
                        location.reload()
                    })
            }
        })
    }

    Array.from(document.querySelectorAll('.delete-banner')).forEach((button) => {
        button.addEventListener('click', deleteBanner);
    })
}
)




