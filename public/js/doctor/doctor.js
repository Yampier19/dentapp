/*API para obtener los datos*/
function getData(name = null) {
    let uri = "/cliente/listado";

    if (name) {
        uri += `?search=${name}`;
    }

    axios.get(uri)
        .then(function (res) {
            table_data_rows(res.data.data)
        });
};
getData();

$('#search-doctor').submit(function (e) {
    e.preventDefault();

    getData(e.target.querySelector("#search").value);
})

/*Creación de la estructura de la tabla*/
function table_data_rows(data) {
    (function (name) {
        var container = $('#pagination-' + name);
        container.pagination({
            dataSource: data,
            locator: 'value',
            pageSize: 5,
            ajax: {
                beforeSend: function () {
                    container.prev().html('Loading data from flickr.com ...');
                }
            },
            callback: function (response, pagination) {
                window.console && console.log(22, response, pagination);
                const dataHtml = () => {
                    var tb2 = document.createElement("tbody");
                    tb2.id = "tbody";
                    $.each(response, function (key, value) {

                        var t2 = document.querySelector('#doctores');
                        var clone = document.importNode(t2.content, true);

                        clone.querySelector("td:first-child span").textContent = value.login.created_at;
                        clone.querySelector("td:nth-child(2)").textContent = value.level?.nombre || 'Por Actualizar';
                        clone.querySelector("td:nth-child(3) h6").textContent = `${value.nombre} ${value.apellido}`;
                        clone.querySelector("td:nth-child(3) p").textContent = value.correo;
                        clone.querySelector("td:nth-child(4) span").textContent = value.document_number || 'Sin datos';
                        clone.querySelector("td:nth-child(5) h6").textContent = value.rol.rol;
                        clone.querySelector("td:nth-child(5) p").textContent = value.especialidad;
                        clone.querySelector("td:nth-child(6) span").textContent = value.login.segmento || 'No posee';
                        clone.querySelector("td:nth-child(7) span").textContent = value.distribuidor;
                        clone.querySelector("td:nth-child(8) span.text-secondary").textContent = value.telefono;
                        clone.querySelector("td:nth-child(9) h6").textContent = value.total_estrellas;
                        clone.querySelector("td:nth-child(9) p").textContent = value.total_reunido;

                        if (Object.prototype.hasOwnProperty.call(value, 'consultorio') && value.consultorio !== null) {
                            const a = document.createElement('a');
                            a.href = `/cliente/consultorio/${value.consultorio.id_consultorio}`;
                            a.textContent = 'ver';
                            a.target = '_blank';
                            a.title = 'Ver consultorio';
                            a.classList.add('btn', 'btn-outline-dark', 'btn-sm', 'rounded');
                            clone.querySelector("td:nth-child(10)").appendChild(a)
                        } else {
                            const span = document.createElement('span');
                            span.textContent = 'No posee'
                            clone.querySelector("td:nth-child(10)").appendChild(span)
                        }

                        clone.querySelector("td:nth-child(11) a.editar").href = `/cliente/${value.id_cliente}/edit`;
                        const deleteButton = clone.querySelector("td:nth-child(11) button.delete-doctor");
                        deleteButton.dataset.id = value.id_cliente;
                        deleteButton.addEventListener('click', deletedoctor);
                        // clone.querySelector("td:nth-child(11) button.delete-doctor").dataset.id = value.id_cliente;
                        clone.querySelector("td:nth-child(11) a.ver").href = `/cliente/detalle/${value.id_cliente}`;

                        tb2.appendChild(clone);
                    });

                    document.getElementById("tbody").replaceWith(tb2);
                }

                container.prev().html(dataHtml);
            }
        })
    })('demo2');

}




/*API para la creacion de un nuevo usuario*/
$('#agregardoctor').click(function (e) {
    // IsValid();
    e.preventDefault();
    let data = new FormData();
    data.append('correo', $('#correo').val());
    data.append('estado', $('#estado').val());
    data.append('segmento', $('#segmento').val());
    data.append('nombre', $('#nombre').val());
    data.append('apellido', $('#apellido').val());
    data.append('city_id', $('#city_id').val());
    data.append('document_type_id', $('#document_type_id').val());
    data.append('direccion', $('#direccion').val());
    data.append('telefono', $('#telefono').val());
    data.append('distribuidor', $('#distribuidor').val());
    data.append('document_number', $('#document_number').val());
    data.append('consultorio', document.getElementById("consultorio").checked ? 1 : 0);
    data.append('especialidad', $('#especialidad').val());
    data.append('total_estrellas', $('#total_estrellas').val());
    data.append('nombre_c', $('#nombre_c').val());
    data.append('direccion_c', $('#direccion_c').val());
    data.append('telefono_c', $('#telefono_c').val());
    data.append('numero_pacientes', $('#numero_pacientes').val());
    data.append('loginrol_id', $('#tipo').val());
    const input = document.getElementById('foto');
    data.append('foto', input.files[0] || "");

    axios.post("/cliente/store", data)
        .then(function (response) {
            Swal.fire({
                icon: 'success',
                title: '¡Buen trabajo!',
                text: '¡Usuario registrado con exito!',
            });
            window.location.reload()
            $('.modal-doctor').modal('hide')
        })
        .catch(function (error) {
            if (error.response.data.errors) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'No se pudo crear el usuario!',
                })
                $('#invalid-correo').text(error.response.data.errors.correo);
                $('#invalid-estado').text(error.response.data.errors.estado);
                $('#invalid-segmento').text(error.response.data.errors.segmento);
                $('#invalid-nombre').text(error.response.data.errors.nombre);
                $('#invalid-apellido').text(error.response.data.errors.apellido);
                $('#invalid-city').text(error.response.data.errors.city_id);
                $('#invalid-document_type').text(error.response.data.errors.document_type_id);
                $('#invalid-direccion').text(error.response.data.errors.direccion);
                $('#invalid-telefono').text(error.response.data.errors.telefono);
                $('#invalid-distribuidor').text(error.response.data.errors.distribuidor);
                $('#invalid-document_number').text(error.response.data.errors.document_number);
                $('#invalid-consultorio').text(error.response.data.errors.consultorio);
                $('#invalid-especialidad').text(error.response.data.errors.especialidad);
                $('#invalid-total_estrellas').text(error.response.data.errors.total_estrellas);
                $('#invalid-nombre_c').text(error.response.data.errors.nombre_c);
                $('#invalid-direccion_c').text(error.response.data.errors.direccion_c);
                $('#invalid-numero_pacientes').text(error.response.data.errors.numero_pacientes);
                $('#invalid-telefono_c').text(error.response.data.errors.telefono_c);
                $('#invalid-tipo').text(error.response.data.errors.tipo);
            }
        });
});


function deletedoctor(e) {
    e.preventDefault();
    let id = $(this).data('id')
    if(confirm('¿Desea eliminar esté usuario?')){
        axios.delete(`/cliente/${id}`).then(function () {
            getData();
            swal.fire(
                'Eliminado!',
                'El registro fue eliminado.',
                'success'
            )
        }).catch(function (error) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: '¡No cuentas con los permisos suficientes!',
            })
        });
    }
}


/*Eliminacion de los usuario*/
// $('.delete-doctor').on("click",function (e) {
// // $('body').on('click', '.delete-doctor', function (e) {
//     e.preventDefault();
//     let id = $(this).data('id')
//     const swalWithBootstrapButtons = Swal.mixin({
//         customClass: {
//             confirmButton: 'btn btn-info mx-2',
//             cancelButton: 'btn btn-danger'
//         },
//         buttonsStyling: false
//     })
//     swalWithBootstrapButtons.fire({
//         title: '¿Estas seguro?',
//         text: "¡No podras revertir estó!",
//         icon: 'warning',
//         showCancelButton: true,
//         confirmButtonText: '¡Si,eliminalo!',
//         cancelButtonText: 'No, cancelar!',
//         reverseButtons: true
//     }).then((result) => {
//         if (result.isConfirmed) {
//             axios.delete(`/cliente/${id}`).then(function (r) {
//                 getData();
//                 swalWithBootstrapButtons.fire(
//                     'Eliminado!',
//                     'El registro fue eliminado.',
//                     'success'
//                 )
//             }).catch(function (error) {
//                 Swal.fire({
//                     icon: 'error',
//                     title: 'Oops...',
//                     text: '¡No cuentas con los permisos suficientes!',
//                 })
//             });
//         } else if (
//             result.dismiss === Swal.DismissReason.cancel
//         ) {
//             swalWithBootstrapButtons.fire(
//                 'Cancelled',
//                 'Tu archivo está seguro :)',
//                 'error'
//             )
//         }
//     })
// });



/*Edicion de los usuarios*/
$('body').on('click', '#updatedoctor', function (e) {
    e.preventDefault();
    let id = $('#id').val()
    let input = document.getElementById('foto');
    let data = new FormData();
    data.append('correo', $('#correo').val())
    data.append('password', $('#password').val())
    data.append('estado', $('#estado').val())
    data.append('segmento', $('#segmento').val())
    data.append('nombre', $('#nombre').val())
    data.append('apellido', $('#apellido').val())
    data.append('city_id', $('#city_id').val())
    data.append('document_type_id', $('#document_type_id').val())
    data.append('document_number', $('#document_number').val())
    data.append('categoria', $('#categoria').val())
    data.append('distribuidor', $('#distribuidor').val())
    data.append('telefono', $('#telefono').val())
    data.append('especialidad', $('#especialidad').val())
    data.append('total_estrellas', $('#total_estrellas').val())
    data.append('consultorio', document.getElementById("consultorio").checked ? 1 : 0)
    data.append('nombre_c', $('#nombre_c').val())
    data.append('direccion_c', $('#direccion_c').val())
    data.append('telefono_c', $('#telefono_c').val())
    data.append('loginrol_id', $('#tipo').val())
    data.append('numero_pacientes', $('#numero_pacientes').val())
    data.append('foto', input.files[0] || "");

    axios.post('/cliente/update/' + id, data)
        .then(function (response) {
            Swal.fire({
                icon: 'success',
                title: '¡Buen trabajo!',
                text: '¡Usuario modificado con exito!',
            }),
                $('.modal').modal('hide')
        })
        .catch(function (error) {
            if (error.response.data.errors) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: '¡No se pudo editar el usuario!',
                })
                $('.modal').modal('hide')
                $('#invalid-nombre').text(error.response.data.errors.nombre)
                $('#invalid-apellido').text(error.response.data.errors.apellido)
                $('#invalid-email').text(error.response.data.errors.email)
                $('#invalid-password').text(error.response.data.errors.password)
                $('#invalid-estado').text(error.response.data.errors.estado)
                $('#invalid-rol').text(error.response.data.errors.FK_id_loginrol)
            }
        });
});


/**
     * Fomats the given date string to locale string
     * @param {string} dateString
     * @returns string
     */
function formatDate(dateString) {

    const date = new Date(dateString);
    const dateOptions = {
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        hour12: true
    };

    return new Intl.DateTimeFormat("es-pe", dateOptions).format(date);
}

/*Formateamos todos los valores del input*/
function inputDefault() {
    $('#nombre').val('');
    $('#apellido').val('');
    $('#city_id').val('');
    $('#consultorio').val('');
    $('#correo').val('');
    $('#document_number').val('');
    $('#document_type_id').val('');
    $('#distribuidor').val('');
    $('#especialidad').val('');
    $('#estado').val('');
    $('#segmento').val('');
    $('#telefono').val('');
    $('#nombre_c').val('');
    $('#direccion_c').val('');
    $('#telefono_c').val('');
    $('#numero_pacientes').val('');
    $('#departamento').val('');
    $('#direccion').val('');
    $('#tipo').val('');
};



/*Formateamos los permisos cuando se cambia de rol*/
function openmodalDoctor(edit = false) {
    if (!edit) {
        inputDefault()
    } else {
    }
    $('.modal-doctor').modal('show')
}

