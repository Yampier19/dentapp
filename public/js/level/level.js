const form = document.getElementById('form-level')

/*Llenamos los inputs con la informacion que llege desde el api*/
$('body').on('click', '.editando', function (e) {
    // IsValid();
    const id = e.target.closest('.editando').dataset.id;
    openmodal(true, id);
    axios.get('/admin/levels/' + id + '/edit')
        .then(function (res) {
            $('#nombre').val(res.data.data.nombre),
                $('#rango_inicio').val(res.data.data.rango_inicio),
                $('#meta').val(res.data.data.meta)
                $('#loginrol_id').val(res.data.data.loginrol_id)
        })
})

function openmodal(edit = false, id = null) {
    if (edit) {
        $('#guardando').show();
        $('#agregar').hide();
        document.getElementById('title').innerHTML = '<span class="iconify" data-width="30" data-icon="fluent:reward-12-filled"></span>  Editar nivel'
        form.action = '/admin/levels/' + id + '/update';
        form.querySelector('[name=_method]').value = 'PATCH';
    } else {
        $('#nombre').val('');
        $('#rango_inicio').val('');
        $('#loginrol_id').val('');
        $('#meta').val('');
        $('#agregar').show();
        $('#guardando').hide();
        document.getElementById('title').innerHTML = '<span class="iconify" data-width="30" data-icon="fluent:reward-12-filled"></span> Crear Nuevo nivel'
        form.action = '/admin/levels/create';
        form.querySelector('[name=_method]').value = 'POST';
    }
    $('.modal').modal('show')
}

$('#creando').click(function () {
    openmodal();
})



