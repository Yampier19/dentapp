document.addEventListener('DOMContentLoaded', () => {

    function deletequestion(e) {
        Swal.fire({
            title: '¿Estás seguro?',
            text: "¡No podras revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, bórralo!'
        }).then((result) => {
            if (result.isConfirmed) {
                const triviaId = e.target.closest('.delete-question').dataset.id;
                axios.delete(`/tpreguntas/delete/${triviaId}`)
                    .then(Response => {
                        Swal.fire(
                            'Eliminado!',
                            'El registro fue eliminado correctamente.',
                            'success'
                        )
                            .then(() => {
                                e.target.closest('tr').remove()
                            })
                    })
                    .catch(Error => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'No se pudo eliminar el registro',
                        })
                        location.reload()
                    })
            }
        })
    }

    Array.from(document.querySelectorAll('.delete-question')).forEach((button) => {
        button.addEventListener('click', deletequestion);
    })
}
)
